DELETE FROM "organization"."orgPerson";
DELETE FROM "organization"."personPosition";
DELETE FROM "organization"."orgUnit";
DELETE FROM "organization"."organization";
DELETE FROM "organization"."orgUnitType";
DELETE FROM "organization"."orgRole";

-- Organization unit types
INSERT INTO "organization"."orgUnitType"(
            "orgUnitTypeCode", "orgUnitTypeName")
    VALUES ('Direction', 'Direction d''une entreprise ou d''une collectivité');

INSERT INTO "organization"."orgUnitType"(
            "orgUnitTypeCode", "orgUnitTypeName")
    VALUES ('Service', 'Service d''une entreprise ou d''une collectivité');

INSERT INTO "organization"."orgUnitType"(
            "orgUnitTypeCode", "orgUnitTypeName")
    VALUES ('Division', 'Division d''une entreprise');

INSERT INTO "organization"."orgUnitType"(
            "orgUnitTypeCode", "orgUnitTypeName")
    VALUES ('Equipe', 'Groupe de personnes');


INSERT INTO "organization"."organization"(
            "orgId", "orgName", "displayName", "orgRoleCode", "registrationNumber")
    VALUES ('ACME', 'ACME', 'ACME', '', '123456789');

-- Organization units ACME
INSERT INTO organization."orgUnit"(
            "orgUnitId", "orgUnitName", "displayName", "orgUnitTypeCode", 
            "ownerOrgId", "parentOrgUnitId", picture)
    VALUES ('ACME_SERVICE_DEV', 'SERVICE_DEV', 'Service dev', 'Service', 
            'ACME', null, null);

INSERT INTO organization."orgUnit"(
            "orgUnitId", "orgUnitName", "displayName", "orgUnitTypeCode", 
            "ownerOrgId", "parentOrgUnitId", picture)
    VALUES ('ACME_DIRECTION', 'DIRECTION', 'Direction', 'Direction', 
            'ACME', null, null);

