DELETE FROM "user"."user";

INSERT INTO "user"."user" (
    "userId", "userName", "password", "emailAddress", "lastName", "firstName", "title", "passwordChangeRequired", "displayName"
    ) VALUES (
    'admin', 'admin', 'd18d97fbba0017e933ce0135ebec0ac8897251c89d13226779cdfdcefb062145', 'info@pomoboard.org', '', 'Admin', 'M.', false, 'Admin'
    );


INSERT INTO "user"."user" (
    "userId", "userName", "password", "emailAddress", "lastName", "firstName", "title", "passwordChangeRequired", "displayName"
    ) VALUES (
    'bdupain', 'bdupain', 'd18d97fbba0017e933ce0135ebec0ac8897251c89d13226779cdfdcefb062145', 'info@pomoboard.org', 'Dupain', 'Benjamin', 'M.', false, 'Dupain Benjamin'
    );

INSERT INTO "user"."user" (
    "userId", "userName", "password", "emailAddress", "lastName", "firstName", "title", "passwordChangeRequired", "displayName"
    ) VALUES (
    'aragot', 'aragot', 'd18d97fbba0017e933ce0135ebec0ac8897251c89d13226779cdfdcefb062145', 'info@pomoboard.org', 'Ragot', 'Alexis', 'M.', false, 'Ragot Alexis'
    );

INSERT INTO "user"."user" (
    "userId", "userName", "password", "emailAddress", "lastName", "firstName", "title", "passwordChangeRequired", "displayName"
    ) VALUES (
    'gpouillat', 'gpouillat', 'd18d97fbba0017e933ce0135ebec0ac8897251c89d13226779cdfdcefb062145', 'info@pomoboard.org', 'Pouillat', 'Gaetan', 'M.', false, 'Pouillat Gaetan'
    );

INSERT INTO "user"."user" (
    "userId", "userName", "password", "emailAddress", "lastName", "firstName", "title", "passwordChangeRequired", "displayName"
    ) VALUES (
    'vlacaze', 'vlacaze', 'd18d97fbba0017e933ce0135ebec0ac8897251c89d13226779cdfdcefb062145', 'info@pomoboard.org', 'Lacaze', 'Vincent', 'M.', false, 'Lacaze Vincent'
    );

INSERT INTO "user"."user" (
    "userId", "userName", "password", "emailAddress", "lastName", "firstName", "title", "passwordChangeRequired", "displayName"
    ) VALUES (
    'jpaillat', 'jpaillat', 'd18d97fbba0017e933ce0135ebec0ac8897251c89d13226779cdfdcefb062145', 'info@pomoboard.org', 'Paillat', 'Jeremy', 'M.', false, 'Paillat Jeremy'
    );

INSERT INTO "user"."user" (
    "userId", "userName", "password", "emailAddress", "lastName", "firstName", "title", "passwordChangeRequired", "displayName"
    ) VALUES (
    'tmathieu', 'tmathieu', 'd18d97fbba0017e933ce0135ebec0ac8897251c89d13226779cdfdcefb062145', 'info@pomoboard.org', 'Mathieu', 'Tibo', 'M.', false, 'Mathieu Tibo'
    );