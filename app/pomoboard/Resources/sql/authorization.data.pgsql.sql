DELETE FROM "authorization"."accessRule";
DELETE FROM "authorization"."groupMember";
DELETE FROM "authorization"."privilege";
DELETE FROM "authorization"."group";

-- group
INSERT INTO "authorization"."group"(
            "groupId", "groupName", "displayName", "description", "enabled", "picture")
    VALUES ('ADMIN', 'admin', 'Administrateur', 'Groupe administrateur', true, NULL);

INSERT INTO "authorization"."group"(
            "groupId", "groupName", "displayName", "description", "enabled", "picture")
    VALUES ('UTILISATEUR', 'utilisateur', 'Utilisateur', 'Groupe utilisateur', true, NULL);


-- accessRule
INSERT INTO "authorization"."accessRule"(
            "groupId", "className", "context")
    VALUES ('ADMIN', '', '');

INSERT INTO "authorization"."accessRule"(
            "groupId", "className", "context")
    VALUES ('UTILISATEUR', '', '');


-- groupMember
INSERT INTO "authorization"."groupMember"(
            "groupId", "userId")
    VALUES ('ADMIN', 'admin');

INSERT INTO "authorization"."groupMember"(
            "groupId", "userId")
    VALUES ('UTILISATEUR', 'aragot');

INSERT INTO "authorization"."groupMember"(
            "groupId", "userId")
    VALUES ('UTILISATEUR', 'gpouillat');

INSERT INTO "authorization"."groupMember"(
            "groupId", "userId")
    VALUES ('UTILISATEUR', 'bdupain');

INSERT INTO "authorization"."groupMember"(
            "groupId", "userId")
    VALUES ('UTILISATEUR', 'jpaillat');

INSERT INTO "authorization"."groupMember"(
            "groupId", "userId")
    VALUES ('UTILISATEUR', 'vlacaze');

INSERT INTO "authorization"."groupMember"(
            "groupId", "userId")
    VALUES ('UTILISATEUR', 'tmathieu');


-- privilege
INSERT INTO "authorization"."privilege"(
            "groupId", "route")
    VALUES ('ADMIN', '*');



INSERT INTO "authorization"."privilege"(
            "groupId", "route")
    VALUES ('UTILISATEUR', 'organization/userPosition');
INSERT INTO "authorization"."privilege"(
            "groupId", "route")
    VALUES ('UTILISATEUR', 'pomoboard/*');