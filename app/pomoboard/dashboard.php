<?php
/*
 * Copyright (C) 2015 Maarch
 *
 * This file is part of bundle application Maarch RM.
 *
 * Application Maarch RM is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Application Maarch RM is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with application Maarch RM.  If not, see <http://www.gnu.org/licenses/>.
 */
namespace app\pomoboard;

/**
 * Dashboard html serializer
 *
 * @package pomoboard
 * @author  Alexis Ragot <alexis.ragot@maarch.org>
 */
class dashboard
{
    public $auth;

    public $storage;

    public $userPosition;
    
    public $lastOpenFind;
    
    /**
     * Constructor of dashboard
     * @param array $menu Menu of the dashboard
     * 
     * @todo Manage auth on org units to display orgUnits to which user has access, not only its positions
     */
    public function __construct(array $menu=null, \dependency\authorization\AuthorizationInterface $auth, \bundle\organization\Controller\userPosition $userPosition)
    {
        $this->auth = $auth;

        //var_dump($userPosition);

        $this->storage = new \stdClass();
        
        $this->lastOpenFind = false;
        $this->storage->menu = $this->filterMenuAuth($menu);
        if ((!$this->lastOpenFind) && (isset($_SESSION['menu'])) && ($_SERVER["REQUEST_URI"] != '/')) {
            $this->storage->menu = $_SESSION['menu'];
        } else {
            $_SESSION['menu'] = $this->storage->menu;
            //var_dump($_SESSION['menu']);
        }
        
        $this->storage->user = $_SESSION['user']['user'];

        $this->storage->positions = $userPosition->getMyPositions();

        if (isset($_SESSION['organization']['currentOrgUnit'])) {
            $this->storage->currentOrgUnit = $_SESSION['organization']['currentOrgUnit'];
        }

        if (isset($_SESSION['organization']['currentOrganization'])) {
            $this->storage->currentOrganization = $_SESSION['organization']['currentOrganization'];
        }
    }

    /**
     * dashboard layout merge
     * 
     * @return object 
     */
    public function layout()
    {
        return $this->storage;
    }

    protected function filterMenuAuth($menu)
    {
        foreach ($menu as $i => $item) {
            if (isset($item['submenu'])) {
                //var_dump("go to submenu of " . $item['label']);
                $menu[$i]['submenu'] = $this->filterMenuAuth($item['submenu']);
                if (count($menu[$i]['submenu']) < 1) {
                    unset($menu[$i]);
                }
            } else {
                if (substr($item['href'], 0, 7) != 'http://') {
                    $path = substr($item['href'], 1);
                    try {
                        $route = \laabs::route('READ', $path);
                        if (!$this->auth->hasUserPrivilege($route->getName())) {
                            unset($menu[$i]);
                        }
                        $path = "/".$path;
                        if (($path == $_SERVER["REQUEST_URI"]) && ($path != "/")) {
                            $this->lastOpenFind = true;
                            $menu[$i]['class'] = $menu[$i]['class'] ." last-open";
                        }

                    } catch (\Exception $e) {
                        unset($menu[$i]);
                    }
                }
            }
        }
        
        return $menu;
    }
}
