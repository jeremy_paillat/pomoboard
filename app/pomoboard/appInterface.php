<?php
namespace app\pomoboard;
/**
 * Interface for user administration
 */
interface appInterface
{
    /**
     * Welcome page
     * 
     * @request READ
     * @output pomoboard/welcome/welcomePage
     * @prompt
     */
    public function welcome();



}