<?php

namespace bundle\audit;
/**
 * Interface for audit view
 */
interface displayEntriesInterface
{
    /**
     * Find entries for an identified object
     * @param string $objectId    The identifier(s) for object
     * @param string $objectClass The type of object
     * 
     * @return audit/entryInfo[] The array of audit entries for the object
     * 
     * @request READ auditEntries/index/([^/]+)/([^/]+)
     */
    public function byObject($objectId, $objectClass=null);
    
    
    /**
     * Get search form for entries
     * @request READ audit/search
     * @action audit/entry/search
     * @prompt
     */
    public function search();
    
    /**
     * Get search form for entries
     * @request READ audit/search/result
     * @action audit/entry/result
     */
    public function result();
}