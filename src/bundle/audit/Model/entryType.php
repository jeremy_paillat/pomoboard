<?php

namespace bundle\audit\Model;
/**
 * audit entry type definition
 *
 * @package Audit
 *
 * @pkey [code]
 */
class entryType
{
    /**
     * @var string
     */
    public $code;

    /**
     * @var string
     */
    public $format;
}
