<?php

namespace bundle\audit\Model;
/**
 * audit entry relationship definition
 *
 * @package audit
 * 
 * @key [fromEntryId, toEntryId]
 * @fkey [fromEntryId] audit/entry[entryId]
 * @fkey [toEntryId] audit/entry[entryId]
 */
class entryRelationship
{
    /**
     * @var string
     */
    public $fromEntryId;

    /**
     * @var string
     */
    public $toEntryId;
}

