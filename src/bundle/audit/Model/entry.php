<?php

namespace bundle\audit\Model;
/**
 * audit entry definition
 *
 * @package audit
 *
 */
class entry
    extends entryInfo
{

    /**
     * @var resource
     */
    public $dataContext;

    /**
     * Constructor
     */
    public function __construct()
    {
        parent::__construct();

        $this->dataContext = \laabs::createMemoryStream("");
    }

    public function setDataContext($dataContext=null)
    {
        switch (gettype($dataContext)) {
            case 'resource':
                $this->dataContext = $dataContext;
                break;

            case 'object':
            case 'array':
                $this->dataContext = \laabs::createMemoryStream(serialize(\laabs::export($dataContext)));
                break;

            default:
                $this->dataContext = \laabs::createMemoryStream(serialize($dataContext));
        }

    }

    public function mergeMessage()
    {
        if ($this->variables) {
            $this->variables = \laabs\json_decode($this->variables);

            $this->message = vsprintf($this->message, $this->variables);
        }
    }

}
