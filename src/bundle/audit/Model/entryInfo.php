<?php

namespace bundle\audit\Model;
/**
 * audit entry definition
 *
 * @package Audit
 *
 * @pkey [entryId]
 * @substitution audit/entry
 */
class entryInfo
{
    /**
     * @var id
     */
    public $entryId;

    /**
     * @var timestamp
     */
    public $entryDate;

    /**
     * @var string
     */
    public $entryType;

    /**
     * @var string
     */
    public $userId;

    /**
     * @var string
     */
    public $message;

    /**
     * @var string
     */
    public $variables;

    /**
     * @var string
     */
    public $objectClass;

    /**
     * @var id
     */
    public $objectId;

    /**
     * @var audit/entryRelationship[]
     */
    public $entryRelationship;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->entryDate = \laabs::newTimestamp();
    }

    /**
     * Add a relationship
     * @param string $toEntryId The associated entry id
     */
    public function addRelationship($toEntryId)
    {
        $entryRelationship = \laabs::newInstance('audit/entryRelationship');
        $entryRelationship->fromEntryId = $this->entryId;
        $entryRelationship->toEntryId = $toEntryId;

        $this->entryRelationship[] = $entryRelationship;
    }
}
