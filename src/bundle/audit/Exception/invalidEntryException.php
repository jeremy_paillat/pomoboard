<?php

namespace bundle\audit\Exception;
/**
 * Exception for invalid audit entry submission
 */
class invalidEntryException
    extends \Exception
{

}