<?php

namespace bundle\audit\Controller;
/**
 * Controller for the audit trail entries types
 * 
 * @package Audit
 */
class entryType
{

    protected $sdoFactory;

    /**
     * Constructor
     * @param \dependency\sdo\Factory $sdoFactory
     */
    public function __construct(\dependency\sdo\Factory $sdoFactory)
    {
        $this->sdoFactory = $sdoFactory;
    }

    /**
     * Get all entry types
     * 
     * @return audit/entryType[] The array of audit types
     */
    public function index()
    {
        $entryTypes = array();

        foreach ($this->sdoFactory->find('audit/entryType') as $entryType) {
            $entryTypes[$entryType->code] = $entryType;
        }

        return $entryTypes;
    }

    /**
     * Get one entry type
     * @param qname $code
     * 
     * @return audit/entryType
     */
    public function get($code)
    {
        return $this->sdoFactory->read('audit/entryType', $code);
    }

   

}
