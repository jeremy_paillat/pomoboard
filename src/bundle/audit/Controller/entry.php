<?php

namespace bundle\audit\Controller;
/**
 * Controller for the audit trail entries
 * 
 * @package audit
 */
class entry
{

    protected $sdoFactory;

    /**
     * Constructor
     * @param \dependency\sdo\Factory $sdoFactory
     */
    public function __construct(\dependency\sdo\Factory $sdoFactory)
    {
        $this->sdoFactory = $sdoFactory;
    }

    /**
     * Create a new audit trail entry
     * @param qname  $entryTypeCode The type code / message code
     * @param qname  $objectClass   The type of object
     * @param  id    $objectId      The identifier(s) for object
     * @param mixed  $message       The display message parts
     * @param string $dataContext   The data associated with entry
     * @param id     $entryId       The new entry id. If non a uniqid
     * @param array  $relationships An indexed array of audit entry ids associated
     * 
     * @return audit/entry[] The array of audit entries for the object
     * @author 
     */
    public function add(
        $entryTypeCode,
        $objectClass,
        $objectId,
        $message=null,
        $variables=null,

        $dataContext=null,
        $entryId=false,

        array $relationships = null)
    {
        $entry = \laabs::newInstance('audit/entry');

        if (!strpos($entryTypeCode, LAABS_URI_SEPARATOR)) {
            throw \laabs::newException("audit/invalidEntryException", "Entry type must have both namespace and local name separated by a slash");
        }
        $entry->entryType = $entryTypeCode;

        $entry->objectClass = $objectClass;
        $entry->objectId = $objectId;

        $entry->message = $message;
        
        if ($variables) {
            $entry->variables = \laabs\json_encode($variables);
        }

        if ($entryId) {
            $entry->entryId = $entryId;
        } else {
            $entry->entryId = \laabs::newId();
        }

        if (isset($_SESSION['user']['user'])) {
            $entry->userId = $_SESSION['user']['user']->userId;
        }

        $entry->entryDate = \laabs::newTimestamp();

        $entry->setDataContext($dataContext);

        $this->sdoFactory->create($entry);

        if (count($relationships) > 0) {
            foreach ($relationships as $fromEntryId) {
                $entryRelationship = \laabs::newInstance("audit/entryRelationship");
                $entryRelationship->fromEntryId = $fromEntryId;
                $entryRelationship->toEntryId = $entry->entryId;

                $this->sdoFactory->create($entryRelationship);
            }
        }
    }

    /**
     * Find entries for an identified object
     * @param string $objectId    The identifier(s) for object
     * @param string $objectClass The type of object
     * 
     * @return audit/entryInfo[] The array of audit entries for the object
     */
    public function byObject($objectId, $objectClass=null)
    {
        $query = "objectId='$objectId'";
        if ($objectClass) {
            $query .= "AND objectClass='$objectClass'";
        }
        $entries = $this->sdoFactory->find('audit/entryInfo', $query);

        $users = $this->sdoFactory->find('user/user');
        foreach ($users as $i => $user) {
            $users[(string) $user->userId] = $user;
            unset($users[$i]);
        }

        foreach ($entries as $i => $entry) {
            if ($entry->userId) {
                $entry->userName = $users[$entry->userId]->displayName;
            }
        }

        return $entries;
    }

    /**
     * Find entries for a given type
     * @param string $entryType The type of event
     * 
     * @return audit/entryInfo[] The array of audit entries for the object
     */
    public function byType($entryType)
    {
        $entries = $this->sdoFactory->find('audit/entryInfo', "entryType='$entryType'");

        return $entries;
    }

    /**
     * Find entries for a given type domain
     * @param string $domain The domain of event
     * 
     * @return audit/entryInfo[] The array of audit entries for the object
     */
    public function byDomain($domain)
    {
        $entries = $this->sdoFactory->find('audit/entryInfo', "entryType='$domain/*'");

        return $entries;
    }

    /**
     * Find entries for a identified user
     * @param string $userId The type of object
     * 
     * @return audit/entry[] The array of audit entries for the object
     */
    public function byUser($userId)
    {
        $entries = $this->sdoFactory->find('audit/entryInfo', "userId='$userId'");

        return $entries;
    }

    /**
     * Find entries for a given type domain
     * @param timestamp $fromdate
     * @param timestamp $todate
     * 
     * @return audit/entryInfo[] The array of audit entries for the object
     */
    public function byDate($fromdate=null, $todate=null)
    {
        $args = array();
        if ($fromdate) {
            $args[] = "entryDate>='$fromdate'";
        }
        if ($todate) {
            $args[] = "entryDate<='$todate'";
        }

        $entries = $this->sdoFactory->find('audit/entryInfo', implode(' and ', $args));

        return $entries;
    }

    /**
     * Find entries related to another
     * @param string $entryId The from/to audit entry id
     * 
     * @return audit/entry[] The array of audit entries related
     */
    public function getRelatedEntries($entryId)
    {
        $relatedEntries = false;
        
        $fromRelationShips = $this->sdoFactory->find('audit/entryRelationShip', "fromEntryId='$entryId'");
        foreach ($fromRelationShips as $fromRelationShip) {
            $relatedEntries[] = $this->sdoFactory->read('audit/entryInfo', $fromRelationShip->toEntryId);
        }

        $toRelationShips = $this->sdoFactory->find('audit/entryRelationShip', "toEntryId='$entryId'");
        foreach ($toRelationShips as $toRelationShip) {
            $relatedEntries[] = $this->sdoFactory->read('audit/entryInfo', $toRelationShip->fromEntryId);
        }

        return $relatedEntries;
    }

    /**
     * Get result of search form
     * @param string $entryDateMin
     * @param string $entryDateMax
     * @param string $entryType
     * @param string $userIdAudit
     * @param string $objectClass
     * @param string $objectId
     * 
     * @return Array Array of audit/entry object
     */
    public function result($entryDateMin=null, $entryDateMax=null, $entryType=null, $userIdAudit=null, $objectClass=null, $objectId=null)
    {
        $entries = array();
        $queryParts = array();

        if ($entryDateMin) {
            $queryParts['entryDateMin'] = "entryDate >= '$entryDateMin'";
        }
        if ($entryDateMax) {
            $queryParts['entryDateMax'] = "entryDate <= '$entryDateMax'";
        }
        if ($entryType) {
            $queryParts['entryType'] = "entryType = '*$entryType*'";
        }
        if ($userIdAudit) {
            $queryParts['userId'] = "userId = '*$userIdAudit*'";
        }
        if ($objectClass) {
            $queryParts['objectClass'] = "objectClass = '*$objectClass*'";
        }
        if ($objectId) {
            $queryParts['objectId'] = "objectId = '*$objectId*'";
        }

        $queryString = implode(' AND ', $queryParts);

        $entries = $this->sdoFactory->find("audit/entry", $queryString, "entryDate DESC", 0, 500);

        $users = $this->sdoFactory->find('user/user');
        foreach ($users as $i => $user) {
            $users[(string) $user->userId] = $user;
            unset($users[$i]);
        }

        foreach ($entries as $i => $entry) {
            if (isset($entry->userId)) {
                $entry->userName = $users[(string) $entry->userId]->displayName;
            }
        }
        
        return $entries;
    }

}
