<?php

namespace bundle\audit\Serializer\Html;

/**
 * Bundle audit html serializer
 *
 * @package Audit
 */
class entry
{
    public $view;

    /**
     * __construct
     *
     * @param \dependency\html\Document $view       A new ready-to-use empty view
     */
    public function __construct(\dependency\html\Document $view)
    {
        $this->view = $view;
    }

    /**
     * index of entries
     * @param array $audit Array of entries
     * 
     * @return string view
     */
    public function byObject(array $entries)
    {
        $this->view->addHeaders();
        $this->view->useLayout();
        $this->view->addContentFile("audit/view/entries.html");

        $dataTable = $this->view->getElementsByClass("dataTable")->item(0)->plugin['dataTable'];
        $dataTable->setPaginationType("full_numbers");

        $this->view->translate();
        $translator = $this->view->translator;

        foreach ($entries as $entry) {
            $catalog = strtok($entry->typeCode, LAABS_URI_SEPARATOR) . LAABS_URI_SEPARATOR . "messages";

            $entry->message = $translator->getText($entry->message, false, $catalog);
            $entry->mergeMessage();
        }

        $this->view->setSource("entries", $entries);
        $this->view->merge();

        return $this->view->saveHtml();
    }

    /**
     * Get form to search entries
     * 
     * @return string view
     */
    public function search()
    {
        $this->view->addHeaders();
        $this->view->useLayout();
        $this->view->addContentFile("audit/view/search.html");

        $sdoFactory = \laabs::newService('dependency/sdo/Factory');
        $entryTypes = $sdoFactory->summarise('audit/entry', 'entryType');
        $objectClasses = $sdoFactory->summarise('audit/entry', 'objectClass');
        
        /*$users = array();
        $userIds = $sdoFactory->summarise('audit/entry', 'userId');
        foreach ($userIds as $userId) {
            if ($user = $sdoFactory->read('user/user', $userId)) {
                $users[] = $user;
            }
        }
        $this->view->setSource("users", $users);
        
        */

        $this->view->setSource("entryTypes", $entryTypes);
        $this->view->setSource("objectClasses", $objectClasses);
        $this->view->merge();
        
        $this->view->translate();

        return $this->view->saveHtml();
    }
    
    
    /**
     * Get reseult
     * 
     * @param Array $entries Array of audit/entry object
     * 
     * @return string view
     */
    public function result($entries)
    {
        $this->view->addContentFile("audit/view/result.html");
        $translator = $this->view->translator;
        foreach ($entries as $entry) {
            $catalog = strtok($entry->entryType, LAABS_URI_SEPARATOR) . LAABS_URI_SEPARATOR . "messages";
            $entry->message = $translator->getText($entry->message, false, $catalog);
            $entry->mergeMessage();
        }
        $table = $this->view->getElementById("list");
        $dataTable = $table->plugin['dataTable'];
        $dataTable->setPaginationType("full_numbers");

        $dataTable->setUnsortableColumns(5);
        $dataTable->setSorting(array(array(0, 'desc')));


        $this->view->setSource("entries", $entries);
        $this->view->merge();
        
        $this->view->translate();

        return $this->view->saveHtml();
    }
}