<?php

namespace bundle\audit\Observer;
/**
 * Observer to notify audit entries
 */
class logger
{

    /**
     * @var \dependency\sdo\factory $sdoFactory 
     */
    public $sdoFactory;


    /**
     * Constructor
     * @param \dependency\sdo\factory $sdoFactory
     */ 
    public function __construct(\dependency\sdo\Factory $sdoFactory)
    {
        $this->sdoFactory = $sdoFactory;
    }


    /**
     * Log a given event by observation
     * @param audit/entry &$entry              The event to log
     * @param array       &$entryRelationships An array of entry ids to create relationships with
     * 
     * @return void
     * 
     * @subject bundle\audit\AUDIT_ENTRY
     */
    public function log(\bundle\audit\Model\entry &$entry, array &$entryRelationships=null)
    {
        if (empty($entry->entryId)) {
            $entry->entryId = \laabs::newId();
        }

        if (isset($_SESSION['user']['user'])) {
            $entry->userId = $_SESSION['user']['user']->userId;
        }

        $this->sdoFactory->create($entry, 'audit/entry');

        if (count($entryRelationships) > 0) {
            foreach ($entryRelationships as $fromEntryId) {
                $entryRelationship = \laabs::newInstance("audit/entryRelationship");
                $entryRelationship->fromEntryId = $fromEntryId;
                $entryRelationship->toEntryId = $entry->entryId;

                $this->sdoFactory->create($entryRelationship);
            }
        }

        return $entry;

    }

}