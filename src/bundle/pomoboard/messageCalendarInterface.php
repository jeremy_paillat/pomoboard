<?php
/*
 * Copyright (C) 2015 Maarch
 *
 * This file is part of bundle Records Management.

 * Bundle Records Management is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Bundle Records Management is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with bundle Records Management.  If not, see <http://www.gnu.org/licenses/>.
 */
namespace bundle\pomoboard;

/**
 *
 * @author ragotalexis
 */
interface messageCalendarInterface {

    /**
     * Create a message calendar
     * @param pomoboard/messageCalendar $messageCalendar The message calendar to create
     * 
     * @request CREATE calendar
     */
    public function create($messageCalendar);
    
    /**
     * Get a message calendar
     * @param pomoboard/messageCalendar $messageCalendar Message calendar
     * 
     * @request UPDATE calendar/getNote
     */
    public function read($messageCalendar);

    /**
     * Update a message calendar
     * @param pomoboard/messageCalendar $messageCalendar The message calendar to update
     * 
     * @request UPDATE calendar
     */
    public function update($messageCalendar);
}
