
DROP SCHEMA IF EXISTS "pomoboard" CASCADE;

CREATE SCHEMA "pomoboard"
  AUTHORIZATION postgres;


-- Table: pomoboard.preference

-- DROP TABLE pomoboard.preference;

CREATE TABLE "pomoboard"."preference"
(
  "userId" text NOT NULL,
  "pomodoro" boolean,
  "weather" boolean,
  "calculator" boolean,
  "calendar" boolean,
  "clock" boolean,
  CONSTRAINT preference_pkey PRIMARY KEY ("userId")
)
WITH (
  OIDS=FALSE
);

-- Table: pomoboard."messageCalendar"

-- DROP TABLE pomoboard."messageCalendar";

CREATE TABLE "pomoboard"."messageCalendar"
(
  "userId" text,
  "date" date,
  "message" text,
  UNIQUE ("userId", "date")
)
WITH (
  OIDS=FALSE
);