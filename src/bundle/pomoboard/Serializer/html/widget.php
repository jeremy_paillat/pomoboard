<?php
/*
 * Copyright (C) 2015 Maarch
 *
 * This file is part of bundle Records Management.

 * Bundle Records Management is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Bundle Records Management is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with bundle Records Management.  If not, see <http://www.gnu.org/licenses/>.
 */
namespace bundle\pomoboard\Serializer\html;

/**
 * widget serializer html
 *
 * @author Alexis Ragot <alexis.ragot@maarch.org>
 */
class widget
{
    
    public $view;

    /**
     * Constuctor of welcomePage html serializer
     * @param \dependency\html\Document $view The view
     */
    public function __construct(\dependency\html\Document $view)
    {
        $this->view = $view;
    }
    
    /** 
     * Get widget view
     * 
     * @return string The view
     */
    public function widget()
    {
        $this->view->addHeaders();
        $this->view->useLayout();
        $this->view->addContentFile("pomoboard/view/widget/widget.html");
        
        $preference = \laabs::newController("pomoboard/preference")->read();
        $this->view->setSource('preference', $preference);
        $this->view->merge();

        return $this->view->saveHtml();
    }
}
