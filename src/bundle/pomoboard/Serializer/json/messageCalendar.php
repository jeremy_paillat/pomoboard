<?php

namespace bundle\pomoboard\Serializer\json;
/**
 * Preference JSON serializer
 *
 * @package pomoboard
 * @author Alexis Ragot <alexis.ragot@maarch.org>
 */
class messageCalendar {

    protected $json;

    protected $translator;

    /**
     * Constructor of admin archival profile class
     * @param \dependency\json\JsonObject $json
     */
    public function __construct(\dependency\json\JsonObject $json)
    {
        $this->json = $json;
        $this->json->status = true;
    }

    /**
     * read a note
     * 
     * @return string
     */
    public function read($messageCalendar)
    {
        $this->json->message = $messageCalendar->message;

        return $this->json->save();
    }

    /**
     * Create a note
     * 
     * @return string
     */
    public function create($messageCalendar)
    {
        if ($messageCalendar->hasUpdated) {
            $this->json->message = "Message mis à jour";

            return $this->json->save();
        }

        $this->json->message = "La note a bien été créée";

        return $this->json->save();
    }

    /**
     * Update a note
     * 
     * @return string
     */
    public function update($messageCalendar)
    {
        $this->json->message = "Message mis à jour";
        $this->json->object = $messageCalendar;

        return $this->json->save();
    }

}

