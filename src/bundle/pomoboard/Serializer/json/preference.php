<?php
/*
 * Copyright (C) 2015 Maarch
 *
 * This file is part of bundle Records Management.

 * Bundle Records Management is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Bundle Records Management is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with bundle Records Management.  If not, see <http://www.gnu.org/licenses/>.
 */
namespace bundle\pomoboard\Serializer\json;
/**
 * Preference JSON serializer
 *
 * @package pomoboard
 * @author Alexis Ragot <alexis.ragot@maarch.org>
 */
class preference {

    protected $json;

    protected $translator;

    /**
     * Constructor of admin archival profile class
     * @param \dependency\json\JsonObject $json
     */
    public function __construct(\dependency\json\JsonObject $json)
    {
        $this->json = $json;
        $this->json->status = true;
    }

    /**
     * Return new digital resource for an archive
     * 
     * @return string
     */
    public function update()
    {
        $this->json->message = "Les préférences sont misent à jour";

        return $this->json->save();
    }
}

