<?php
/*
 * Copyright (C) 2015 Maarch
 *
 * This file is part of bundle Records Management.

 * Bundle Records Management is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Bundle Records Management is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with bundle Records Management.  If not, see <http://www.gnu.org/licenses/>.
 */
namespace bundle\pomoboard\Controller;
/**
 * Class for Pomoboard preferences
 */
class preference
{
    protected $sdoFactory;

    /**
     * Constructor
     * @param \dependency\sdo\Factory $sdoFactory The dependency sdo factory service
     */
    public function __construct(\dependency\sdo\Factory $sdoFactory) 
    {
        $this->sdoFactory = $sdoFactory;
    }
    
    /** 
     * Get a new preference
     * 
     * @return pomoboard/preference
     */
    public function newPreference()
    {
        $userId = $_SESSION["user"]["user"]->userId;
        
        if ($userId == null) {
            // TODO Exception 
        }
        
        $preference = \laabs::newInstance("pomoboard/preference");
        $preference->userId = $userId;
        $preference->pomodoro = true;
        $preference->weather = true;
        $preference->calculator = true;
        $preference->calendar = true;
        $preference->clock = true;
        
        return $preference;
    }

    /** 
     * Get form to manage preferences
     * 
     * @return pomoboard/preference
     */
    public function read()
    {
        $userId = $_SESSION["user"]["user"]->userId;
        
        if ($userId == null) {
            // TODO Exception 
        }
        
        if ($this->sdoFactory->exists("pomoboard/preference", $userId)) {
            $preference = $this->sdoFactory->read("pomoboard/preference", $userId);
        } else {
            $preference = $this->newPreference();
            $this->sdoFactory->create($preference, "pomoboard/preference");
        }

        return $preference;
    }
    
    /**
     * Update a preference
     * @param pomoboard/preference $preference
     * 
     * @return boolean
     */
    public function update($preference) {
        if ($preference == null) {
            // TODO Exception
        }
        
        $preference->userId = $_SESSION["user"]["user"]->userId;
        
        if (!$this->sdoFactory->exists("pomoboard/preference", $preference->userId)) {
            // TODO EXCEPTION
        }
        
        return $this->sdoFactory->update($preference, "pomoboard/preference");
    }
}
