<?php
/*
 * Copyright (C) 2015 Maarch
 *
 * This file is part of bundle Records Management.

 * Bundle Records Management is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Bundle Records Management is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with bundle Records Management.  If not, see <http://www.gnu.org/licenses/>.
 */
namespace bundle\pomoboard\Controller;
/**
 * Class for Pomoboard message of calendar
 */
class messageCalendar
{
    protected $sdoFactory;

    /**
     * Constructor
     * @param \dependency\sdo\Factory $sdoFactory The dependency sdo factory service
     */
    public function __construct(\dependency\sdo\Factory $sdoFactory) 
    {
        $this->sdoFactory = $sdoFactory;
    }
    
    /** 
     * Get a new preference
     * 
     * @return pomoboard/preference
     */
    public function newMessageCalendar()
    {
        $userId = $_SESSION["user"]["user"]->userId;
        
        if ($userId == null) {
            // TODO Exception 
        }
        
        $messageCalendar = \laabs::newInstance("pomoboard/messageCalendar");
        $messageCalendar->userId = $userId;
        
        return $messageCalendar;
    }

    /** 
     * Get form to manage preferences
     * @param pomoboard/messageCalendar $messageCalendar The message calendar to create
     * 
     * @return pomoboard/messageCalendar
     */
    public function create($messageCalendar)
    {
        if ($messageCalendar == null) {
            // TODO Exception 
        }

        $messageCalendar->userId = $_SESSION["user"]["user"]->userId;
        $messageCalendar->date = \laabs\date($messageCalendar->date, Y-m-d);

        if ($this->sdoFactory->exists("pomoboard/messageCalendar", array("userId" => $messageCalendar-> userId, "date" => $messageCalendar->date))) {
            $messageCalendar = $this->update($messageCalendar);
        } else {
            $this->sdoFactory->create($messageCalendar, "pomoboard/messageCalendar");
        }

        return $messageCalendar;
    }
    
    /**
     * Read a message calendar
     * @param pomoboard/messageCalendar $messageCalendar
     * 
     * @return boolean
     */
    public function read($messageCalendar) {

        if ($messageCalendar->date == null || $messageCalendar->date == "") {
            // TODO Exception
            var_dump("date null");
        }

        $messageCalendar->userId = $_SESSION["user"]["user"]->userId;

        if (!$this->sdoFactory->exists("pomoboard/messageCalendar", array("userId" => $messageCalendar->userId, "date" => $messageCalendar->date))) {
            // TODO EXCEPTION
            var_dump("no exist");
        }

        $message = $this->sdoFactory->read("pomoboard/messageCalendar", array("userId" => $messageCalendar->userId, "date" => $messageCalendar->date));

        return $message;
    }
    
    /**
     * Update a message calendar
     * @param pomoboard/messageCalendar $messageCalendar The message calendar object
     * 
     * @return boolean
     */
    public function update($messageCalendar) {

        if ($messageCalendar == null) {
            // TODO Exception
            var_dump("message null");
        }
        
        if (!$this->sdoFactory->exists("pomoboard/messageCalendar", array("userId" => $messageCalendar->userId, "date" => $messageCalendar->date))) {
            // TODO EXCEPTION
            var_dump("no exist");
        }
        
        $this->sdoFactory->update($messageCalendar, "pomoboard/messageCalendar");
        $messageCalendar->hasUpdated = true;

        return $messageCalendar;
    }
}
