<?php

namespace bundle\pomoboard\Parser\json;
/**
 * Json Parser for message control
 */
class messageCalendar
{
    public $jsonObject;

    /**
     * Constructor
     * @param \dependency\json\JsonObject $jsonObject The Json utility
     */
    public function __construct(\dependency\json\JsonObject $jsonObject)
    {
        $this->jsonObject = $jsonObject;
    }


    /**
     * Create messageCalendar
     * @param string $messageCalendarJson Message in JSON
     * 
     * @return array
     */
    public function create($messageCalendarJson)
    {
        $messageCalendar = \laabs::cast(\laabs\json_decode($messageCalendarJson), 'pomoboard/messageCalendar');

        return array('messageCalendar' => $messageCalendar);
    }

    /**
     * Read messageCalendar
     * @param string $messageCalendarJson Message in JSON
     * 
     * @return array
     */
    public function read($messageCalendarJson)
    {
        $messageCalendar = \laabs::cast(\laabs\json_decode($messageCalendarJson), 'pomoboard/messageCalendar');

        return array('messageCalendar' => $messageCalendar);
    }
}