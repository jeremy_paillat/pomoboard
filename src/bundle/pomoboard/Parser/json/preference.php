<?php

namespace bundle\pomoboard\Parser\json;
/**
 * Json Parser for archive control
 */
class preference
{
    public $jsonObject;

    /**
     * Constructor
     * @param \dependency\json\JsonObject $jsonObject The Json utility
     */
    public function __construct(\dependency\json\JsonObject $jsonObject)
    {
        $this->jsonObject = $jsonObject;
    }


    /**
     * Update preference
     * @param string $preferenceJson Preference in JSON
     * 
     * @return array
     */
    public function update($preferenceJson)
    {
        $preference = \laabs::cast(\laabs\json_decode($preferenceJson), 'pomoboard/preference');

        return array('preference' => $preference);
    }
}