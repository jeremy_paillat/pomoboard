<?php

namespace bundle\user\Serializer\html;

/**
 * user authentication html serializer
 *
 * @package User
 */
class authentication
{
    /**
     *
     */
    public $view;

    /**
     * Constructor
     * @param object $view A new empty Html document
     */
    public function __construct(\dependency\html\Document $view)
    {
        $this->view = $view;
    }

    /**
     * View for the users admin index panel
     * @param string $logoUri The URI of logo
     * 
     * @return string The html view string
     */
    public function prompt($logoUri)
    {
        $view = $this->view;

        $view->addHeaders();
        $view->addContentFile("user/view/login/form.html");
        $view->setSource('logo', $logoUri);
        $view->translate();
        $view->merge();

        return $view->saveHtml();
    }

    /**
     * Log out -> login
     *
     * @return void
     **/
    public function logout()
    {
        $this->view->addHeaders();
        $this->view->addContent("<script type='application/javascript'>$(location).attr('href', '/user/prompt');</script>");
        
        return $this->view->saveHtml();
    }

}