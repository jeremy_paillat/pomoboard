<?php

namespace bundle\user\Serializer\html;

/**
 * user admin html serializer
 *
 * @package User
 */
class user
{
    /**
     *
     */
    public $view;

    /**
     * Constructor
     * @param object $view A new empty Html document
     */
    public function __construct(\dependency\html\Document $view)
    {
        $this->view = $view;
    }

    /**
     * View for the users admin index panel
     * @param array $users An array of user objects to display
     * 
     * @return string The html view string
     */
    public function index($users, $offset=0, $length=10)
    {
        $view = $this->view;

        $view->addHeaders();
        $view->useLayout();
        $view->addContentFile("user/view/admin/index.html");
        $view->translate();
        
        $table = $view->getElementById("list");
        $dataTable = $table->plugin['dataTable'];
        $dataTable->setPaginationType("full_numbers");

        //$dataTable->setServerSideProcessing("/user/user/index");
        $dataTable->setUnsortableColumns(3, 4, 5);
        $dataTable->setUnsearchableColumns(3, 4, 5);

        $view->setSource('users', $users);
      
        $view->merge();
        
        //var_dump($users);exit;

        return $view->saveHtml();
    }

    /**
     * View for the edit user form
     * @param user/user $user The user object
     * 
     * @return string The html view string
     */
    public function edit($user)
    {
        $view = $this->view;

        $view->addHeaders();
        $view->useLayout();
        $view->addContentFile("user/view/admin/edit.html");

        //loading of the picture
        if($user->picture != null) {
            $content = stream_get_contents($user->picture);
            $finfo = finfo_open(FILEINFO_MIME_TYPE);
            $mimeType = finfo_buffer($finfo,$content,FILEINFO_MIME_TYPE);
            $user->picture = "data:".$mimeType.";base64,".base64_encode($content);
        }

        $view->setSource('user', $user);

        $view->merge();
        $view->translate();

        return $view->saveHtml();
    }   

    /**
     * View for the create user form
     * @param object $user The user object
     * 
     * @return string The html view string
     */
    public function newUser($user)
    {
        $view = $this->view;

        $view->addHeaders();
        $view->useLayout();
        $view->addContentFile("user/view/admin/edit.html");

        $view->setSource('user', $user);

        $view->merge();
        $view->translate();

        return $view->saveHtml();
    }

    /**
     * View to see information on user
     * @param object $user The user object
     * 
     * @return string The html view string
     */
    public function visualisation($user)
    {
        $view = $this->view;

        $view->addHeaders();
        $view->useLayout();
        $view->addContentFile("user/view/admin/visualisation.html");

        if (\laabs::hasBundle('organization')) {
            $orgModel = \laabs::newInstance("organization/organization");

            $organizations = $orgModel->getOrganizationTree();
            $this->mergeOrganizations($organizations);
        }

        $view->setSource('user', $user);

        $view->merge();
        $view->translate();

        return $view->saveHtml();
    }

    /**
     * undocumented function
     *
     * @return void
     **/
    protected function mergeOrganizations($organizations)
    {
        $orgList = $this->view->getElementById("organizationList");

        foreach ($organizations as $organization) {
            $orgFragment = $this->view->createDocumentFragment();
            $orgFragment->appendHtmlFile("organization/view/organizationItem.html");

            $this->view->merge($orgFragment, $organization);

            $orgItem = $orgList->appendChild($orgFragment);

            $this->mergeOrgUnits($organization, $orgItem);
        }
    }

    protected function mergeOrgUnits($parent, $container)
    {
        $orgUnitList = $this->view->createElement('ul');
        $container->appendChild($orgUnitList);

        foreach ($parent->orgUnit as $orgUnit) {
            $orgUnitFragment = $this->view->createDocumentFragment();
            $orgUnitFragment->appendHtmlFile("organization/view/orgUnitItem.html");
            $this->view->merge($orgUnitFragment, $orgUnit);

            $orgUnitItem = $orgUnitList->appendChild($orgUnitFragment);

            $this->mergeOrgUnits($orgUnit, $orgUnitItem);
        }
    }
}
