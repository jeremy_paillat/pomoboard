<?php

namespace bundle\user\Serializer\html;

/**
 * user management html serializer
 *
 * @package User
 */
class userManagement
{
    /**
     *
     */
    public $view;

    /**
     * Constructor
     * @param object $view A new empty Html document
     */
    public function __construct(\dependency\html\Document $view)
    {
        $this->view = $view;
    }

    /**
     * View edit user profil
     * @param user/userInformation $user User object to display
     * 
     * @return string The html view string
     */
    public function editUserInformation($user)
    {
        $view = $this->view;
        $view->addHeaders();
        $view->useLayout();
        $view->addContentFile("user/view/userManagement/userInformation.html");

         //loading of the picture
        if($user->picture != null) {
            $content = stream_get_contents($user->picture);

            ob_start();
            //imagepng($user->picture);
            $contents = ob_get_contents();
            ob_end_clean();

            $finfo = finfo_open(FILEINFO_MIME_TYPE);
            $mimeType = finfo_buffer($finfo,$content,FILEINFO_MIME_TYPE);
            $user->picture = "data:".$mimeType.";base64,".base64_encode($content);
        }

        $view->setSource('user', $user);

        $view->merge();
        $view->translate();

        return $view->saveHtml();
    }

    /**
     * View edit user password
     * 
     * @return string The html view string
     */
    public function editUserPassword()
    {
        $view = $this->view;
        $view->addHeaders();
        $view->useLayout();
        $view->addContentFile("user/view/userManagement/userPasswordChange.html");

        $view->setSource('userName', $_SESSION['user']['user']->userName);
        $view->merge();
        $view->translate();

        return $view->saveHtml();
    }
}