<?php

namespace bundle\user\Serializer\json;

/**
 * user admin json serializer
 *
 * @package User
 */
class user
{
    public $json;
    public $translator;

    /**
     * undocumented function
     *
     * @return void
     */
    public function __construct(\dependency\json\JsonObject $json,\dependency\localisation\TranslatorInterface $translator)
    {
        $this->json = $json;
        $this->translator = $translator;
        $this->translator->setCatalog('user/messages');
        $this->json->status = true;
    }

    /**
     * undocumented function
     *
     * @return void
     */
    public function addUser($user)
    {
        $json = $this->json;
        $json->message = "User added";
        $json->message = $this->translator->getText($json->message);

        return $json->save();
    }

    public function lock()
    {
        $json = $this->json;
        $json->message = "User locked";
        $json->message = $this->translator->getText($json->message);

        return $json->save();
    }

    public function unlock()
    {
        $json = $this->json;
        $json->message = "User unlocked";
        $json->message = $this->translator->getText($json->message);

        return $json->save();
    }

    public function enable()
    {
        $json = $this->json;
        $json->message = "User enable";
        $json->message = $this->translator->getText($json->message);

        return $json->save();
    }

    public function disable()
    {
        $json = $this->json;
        $json->message = "User disable";
        $json->message = $this->translator->getText($json->message);

        return $json->save();
    }

    public function setPassword()
    {
        $json = $this->json;
        $json->message = "Password has been changed";
        $json->message = $this->translator->getText($json->message);

        return $json->save();
    }

    public function requirePasswordChange()
    {
        $json = $this->json;
        $json->message = "Request to changed password sent.";
        $json->message = $this->translator->getText($json->message);

        return $json->save();
    }

    /**
     * Modify a user information
     * 
     * @return array
     */
    public function modifyUserInformation()
    {
        $this->json->message = "User updated";
        $this->json->message = $this->translator->getText($this->json->message);

        return $this->json->save();
    }

    /**
     * invalidUserInformationException
     *
     * @return void
     */
    public function invalidUserInformationException($exception)
    {
        $this->translator->setCatalog("user/validationErrors");
        $exception->status = false;
        $exception->message = $this->translator->getText($exception->message);
        $this->json->load($exception);

        /*for ($i=0, $l=count($exception->validationErrors); $i<$l; $i++) {
            $errorTxt = $exception->validationErrors[$i]->getMessage();
            $exception->validationErrors[$i] = $this->translator->getText($errorTxt);
        }*/

        return $this->json->save();
        //return json_encode($exception);
    }

    public function queryUsers($users) {
        return \laabs\json_encode($users);
    }
}