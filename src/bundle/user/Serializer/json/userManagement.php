<?php

namespace bundle\user\Serializer\json;

/**
 * user management json serializer
 *
 * @package User
 */
class userManagement
{
    protected $json;
    protected $transletor;

    /**
     * __construct
     * @param \dependency\json\JsonObject                  $json
     * @param \dependency\localisation\TranslatorInterface $transletor
     */
    public function __construct(\dependency\json\JsonObject $json, \dependency\localisation\TranslatorInterface $transletor)
    {
        $this->json = $json;
        $this->transletor = $transletor;
        $this->transletor->setCatalog('user/authentificationMessages');
        $this->json->status = true;
    }

    /**
     * Org unit users typeahead
     * @param array $users An array of users matching the user query
     *
     * @return string
     **/
    public function queryUsers($users)
    {
        return \laabs\json_encode($users);
    }
   
}