<?php

namespace bundle\user\Serializer\json;

/**
 * user authentication json serializer
 *
 * @package User
 */
class authentication
{
    protected $json;
    protected $translator;

    /**
     * __construct
     * @param \dependency\json\JsonObject                  $json
     * @param \dependency\localisation\TranslatorInterface $translator
     */
    public function __construct(\dependency\json\JsonObject $json, \dependency\localisation\TranslatorInterface $translator)
    {
        $this->json = $json;
        $this->translator = $translator;
        $this->translator->setCatalog('user/authentificationMessages');
        $this->json->status = true;
    }

    public function login($requestPath)
    {
        $json = $this->json;
        $json->message = $this->translator->getText("User connected");
        $json->requestPath = $requestPath;

        return $json->save();
    }

    public function definePassword($requestPath)
    {
        $json = $this->json;
        $json->message = "Password changed.";
        $json->requestPath = $requestPath;

        return $json->save();
    }

    public function authenticationException()
    {
        $json = $this->json;
        $json->status = false;
        $json->message = $this->translator->getText("Username not registered or wrong password.");

        return $json->save();
    }

    public function userDisabledException()
    {
        $json = $this->json;
        $json->status = false;
        $json->message = $this->translator->getText("User is disabled");

        return $json->save();
    }

    public function samePasswordException()
    {
        $json = $this->json;
        $json->status = false;
        $json->message = $this->translator->getText("Same password");

        return $json->save();
    }

    public function userLockException()
    {
        $json = $this->json;
        $json->status = false;
        $json->message = $this->translator->getText("User locked");

        return $json->save();
    }

    public function passwordChangeRequired()
    {
        $json = $this->json;
        $json->status = false;
        $json->passwordChangeRequired = true;

        return $json->save();
    }
}