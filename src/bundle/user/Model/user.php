<?php

namespace bundle\user\Model;
/**
 * User definition
 * 
 * @package User
 * 
 * @pkey [userId]
 * @key user_unique_userName[userName]
 */
final class user
    extends userInformation
{
    /**
     * @var string
     * @notempty
     */
    public $password;

    /**
     * @var timestamp
     */
    public $passwordLastChange;

    /**
     * @var bool
     */
    public $passwordChangeRequired = false;

    /**
     * @var bool
     */
    public $locked = false;

    /**
     * @var integer
     */
    public $badPasswordCount = 0;

    /**
     * @var timestamp
     */
    public $lastLogin;

    /**
     * @var string
     */
    public $lastIp;

    /**
     * @var id
     */
    public $replacingUserId;

}
