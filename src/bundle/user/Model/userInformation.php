<?php

namespace bundle\user\Model;
/**
 * User of the application
 *
 * @package User
 *
 * @substitution user/user
 * @pkey [userId]
 */
class userInformation
    extends \bundle\abstractDirectory\Model\abstractPerson
{
    /**
     * The user id
     *
     * @var string
     * @notempty
     */
    public $userId;

    /**
     * The user name
     *
     * @var string
     * @notempty
     */
    public $userName;

    /**
     * The status
     *
     * @var bool
     */
    public $enabled = true;

    /**
     * email address for notifications
     *
     * @var string
     */
    public $emailAddress;

    /**
     * Phone/cell number for notifications
     *
     * @var string
     */
    public $telephoneNumber;

    /**
     * The person's picture
     *
     * @var resource
     */
    public $picture;

}
