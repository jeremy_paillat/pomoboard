<?php

namespace bundle\user\Model;
/**
 * User definition
 * 
 * @package user
 * 
 */
final class userLoginResponse
{
    /**
     * @var id
     */
    public $userId;

    /**
     * @var bool
     */
    public $locked = false;

    /**
     * @var int
     */
    public $badPasswordCount = 0;

    /**
     * @var timestamp
     */
    public $lastLogin;

    /**
     * @var string
     */
    public $lastIp;

}
