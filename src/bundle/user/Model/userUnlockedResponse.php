<?php

namespace bundle\user\Model;
/**
 * userUnlockedResponse definition
 * 
 * @pkey [userId]
 * @substitution user/user
 * 
 */
final class userUnlockedResponse
{
    /**
     * @var id
     */
    public $userId;

    /**
     * @var bool
     */
    public $locked = false;

    /**
     * @var int
     */
    public $badPasswordCount = 0;
}
