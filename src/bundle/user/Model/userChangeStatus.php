<?php

namespace bundle\user\Model;
/**
 * userChangeStatus definition
 * 
 * @substitution user/user
 * @pkey [userId]
 */
class userChangeStatus 
{

    /**
     * @var id
     */
    public $userId;

    /**
     * @var id
     */
    public $replacingUserId;

    /**
     * @var bool
     */
    public $enabled;
}