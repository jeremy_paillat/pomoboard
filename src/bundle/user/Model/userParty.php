<?php

namespace bundle\user\Model;
/**
 * User of the application
 *
 * @package User
 * 
 * @substitution user/user
 *
 * @pkey [objectId]
 */
class userParty
    extends \bundle\abstractDirectory\Model\abstractPerson
{
    /**
     * The party's identifier
     *
     * @var id
     * @substitution userId
     * @notempty
     */
    public $objectId;

    /**
     * The party type
     *
     * @var string
     */
    protected $objectClass = 'user/userParty';

    /**
     * The party's type (person or org)
     *
     * @var string
     * @enumeration [person, organization]
     */
    protected $partyType = 'person';

    /**
     * The person's picture
     *
     * @var resource
     */
    public $picture;

}
