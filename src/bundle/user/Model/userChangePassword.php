<?php

namespace bundle\user\Model;
/**
 * UserChangePassword definition
 * 
 * @pkey [userId]
 * 
 */
final class userChangePassword
{
    /**
     * The user id
     *
     * @var id
     * @notempty
     */
    public $userId;

    /**
     * @var string
     */
    public $password;

    /**
     * @var integer
     */
    public $badPassword = 0;

    /**
     * @var timestamp
     */
    public $passwordLastChange;

    /**
     * @var boolean
     */
    public $passwordChangeRequired = false;
}
