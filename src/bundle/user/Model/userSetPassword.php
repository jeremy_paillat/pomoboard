<?php

namespace bundle\user\Model;
/**
 * userRequirePasswordChange definition
 * 
 * @pkey [userId]
 * 
 */
final class userSetPassword
{
    /**
     * The user id
     *
     * @var id
     * @notempty
     */
    public $userId;

    /**
     * @var string
     */
    public $password;

    /**
     * @var integer
     */
    public $badPassword = 0;

    /**
     * @var boolean
     */
    public $passwordChangeRequired = true;
}
