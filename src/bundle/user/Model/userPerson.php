<?php

namespace bundle\user\Model;
/**
 * User of the application
 *
 * @package User
 * 
 * @substitution user/user
 * 
 * @pkey [objectId]
 */
class userPerson
    extends \bundle\abstractDirectory\Model\abstractPerson
{
    /**
     * The party's identifier
     *
     * @var id
     * @substitution userId
     * @notempty
     */
    public $objectId;

    /**
     * The person type
     *
     * @var string
     */
    protected $objectClass = 'user/userPerson';

    /**
     * The person's picture
     *
     * @var resource
     */
    public $picture;
    
    
    /**
     * The enable status
     *
     * @var bool
     */
    public $enabled;
    
}
