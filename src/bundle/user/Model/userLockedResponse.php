<?php

namespace bundle\user\Model;
/**
 * UserLockedResponse definition
 * 
 * @pkey [userId]
 * @substitution user/user
 */
final class userLockedResponse
{
    /**
     * @var id
     */
    public $userId;

    /**
     * @var boolean
     */
    public $locked = true;
}
