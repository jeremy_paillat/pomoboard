DROP SCHEMA IF EXISTS "user" CASCADE;

CREATE SCHEMA "user"
  AUTHORIZATION postgres;


-- Table: "user"."user"

-- DROP TABLE "user"."user";

CREATE TABLE "user"."user"
(
  "userId" text NOT NULL,

  -- User account properties
  "userName" text NOT NULL,
  password text,
  enabled boolean DEFAULT true,
  "passwordChangeRequired" boolean DEFAULT true,
  "passwordLastChange" timestamp,
  locked boolean DEFAULT false,
  "emailAddress" text,
  "telephoneNumber" text,
  "badPasswordCount" integer,
  "lastLogin" timestamp,
  "lastIp" text,
  "replacingUserId" text,

  -- Person properties
  "firstName" text,
  "lastName" text,
  gender text,
  "birthName" text,
  title text,
  "dateOfBirth" date,

  -- directory object properties
  "displayName" text,
  picture bytea,

  CONSTRAINT user_pkey PRIMARY KEY ("userId"),
  CONSTRAINT "user_userName_key" UNIQUE ("userName")
)
WITH (
  OIDS=FALSE
);
