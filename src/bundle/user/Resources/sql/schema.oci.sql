DROP TABLE "user";

CREATE TABLE "user"
(
  "userId"                  VARCHAR2(512)   NOT NULL,
  "userName"                VARCHAR2(4000)  NOT NULL,
  "password"                VARCHAR2(4000),
  "enabled"                 NUMBER(1)       DEFAULT 1 NOT NULL,
  "passwordChangeRequired"  NUMBER(1)       DEFAULT 1 NOT NULL,
  "passwordLastChange"      TIMESTAMP,
  "locked"                  NUMBER(1)       DEFAULT 0 NOT NULL,
  "emailAddress"            VARCHAR2(4000),
  "telephoneNumber"         VARCHAR2(4000),
  "badPasswordCount"        INTEGER,
  "lastLogin"               TIMESTAMP,
  "lastIp"                  VARCHAR2(4000),
  "replacingUserId"         VARCHAR2(512),

  "firstName"               VARCHAR2(4000),
  "lastName"                VARCHAR2(4000),
  "gender"                  VARCHAR2(4000),
  "birthName"               VARCHAR2(4000),
  "dateOfBirth"             DATE,
  "title"                   VARCHAR2(4000),
  
  "displayName"             VARCHAR2(4000),
  "picture"                 BLOB,

  PRIMARY KEY ("userId"),
  UNIQUE ("userName")
);
