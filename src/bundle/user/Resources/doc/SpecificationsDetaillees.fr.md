<meta charset="UTF-8">
<!-- MarkdownTOC -->

- [Spécifications fonctionnelles](#specifications-fonctionnelles)
    - [Modèle statique](#modele-statique)
        - [Classe userPersonTrait](#classe-userPersonTrait)
        - [Classe userInformation](#classe-userInformation)
        - [Classe user](#classe-user)
        - [Classe userPerson](#classe-userPerson)
    - [Messages](#messages)
    - [Interface Homme-Machine](#interface-homme-machine)
    - [Classes participantes](#classes-participantes)
        - [Contrôleur abstractManagement](#contrôleur-abstractManagement)
        - [Contrôleur authentication](#controleur-authentication)
        - [Contrôleur useParty](#controleur-useParty)
        - [Contrôleur userManagement](#controleur-userManagement)
        - [Contrôleur userList](#controleur-userList)
        - [Contrôleur admin](#controleur-admin)
        - [Package dependency/sdo](#package-dependencysdo)
        - [Package dependency/authentication](#package-dependencyauthentication)

<!-- /MarkdownTOC -->

<span id="specifications-fonctionnelles"/>
# Spécifications fonctionnelles
Ce chapitre fournit la description de l’implémentation fournie par Maarch Bundle User.

<span id="modele-statique"/>
## Modèle statique
Le diagramme ci-dessous représente les classes statiques qui entrent en jeu dans les différentes activités liées à la gestion des utilisateurs.

![Static Model](img/userStaticModel.png)  

<span id="modele-userPersonTrait"/>
### Classe userPersonTrait


<span id="modele-userInformation"/>
### Classe userInformation


<span id="modele-user"/>
### Classe user


<span id="modele-userPerson"/>
### Classe userPerson


<span id="modele-messages"/>
## Messages
Ce chapitre décrit la structure des objets échangés dans les interactions entre les composants, notamment entre l’interface homme-machine et le contrôle métier au travers des couches d’interprétation et de présentation.

Il ne décrit que les membres de messages de types complexes qui n'appartiennent pas au modèle statique. Les arguments simples sont échangés tels quels entre les composants et ne requièrent pas de description particulière autre que celle des APIs.


<span id="interface-homme-machine"/>
## Interface Homme-Machine
Ce chapitre décrit le modèle d'interface homme-machine, c'est-à-dire la présentation faire à l'utilisateur des messages réponse de l'application ainsi que les opérations de navigation possibles à partir de cette présentation.


<span id="classes-participantes"/>
## Classes participantes
Ce chapitre effectue la jonction entre la modélisation (les cas d'utilisation, le modèle du domaine et l'IHM) et la conception logicielle utilisée pour l'implémentation mais non détaillée dans ce document (classes de conception, diagrammes d'interaction).

![Classes participantes](img/userClassesParticipantes.png)

<span id="controleur-dependencysdo"/>
### Contrôleur abstractManagement

<span id="controleur-authentication"/>
### Contrôleur authentication

<span id="controleur-useParty"/>
### Contrôleur useParty

<span id="controleur-userManagement"/>
### Contrôleur userManagement

<span id="controleur-userList"/>
### Contrôleur userList

<span id="controleur-admin"/>
### Contrôleur admin


<span id="package-dependencysdo"/>
### Package dependency/sdo
Ce package fournit les classes de service nécessaire à la gestion du stockage persistant des données  structurées. 

Il est utilisé par les contrôleurs du package digitalResource pour toutes les opérations de création, de lecture, de modification et de suppression des objets de données et de leurs propriétés, à l’exclusion des ressources et de leurs métadonnées qui ne sont pas vues dans le package comme des informations structurées mais des paquets de données.

Il s’agit de la couche d’abstraction qui utilise le modèle de données de l’application pour gérer les opérations de base sur le stockage persistant indépendamment de la technologie de stockage : moteur de base de données relationnelle, base noSQL, XML, fichier à plat, etc.

Le service principalement utilisé est Factory et les méthodes associées aux opérations de base sur la persistance des données structurées :
*   Create : crée une nouvelle représentation d’un objet dans le stockage persistant
*   Read : lit un objet à partir du stockage persistant
*   Update : modifie un objet dans le stockage persistant
*   Delete : supprime un objet du stockage persistant

<span id="package-dependencyauthentication"/>
### Package dependency/authentication