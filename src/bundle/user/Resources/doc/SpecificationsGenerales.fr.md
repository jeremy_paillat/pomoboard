<meta charset="UTF-8">
<!-- MarkdownTOC -->
- [Spécifications Générales](#specifications-generales)
    - [Introduction](#introduction)
        - [Domaine d'application](#domaine-dapplication)
        - [Termes et définitions](#termes-et-definitions)
    - [Contextualisation et modélisation](#contextualisation-et-modelisation)
        - [Les acteurs](#les-acteurs)
        - [Les fonctions](#les-fonctions)
        - [Les entités](#les-entites)

<!-- /MarkdownTOC -->
<span id="specifications-generales"/>
# Spécifications Générales
<span id="introduction"/>
## Introduction
<span id="domaine-dapplication"/>
### Domaine d'application
Le présent document décrit les spécifications du paquet User développé par Maarch pour l'implémentation des fonctionnalités liées à la gestion des utilisateurs.

Le but de cette documentation est de fournir toutes les informations nécessaires à la compréhension du fonctionnement du paquet Maarch User afin 
* de valider l'adéquation de la solution aux besoins de l‘organisation
* de démontrer comment elle peut être utilisée dans le développement d'une application qui utilise des utilisateurs

Le document est destiné :
*   aux directions informatiques des organisations souhaitant mettre en œuvre un tel système 
*   aux développeurs qui utiliseront le paquet dans leurs applications

<span id="termes-et-definitions"/>
### Termes et définitions

<span id="contextualisation-et-modelisation"/>
## Contextualisation et modélisation

<span id="les-acteurs"/>
### Les acteurs
Un acteur représente un ensemble cohérent de rôles vis-à-vis du système d’information. L'acteur ne doit pas être confondu avec l'utilisateur qui est une personne physique ou morale identifiée dans le dispositif. L’utilisateur peut en effet assurer différents rôles suivant la transaction utilisée, et ainsi être représenté par plusieurs acteurs. 

#### Utilisateur

#### Acteur lambda

<span id="les-fonctions"/>
### Les fonctions

![Use cases](img/userUseCase.png)  
 
### S'authentifier
#### Flux standard
Lors de l’authentification, l’utilisateur transmet au service d’authentification les informations nécessaires afin de pouvoir se connecter :
*   L’identifiant utilisateur
*   Le mot de passe

Le service d’authentification contrôle la validité des informations transmises par rapport aux informations existant sur le système de données et aux exigences techniques et fonctionnelles définies dans le système : 
*   Vérification des informations transmises
*   Vérification d’utilisateur actif
*   Vérification d’utilisateur non verrouillé

Une fois l’utilisateur authentifié, le système procède à l’enregistrement des informations :    
*   Date et heure de la dernière connexion
*   Compteur des mauvaises connexions remis à zéro
*   Dernière IP de l’utilisateur
L’utilisateur a accès à l’application.


#### Flux alternatif
#####Première connexion
L’utilisateur transmet les informations d’authentification. Le flux standard s’exécute jusqu’à l’authentification, mais ne donnera pas l’accès à l’application et demandera à l’utilisateur de choisir un nouveau mot de passe. Lorsque le nouveau mot de passe aura été défini et validé par le service d’authentification, le flux standard sera de nouveau exécuté du côté du service d’authentification pour donner à l’utilisateur l’accès à l’application.

#### Flux d’exception
Tous les flux d’exception provoquent une interruption du traitement.

##### Utilisateur inconnu
Lors du contrôle des informations, le système ne parvient pas à identifier l’utilisateur sur la base des informations fournies.


##### Mot de passe invalide
L’utilisateur est identifié mais le mot de passe ne correspond pas avec celui enregistré. Le compteur de mauvaise connexion de cet utilisateur s’incrémente, une fois à trois, l’utilisateur est verrouillé.


##### Utilisateur verrouiller
L’utilisateur est identifier mais il est verrouillé, il ne pourra se connecter que lorsqu’un administrateur utilisateur l’aura déverrouillé.


##### Utilisateur désactiver
L’utilisateur est identifier mais il est désactivé, il ne pourra se connecter que lorsqu’un administrateur utilisateur l’aura activé. 


#### Points d’inclusion
##### Informations de connexion
Afin de pouvoir se connecter les informations transmissent au service d’authentification seront :
*   L’identifiant de connexion utilisateur
*   Le mot de passe

### Changer le mot de passe
#### Flux standard
Une fois l’utilisateur authentifié, celui-ci transmet au service de changement de mot de passe les informations nécessaires :

*   L’identifiant utilisateur
*   Le nouveau mot de passe

Le service de changement de mot de passe contrôle la validité des informations transmises par rapport aux informations existant sur le système de données et aux exigences techniques et fonctionnelles définies dans le système.
Le système procède à l’enregistrement de la date et heure du dernier changement de mot de passe.

#### Flux d’exception
Tous les flux d’exception provoquent une interruption du traitement.
##### Utilisateur inconnu
Lors du contrôle des informations, le système ne parvient pas à identifier l’utilisateur sur la base des informations fournies.

#### Points d’inclusion
##### Informations de connexion
Afin de pouvoir se connecter les informations transmissent au service de changement de mot de passe seront :
*   L’identifiant de connexion utilisateur
*   Le nouveau mot de passe

###  Modifier ses informations
#### Flux standard
Une fois l’utilisateur authentifié, celui-ci transmet au service de modification utilisateur les informations qu’il souhaite changer :
*   Le titre utilisateur
*   L’adresse de courrier électronique
*   Le numéro de téléphone
*   L’image
*   Le nom d’affichage

Le service de modification utilisateur contrôle la validité des informations transmises par rapport aux informations existant sur le système de données et aux exigences techniques et fonctionnelles définies dans le système.

Le système procède à l’enregistrement des informations utilisateur.

#### Flux d’exception
##### Donnée invalide
Lors du contrôle des informations, le système vérifie que les informations fournies correspondent avec les critères défini.

#### Points d’inclusion
##### Informations de connexion
Afin de pouvoir se connecter les informations transmissent au service de changement de mot de passe seront :
*   L’identifiant de connexion utilisateur
*   Le nouveau mot de passe

###  Lister / rechercher les utilisateurs

#### Flux standard
Une fois l’utilisateur authentifié, celui-ci peut rechercher ou lister les autres utilisateurs.

###  Ajouter un utilisateur

#### Flux standard
Une fois l’administrateur authentifié, celui-ci transmet au service d’ajout utilisateur les informations nécessaires à sa création :
*   L’identifiant de connexion
*   Le mot de passe de connexion
*   Son état d’activation
*   Son état de verrouillage
*   L’état du changement obligatoire de mot de passe

Le service d’ajout utilisateur contrôle la validité des informations transmises par rapport aux informations existant sur le système de données et aux exigences techniques et fonctionnelles définies dans le système. Le système procède à l’enregistrement des informations

#### Flux d’exception
Tous les flux d’exception provoquent une interruption du traitement.

##### L’utilisateur existe déjà
Lors du contrôle des informations, le système identifie un utilisateur ayant déjà cet identifiant de connexion.


### Modifier les informations

#### Flux standards
Une fois l’administrateur authentifié, celui-ci transmet au service de modification les informations qu’il souhaite changer :
*   Le titre utilisateur
*   L’adresse de courrier électronique
*   Le numéro de téléphone
*   L’image
*   Le nom d’affichage
*   …
Le service de modification contrôle la validité des informations transmises par rapport aux informations existant sur le système de données et aux exigences techniques et fonctionnelles définies dans le système.
Le système procède à l’enregistrement des informations utilisateur.

#### Flux d’exception
Tous les flux d’exception provoquent une interruption du traitement.

##### Donnée invalide
Lors du contrôle des informations, le système vérifie que les informations fournies correspondent avec les critères défini.

#### Points d’inclusion
##### Informations de connexion
Afin de pouvoir se connecter les informations transmissent au service de changement de mot de passe seront :
*   L’identifiant de connexion utilisateur
*   Le nouveau mot de passe


### Modifier le mot de passe
#### Flux standards
Une fois l’administrateur authentifié, celui-ci transmet au service de changement de mot de passe les informations nécessaires :
*   L’identifiant utilisateur
*   Le nouveau mot de passe

Le service de changement de mot de passe contrôle la validité des informations transmises par rapport aux informations existant sur le système de données et aux exigences techniques et fonctionnelles définies dans le système.
Le système procède à l’enregistrement de la date et heure du dernier changement de mot de passe.

#### Flux d’exception
##### Utilisateur inconnu
Lors du contrôle des informations, le système ne parvient pas à identifier l’utilisateur sur la base des informations fournies.
##### Le mot de passe est identique au précédent
Lors du contrôle des informations, le système à identifier le nouveau mot de passe identique à l’ancien.

#### Points d’inclusion
##### Informations de connexion
Afin de pouvoir modifier le mot de passe, les informations transmissent au service de modification de mot de passe seront :
*   L’identifiant de connexion utilisateur
*   Le nouveau mot de passe

<span id="les-entites"/>
### Les entités
Les entités sont les représentations dans le système des informations manipulées, structurées sous forme d’objets.

![Business Model](img/userBusinessModel.png)
 
### "interface" Personne utilisatrice
Cette classe est une interface contenant un ensemble de caractéristique décrivant une personne utilisatrice.

### Personne utilisateur
Cette classe permet de définir un autre type de donnée en t'en que personne utilisatrice. Pour ce faire, l'identifant et la classe de l'objet personne utilisatrice devra correspondre avec celui de l'objet tiers.

### Information utilisateur
Cette classe représente les informations minimum afin d'être gérer par le systeme. Elle implémente l'interface personne utilisateur.

### Utilisateur
Cette classe représente un utilisateur au sein du système avec un ensemble de caractéristique non obligatoire permettant de mieux gérer les utilisateur tant sur le point fonctionnel que sécuritaire.