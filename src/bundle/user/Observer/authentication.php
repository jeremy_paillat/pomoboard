<?php

namespace bundle\user\Observer;
/**
 * Service for authentication check
 *
 * @package User
 * 
 */
class authentication
{
    public $authenticationService;

    protected $sdoFactory;

    protected $ignoreRoutes;

    protected $crypt;
    protected $cryptCipher;
    protected $cryptMode;
    protected $cryptKey;
    protected $cryptIV;

    /**
     * Construct the observer
     * @param dependency\authentication\AuthenticationInterface $authenticationService The type of authentication (Database, Ldap, Kerberos...)
     * @param object                                            $sdoFactory            The user model
     * @param array                                             $ignoreRoutes          An array of route patterns to ignore when checking authentication
     * @param string                                            $cryptCipher           The name of cipher to use to encrypt credential
     * @param string                                            $cryptMode             The crypt mode (cbc)
     * @param string                                            $cryptKey              The secret key
     * @param string                                            $cryptIV               The IV (8 chars)
     */
    public function __construct(
        \dependency\authentication\AuthenticationInterface $authenticationService,
        \dependency\sdo\Factory $sdoFactory,
        $ignoreRoutes=array('user/authentication/*'),
        $cryptCipher=false,
        $cryptMode=false,
        $cryptKey=false,
        $cryptIV=false)
    {
        $this->authenticationService = $authenticationService;

        $this->sdoFactory = $sdoFactory;

        $this->ignoreRoutes = $ignoreRoutes;

        if (extension_loaded('mcrypt')) {
            $this->cryptCipher = \laabs\coalesce($cryptCipher, \MCRYPT_BLOWFISH);
            $this->cryptMode = \laabs\coalesce($cryptMode, 'cbc');
            $this->cryptKey =  \laabs\coalesce($cryptKey, "LAABS");
            $this->cryptIV = \laabs\coalesce($cryptIV, "12345678");
        }

    }

    /**
     * Observer for user authentication 
     * @subject LAABS_REQUEST_ROUTE
     */
    public function check(&$requestRoute, array &$args=null)
    {
        $routename = $requestRoute->getName();

        foreach ($this->ignoreRoutes as $pattern) {
            if (fnmatch($pattern, $routename)) {
                return true;
            }
        }

        $token = null;
        if (isset($_COOKIE['LAABS-AUTH']) && !empty($_COOKIE['LAABS-AUTH'])) {
            $credential = $this->decrypt($_COOKIE['LAABS-AUTH']);
            $_SESSION['dependency']['authentication']['credential'] = $credential;

            $user = $this->sdoFactory->read('user/user', array('userName'=> $credential->user));
            $_SESSION['user']['user'] = $user;

            return $_COOKIE['LAABS-AUTH'];

        } elseif (isset($_SESSION['dependency']['authentication']['credential'])) {
            
            $token = $this->encrypt($_SESSION['dependency']['authentication']['credential']);

        } else {
            $requestAuth = \core\Kernel\abstractKernel::get()->request->authentication;

            switch ($requestAuth::$mode) {
                case LAABS_BASIC_AUTH:
                    if ($this->authenticationService->logIn($requestAuth->username, $requestAuth->password)) {
                        $token = $this->encrypt($_SESSION['dependency']['authentication']['credential']);
                    }
                    break;

                case LAABS_DIGEST_AUTH:
                    if ($this->authenticationService->logIn($requestAuth->username, $requestAuth->nonce, $requestAuth->uri, $requestAuth->response, $requestAuth->qop, $requestAuth->nc, $requestAuth->cnonce)) {
                        $token = $this->encrypt($_SESSION['dependency']['authentication']['credential']);
                    }
                    break;

                case LAABS_APP_AUTH:
                    if (isset($_SERVER['LAABS_AUTH_TOKEN'])) {
                        $token = $_SERVER['LAABS_AUTH_TOKEN'];

                        $credential = $this->decrypt($token);
                        $_SESSION['dependency']['authentication']['credential'] = $credential;
                    }
                    break;
            }

            if (!$token) {
                $authRoute = \laabs::route('READ', 'user/prompt');

                $requestRoute->reroute($authRoute);
                for ($i=0, $l=count($args); $i<$l; $i++) {
                    unset($args[$i]);
                }

                return false;
            }
            
        }

        return $token;
    }

    /**
     * Crypt the creadential into an authentication token
     * @param dependency\Authentication\credential $credential
     * 
     * @return string
     */
    protected function encrypt($credential)
    {
        $jsonCredential = \laabs\json_encode($credential);

        if (extension_loaded('mcrypt')) {
            $stringToken = mcrypt_encrypt($this->cryptCipher, $this->cryptKey, $jsonCredential, $this->cryptMode, $this->cryptIV);
        } elseif (extension_loaded('zlib')) {
            $stringToken = gzcompress($jsonCredential);
        } else {
            $stringToken = $jsonCredential;
        }

        $base64Token = base64_encode($stringToken);

        return $base64Token;
    }

    /**
     * Crypt the creadential into an authentication token
     * @param string $base64Token
     * 
     * @return string
     */
    protected function decrypt($base64Token)
    {
        $stringToken = base64_decode($base64Token);
        if (extension_loaded('mcrypt')) {
            $jsonCredential = trim(mcrypt_decrypt($this->cryptCipher, $this->cryptKey, $stringToken, $this->cryptMode, $this->cryptIV));
        } elseif (extension_loaded('zlib')) {
            $jsonCredential = gzuncompress($stringToken);
        } else {
            $jsonCredential = $stringToken;
        }
        $credential = json_decode($jsonCredential);

        return $credential;
    }


}