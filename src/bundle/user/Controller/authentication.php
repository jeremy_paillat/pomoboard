<?php

namespace bundle\user\Controller;

/**
 * user login controller
 *
 * @package User
 */
class authentication
{
    /**
     * authentication
     */
    protected $auth;

    /**
     * Redirect after login
     */
    protected $welcomeUri;

    /**
     * The password encryption
     *
     * @var string
     **/
    protected $passwordEncryption;

    /**
     * The URI of logo
     *
     * @var string
     **/
    protected $logoUri;
    /**
     * The security policy of the password
     *
     * @var string
     **/
    protected $securityPolicy;

    /**
     * Constructor
     * @param object $sdoFactory The user model
     * @param object $auth       The authentication dependency service
     * @param string $welcomeUri The default page for redirect after successfull login
     * @param string $passwordEncryption The password encryption
     * @param string $logoUri The URI of logo
     */
    public function __construct(\dependency\sdo\Factory $sdoFactory, \dependency\authentication\AuthenticationInterface $auth, $welcomeUri='/', $passwordEncryption, $logoUri, $securityPolicy)
    {
        $this->sdoFactory = $sdoFactory;
        $this->auth = $auth;
        $this->welcomeUri = $welcomeUri;
        $this->passwordEncryption = $passwordEncryption;
        $this->logoUri = $logoUri;
        $this->securityPolicy = $securityPolicy;
    }

    /**
     * Display login form
     *
     * @return string The URI of logo
     */
    public function prompt()
    {
        return $this->logoUri;
    }

    /**
     * authenticate a user
     * @param string $userName    The user name
     * @param string $password    The user password
     * @param string $requestPath The requested action
     *
     * @return bool
     */
    public function login($userName, $password, $requestPath)
    {
        try {
            $encryptedPassword = $password;
            if($this->passwordEncryption != null) {
                $encryptedPassword = hash($this->passwordEncryption, $password);
            }

            $user = $this->sdoFactory->read('user/user', array('userName'=>$userName));
            $_SESSION['user']['user'] = $user;

            if($this->securityPolicy['passwordValidity']) {
                $currentDate = \laabs::newTimestamp();
                $passwordDate = $user->passwordLastChange;

                if($currentDate->diff($passwordDate)->days > $this->securityPolicy['passwordValidity']) {
                    $userChangePassword = \laabs::newInstance('user/userRequirePasswordChange');
                    $userChangePassword->userId = $user->userId;
                    $userChangePassword->password = $user->password;
                }
            }

            $this->auth->logIn($userName, $encryptedPassword);

            $userLoginResponse = \laabs::newInstance('user/userLoginResponse');
            $userLoginResponse->userId = $user->userId;
            $userLoginResponse->badPasswordCount = 0;
            $userLoginResponse->locked = false;
            $userLoginResponse->lastLogin = \laabs::newTimestamp();
            $userLoginResponse->lastIp = $_SERVER["REMOTE_ADDR"];

            //var_dump($userLoginResponse);exit;

            $this->sdoFactory->update($userLoginResponse, 'user/user');

            $token = $this->encrypt($_SESSION['dependency']['authentication']['credential']);
            setcookie("LAABS-AUTH", $token, 0, '/');

            \laabs::newController('audit/entry')->add(
                $entryType = "user/authenticationSuccess",
                $objectClass = "user/user",
                $objectId = $user->userId,
                $message = "User " . $userName . " connected" //,
                // $action = "registeredMail/registeredMail/create",
                //$dataContext = null,
                //$entryId = false
            );

            if (in_array($requestPath, array(
                "/",
                "/user/prompt",
                "/user/login",
                "/user/logout",
                "/user/modifyUserPassword"
                ))
            ) {
                return $this->welcomeUri;
            }

            return $requestPath;

        } catch (\dependency\authentication\Exception\authenticationException $exception) {
            \laabs::newController('audit/entry')->add(
                $entryType = "user/authenticationFailure",
                $objectClass = "user/user",
                $objectId = "",
                $message = "Connection failure, unknow user " . $userName . " or bad password"//,
                // $action = "registeredMail/registeredMail/create",
                //$dataContext = null,
                //$entryId = false
            );
            
            throw $exception;
        } catch (\dependency\authentication\Exception\userUnknowException $exception) {
            \laabs::newController('audit/entry')->add(
                $entryType = "user/authenticationFailure",
                $objectClass = "user/user",
                $objectId = "",
                $message = "Connection failure, unknow user " . $userName //,
                // $action = "registeredMail/registeredMail/create",
                //$dataContext = null,
                //$entryId = false
            );
            
            throw $exception;
        } catch (\dependency\authentication\Exception\passwordChangeRequired $exception) {
            throw $exception;
        } catch (\dependency\sdo\Exception\objectNotFoundException $exception) {
            throw new \dependency\authentication\Exception\authenticationException("Username not registred or wrong password.");
        } catch (\dependency\authentication\Exception\wrongPasswordException $exception) {
            $_SESSION['user']['user'] = $user;

            \laabs::newController('audit/entry')->add(
                $entryType = "user/authenticationFailure",
                $objectClass = "user/user",
                $objectId = $user->userId,
                $message = "Connection failure for user " . $userName //,
                // $action = "registeredMail/registeredMail/create",
                //$dataContext = null,
                //$entryId = false
            );
            $userLoginResponse = \laabs::newInstance('user/userLoginResponse');
            $userLoginResponse->userId = $user->userId;
            $userLoginResponse->badPasswordCount = $user->badPasswordCount + 1;
            if ($this->securityPolicy['loginAttempts'] && $userLoginResponse->badPasswordCount > $this->securityPolicy['loginAttempts'] - 1) {
                $userLoginResponse->locked = true;
                $userLoginResponse->lastLogin = \laabs::newTimestamp();
                $userLoginResponse->lastIp = $_SERVER["REMOTE_ADDR"];
            }
            $this->sdoFactory->update($userLoginResponse, 'user/user');
            throw $exception;
        } catch (\dependency\authentication\Exception\userDisabledException $exception) {
            $_SESSION['user']['user'] = $user;

            \laabs::newController('audit/entry')->add(
                $entryType = "user/authenticationFailure",
                $objectClass = "user/user",
                $objectId = $user->userId,
                $message = "Connection failure, user " . $userName ." is disabled" //,
                // $action = "registeredMail/registeredMail/create",
                //$dataContext = null,
                //$entryId = false
            );
            
            throw $exception;
        } catch (\dependency\authentication\Exception\userLockException $exception) {
            $_SESSION['user']['user'] = $user;

            \laabs::newController('audit/entry')->add(
                $entryType = "user/authenticationFailure",
                $objectClass = "user/user",
                $objectId = $user->userId,
                $message = "Connection failure, user " . $userName ." is locked" //,
                // $action = "registeredMail/registeredMail/create",
                //$dataContext = null,
                //$entryId = false
            );
            
            throw $exception;
        }
    }

    /**
     * Get form to edit user information
     * @param string $userName      The user's name
     * @param string $oldPassword   The user's old password
     * @param string $newPassword   The user's new password
     * @param string $requestPath   The requested path
     */
    public function definePassword($userName, $oldPassword, $newPassword, $requestPath)
    {
        if($user = $this->sdoFactory->find('user/user', "userName='$userName'")) {
            $user = $user[0];

            //validation of security policy
            if($this->securityPolicy['passwordMinLength'] && strlen($newPassword) < $this->securityPolicy['passwordMinLength']) {
                throw new \bundle\user\Exception\invalidPasswordException("The password is to short.");
            }
            if($this->securityPolicy['passwordRequiresSpecialChars'] && !ctype_alnum($newPassword)) {
                throw new \bundle\user\Exception\invalidPasswordException("The password must contain special characters.");
            }
            if($this->securityPolicy['passwordRequiresDigits'] && preg_match('/.*\d.*', $newPassword)) {
                throw new \bundle\user\Exception\invalidPasswordException("The password must contain digits.");
            }
            if($this->securityPolicy['passwordRequiresMixedCase'] && !preg_match('^(?=.*[a-z])(?=.*[A-Z]).+$', $newPassword)) {
                throw new \bundle\user\Exception\invalidPasswordException("The password must contain upper and lower case characters");
            }


            $encryptedPassword = $newPassword;
            if($this->passwordEncryption != null) {
                $encryptedPassword = hash($this->passwordEncryption, $newPassword);
            }
            if ($user->password == $encryptedPassword) {
                throw new \bundle\user\Exception\invalidPasswordException("The password is the same as the precedent.");
            }

            $userPassword = \laabs::newInstance('user/userChangePassword');
            $userPassword->userId = $user->userId;
            $userPassword->userName = $userName;
            $userPassword->password = $encryptedPassword;
            $userPassword->passwordLastChange = \laabs::newTimestamp();
            $userPassword->lastLogin = \laabs::newTimestamp();
            $userPassword->lastIp = $_SERVER["REMOTE_ADDR"];
            $this->sdoFactory->update($userPassword, 'user/user');

            return $this->login($user->userName, $newPassword, $requestPath);
        }

        return false;
    }

    /**
     * Log out a  user
     *
     * @return bool
     */
    public function logout()
    {

        $this->auth->logOut();

        setcookie("LAABS-AUTH", '', time()-3600, '/');
    }

    /**
     * Crypt the creadential into an authentication token
     * @param dependency\Authentication\credential $credential
     * 
     * @return string
     */
    protected function encrypt($credential)
    {
        if (extension_loaded('mcrypt')) {
            $this->cryptCipher = \MCRYPT_BLOWFISH;
            $this->cryptMode = 'cbc';
            $this->cryptKey =  "LAABS";
            $this->cryptIV = "12345678";
        }

        $jsonCredential = \laabs\json_encode($credential);

        if (extension_loaded('mcrypt')) {
            $stringToken = mcrypt_encrypt($this->cryptCipher, $this->cryptKey, $jsonCredential, $this->cryptMode, $this->cryptIV);
        } elseif (extension_loaded('zlib')) {
            $stringToken = gzcompress($jsonCredential);
        } else {
            $stringToken = $jsonCredential;
        }

        $base64Token = base64_encode($stringToken);

        return $base64Token;
    }
}
