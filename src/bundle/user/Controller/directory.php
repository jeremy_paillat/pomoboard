<?php

namespace bundle\user\Controller;

/**
 * Control of the user
 *
 * @package user
 */
class directory
    implements \bundle\abstractDirectory\Controller\directoryInterface
{
    protected $sdoFactory;

    /**
     * Constructor
     * @param object $sdoFactory The model for organization
     *
     * @return void
     */
    public function __construct(\dependency\sdo\Factory $sdoFactory)
    {
        $this->sdoFactory = $sdoFactory;
    }

    /**
     * Get the list of orgPersons
     * @param string $objectClass The class of the object
     * @param string $query
     *
     * @return array The list object
     */
    public function find($objectClass, $query=false)
    {
        $queryTokens = \laabs\explode(" ", $query);
        $queryTokens = array_unique($queryTokens);

        $queryProperties = array("displayName", "firstName", "lastName");
        $queryPredicats = array();
        foreach ($queryProperties as $queryProperty) {
            foreach ($queryTokens as $queryToken) {
                $queryPredicats[] = $queryProperty. "=" . "'*" . $queryToken . "*'";
            }
        }
        $queryString = implode(" OR ", $queryPredicats);
        $queryString ="(".$queryString.") AND enabled=true";

        $result = $this->sdoFactory->find($objectClass, $queryString);

        return $result;
    }

    /**
     * Display an object with its id
     * @param string $objectId The class of the object
     * @param string $objectClass The class of the object
     *  
     * @return mixed The object
     */
    public function read($objectId, $objectClass)
    {
        $user = $this->sdoFactory->read($objectClass, $objectId);
        if($user && $user->enabled) {
            return $user;
        }        
        return false;
    }
}
