<?php

namespace bundle\user\Controller;

/**
 * user admin controller
 *
 * @package User
 */
class user
{
    protected $sdoFactory;
    protected $passwordEncryption;
    protected $securityPolicy;

    /**
     * Constructor
     * @param \dependency\sdo\Factory $sdoFactory           The dependency Sdo Factory object
     * @param string                  $passwordEncryption   The password encryption algorythm
     */
    public function __construct(\dependency\sdo\Factory $sdoFactory=null, $passwordEncryption='md5', $securityPolicy)
    {
        $this->sdoFactory = $sdoFactory;
        $this->passwordEncryption = $passwordEncryption;
        $this->securityPolicy = $securityPolicy;
    }

    /**
     * List all users for administration
     * 
     * @return user/user[] The array of users
     */
    public function index()
    {
        return $this->sdoFactory->find('user/user', false, "lastName");
    }

    /**
     *  Prepare an empty user object
     * 
     * @return user/user The user object
     */
    public function newUser()
    {
        return \laabs::newInstance('user/user');
    }

    /**
     * Record a new user
     * @param user/user $user The user object
     * 
     * @return user/user The user object
     */
    public function addUser($user)
    {
  
        //$user->userId = \laabs::newId();
        $user->userId = $user->userName;

        if (is_null($user->picture)) {
            $user->picture = \laabs::createMemoryStream('');
        }
        else if (((strlen($user->picture)) / 1024) > 100) {
            throw new \bundle\user\Exception\invalidUserInformationException();
        }

        if ($this->sdoFactory->exists('user/user', array('userName' => $user->userName))) {
            throw new \bundle\user\Exception\userAlreadyExistException();
        }
        // Verifier l'antériorité de dateOfBirth avec la date du jour ou nul
        // Vérifier le password avec la strategie de mot de passe
        // Vérifier l'email

        if (!\laabs::validate($user, 'user/user')) {
            $validationErrors = \laabs::getValidationErrors();
            throw new \bundle\user\Exception\invalidUserInformationException($validationErrors);
        }

        $encryptedPassword = $user->password;
        if($this->passwordEncryption != null) {
            $encryptedPassword = hash($this->passwordEncryption, $user->password);
        }

        $user->password = $encryptedPassword;
        $user->passwordLastChange = \laabs::newDate();
        $user->badPasswordCount = 0;
        $user->lastLogin = null;
        $user->lastIp = null;

        $this->sdoFactory->create($user, 'user/user');

        return $user;
    }

    /**
     * Prepare a user object for update
     * @param id $userId The user unique identifier
     * 
     * @return user/user The user object
     */
    public function edit($userId)
    {
        return $this->sdoFactory->read('user/user', $userId);
    }

    /**
     * Modify user information
     * @param user/userInformation $user The user object
     *
     * @return boolean The result of the request
     */
    public function modifyUserInformation(\bundle\user\Model\userInformation $user)
    {        
        if (is_null($user->picture)) {
            $user->picture = \laabs::createMemoryStream('');
        }
        $user->userId = $user->userName;
        if (!$this->sdoFactory->exists('user/user', array('userId' => $user->userId))) {
            throw new \bundle\user\Exception\unknownUserException();
        }
        
        return $this->sdoFactory->update($user, 'user/userInformation');
    }

    /**
     * Change a user password
     * @param string $userId      The identifier of the user
     * @param string $newPassword The new password
     *
     * @return boolean The result of the request
     */
    public function setPassword($userId, $newPassword)
    {
        $user = \laabs::newInstance('user/userSetPassword');

        $encryptedPassword = $newPassword;
        if($this->passwordEncryption != null) {
            $encryptedPassword = hash($this->passwordEncryption, $newPassword);
        }

        $user->password = $encryptedPassword;
        $user->userId = $userId;

        return $this->sdoFactory->update($user, 'user/user');
    }

    /**
     * Required password change
     * @param string $userId The identifier of the user
     *
     * @return boolean The result of the request
     */
    public function requirePasswordChange($userId)
    {
        $user = \laabs::newInstance('user/userRequirePasswordChange');
        $user->userId = $userId;

        return $this->sdoFactory->update($user, 'user/user');
    }

    /**
     * Lock a user
     * @param string $userId The identifier of the user
     *
     * @return boolean The result of the request
     */
    public function lock($userId)
    {
        $user = \laabs::newInstance('user/userLockedResponse');
        $user->userId = $userId;

        return $this->sdoFactory->update($user);
    }

    /**
     * Unlock a user
     * @param string $userId The identifier of the user
     *
     * @return boolean The result of the request
     */
    public function unlock($userId)
    {
        $user = \laabs::newInstance('user/userUnlockedResponse');
        $user->userId = $userId;

        return $this->sdoFactory->update($user);
    }

    /**
     * Enable a user
     * @param string $userId The identifier of the user
     *
     * @return boolean The result of the request
     */
    public function enable($userId)
    {
        $userChangeStatus = \laabs::newInstance('user/userChangeStatus');
        $userChangeStatus->enabled = true;
        $userChangeStatus->userId = $userId;
        $userChangeStatus->replacingUserId = null;

        return $this->sdoFactory->update($userChangeStatus);
    }

    /**
     * Disable a user
     * @param string $userId          The identifier of the user
     * @param string $replacingUserId The identifier of the replacing user
     *
     * @return boolean The result of the request
     */
    public function disable($userId, $replacingUserId)
    {
        $userChangeStatus = \laabs::newInstance('user/userChangeStatus');
        $userChangeStatus->userId = $userId;
        $userChangeStatus->replacingUserId = $replacingUserId;
        $userChangeStatus->enabled = false;

        return $this->sdoFactory->update($userChangeStatus);
    }

    /**
     * Search users
     * @param string $query The query
     *
     * @return array The list of fouded users
     */
    public function queryUsers($query=false)
    {
        $queryTokens = \laabs\explode(" ", $query);
        $queryTokens = array_unique($queryTokens);

        $userQueryProperties = array("displayName");
        $userQueryPredicats = array();
        foreach ($userQueryProperties as $userQueryProperty) {
            foreach ($queryTokens as $queryToken) {
                $userQueryPredicats[] = $userQueryProperty. "=" . "'*" . $queryToken . "*'";
            }
        }
        $userQueryString = implode(" OR ", $userQueryPredicats);
        $persons = $this->sdoFactory->find('user/userInformation', $userQueryString);

        return $persons;
    }
}