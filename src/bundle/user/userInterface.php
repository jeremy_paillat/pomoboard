<?php

namespace bundle\user;
/**
 * Interface for user
 */
interface userInterface
{
    /**
     * Prepare a user object for update
     * @param id $userId The user unique identifier
     * 
     * @return user/user The user object
     * 
     * @request READ userInfo/edit/([^/]+)
     * @action user/user/edit
     * @output user/userManagement/editUserInformation
     */
    public function edit($userId);

    /**
     * Allow to modify user information
     * @param user/userInformation $user The user object
     *
     * @return boolean The result of the request
     * 
     * @request UPDATE userInfo/update
     * @action user/user/modifyUserInformation
     */
    public function modifyUserInformation(\bundle\user\Model\userInformation $user);

    /**
     * Allow to modify user password
     *
     * @prompt
     * @request READ userInfo/password
     * @output user/userManagement/editUserPassword
     */
    public function editUserPassword();

    /**
     * Modify user password
     *
     * @request UPDATE userInfo/changePassword
     * @output user/authentication/definePassword
     */
    public function modifyUserPassword($userName, $oldPassword, $newPassword, $requestPath);
}