<?php

namespace bundle\user;
/**
 * Interface for user administration
 */
interface adminInterface
{
    /**
     * List all users for administration
     * 
     * @return user/user[] The array of users
     * 
     * @request READ user/list
     * @action user/user/index
     */
    public function index();

    /**
     *  Prepare an empty user object
     * 
     * @return user/user The user object
     *
     * @request READ user/new
     * @action user/user/newUser
     */
    public function newUser();

    /**
     * Record a new user
     * @param user/user $user The user object
     * 
     * @return user/user The user object
     * 
     * @request CREATE user/add
     * @action user/user/addUser
     */
    public function addUser($user);

    /**
     * Prepare a user object for update
     * @param id $userId The user unique identifier
     * 
     * @return user/user The user object
     * 
     * @request READ user/edit/([^/]+)
     * @action user/user/edit
     */
    public function edit($userId);

    /**
     * Allow to modify user information
     * @param user/userInformation $user The user object
     *
     * @return boolean The result of the request
     * 
     * @request UPDATE user/update
     * @action user/user/modifyUserInformation
     */
    public function modifyUserInformation(\bundle\user\Model\userInformation $user);

    /**
     * Disable a user
     * @param string $userId          The identifier of the user
     * @param string $replacingUserId The identifier of the replacing user
     *
     * @return boolean The result of the request
     * 
     * @request UPDATE user/disable/([^/]+)/([^/]+)
     * @action user/user/disable
     */
    public function disable($userId, $replacingUserId);

    /**
     * Enable a user
     * @param string $userId The identifier of the user
     *
     * @return boolean The result of the request
     * 
     * @request UPDATE user/enable/([^/]+)
     * @action user/user/enable
     */
    public function enable($userId);

    /**
     * Lock a user
     * @param string $userId The identifier of the user
     *
     * @return boolean The result of the request
     * 
     * @request UPDATE user/lock/([^/]+)
     * @action user/user/lock
     */
    public function lock($userId);

    /**
     * Unlock a user
     * @param string $userId The identifier of the user
     *
     * @return boolean The result of the request
     * 
     * @request UPDATE user/unlock/([^/]+)
     * @action user/user/unlock
     */
    public function unlock($userId);

    /**
     * Change a user password
     * @param string $userId      The identifier of the user
     * @param string $newPassword The identifier of the user
     *
     * @return boolean The result of the request
     * 
     * @request UPDATE user/setPassword
     * @action user/user/setPassword
     */
    public function setPassword($userId, $newPassword);

    /**
     * Required password change
     * @param string $userId The identifier of the user
     *
     * @return boolean The result of the request
     * 
     * @request UPDATE user/requirePasswordChange/([^/]+)
     * @action user/user/requirePasswordChange
     */
    public function requirePasswordChange($userId);

    /**
     * Search users
     * @param string $query The query
     *
     * @return array The list of fouded users
     * 
     * @request READ user/query/([^/]+)
     * @action user/user/queryUsers
     */
    public function queryUsers($query=false);

}