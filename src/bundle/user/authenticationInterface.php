<?php

namespace bundle\user;
/**
 * Interface for authentication
 */
interface authenticationInterface
{
    /**
     * authenticate a user
     * @param string $userName  The user name
     * @param string $password  The user password
     * @param string $actionUri The requested action
     *
     * @return bool
     * 
     * @request CREATE user/login
     */
    public function login($userName, $password, $actionUri);

    /**
     * Authenticate a user
     *
     * @return bool
     * 
     * @request READ user/prompt
     */
    public function prompt();

    /**
     * Log off a user
     *
     * @return bool
     * 
     * @request READ user/logout
     */
    public function logout();

    /**
     * Get form to edit user information
     * @param object $passwordInformation This object contain three attribut, id of user "userId", the old password "oldPassword" and the new password "newPassword"
     * 
     * @request UPDATE user/definePassword
     */
    public function definePassword($userName, $oldPassword, $newPassword, $requestPath);

}