<?php

namespace bundle\user\Parser\json;

/**
 * user json parser
 *
 * @package User
 * 
 */
class user
{
    public $jsonObject;

    /**
     * Constructor
     * @param \dependency\json\JsonObject $jsonObject The Json utility
     */
    public function __construct(\dependency\json\JsonObject $jsonObject)
    {
        $this->jsonObject = $jsonObject;
    }

    /**
     * Converting a base64 image to binary
     * @param string $imageBase64 The image
     * 
     * @return string
     */
    public function decodeImage($imageBase64)
    {
        if ($imageBase64 == '' || sizeof($imageBase64) == 1) {
            return null;
        }
        
        $imageBase64 = substr($imageBase64, strpos($imageBase64, "base64,")+7);
        $imageBinary = base64_decode($imageBase64);

        return \laabs::createMemoryStream($imageBinary);
    }

    /**
     * Parse data for add user
     * @param Object $userJson JSON data of creation form
     * 
     * @return array
     */
    public function addUser($userJson)
    {

        $this->jsonObject->loadJson($userJson, 'user/user');

        $this->jsonObject->picture = $this->decodeImage($this->jsonObject->picture);
        
        if ($this->jsonObject->dateOfBirth == ''){
            $this->jsonObject->dateOfBirth = null;
        }

        switch (strtolower($this->jsonObject->enabled)) {
            case '1':
            case 'y':
            case 'yes':
            case 'true':
            case 'on':
            case true:
                $this->jsonObject->enabled = true;
                break;

            default:
                $this->jsonObject->enabled = false;
                break;
        }
        
        return array('user' => $this->jsonObject->export());
    }

    /**
     * Parse data for set password
     * @param Object $passwordJson JSON data to change password
     * 
     * @return array
     */
    public function setPassword($passwordJson)
    {
        $passwordObject = json_decode($passwordJson);

        return array('passwordObject' => $passwordObject);
    }

    /**
     * Parse data for update
     * @param Object $userJson JSON data of update form
     * 
     * @return array
     */
    public function modifyUserInformation($userJson)
    {
        $this->jsonObject->loadJson($userJson, 'user/userInformation');

        $this->jsonObject->picture = $this->decodeImage($this->jsonObject->picture);

        if ($this->jsonObject->dateOfBirth == ''){
            $this->jsonObject->dateOfBirth = null;
        }
        
        return array('user' => $this->jsonObject->export());
    }
}
