<?php

namespace bundle\user\Exception;

class userAlreadyExistException
    extends \Exception
{
    public $message = false;

    /**
     * undocumented function
     *
     * @return void
     */
    public function __construct()
    {
        $this->message = "User already exist";
    }

}
