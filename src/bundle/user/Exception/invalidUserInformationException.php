<?php

namespace bundle\user\Exception;

class invalidUserInformationException
    extends \Exception
{
    public $message = false;
    public $validationErrors = array();

    /**
     * undocumented function
     *
     * @return void
     */
    public function __construct(array $validationErrors=null) 
    {
        parent::__construct("Invalid user information");
        $this->message = "Invalid user information"."\n";
        $this->validationErrors = $validationErrors;
    }

}
