<?php

namespace bundle\user\Exception;

class invalidPasswordException
    extends \Exception
{
    public $message = false;
    public $validationErrors = array();

    /**
     * undocumented function
     *
     * @return void
     */
    public function __construct($message, array $validationErrors=null)
    {
        parent::__construct("Same password");
        $this->message = $message;
        $this->validationErrors = $validationErrors;
    }

}
