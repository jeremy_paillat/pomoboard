<?php

namespace bundle\user\Exception;

class unknownUserException
    extends \Exception
{
    public $message = false;

    /**
     * undocumented function
     *
     * @return void
     */
    public function __construct()
    {
        $this->message = "User unknown";
    }

}
