<?php

namespace bundle\authorization;
/**
 * Interface for authorization group users
 */
interface userAuthorizationInterface
{

    /**
     * List the user groups
     *
     * @return array Array of authorization/group object
     * 
     * @request READ authorization/usergroups
     */
    public function getMyGroups();

}