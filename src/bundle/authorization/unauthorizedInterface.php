<?php

namespace bundle\authorization;
/**
 * Interface for not authorized event
 */
interface unauthorizedInterface
{

    /**
     * Notify missing privilege
     * 
     * @request READ authorization/noPrivilege
     * @prompt
     */
    public function noPrivilege();

    /**
     * Notify missing privilege
     * 
     * @request READ authorization/noAccess
     * @prompt
     */
    public function noAccess();

}