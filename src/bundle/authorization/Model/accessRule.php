<?php

namespace bundle\authorization\Model;
/**
 * Class model that represents the rule of a group
 *
 * @package authorization
 * 
 * @fkey [groupId] authorization/group [groupId]
 */
class accessRule
{
    /**
     * The group's identifier
     *
     * @var id
     */
    public $groupId;

    /**
     * The class concerned by the rule
     *
     * @var string
     */
    public $className;

    /**
     * The context of the rul
     *
     * @var string
     */
    public $context;
}
