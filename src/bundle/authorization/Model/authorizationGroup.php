<?php

namespace bundle\authorization\Model;
/**
 * Class model that represents a group of user 
 *
 * @package authorization
 * 
 * @substitution authorization/group
 * @pkey [objectId]
 */
class authorizationGroup
    extends \bundle\abstractDirectory\Model\abstractGroup
{
    /**
     * The group's identifier
     *
     * @var id
     * @substitution groupId
     * @notempty
     */
    public $objectId;

    /**
     * The person type
     *
     * @var string
     */
    protected $objectClass = 'authorization/authorizationGroup';
}
