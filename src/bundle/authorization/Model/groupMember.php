<?php

namespace bundle\authorization\Model;
/**
 * Class model that represents the members of a group
 *
 * @package authorization
 * 
 * @fkey [groupId] authorization/group [groupId]
 * @fkey [userId] user/user [userId]
 */
class groupMember
{
    /**
     * The group's identifier
     *
     * @var id
     */
    public $groupId;

    /**
     * The user's identifier
     *
     * @var id
     */
    public $userId;
}
