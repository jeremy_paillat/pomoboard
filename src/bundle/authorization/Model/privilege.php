<?php

namespace bundle\authorization\Model;
/**
 * Class model that represents the privileges of a group
 *
 * @package authorization
 * 
 * @fkey [groupId] authorization/group [groupId]
 */
class privilege
{
    /**
     * The group's identifier
     *
     * @var id
     */
    public $groupId;

    /**
     * The route of action granted for the group
     *
     * @var string
     */
    public $route;
}

