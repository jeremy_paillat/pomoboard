<?php

namespace bundle\authorization\Model;
/**
 * Class model that represents a group of user 
 *
 * @package authorization
 * 
 * @pkey [groupId]
 */
class group
{
    /**
     * The group's identifier
     *
     * @var id
     */
    public $groupId;

    /**
     * The group's name
     *
     * @var string
     */
    public $groupName;

    /**
     * The displayed name
     *
     * @var string
     */
    public $displayName;

    /**
     * The group's description
     * @var string
     */
    public $description;

    /**
     * The status of the group (enabled or not)
     *
     * @var boolean
     */
    public $enabled = true;

    /**
     * The group's picture
     *
     * @var resource
     */
    public $picture;

    /**
     * @var authorization/privilege[]
     */
    public $privileges = array();

    /**
     * @var authorization/accessRule[]
     */
    public $accessRules = array();

    /**
     * @var authorization/groupMember[]
     */
    public $groupMembers = array();
}
