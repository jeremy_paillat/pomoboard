<?php

namespace bundle\authorization\Controller;

/**
 * Control of the authorizations of a user
 *
 * @package Authorization
 */
class userAuthorization
{
    protected $sdoFactory;

    /**
     * Constructor
     * @param object $sdoFactory The model for authorization
     *
     * @return void
     */
    public function __construct(\dependency\sdo\Factory $sdoFactory)
    {
        $this->sdoFactory = $sdoFactory;
    }

    /**
     * Get all my groups
     * @return array The list of my groups
     */
    public function getMyGroups()
    {
        $userId = $_SESSION['user']['user']->userId;
        
        $groupMembers = $this->sdoFactory->find('authorization/groupMember', "userId = '$userId'");
        
        $groups = array();

        foreach ($groupMembers as $groupMember) {
            array_push($groups, $this->sdoFactory->read('authorization/group', $groupMember->groupId)); 
        }

        if (count($groups) == 1) {
            $_SESSION['authorization']['currentGroup'] = reset($groups);
        }

        return $groups;
    }

    /**
     * List user positions orgUnits ids
     *
     * @return array The list of orgUnit ids
     */
    public function listMyGroupIds()
    {
        $userId = $_SESSION['user']['user']->userId;
        $groupMembers = $this->sdoFactory->find('authorization/groupMember', "userId = '$userId'");
        
        $groupIds = array();

        foreach ($groupMembers as $groupMember) {
            $groupIds[] = (string) $groupMember->groupId; 
        }

        return $groupIds;        
    }

}