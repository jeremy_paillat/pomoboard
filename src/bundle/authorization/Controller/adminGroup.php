<?php

namespace bundle\authorization\Controller;
/**
 * Controler for the authorization group management
 *
 * @package authorization 
 */
class adminGroup
    extends \bundle\abstractDirectory\Controller\abstractDirectory
{

    /**
     * Constructor of adminGroup class
     * @param string                  $groupId    The identifier of the group
     * @param \dependency\sdo\Factory $sdoFactory The factory
     */
    public function __construct(\dependency\sdo\Factory $sdoFactory)
    {
        parent::__construct($sdoFactory);
    }

    /**
     * List the authorization's groups
     *
     * @return array Array of authorization/group object
     */
    public function index()
    {
        return $this->sdoFactory->find("authorization/group");
    }

    /**
     * Prepare an empty group object
     *
     * @return authorization/group THe empty group object
     */
    public function newGroup()
    {
        return \laabs::newInstance('authorization/group');
    }

    /**
     * Prepares a group object for update
     * @param string $groupId The identifier of the group
     *
     * @return authorization/group The requested group
     */
    public function edit($groupId)
    {
        $group = $this->sdoFactory->read("authorization/group", $groupId);
        $group->privileges = $this->sdoFactory->readChildren("authorization/privilege", $group);
        $group->rules = $this->sdoFactory->readChildren("authorization/accessRule", $group);
        $groupMembers = $this->sdoFactory->readChildren("authorization/groupMember", $group);
        foreach ($groupMembers as $user) {
            $member = parent::read("authorization/group", "user/userPerson", $user->userId);
            if($member) {
                if (empty($member->lastName)) {
                    $group->groupMembers[] = $member;
                } else {
                    $group->groupMembers[$member->lastName] = $member;
                }
            }
        }
        
        ksort($group->groupMembers);

        return $group;
    }

    /**
     * Recorde a new group
     * @param authorization/group $group The group object to create
     *
     * @return boolean The status of the query
     */
    public function create($group)
    {
        $res = false;
        $this->sdoFactory->beginTransaction();
        try {
            $group->groupId = \laabs::newId();;
            $this->sdoFactory->create($group);
            
            if (!empty($group->privileges)) {
                foreach ($group->privileges as $privilege) {
                    $privilege->groupId = $group->groupId;
                    $this->sdoFactory->create($privilege);
                }
            }

            if (!empty($group->accessRules)) {
                foreach ($group->accessRules as $accessRule) {
                    $accessRule->groupId = $group->groupId;
                    $this->sdoFactory->create($accessRule);
                }
            }

            if (!empty($group->groupMembers)) {
                foreach ($group->groupMembers as $member) {
                    $member->groupId = $group->groupId;
                    $this->sdoFactory->create($member);
                }
            }

            $this->sdoFactory->commit();
            
            if (\laabs::hasBundle('audit')) {
                $auditEntry = \laabs::newInstance('audit/entry');
                $auditEntry->entryType = "authorization/groupCreation";
                $auditEntry->objectClass = "authorization/group";
                $auditEntry->objectId = $group->groupId;
                $auditEntry->message = "Group created";
                $auditEntry->action = 'authorization/adminGroup/create';

                \laabs::notify(\bundle\audit\AUDIT_ENTRY, $auditEntry);
            }
        
            $res = true;
        } catch (\Exception $e) {
            $this->sdoFactory->rollback();
        }

        return $res;
    }

    /**
     * Updates a group
     * @param authorization/group $group The group object to update
     *
     * @return boolean The status of the query
     */
    public function update($group)
    {
        $res = false;
        $this->sdoFactory->beginTransaction();
        try {
            $this->sdoFactory->update($group);

            $this->sdoFactory->deleteChildren("authorization/privilege", $group);
            if (!empty($group->privileges)) {
                foreach ($group->privileges as $privilege) {
                    $privilege->groupId = $group->groupId;
                    $this->sdoFactory->create($privilege);
                }
            }

            $this->sdoFactory->deleteChildren("authorization/accessRule", $group);
            if (!empty($group->accessRules)) {
                foreach ($group->accessRules as $accessRule) {
                    $accessRule->groupId = $group->groupId;
                    $this->sdoFactory->create($accessRule);
                }
            }

            $this->sdoFactory->deleteChildren("authorization/groupMember", $group);
            if (!empty($group->groupMembers)) {
                foreach ($group->groupMembers as $member) {
                    $member->groupId = $group->groupId;
                    $this->sdoFactory->create($member);
                }
            }
            
            $this->sdoFactory->commit();
            
            if (\laabs::hasBundle('audit')) {
                $auditEntry = \laabs::newInstance('audit/entry');
                $auditEntry->entryType = "authorization/groupModification";
                $auditEntry->objectClass = "authorization/group";
                $auditEntry->objectId = $group->groupId;
                $auditEntry->message = "Group updated";
                $auditEntry->action = 'authorization/adminGroup/update';

                \laabs::notify(\bundle\audit\AUDIT_ENTRY, $auditEntry);
            }
            
            $res = true;
        }
        catch (\Exception $e) {
            $this->sdoFactory->rollback();
        }

        return $res;
    }

    /**
     * Lock or unlock a group
     * @param boolean $status The new status of the group
     * @param authorization/group $group The group object to update
     * 
     * @return boolean The status of the query
     */
    public function changeStatus($groupId, $status)
    {
        $group = $this->sdoFactory->read("authorization/group", $groupId);
        if($status=="true"){
            $group->enabled = 1;
        } else {
            $group->enabled = 0;
        }
        $result = $this->sdoFactory->update($group, 'authorization/group');
        
        if (\laabs::hasBundle('audit')) {
            $auditEntry = \laabs::newInstance('audit/entry');
            $auditEntry->entryType = "authorization/groupModification";
            $auditEntry->objectClass = "authorization/group";
            $auditEntry->objectId = $group->groupId;
            $auditEntry->message = "Status group updated";
            $auditEntry->action = 'authorization/adminGroup/changeStatus';

            \laabs::notify(\bundle\audit\AUDIT_ENTRY, $auditEntry);
        }
        
        return $result;
    }

    /**
     * Delete an authorization group
     * @param authorization/group $group The group object to update
     * 
     * @return boolean The status of the query
     */
    public function delete($groupId)
    {   
        $res = false;
        $this->sdoFactory->beginTransaction();
        try {
            $this->sdoFactory->delete($group);
            $this->sdoFactory->deleteChildren("authorization/privilege", $group);
            $this->sdoFactory->deleteChildren("authorization/accessRule", $group);
            $this->sdoFactory->deleteChildren("authorization/groupMember", $group);
            $this->sdoFactory->commit();
            
            if (\laabs::hasBundle('audit')) {
                $auditEntry = \laabs::newInstance('audit/entry');
                $auditEntry->entryType = "group/delete";
                $auditEntry->objectClass = "authorization/group";
                $auditEntry->objectId = $group->groupId;
                $auditEntry->message = "Group deleted";
                $auditEntry->action = 'authorization/adminGroup/delete';

                \laabs::notify(\bundle\audit\AUDIT_ENTRY, $auditEntry);
            }
        
            $res = true;
        }
        catch (\Exception $e) {
            $this->sdoFactory->rollback();
        }

        return $res;
    }

    /**
     * Get the list of available persons
     * @param string $query A query string of tokens
     * 
     * @return array The list of persons
     */
    public function queryPersons($query)
    {
        return parent::query('authorization/group', $query);
    }

}
