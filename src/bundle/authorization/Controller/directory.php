<?php

namespace bundle\authorization\Controller;
/**
 * organization controller
 *
 * @package Authorization
 */
class directory
    implements \bundle\abstractDirectory\Controller\directoryInterface
{
    protected $sdoFactory;

    /**
     * Constructor
     * @param object $sdoFactory The model for organization
     *
     * @return void
     */
    public function __construct(\dependency\sdo\Factory $sdoFactory)
    {
        $this->sdoFactory = $sdoFactory;
    }

    /**
     * Get the list of persons
     * @param string $objectClass The class of the object to request
     * @param string $query
     *
     * @return array The list object
     */
    public function find($objectClass, $query=false)
    {
        // userPerson substitutes to user, personId substitutes to userId
        $queryTokens = \laabs\explode(" ", $query);
        $queryTokens = array_unique($queryTokens);

        $queryProperties = array("displayName");
        $queryPredicats = array();
        foreach ($queryProperties as $queryProperty) {
            foreach ($queryTokens as $queryToken) {
                $queryPredicats[] = $queryProperty. "=" . "'*" . $queryToken . "*'";
            }
        }
        $queryString = implode(" OR ", $queryPredicats);

        $result = $this->sdoFactory->find($objectClass, $queryString);

        return $result;
    }

    /**
     * Display an object with its id
     * @param string $objectId The class of the object
     * @param string $objectClass The class of the object
     *  
     * @return mixed The object
     */
    public function read($objectId, $objectClass)
    {
        return $this->sdoFactory->read($objectClass, $objectId);
    }
}