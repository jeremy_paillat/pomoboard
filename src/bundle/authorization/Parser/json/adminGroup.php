<?php

namespace bundle\authorization\Parser\json;

/**
 * adminGroup parser
 *
 * @package Authorization
 */
class adminGroup
{
    public $jsonObject;

    /**
     * Constructor
     * @param \dependency\json\JsonObject $jsonObject The Json utility
     */
    public function __construct(\dependency\json\JsonObject $jsonObject)
    {
        $this->jsonObject = $jsonObject;
    }

    /**
     * Parse data for creating a group
     * @param Object $groupJson JSON data of creation form
     * 
     * @return array
     */
    public function create($groupJson)
    {
        $this->jsonObject->loadJson($groupJson, 'authorization/group');
        return array('group' => $this->jsonObject->export());

    }

    /**
     * Parse data for updating a group
     * @param Object $groupJson JSON data of creation form
     * 
     * @return array
     */
    public function update($groupJson)
    {
        $this->jsonObject->loadJson($groupJson, 'authorization/group');

        return array('group' => $this->jsonObject->export());
    }

}