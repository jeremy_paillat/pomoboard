<?php

namespace bundle\authorization;
/**
 * Interface for authorization group administration
 */
interface adminGroupInterface
{

    /**
     * List the authorization's groups
     *
     * @return array Array of authorization/group object
     * 
     * @request READ authorization/index
     */
    public function index();

    /**
     * Prepare an empty group object
     *
     * @return authorization/group THe empty group object
     * 
     * @request READ authorization/new
     */
    public function newGroup();

    /**
     * Prepares access control object for update or create
     * @param string $groupId The identifier of the group
     *
     * @return authorization/group The requested group
     * 
     * @request READ authorization/edit/([^/]+)
     */
    public function edit($groupId);

    /**
     * Recorde a new group
     * @param authorization/group $group The group object to create
     *
     * @return boolean The status of the query
     * 
     * @request CREATE authorization/create
     */
    public function create($group);

    /**
     * Updates a group
     * @param authorization/group $group The group object to update
     *
     * @return boolean The status of the query
     * 
     * @request UPDATE authorization/update
     */
    public function update($group);

    /**
     * Lock or unlock a group
     * @param boolean $status The new status of the group
     * @param authorization/group $group The group object to update
     * 
     * @return boolean The status of the query
     * 
     * @request UPDATE authorization/changeStatus/([^/]+)/([^/]+)
     */
    public function changeStatus($groupId, $status);

    /**
     * Delete an authorization group
     * @param authorization/group $group The group object to update
     * 
     * @return boolean The status of the query
     * 
     * @request UPDATE authorization/delete/([^/]+)
     */
    public function delete($groupId);

    /**
     * Get the list of available persons
     * @param string $query A query string of tokens
     * 
     * @return array THe list of person
     * 
     * @request READ authorization/queryPersons/([^/]+)
     */
    public function queryPersons($query);

}