-- Schema: authorization
DROP SCHEMA IF EXISTS "authorization" CASCADE;
CREATE SCHEMA "authorization";

-- Table: "accessControl"."accessControl"
DROP TABLE IF EXISTS "authorization"."group" CASCADE;
CREATE TABLE "authorization"."group"
(
  "groupId" text PRIMARY KEY,
  "groupName" text NOT NULL,
  "displayName" text NOT NULL,
  description text,
  enabled boolean default true,
  picture bytea
)
WITH (
  OIDS=FALSE
);

-- Table: "authorization"."groupMember"
DROP TABLE IF EXISTS "authorization"."groupMember" CASCADE;
CREATE TABLE "authorization"."groupMember"
(
  "groupId" text,
  "userId" text,
  CONSTRAINT "groupMember_groupId_fkey" FOREIGN KEY ("groupId")
      REFERENCES "authorization"."group" ("groupId") MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
)
WITH (
  OIDS=FALSE
);

-- Table: "authorization".privileges
DROP TABLE IF EXISTS "authorization"."privilege" CASCADE;
CREATE TABLE "authorization"."privilege"
(
  "groupId" text,
  "route" text,
  CONSTRAINT "privilege_groupId_fkey" FOREIGN KEY ("groupId")
      REFERENCES "authorization"."group" ("groupId") MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
)
WITH (
  OIDS=FALSE
);

-- Table: "authorization".rules
DROP TABLE IF EXISTS "authorization"."accessRule" CASCADE;
CREATE TABLE "authorization"."accessRule"
(
  "groupId" text,
  "className" text NOT NULL,
  context text,
  CONSTRAINT "accessRule_groupId_fkey" FOREIGN KEY ("groupId")
      REFERENCES "authorization"."group" ("groupId") MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
)
WITH (
  OIDS=FALSE
);