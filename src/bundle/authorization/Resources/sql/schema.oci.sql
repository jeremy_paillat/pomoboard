DROP TABLE "accessRule";
DROP TABLE "privilege";
DROP TABLE "groupMember";
DROP TABLE "group";

CREATE TABLE "group"
( 
    "groupId"     VARCHAR2(512) NOT NULL,
    "groupName"   VARCHAR2(512) NOT NULL,
    "displayName" VARCHAR2(512) NOT NULL,
    "description" VARCHAR2(512),
    "enabled"     NUMBER(1) DEFAULT 1 NOT NULL,
    "picture"     BLOB,
    PRIMARY KEY ("groupId")
);

CREATE TABLE "groupMember"
( 
    "groupId" VARCHAR2(512) NOT NULL,
    "userId"  VARCHAR2(512) NOT NULL,
    FOREIGN KEY ("groupId")
        REFERENCES "group" ("groupId")
);

CREATE TABLE "privilege"
( 
    "groupId" VARCHAR2(512) NOT NULL,
    "route"   VARCHAR2(512) NOT NULL,
    FOREIGN KEY ("groupId")
        REFERENCES "group" ("groupId")
);

CREATE TABLE "accessRule"
( 
    "groupId"   VARCHAR2(512) NOT NULL,
    "className" VARCHAR2(512) NOT NULL,
    "context" VARCHAR2(2000),
    FOREIGN KEY ("groupId")
        REFERENCES "group" ("groupId")
);

