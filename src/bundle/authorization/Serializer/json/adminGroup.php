<?php

namespace bundle\authorization\Serializer\json;

/**
 * administration of authorization group JSON serializer
 *
 * @package authorization
 */
class adminGroup
{
    protected $json;
    protected $translator;

    /**
     * Constructor of adminGroup class
     * @param \dependency\json\JsonObject $json
     */
    public function __construct(\dependency\json\JsonObject $json, \dependency\localisation\TranslatorInterface $translator)
    {
        $this->json = $json;
        $this->translator = $translator;
        $this->translator->setCatalog('authorization/messages');
        $this->json->status = true;
    }

    /**
     * Serializer JSON for create method
     * 
     * @return object JSON object with a status and message parameters
     */
    public function create()
    {
        $this->json->message = "New group created.";
        $this->json->message = $this->translator->getText($this->json->message);

        return $this->json->save();
    }

    /**
     * Serializer JSON for update method
     * 
     * @return object JSON object with a status and message parameters
     */
    public function update()
    {
        $this->json->message = "Group updated.";
        $this->json->message = $this->translator->getText($this->json->message);

        return $this->json->save();
    }

    /**
     * Serializer JSON for changeStatus method
     * 
     * @return object JSON object with a status and message parameters
     */
    public function changeStatus()
    {
        $this->json->message = "Status changed.";
        $this->json->message = $this->translator->getText($this->json->message);

        return $this->json->save();
    }

    /**
     * Serializer JSON for queryPersons method
     * @param array $persons An array of orgPersonParty matching the user query
     *
     * @return string
     **/
    public function queryPersons($persons)
    {
        return \laabs\json_encode($persons);
    }

}
