<?php

namespace bundle\authorization\Serializer\html;
/**
 * Serializer for authorization group administration in Html
 *
 * @package authorization
 */
class unauthorized
{
    public $view;

    /**
     * Constructor
     * @param \dependency\html\Document $view The default view document
     *
     * @return void
     */
    public function __construct(\dependency\html\Document $view)
    {
        $this->view = $view;
    }

    /**
     * View for no privilege on route
     *
     * @return string
     */
    public function noPrivilege()
    {
        $this->view->addHeaders();
        //$this->view->useLayout();
        $this->view->addContentFile("authorization/view/noPrivilege.html");

        $this->view->translate();

        return $this->view->saveHtml();
    }

    /**
     * View for no privilege on route
     *
     * @return string
     */
    public function noAccess()
    {
        $this->view->addHeaders();
        //$this->view->useLayout();
        $this->view->addContentFile("authorization/view/noPrivilege.html");

        $this->view->translate();

        return $this->view->saveHtml();
    }

    
}