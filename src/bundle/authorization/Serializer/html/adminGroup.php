<?php

namespace bundle\authorization\Serializer\html;
/**
 * Serializer for authorization group administration in Html
 *
 * @package authorization
 */
class adminGroup
{
    public $view;
    public $ignoreRoutes;
    public $ignoreClasses;

    /**
     * Constructor
     * @param \dependency\html\Document $view The default view document
     *
     * @return void
     */
    public function __construct(\dependency\html\Document $view, array $ignoreRoutes=array(), array $ignoreClasses=array())
    {
        $this->view = $view;
        $this->ignoreRoutes = $ignoreRoutes;
        $this->ignoreClasses = $ignoreClasses;
    }

    /**
     * View for group admin index panel
     * @param array $groups The list of groups
     *
     * @return string
     */
    public function index(array $groups)
    {
        $this->view->addHeaders();
        $this->view->useLayout();
        $this->view->addContentFile("authorization/view/index.html");

        $table = $this->view->getElementById("list");
        $dataTable = $table->plugin['dataTable'];
        $dataTable->setPaginationType("full_numbers");
        $dataTable->setUnsortableColumns(1);
        $dataTable->setUnsearchableColumns(1);
        $dataTable->setUnsortableColumns(3);
        $dataTable->setUnsearchableColumns(3);

        $this->view->setSource('groups', $groups);
        $this->view->translate();
        $this->view->merge();

        return $this->view->saveHtml();
    }

    /**
     * View for new group
     * @param authorization/group $group The new group
     *
     * @return string
     */
    public function newGroup($group)
    {
        $group->rules = array();
        $group->privileges = array();
        return $this->edit($group);
    }

    /**
     * View for group edition
     * @param authorization/group $group The group to administrate
     *
     * @return string
     */
    public function edit($group=null)
    {
        $this->view->addHeaders();
        $this->view->useLayout();
        $this->view->addContentFile("authorization/view/edit.html");
        
        $group->superadmin = false;

        foreach ($this->ignoreRoutes as $i => $route) {
            $this->ignoreRoutes[$i] = substr($route, 0, -2);
        }

        $privilegesList = array();
        foreach ($group->privileges as $privilege) {
            $privilegesList[] = $privilege->route;
        }

        $bundles = array();
        foreach (\laabs::bundles() as $bundle) {

            $bundleObject = new \stdClass();
            $bundleObject->name = $bundle->name;
            $bundleObject->value = $bundle->name.'/*';
            $bundleObject->api = array();
            $bundleObject->privilegeStatus = in_array($bundleObject->value, $privilegesList);
            $bundleObject->ruleStatus = false;
            foreach ($group->rules as $rule) {
                        if($bundleObject->value == $rule->className) {
                            $bundleObject->ruleStatus = true;
                            break;
                        }
                    }
            $bundleObject->class = array();

            //interfaces
            foreach ($bundle->getAPIs() as $api) {
                if(!in_array($api->getName(), $this->ignoreRoutes)) {
                    $interface = new \stdClass();
                    $interface->name = $api->interface;
                    $interface->value = $api->getName();
                    $interface->status = in_array($interface->value, $privilegesList);
                    
                    $interface->parentStatus = $bundleObject->privilegeStatus;
                    $bundleObject->api[] = $interface;
                }
            }

            //classes
            foreach ($bundle->getClasses() as $class) {
                if(!in_array($class->getName(), $this->ignoreClasses)) {
                    $model = new \stdClass();
                    $model->value = $class->getName();
                    $model->name = \laabs\basename($model->value);
                    $model->status = false;
                    foreach ($group->rules as $rule) {
                        if ($model->value == $rule->className) {
                            $model->status = true;
                            $model->context = $rule->context;
                        }
                    }
                    $interface->parentStatus = $bundleObject->ruleStatus;
                    $bundleObject->class[] = $model; 
                }
            }

            if (!empty($bundleObject->api)) {

                $bundles[] = $bundleObject;
            }
        }
        $this->view->setSource("bundles", $bundles);

        $privilege = $this->view->getElementById('privileges');
        $this->view->merge($privilege);

        $rules = $this->view->getElementById('rules');
        $this->view->merge($rules);

        //group members
        $membersids = array();
        foreach ($group->groupMembers as $member) {
            array_push($membersids, $member->objectId);
        }
        $group->membersids = \laabs\json_encode($membersids);
        /*
        //privileges tree
        $privileges = array();
        $tree = $this->view->getElementById('privilegesTree');
        $granted = [];
        if ($group != null) {
            $granted = $group->privileges;
        }

        foreach (\laabs::bundles() as $bundle) {
            foreach ($bundle->getAPIs() as $api) {
                $bundle->api[] = $api;
            }
            if (isset($bundle->api)) {
                $this->addPrivilegeTree($bundle, $tree, $granted);   
            }
        }

        //rules tree
        $objects = array();
        $tree = $this->view->getElementById('rulesTree');
        $granted = [];
        if ($group != null && isset($group->rules)) {
            $granted = $group->rules;
        }
        foreach (\laabs::bundles() as $bundle) {
            $this->addRuleTree($bundle, $tree, $granted);   
        }
*/        
        if(isset($group->privileges[0]) && $group->privileges[0]->route == "*") {
            $group->superadmin = true;
        }

        $this->view->translate();

        $this->view->setSource('group', $group);
        $this->view->merge();

        return $this->view->saveHtml();
    }

    /**
     * Build an object tree
     * @param object     $objects   The list of objects of a bundle
     * @param DOMelement $container The element of the html view where the tree must be build
     * @param object     $granted   The list of privileges granted to the edited roleGroup 
     *
     */
    public function addRuleTree($objects, $container, $granted)
    {
        $bundleFragmentTemplate = $this->view->createDocumentFragment();
        $bundleFragmentTemplate->appendHtmlFile("authorization/view/bundleItem.html");
        //$this->view->translate($bundleFragmentTemplate);

        $privilegeObject = new \stdClass();
        $privilegeObject->bundle = $objects->getName();
        $privilegeObject->context = '';
        $privilegeObject->granted = false;

        for ($i=0; $i<sizeof($granted)&& !$privilegeObject->granted; $i++) {
            if ($granted[$i]->className == $privilegeObject->bundle.'/*') {
                $privilegeObject->context = $granted[$i]->context;
                $privilegeObject->granted = true;
            }
        }

        $this->view->merge($bundleFragmentTemplate, $privilegeObject);
        $bundleItem = $container->appendChild($bundleFragmentTemplate);

        $objectList = $bundleItem->getElementsByTagName('ul')->item(0);
        $objectFragmentTemplate = $this->view->createDocumentFragment();
        $objectFragmentTemplate->appendHtmlFile("authorization/view/classItem.html");

        foreach ($objects->getClasses() as $class) {
            $objectFragment = $objectFragmentTemplate->cloneNode(true);

            $privilegeObject = new \stdClass();
            $privilegeObject->object = \laabs\basename($class->getName());
            $privilegeObject->bundle = $class->container;
            $privilegeObject->context = '';
            $privilegeObject->granted = false;

            for ($i=0; $i<sizeof($granted)&& !$privilegeObject->granted; $i++) {
                if ($granted[$i]->className == $privilegeObject->bundle.'/'.$privilegeObject->object) {
                    $privilegeObject->context = $granted[$i]->context;
                    $privilegeObject->granted = true;
                }
            }

            $this->view->merge($objectFragment, $privilegeObject);
            $objectList->appendChild($objectFragment);
        }
    }

    /**
     * Build a privileges tree
     * @param object     $privilege The list of privileges of a bundle
     * @param DOMelement $container The element of the html view where the tree must be build
     * @param object     $granted   The list of privileges granted to the edited roleGroup 
     *
     */
    public function addPrivilegeTree($privilege, $container, $granted)
    {
        $bundleFragmentTemplate = $this->view->createDocumentFragment();
        $bundleFragmentTemplate->appendHtmlFile("authorization/view/privilegeBundleItem.html");
        //$this->view->translate($bundleFragmentTemplate);

        $bundlePrivilege = new \stdClass();
        $bundlePrivilege->bundle = $privilege->getName();
        $bundlePrivilege->granted = false;

        foreach ($granted as $grantedPrivilege) {
            if ($privilege->getName().'/*'== $grantedPrivilege->route) {
                $bundlePrivilege->granted = true;
                break;
            }
        }

        $this->view->merge($bundleFragmentTemplate, $bundlePrivilege);
        $bundleItem = $container->appendChild($bundleFragmentTemplate);

        $apiList = $bundleItem->getElementsByTagName('ul')->item(0);
        $apiFragmentTemplate = $this->view->createDocumentFragment();
        $apiFragmentTemplate->appendHtmlFile("authorization/view/privilegeApiItem.html");

        foreach ($privilege->getAPIs() as $api) {
            $apiFragment = $apiFragmentTemplate->cloneNode(true);

            $apiPrivilege = new \stdClass();
            $apiPrivilege->api = $api->interface;
            $apiPrivilege->bundle = $api->domain;
            $apiPrivilege->granted = false;
            $apiPrivilege->bundleChecked = $bundlePrivilege->granted;

            foreach ($granted as $grantedPrivilege) {
                if ($api->getName()== $grantedPrivilege->route) {
                    $apiPrivilege->granted = true;
                    break;
                }
            }

            $this->view->merge($apiFragment, $apiPrivilege);

            $apiList->appendChild($apiFragment);
        }
    }
}