<?php

namespace bundle\authorization\Observer;
/**
 * Service for authorization check observer
 *
 * @package Authorization
 */
class authorization
{
    protected $auth;
    public $ignoreRoutes;

    public function __construct(array $ignoreRoutes=array(), \dependency\authorization\AuthorizationInterface $auth)
    {
        $this->auth = $auth;
        $this->ignoreRoutes = $ignoreRoutes;
    }

    /**
     * Check user privilege against requested route
     * @param \core\Reflection\Route &$requestRoute The reflection of requested route
     * @param array                  &$args         The arguments
     * 
     * @subject LAABS_REQUEST_ROUTE
     */
    public function checkPrivilege(&$requestRoute, array &$args=null)
    {
        $routename = $requestRoute->getName();

        foreach ($this->ignoreRoutes as $pattern) {
            if (fnmatch($pattern, $routename)) {
                return true;
            }
        }

        if (!$this->auth->hasUserPrivilege($routename)) {
            $authRoute = \laabs::route('READ', 'authorization/noPrivilege');
            $requestRoute->reroute($authRoute);
            for ($i=0, $l=count($args); $i<$l; $i++) {
                unset($args[$i]);
            }

            return false;
        }

    }

}