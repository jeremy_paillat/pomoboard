<?php

namespace bundle\organization\Serializer\Html;

/**
 * Bundle organization html serializer
 *
 * @package Organization
 */
class adminOrgUnitType
{
    public $view;
    public $sdoFactory;

    /**
     * __construct
     *
     * @param \dependency\html\Document $view       A new ready-to-use empty view
     * @param \dependency\sdo\Factory   $sdoFactory The Sdo Factory for data access
     */
    public function __construct(\dependency\html\Document $view, \dependency\sdo\Factory $sdoFactory)
    {
        $this->view = $view;
        $this->sdoFactory = $sdoFactory;
    }

    /**
     * index
     * @param array $orgUnitTypes Array of organization unit type
     * 
     * @return view
     */
    public function index(array $orgUnitTypes)
    {
        $this->view->addHeaders();
        $this->view->useLayout();
        $this->view->addContentFile("organization/view/adminOrgUnitType/orgUnitType.html");
        $this->view->setSource("orgUnitTypes", $orgUnitTypes);
        
        $dataTable = $this->view->getElementsByClass("dataTable")->item(0)->plugin['dataTable'];
        $dataTable->setPaginationType("full_numbers");
        $dataTable->setUnsortableColumns(2);

        $this->view->merge();
        $this->view->translate();

        return $this->view->saveHtml();
    }

    /**
     * edit
     * @param object $orgUnitType Organization unit type object
     * 
     * @return view
     */
    public function editOrgUnitType($orgUnitType)
    {
        $this->view->addHeaders();
        $this->view->useLayout();
        $this->view->addContentFile("organization/view/adminOrgUnitType/edit.html");

        $this->view->translate();
        
        $this->view->setSource("orgUnitType", $orgUnitType);
        $this->view->merge();
        

        return $this->view->saveHtml();
    }

    /**
     * edit
     * @param object $orgUnitType Organization unit type object
     * 
     * @return view
     */
    public function newOrgUnitType($orgUnitType)
    {
        $this->view->addHeaders();
        $this->view->useLayout();
        $this->view->addContentFile("organization/view/adminOrgUnitType/edit.html");
        $this->view->setSource("orgUnitType", $orgUnitType);
        $this->view->merge();
        $this->view->translate();

        return $this->view->saveHtml();
    }
}