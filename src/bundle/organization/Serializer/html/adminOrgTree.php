<?php

namespace bundle\organization\Serializer\Html;

/**
 * Bundle registeredMail html serializer
 *
 * @package Organization
 */
class adminOrgTree
{
    public $view;
    public $sdoFactory;

    /**
     * __construct
     *
     * @param \dependency\html\Document $view       A new ready-to-use empty view
     * @param \dependency\sdo\Factory   $sdoFactory The Sdo Factory for data access
     */
    public function __construct(\dependency\html\Document $view, \dependency\sdo\Factory $sdoFactory)
    {
        $this->view = $view;
        $this->sdoFactory = $sdoFactory;
    }

    /**
     * index
     * @param array $organizations Array of organization
     * 
     * @return view View with the list of organizations
     */
    public function index($organizations)
    {
        $this->view->addHeaders();
        $this->view->useLayout();
        $this->view->addContentFile("organization/view/orgTree.html");

        $orgUnitType = $this->sdoFactory->find("organization/orgUnitType");
        $this->view->setSource("orgUnitType", $orgUnitType);

        $orgRoles = $this->sdoFactory->find("organization/orgRole");
        $this->view->setSource("orgRoles", $orgRoles);
        
        $this->view->translate();
        $this->view->merge();

        if(sizeof($organizations)!=0) {
            $tree = $this->contructTree($organizations);

            if($tree != null) {
                $orgList = $this->view->getElementsByClass('dataTree')->item(0);
                $orgList->appendChild($tree);
            } 
        } 

        return $this->view->saveHtml();
    }

    /**
     * getTree
     * @param array $organizations Tree of organisations
     * 
     * @return view View with the tree of organizations
     */
    public function getTree($organizations)
    {

        if(sizeof($organizations)!=0) {
            $tree = $this->contructTree($organizations);
            $this->view->appendChild($tree);
        }

        return $this->view->saveHtml();
    }

    /**
     * contructTree
     * @param array $filePlan The tree representing the file plan 
     * 
     * @return view View with the tree
     */
    protected function contructTree($organizations)
    {
        $orgList = $this->view->createDocumentFragment();

        $orgFragmentTemplate = $this->view->createDocumentFragment();
        $orgFragmentTemplate->appendHtmlFile("organization/view/organizationItem.html");
        $this->view->translate($orgFragmentTemplate);

        foreach ($organizations as $organization) {
            $orgFragment = $orgFragmentTemplate->cloneNode(true);

            $orgItem = $orgList->appendChild($orgFragment);

            $this->view->merge($orgItem, $organization);

            $orgUnitFragmentTemplate = $this->view->createDocumentFragment();
            $orgUnitFragmentTemplate->appendHtmlFile("organization/view/orgUnitItem.html");
            $this->view->translate($orgUnitFragmentTemplate);

            $orgPersonFragmentTemplate = $this->view->createDocumentFragment();
            $orgPersonFragmentTemplate->appendHtmlFile("organization/view/orgPersonItem.html");
            $this->view->translate($orgPersonFragmentTemplate);

            if(sizeof($organization->orgUnit)) {
                $orgItem = $orgItem->getElementsByTagName('li')->item(0);
                $orgUnitList = $this->view->createElement('ul');
                $orgUnitList = $orgItem->appendChild($orgUnitList);

                $this->mergeOrgUnits($organization, $orgUnitList, $orgUnitFragmentTemplate, $orgPersonFragmentTemplate);
            }
        }

        return $orgList;
    }

    protected function mergeOrgUnits($parent, $container, $orgUnitFragmentTemplate, $orgPersonFragmentTemplate)
    {
        foreach ($parent->orgUnit as $orgUnit) {
            $orgUnit->orgName = $parent->orgName;
            $orgUnitFragment = $orgUnitFragmentTemplate->cloneNode(true);
            
            $this->view->merge($orgUnitFragment, $orgUnit);

            $orgUnitItem = $container->appendChild($orgUnitFragment);

            if(sizeof($orgUnit->orgUnit) || sizeof($orgUnit->orgPerson)) {
                $orgUnitList = $this->view->createElement('ul');
                $orgUnitItem->appendChild($orgUnitList);

                foreach ($orgUnit->orgPerson as $person) {
                    $orgPersonFragment = $orgPersonFragmentTemplate->cloneNode(true);
                    $this->view->merge($orgPersonFragment, $person);
                    $orgPersonItem = $orgUnitList->appendChild($orgPersonFragment);
                }

                $this->mergeOrgUnits($orgUnit, $orgUnitList, $orgUnitFragmentTemplate, $orgPersonFragmentTemplate);
                
            }
        }


    }

}