<?php

namespace bundle\organization\Serializer\json;

/**
 * organization serializer
 *
 * @package Organization
 */
trait adminOrgUnitTrait
{

    /**
     * Serializer JSON for create method
     * @param object $orgUnit The organization unit
     * 
     * @return object JSON object with a status and message parameters
     */
    public function newOrgUnit($orgUnit)
    {
        return \laabs\json_encode($orgUnit);
    }


    /**
     * Serializer JSON for create method
     * 
     * @return object JSON object with a status and message parameters
     */
    public function addOrgUnit()
    {
        $this->json->message = "Organization unit added";
        $this->json->message = $this->translator->getText($this->json->message);

        return $this->json->save();
    }

    /**
     * Serializer JSON for update method
     * 
     * @return object JSON object with a status and message parameters
     */
    public function modifyOrgUnit()
    {
        $this->json->message = "Organization unit updated";
        $this->json->message = $this->translator->getText($this->json->message);

        return $this->json->save();
    }

    /**
     * Serializer JSON for get method
     * @param object $orgUnit The organization unit
     * 
     * @return object JSON object with a status and message parameters
     */
    public function editOrgUnit($orgUnit)
    {
        return \laabs\json_encode($orgUnit);
    }

    /**
     * Serializer JSON for getElementsForMove method
     * @param array $list The list
     * 
     * @return object JSON object of the list
     */
    public function getElementsForMove($list)
    {
        foreach ($list['organization'] as $org) {
            unset($org->picture);
        }
        return \laabs\json_encode($list);
    }

    /**
     * Serializer JSON for move method
     * 
     * @return object JSON object with a status and message parameters
     */
    public function moveOrgunit()
    {
        $this->json->message = "The organization unit is moved";
        $this->json->message = $this->translator->getText($this->json->message);
     
        return $this->json->save();
    }

    /**
     * Serializer JSON for update method
     * 
     * @return object JSON object with a status and message parameters
     */
    public function deleteOrgUnit()
    {
        $this->json->message = "Organization unit and his childrens are deleted";
        $this->json->message = $this->translator->getText($this->json->message);

        return $this->json->save();
    }

    /**
     * Serializer JSON for orgUnitTypeException method
     * 
     * @return object JSON object with a status and message parameters
     */
    public function orgUnitTypeException()
    {
        $this->json->status = false;
        $this->json->message = "Missing display name";
        $this->json->message = $this->translator->getText($this->json->message);

        return $this->json->save();
    }
}
