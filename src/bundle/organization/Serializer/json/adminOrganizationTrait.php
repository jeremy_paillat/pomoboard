<?php

namespace bundle\organization\Serializer\json;

/**
 * organization serializer
 *
 * @package Organization
 */
trait adminOrganizationTrait
{
    /**
     * Serializer JSON for create method
     * @param object $organization The organization
     * 
     * @return object JSON object with a status and message parameters
     */
    public function newOrganization($organization)
    {
        return \laabs\json_encode($organization);
    }

    /**
     * Serializer JSON for create method
     * 
     * @return object JSON object with a status and message parameters
     */
    public function addOrganization()
    {
        $this->json->message = "Organization added";
        $this->json->message = $this->translator->getText($this->json->message);

        return $this->json->save();
    }

    /**
     * Serializer JSON for delete method
     * 
     * @return object JSON object with a status and message parameters
     */
    public function deleteOrganization()
    {
        $this->json->message = "Organization and his childrens are deleted";
        $this->json->message = $this->translator->getText($this->json->message);

        return $this->json->save();
    }

/**
     * Serializer JSON for update method
     * 
     * @return object JSON object with a status and message parameters
     */
    public function modifyOrganization()
    {
        $this->json->message = "Organization updated";
        $this->json->message = $this->translator->getText($this->json->message);

        return $this->json->save();
    }

    /**
     * Serializer JSON for get method
     * @param object $organization The organization
     * 
     * @return object JSON object with a status and message parameters
     */
    public function editOrganization($organization)
    {
        if($organization->picture != null) {
            $content = stream_get_contents($organization->picture);

            ob_start();
            $contents = ob_get_contents();
            ob_end_clean();

            $finfo = finfo_open(FILEINFO_MIME_TYPE);
            $mimeType = finfo_buffer($finfo,$content,FILEINFO_MIME_TYPE);
            $organization->picture = "data:".$mimeType.";base64,".base64_encode($content);
        }

        $this->json->load($organization);

        //var_dump($this->json);

        return $this->json->save();
    }

    /**
     * Serializer JSON for orgTypeException method
     * 
     * @return object JSON object with a status and message parameters
     */
    public function orgTypeException() 
    {
        $this->json->status = false;
        $this->json->message = "Missing display name";
        $this->json->message = $this->translator->getText($this->json->message);

        return $this->json->save();
    }
}
