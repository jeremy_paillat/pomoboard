<?php

namespace bundle\organization\Serializer\json;

/**
 * organization serializer
 *
 * @package Organization
 */
class userPosition
{
    protected $json;

    /**
     * __construct
     * @param \dependency\json\JsonObject $json
     */
    public function __construct(\dependency\json\JsonObject $json)
    {
        $this->json = $json;
        $this->json->status = true;
    }

    /**
     * Serializer JSON for create method
     * 
     * @return object JSON object with a status and message parameters
     */
    public function setCurrentPosition()
    {
        return $this->json->save();
    }
}