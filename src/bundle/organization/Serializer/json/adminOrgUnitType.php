<?php

namespace bundle\organization\Serializer\json;

/**
 * organization serializer
 *
 * @package Organization
 */
class adminOrgUnitType
{
    protected $json;

    /**
     * Constructor of adminOrgUnitType class
     * @param \dependency\json\JsonObject $json
     */
    public function __construct(\dependency\json\JsonObject $json)
    {
        $this->json = $json;
        $this->json->status = true;
    }

    /**
     * Serializer JSON for create method
     * 
     * @return object JSON object with a status and message parameters
     */
    public function addOrgUnitType()
    {
        $this->json->message = "Organization unit type added";
        $this->json->message = $this->translator->getText($this->json->message);

        return $this->json->save();
    }

    /**
     * Serializer JSON for update method
     * 
     * @return object JSON object with a status and message parameters
     */
    public function modifyOrgUnitType()
    {
        $this->json->message = "Organization unit type updated";
        $this->json->message = $this->translator->getText($this->json->message);

        return $this->json->save();
    }
}
