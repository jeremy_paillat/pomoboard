<?php

namespace bundle\organization\Serializer\json;

/**
 * organization serializer
 *
 * @package Organization
 */
trait adminOrgPersonTrait
{
    /**
     * Serializer JSON for get method
     * @param object $person The new person
     * 
     * @return object JSON object with a status and message parameters
     */
    public function newOrgPerson($person)
    {
        $this->json->status=true;
        $this->json->result=$person;
        return $this->json->save();
    }

    /**
     * Serializer JSON for get method
     * @param object $person The new person
     * 
     * @return object JSON object with a status and message parameters
     */
    public function addOrgPerson($person)
    {
        $this->json->status=true;
        $this->json->message = "New person added";
        $this->json->message = $this->translator->getText($this->json->message);
        $this->json->id=$person;
        return $this->json->save();
    }

    /**
     * Serializer JSON for get method
     * @param object $person The person
     * 
     * @return object JSON object with a status and message parameters
     */
    public function editOrgPerson($person)
    {   
        if($person->picture != NULL) {
            $content = stream_get_contents($person->picture);

            ob_start();
            imagepng($person->picture);
            $contents = ob_get_contents();
            ob_end_clean();

            $finfo = finfo_open(FILEINFO_MIME_TYPE);
            $mimeType = finfo_buffer($finfo,$content,FILEINFO_MIME_TYPE);
            $person->picture = "data:".$mimeType.";base64,".base64_encode($content);
        }

        $this->json->status=true;
        $this->json->result=$person;

        return $this->json->save();
    }

    /**
     * Serializer JSON for update method
     * 
     * @return object JSON object with a status and message parameters
     */
    public function modifyOrgPerson()
    {
        $this->json->message = "Person updated";
        $this->json->message = $this->translator->getText($this->json->message);

        return $this->json->save();
    }

    /**
     * Serializer JSON for get method
     * @param array $persons The list of persons
     * 
     * @return object JSON object with a status and message parameters
     */
    public function getOrgUnitPersons($persons)
    {
        foreach ($persons as $person) {
            unset($person->picture);
        }
        //var_dump($persons);
        return \laabs\json_encode($persons);
    }

    /**
     * orgPersonParty typeahead
     * @param array $persons An array of orgPersonParty matching the user query
     *
     * @return string
     **/
    public function queryPersons($persons)
    {
        /*foreach ($persons as $person) {
            unset($person->picture);
        }

        unset($this->json->status);*/
        $this->json->load($persons);

        return $this->json->save();
    }

    /**
     * Serializer JSON for adding person position method
     * @param string $positionId The position identifier
     * 
     * @return object JSON object with a status and message parameters
     */
    public function addPersonPosition($positionId)
    {
        $this->json->message = "Person added in the organization unit";
        $this->json->message = $this->translator->getText($this->json->message);
        $this->json->positionId = $positionId;
        return $this->json->save();
    }

    /**
     * Serializer JSON for adding person position method
     * 
     * @return object JSON object with a status and message parameters
     */
    public function deletePersonPosition()
    {
        $this->json->message = "Person removed";
        $this->json->message = $this->translator->getText($this->json->message);

        return $this->json->save();
    }
    
    /**
     * Serializer JSON for orgTypeException method
     * 
     * @return object JSON object with a status and message parameters
     */
    public function orgPersonTypeException() 
    {
        $this->json->status = false;
        $this->json->message = "Missing display name";
        $this->json->message = $this->translator->getText($this->json->message);

        return $this->json->save();
    }
    
}
