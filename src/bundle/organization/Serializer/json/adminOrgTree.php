<?php

namespace bundle\organization\Serializer\json;

/**
 * organization serializer
 *
 * @package Organization
 */
class adminOrgTree
{
    use adminOrganizationTrait;
    use adminOrgUnitTrait;
    use adminOrgPersonTrait;
    
    protected $json;

    protected $translator;

    /**
     * Constructor of batch class
     * @param \dependency\json\JsonObject                  $json
     * @param \dependency\localisation\TranslatorInterface $translator
     */
    public function __construct(\dependency\json\JsonObject $json, \dependency\localisation\TranslatorInterface $translator)
    {
        $this->json = $json;
        $this->json->status = true;

        $this->translator = $translator;
        $this->translator->setCatalog('organization/messages');

    }
}


