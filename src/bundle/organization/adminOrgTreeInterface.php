<?php

namespace bundle\organization;
/**
 * Interface for organiaztion administration
 */
interface adminOrgTreeInterface
{
    /**
     * Get the organizations' index
     *
     * @return array The list of orgs with their position
     * 
     * @request READ organization/adminOrgTree/index
     */
    public function index();

    /**
     * Get the organizations' index
     *
     * @return array The list of file plans with their position
     * 
     * @request READ organization/adminOrgTree/getTree
     * @action organization/adminOrgTree/index
     * @output organization/adminOrgTree/getTree
     */
    public function getTree();

    /**
     * Create a new organiazation object
     *
     * @return bundle\organization\Model\organization The organization object
     * 
     * @request READ organization/adminOrgTree/newOrganization
     */
    public function newOrganization();

    /**
     * Add an organiazation
     * @param object $organization The organization object to add
     *
     * @return string the organization's Id 
     * 
     * @request CREATE organization/organization
     */
    public function addOrganization($organization);

    /**
     * Edit an organiazation
     * @param string $orgId The Identifier of the organization to edit
     *
     * @return object the organization 
     * 
     * @request READ organization/adminOrgTree/editOrganization/([^/]+)
     */
    public function editOrganization($orgId);

    /**
     * Update an organization
     * @param object $organization The organization object to update
     *
     * @return boolean
     * 
     * @request UPDATE organization/adminOrgTree/modifyOrganization
     */
    public function modifyOrganization($organization);

    /**
     * Delete an organization
     * @param string $orgId The organization id
     *
     * @return boolean
     * 
     * @request DELETE organization/adminOrgTree/deleteOrganization/([^/]+)
     */
    public function deleteOrganization($orgId);

    /**
     * Create a new organiazation unit object
     *
     * @return bundle\organization\Model\orgUnit The orgUnit object
     * 
     * @request READ organization/adminOrgTree/newOrgUnit
     */
    public function newOrgUnit();

    /**
     * Add an organiazation unit
     * @param object $orgUnit The organization unit object to add
     *
     * @return string the organization unit's Id 
     * 
     * @request CREATE organization/adminOrgTree/addOrgUnit
     */
    public function addOrgUnit($orgUnit);

    /**
     * Edit an organiazation unit
     * @param string $orgUnitId The Id of the organization unit to edit
     *
     * @return object the organization unit
     * 
     * @request READ organization/adminOrgTree/editOrgUnit/([^/]+)
     */
    public function editOrgUnit($orgUnitId);

    /**
     * Update an organization unit
     * @param object $orgUnit The organization unit object to update
     *
     * @return boolean
     * 
     * @request UPDATE organization/adminOrgTree/modifyOrgUnit
     */
    public function modifyOrgUnit($orgUnit);

    /**
     * Get the list of element where a given orgUnit can be moved
     * @param string $orgUnitId The identifier of the orgUnit to move
     *
     * @return array
     * 
     * @request READ organization/adminOrgTree/getElementsForMove/([^/]+)
     */
    public function getElementsForMove($orgUnitId);


    /**
     * Move an orgUnit either to a new ownerOrg or to a new parentOrgUnit that can be held by another org
     * @param string $orgUnitId          The org unit id
     * @param string $newParentOrgUnitId The new parent organization unit id 
     * @param string $newOwnerOrgId      The new owner organization id 
     *
     * @return boolean
     * 
     * @request UPDATE organization/adminOrgTree/moveOrgUnit
     */
    public function moveOrgUnit($orgUnitId, $newParentOrgUnitId, $newOwnerOrgId);

    /**
     * Delete an orgUnit
     * @param string $orgUnitId The orgUnit id
     *
     * @return boolean 
     *
     * @request DELETE organization/adminOrgTree/deleteOrgUnit/([^/]+)
     */
    public function deleteOrgUnit($orgUnitId);

    /**
     * Get the list of persons of a given organization unit
     * @param string $orgUnitId The identifier organization unit
     *
     * @return array The list of person
     *
     * @request READ organization/adminOrgTree/getOrgUnitPersons/([^/]+)
     */
    public function getOrgUnitPersons($orgUnitId);

    /**
     * Create a new orgPerson object
     *
     * @return bundle\organization\Model\orgPerson The orgPerson object
     *
     * @request READ organization/adminOrgTree/newOrgPerson
     */
    public function newOrgPerson();

    /**
     * Add a person
     * @param object $orgPerson The person object to add
     *
     * @return string the person's Id 
     *
     * @request CREATE organization/adminOrgTree/addOrgPerson
     */
    public function addOrgPerson($orgPerson);

    /**
     * Edit a person
     * @param string $orgPersonId The identifier of the orgPerson
     *
     * @return object the organization unit
     *
     * @request READ organization/adminOrgTree/editOrgPerson/([^/]+)
     */
    public function editOrgPerson($orgPersonId);

    /**
     * Update a person
     * @param object $orgPerson The person object to update
     *
     * @return boolean
     *
     * @request UPDATE organization/adminOrgTree/modifyOrgPerson
     */
    public function modifyOrgPerson($orgPerson);

    /**
     * Delete an orgPerson
     * @param string $orgPersonId The identifier of the orgPerson
     *
     * @return boolean 
     *
     * @request DELETE organization/adminOrgTree/deleteOrgPerson/([^/]+)
     */
    public function deleteOrgPerson($orgPersonId);

    /**
     * Add a position to a person
     * @param object $personPosition The position of the person
     *
     * @return string The new identifier
     *
     * @request CREATE organization/adminOrgTree/addPersonPosition
     */
    public function addPersonPosition($personPosition);

    /**
     * Remove a person's position
     * @param string $personPositionId The position of the person
     *
     * @return boolean
     *
     * @request DELETE organization/adminOrgTree/deletePersonPosition/([^/]+)
     */
    public function deletePersonPosition($personPositionId);

    /**
     * Get the list of available persons
     * @param string $query A query string of tokens
     * 
     * @return array
     * 
     * @request READ organization/adminOrgTree/queryPersons/([^/]+)
     */
    public function queryPersons($query);

}