<meta charset="UTF-8">
<!-- MarkdownTOC -->

- [Spécifications techniques](#spécifications-techniques)
    - [Description des APIs](#description-des-apis)
        - [Contrôleur resource](#contrôleur-resource)
        - [Contrôleur store](#contrôleur-store)
        - [Contrôleur retrieve](#contrôleur-retrieve)
        - [Contrôleur modify](#contrôleur-modify)
        - [Contrôleur delete](#contrôleur-delete)
        - [Contrôleur copy](#contrôleur-copy)
        - [Contrôleur migrate](#contrôleur-migrate)
    - [Configuration](#configuration)
        - [Directives de configuration](#directives-de-configuration)
        - [Définition](#définition)

<!-- /MarkdownTOC -->
# Spécifications techniques

## Description des APIs

![Involved Model](img/involvedModel.png)  

### Contrôleur adminOrgTree
C’est le contrôleur métier qui permet la gestion de l'aborescance des organisations.

#### Action index
Fournit l'arborescance des organisations. 

##### Description
    array index ()

##### Valeur de retour
La méthode retourne l'arborescance sous la forme d'une lise d'objets. Cette liste peut contenir des objets de type organization, orgUnit et person.

#### Action newOrganization
Fournit une nouvelle organisation.

##### Description
    organization/organization newOrganization ()

##### Valeur de retour
La méthode retourne un nouvel objet organization.

#### Action addOrganization
Enregistre une nouvelle organisation.

##### Description
    string addOrganization ( organization/organization $organization )
    
##### Paramètres 
* **organization** : organization/organization. L'organisation à enregistrer.

##### Valeur de retour
La méthode retourne l'identifiant universel de la nouvelle oranisation. 

#### Action editOrganization
Lit une organisation déjà enregitrée.

##### Description
    organization/organization editOrganization ( string $orgId )
    
##### Paramètres 
* **orgId** : Optionnel. L'identifiant universelle de l'organisation à lire. Si ce paramètre n'est pas précisé, c'est l'identifiant précisé par la route d'action qui sera utilisé.

##### Valeur de retour
La méthode retourne l'oranisation correspondant à l'identifiant. 

#### Action modifyOrganization
Modifie une organisation préalablement enregistré.

##### Description
    boolean editOrganization ( organization/organization $organization )
    
##### Paramètres 
* **organization** : organization/organization. L'organisation modifée.

##### Valeur de retour
La méthode retourne la résultat de la modification. 

#### Action deleteOrganization
Supprimme une organisation ainsi que les unités organisation (UO) rattachées.

##### Description
    boolean editOrganization ( string $orgId )
    
##### Paramètres 
* **orgId** : Optionnel. L'identifiant universelle de l'organisation à supprimer. Si ce paramètre n'est pas précisé, c'est l'identifiant précisé par la route d'action qui sera utilisé.

##### Valeur de retour
La méthode retourne la résultat de la supression. 

#### Action newOrgUnit
Fournit une nouvelle UO.

##### Description
    organization/orgUnit newOrgUnit ()

##### Valeur de retour
La méthode retourne un nouvel objet orgUnit.

#### Action addOrgUnit
Enregistre une nouvelle UO.

##### Description
    string addOrgUnit ( organization/orgUnit $orgUnit )
    
##### Paramètres 
* **orgUnit** : organization/orgUnit. L'UO à enregistrer.

##### Valeur de retour
La méthode retourne l'identifiant universel de la nouvelle UO. 

#### Action editOrgUnit
Lit une UO déjà enregitrée.

##### Description
    organization/orgUnit editOrgUnit ( string $orgUnitId )
    
##### Paramètres 
* **orgUnitId** : Optionnel. L'identifiant universelle de l'UO à lire. Si ce paramètre n'est pas précisé, c'est l'identifiant précisé par la route d'action qui sera utilisé.

##### Valeur de retour
La méthode retourne l'UO correspondant à l'identifiant. 

#### Action modifyOrgUnit
Modifie une UO préalablement enregistré.

##### Description
    boolean modifyOrgUnit ( organization/orgUnit $orgUnit )
    
##### Paramètres 
* **orgUnit** : organization/orgUnit. L'UO modifée.

##### Valeur de retour
La méthode retourne la résultat de la modification.

#### Action getElementsForMove
Fournit les organisations et les UO auxquelles il est possible de rattacher une UO.
##### Description
    array getElementsForMove ( string $orgUnitId )
    
##### Paramètres 
* **orgUnitId** : Optionnel. L'identifiant universelle de l'UO à déplacer. Si ce paramètre n'est pas précisé, c'est l'identifiant précisé par la route d'action qui sera utilisé.

##### Valeur de retour
La méthode retourne une liste de deux listes distinctes: la première liste comportant les organisations et la seconde qui contient les UO. 

#### Action moveOrgUnit
Déplace une UO avec les UO qui y sont rattachées.
##### Description
    boolean moveOrgUnit ( string $orgUnitId, string $newParentOrgUnitId, string newOwnerOrgId )
    
##### Paramètres 
* **orgUnitId** : id. L'identifiant universelle de l'UO à déplacer.
* **newParentOrgUnitId** : id. L'identifiant universelle de la nouvelle UO parent.
* **newOwnerOrgId** : id. L'identifiant universelle de l'organisation parent.

##### Valeur de retour
La méthode retourne la résultat du déplacement.

#### Action deleteOrgUnit
Supprimme une UO, les UO rattachées et les informations concernant les positions des personnes dans cette UO.

##### Description
    boolean editOrgUnit ( string $orgUnitId )
    
##### Paramètres 
* **orgUnitId** : Optionnel. L'identifiant universelle de l'UO à supprimer. Si ce paramètre n'est pas précisé, c'est l'identifiant précisé par la route d'action qui sera utilisé.

##### Valeur de retour
La méthode retourne la résultat de la supression. 

#### Action newOrgPerson
Fournit une nouvelle personne.

##### Description
    organization/orgUnit newOrgPerson ()

##### Valeur de retour
La méthode retourne un nouvel objet orgPerson.

#### Action addOrgPerson
Enregistre une nouvelle personne.

##### Description
    string addOrgUnit ( organization/orgPerson $orgPerson )
    
##### Paramètres 
* **orgPerson** : organization/orgPerson. La personne à enregistrer.

##### Valeur de retour
La méthode retourne l'identifiant universel de la nouvelle personne. 

#### Action editOrgPerson
Lit une persone déjà enregitrée.

##### Description
    organization/orgPerson editOrgUnit ( string $orgPersonId )
    
##### Paramètres 
* **orgPersonId** : Optionnel. L'identifiant universelle de la personne à lire. Si ce paramètre n'est pas précisé, c'est l'identifiant précisé par la route d'action qui sera utilisé.

##### Valeur de retour
La méthode retourne l'UO correspondant à l'identifiant. 

#### Action modifyOrgPerson
Modifie une personne préalablement enregistré.

##### Description
    boolean modifyOrgPerson ( organization/orgPerson $orgPerson )
    
##### Paramètres 
* **orgPerson** : organization/orgPerson. La personne modifée.

##### Valeur de retour
La méthode retourne la résultat de la modification.

#### Action deleteOrgPerson
Supprimme une personne.

##### Description
    boolean deleteOrgPerson ( string $orgPersonId )
    
##### Paramètres 
* **orgPersonId** : Optionnel. L'identifiant universelle de la personne à supprimer. Si ce paramètre n'est pas précisé, c'est l'identifiant précisé par la route d'action qui sera utilisé.

##### Valeur de retour
La méthode retourne la résultat de la supression. 

#### Action addPersonPosition
Ajoute une personne dans une UO.

##### Description
    string addPersonPosition ( organization/personPosition $personPosition )
    
##### Paramètres 
* **personPosition** : organization/personPostion. L'objet personPosition à ajouter.

##### Valeur de retour
La méthode retourne l'identifiant universelle de la nouvelle position. 

#### Action deletePersonPosition
Ajoute une personne dans une UO.

##### Description
    boolean deletePersonPosition ( organization/personPosition $personPosition )
    
##### Paramètres 
* **personPosition** : organization/personPostion. L'objet personPosition à supprimer.

##### Valeur de retour
La méthode retourne la résultat de la supression. 

#### Action queryPersons
Recherche une personne.

##### Description
    array queryPersons ( string $query )
    
##### Paramètres 
* **query** : string. Le nom ou une partie du nom de la personne à rechercher.

##### Valeur de retour
La méthode retourne la liste des personnes correspondant à la recherche. 





 
## Configuration
### Directives de configuration
#### Configuration du paquet
Le paquet fournit un jeu de paramètres de configuration qui permettent de modifier le comportement de certaines fonctions lors de l’exécution.

Les directives correspondantes sont inscrites dans un fichier de configuration de l’application qui utilise le package sous la forme de paires clés/valeur au format Laabs Ini.

#### Configuration des dépendances
##### Dépendance Service Data Objects
##### Dépendance Repository 

### Définition
#### Routes

#### Jobs


