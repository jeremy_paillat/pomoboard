<meta charset="UTF-8">
<!-- MarkdownTOC -->

- [Spécifications Générales](#SpecificationGenerales)
    - [Introduction](#introduction)
        - [Domaine d'application](#DomaineDapplication)
    - [Contextualisation et modélisation](#ContextualisationEtModelisation)
        - [Les acteurs](#les-acteurs)
        - [Les fonctions](#les-fonctions)
        - [Les entités](#LesEntites)
        - [Cas d'utilisation](#CasDUtilisation)

<!-- /MarkdownTOC -->
<span id="SpecificationGenerales">
# Spécifications Générales

## Introduction
<span id="DomaineDapplication">
### Domaine d'application
Le présent document décrit les spécifications du paquet Organization développé par Maarch pour l’implémentation des fonctionnalités liées à la gestion des organisations dans les applications de gestion électronique de documents ou toute autre application qui requiert des fonctionnalités de gestion hiérarchique d’organisation.
Le but de cette documentation est de fournir toutes les informations nécessaires à la compréhension du fonctionnement du paquet Maarch Organization afin
* de valider l'adéquation de la solution aux besoins de l‘organisation
* de démontrer comment elle peut être utilisée dans le développement d'une application qui utilise des ressources numériques

Le document est destiné :
*   aux directions informatiques des organisations souhaitant mettre en œuvre un tel système 
*   aux développeurs qui utiliseront le paquet dans leurs applications


<span id="ContextualisationEtModelisation">
## Contextualisation et modélisation

### Les acteurs
Un acteur représente un ensemble cohérent de rôles vis-à-vis du système d’information. L'acteur ne doit pas être confondu avec l'utilisateur qui est une personne physique ou morale identifiée dans le dispositif. L’utilisateur peut en effet assurer différents rôles suivant la transaction utilisée, et ainsi être représenté par plusieurs acteurs. 

####Administrateur
Il détient les droits de gestions des organisations. Il peut créer ou modifier les structures hiérarchiques représentant les organisations.

####Utilisateur
C’est l’ensemble des personnes morales ou physiques qui ont utiliseront les informations liées aux organisations.


## Les fonctions
###Gérer les organisations
Cela regroupe l’ensemble des actions, par un administrateur, lié à la mise en place et à la modification d’une hiérarchie d’organisation.  
###Choisir son service de travail
C’est l’action par un utilisateur de choisir un service de travail auquel il est rattaché au sein d’une organisation.

<span id="LesEntites">
## Les entités
Les entités sont les représentations dans le système des informations manipulées, structurées sous forme d’objets.
###Les organisations
Ce sont les entités qui représentent les organisations. Elles contiennent des données descriptives.
###Les unités organisationnelles
Ce sont les entités qui structurent une organisation en la divisant en plusieurs groupes et sous-groupes. Une unité organisationnelle peut elle-même être divisée en plusieurs unités. Chacune de ces entités contient des données descriptives de l’unité organisationnelle qu’elle représente. 
###Les personnes
Ce sont les personnes physiques ou morales qui font partie d’une ou de plusieurs unités organisationnelles. 


<span id="CasDUtilisation">
## Cas d’utilisation

![Use cases](img/useCases.jpg)

###Gérer les organisations
Ces procédures permettent à l’administrateur de gérer les organisations de l’application.

 ![Use case - Stocker](img/useCase_organization.jpg)

####Ajouter une organisation
#####Flux standard
L’administrateur transmet les données représentant une organisation. Ces données se composeront au minimum, du nom de l’organisation et contiendra éventuellement d’autres informations descriptives dont un rôle et un type d’organisation qui seront choisis parmi un répertoire de données.
Le système valide ces informations et enregistre la nouvelle organisation en lui octroyant identifiant unique.
#####Flux d’exception
######Données erronées
Les données transmises sont erronées.
######Rôle non identifié
Le rôle n’est pas répertorié.
######Type  non identifié
Le type n’est pas répertorié.

####Modifier une organisation
#####Flux standard
L’administrateur transmet l’identifiant de l’organisation à modifier ainsi que les nouvelles données à enregistrer pour cette organisation.
Le système valide ces informations et modifie l’organisation.
#####Flux d’exception
######Données erronées
Les données sont erronées.
######Identifiant introuvable
L’identifiant transmis ne permet pas de retrouver une organisation. 
######Rôle non identifié
Le rôle transmis n’est pas répertorié.
#####Type  non identifié
Le type transmis n’est pas répertorié.

####Supprimer une organisation
#####Flux standard
L’administrateur transmet l’identifiant de l’organisation à supprimer. 
Le système retrouve tous les services rattachés à l’organisation et les supprime. Il supprime ensuite l’organisation en question.
#####Flux d’exception
######Identifiant introuvable
L’identifiant ne permet pas de retrouver une organisation.

###Gérer les unités organisationnelles
Ces procédures permettent à l’administrateur de gérer les unités organisationnelles de l’application.

 ![Use case - Stocker](img/useCase_orgUnit.jpg)

####Ajouter une unité organisationnelle
#####Flux standard
L’administrateur transmet les données représentant une unité organisationnelle. Ces données se composent au moins d’un nom et un de l’identifiant de l’organisation à laquelle l’unité est rattachée.

Dans le cas où l’unité organisationnelle est rattachée à une autre unité, l’administrateur doit également préciser l’identifiant de cette dernière.

Parmi les données descriptives de l’unité organisationnelle, l’administrateur peut transmettre un type d’unité qui sera sélectionné dans un référencie de données.

Le système valide ces informations et enregistre la nouvelle unité organisationnelle en lui octroyant identifiant unique.

#####Flux d’exception
######Données erronées
Les données sont erronées.
######Organisation non identifiée
L’identifiant ne permet pas de retrouver l’organisation.
######Unité organisationnelle parent inconnu
L’identifiant du service parent ne correspond à aucune unité organisationnelle.
######Unité organisationnelle parent n’appartenant pas à l’organisation
L’unité organisationnelle parent n’est pas positionné dans l’organisation.
######Type  non identifié
Le type n’est pas répertorié.

####Modifier une unité organisationnelle
#####Flux standard
L’administrateur transmet l’identifiant d’une unité organisationnelle à modifier ainsi que les nouvelles données.

Le système valide ces informations et met à jour l’organisation.
#####Flux d’exception
######Données erronées
Les données sont erronées. Le service ne sera pas modifié.
######Organisation non identifiée
L’identifiant ne permet pas de retrouver l’organisation.
######Unité organisationnelle parent inconnu
L’identifiant du service parent ne correspond à aucune unité organisationnelle.
######Unité organisationnelle parent n’appartenant pas à l’organisation
L’unité organisationnelle parent n’est pas positionné dans l’organisation.
######Type  non identifié
Le type n’est pas répertorié.

####Supprimer une unité organisationnelle
#####Flux standard
L’administrateur transmet l’identifiant associé à l’unité organisationnelle à supprimer.

Le système retrouve l’unité et la supprime ainsi que toutes les unités organisationnelles qui y sont rattachées. 
#####Flux d’exception
######Unité organisationnelle inconnu
L’identifiant unique ne permet pas de retrouver une unité organisationnelle.

####Déplacer une unité organisationnelle
#####Flux standard
L’administrateur transmet l’identifiant de l’unité organisationnelle à déplacer et l’identifiant de l’organisation sous laquelle le service doit être déplacé et, éventuellement, l’identifiant d’une autre unité organisationnelle de cette organisation afin d’y être rattachée.

Le système change la position du service à déplacer. Toutes les entités positionnées sous cette unité sont déplacée en même temps.
#####Flux d’exception
######Unité organisationnelle inconnu
L’identifiant ne permet pas de retrouver une unité organisationnelle.
######Organisation non identifiée
L’identifiant de l’organisation ne permet pas de retrouver l’organisation.
######Unité organisationnelle parent non identifiée
L’identifiant de l’unité organisationnelle parent ne permet pas de le retrouver.


###Gérer les personnes
Ces procédures permettent à l’administrateur de gérer les personnes des organisations.

 ![Use case - Stocker](img/useCase_person.jpg)


####Ajouter une personne
#####Flux standard
L’administrateur transmet les données représentant une personne. Ces données contiennent obligatoirement le nom de la personne et éventuellement d’autres données descriptives. 
Le système valide ces informations et enregistre la nouvelle personne en lui octroyant identifiant unique.
#####Flux d’exception
Données erronées
Les données sont erronées.

####Modifier une personne
#####Flux standard
L’administrateur transmet l’identifiant de la personne à modifier ainsi que les nouvelles données.

 Le système valide ces informations et modifie la personne.
#####Flux d’exception
######Données erronées
Les données sont erronées.
######Personne  non identifiée
L’identifiant de la personne ne permet pas de la retrouver.

####Assigner une personne à un service (ou unité organisationnelle)
#####Flux standard
L’administrateur fournit l’identifiant d’une personne et celui d’un service. Il fournit éventuellement la fonction de la personne dans ce service.

Le système valide ces informations et enregistre la relation en lui octroyant identifiant unique.
#####Flux d’exception
######Personne  non identifiée
L’identifiant de la personne ne permet pas de la retrouver.
#######Service parent non identifié
L’identifiant du service parent ne permet pas de le retrouver.

####Retirer une personne d’un service
#####Flux standard
L’administrateur fournit l’identifiant d’une relation entre une personne et un service.

Le système supprime cette relation.
#####Flux d’exception
######Personne  non identifiée
L’identifiant de la personne ne permet pas de la retrouver. La relation ne sera pas supprimée.
######Service parent non identifié
L’identifiant du service parent ne permet pas de le retrouver. La relation ne sera pas supprimée.


###Gérer les types d’organisation
Ces procédures permettent d’entretenir le référentiel des types d’organisation.

 ![Use case - Stocker](img/useCase_orgType.jpg)

####Créer un type d’organisation
#####Flux standard
L’administrateur fournit un code et un libellé du nouveau type d’organisation.

Le système crée le type d’organisation.
#####Flux d’exception
######Données erronées
Les données sont erronées.
######Le code est déjà utilisé
Le code fourni par l’administrateur est déjà utilisé par une autre donnée du référentiel.

####Modifier un type d’organisation
#####Flux standard
L’administrateur fournit le code et le nouveau libellé du type d’organisation à modifier.

Le système modifie le type d’organisation.
#####Flux d’exception
######Données erronées
Les données sont erronées.
######Code non identifié
Le code ne permet pas de retrouver le type d’organisation.

###Gérer les rôles des organisations
Ces procédures permettent d’entretenir le référentiel des rôles des organisations.

 ![Use case - Stocker](img/useCase_orgRole.jpg)

####Ajouter un rôle
#####Flux standard
L’administrateur fournit un code et un libellé du nouveau rôle.

Le système crée le rôle.
#####Flux d’exception
######Données erronées
Les données sont erronées.
######Le code est déjà utilisé
Le code fourni par l’administrateur est déjà utilisé par une autre donnée du référentiel.

####Modifier un rôle
#####Flux standard
L’administrateur fournit le code et le nouveau libellé du rôle à modifier.

Le système modifie le rôle.
#####Flux d’exception
######Données erronées
Les données sont erronées.
######Code non identifié
Le code ne permet pas de retrouver le rôle.

###Gérer les types d’unité organisationnelle
Ces procédures permettent d’entretenir le référentiel des types d’unité organisationnelle.

 ![Use case - Stocker](img/useCase_orgUnitType.jpg)

####Créer un type d’unité organisationnelle
#####Flux standard
L’administrateur fournit un code et un libellé du nouveau type d’organisation.

Le système crée le type d’organisation.
#####Flux d’exception
######Données erronées
Les données sont erronées.
######Le code est déjà utilisé
Le code fourni par l’administrateur est déjà utilisé par une autre donnée du référentiel.

####Modifier un type d’organisation
#####Flux standard
L’administrateur fournit le code et le nouveau libellé du type d’organisation à modifier.

Le système modifie le type d’organisation.
#####Flux d’exception
######Données erronées
Les données sont erronées.
######Code non identifié
Le code ne permet pas de retrouver le type d’organisation.
