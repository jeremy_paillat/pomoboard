<meta charset="UTF-8">
<!-- MarkdownTOC -->

- [Spécifications fonctionnelles](#specificationFonctionnelles)
    - [Modèle statique](#modeleStatique)
        - [Classe organization](#classeOrganization)
        - [Classe orgUnit](#classeOrgUnit)
        - [Classe orgPerson](#classeOrgPerson)
        - [Classe abstractPerson](#classeAbstractPerson)
        - [Classe personPosition](#classePersonPosition)
        - [Classe orgType](#classeOrgType)
        - [Classe orgRole](#classeOrgRole)
        - [Classe orgUnitType](#classeOrgUnitType)
    - [Messages](#messages)
        - [Création et modification d'une organisation](#creationEtModificationDuneOrganisation)
        - [Création et modification d'une unité organisationnelle](#creationEtModificationDuneUniteOrganisationnelle)
        - [Création et modification d'une personne](#creationEtModificationDunePersonne)
        - [Création et modification d'une position](#creationDunePosition)
        - [Création et modification d'un type d'organisation](#creationEtModificationDunTypeDorganisation)
        - [Création et modification d'un role d'organisation](#creationEtModificationDunRoleDorganisation)
        - [Création et modification d'un type d'unité organisationnelle](#creationEtModificationDunTypeDUO)
    - [Interface Homme-Machine](#interfaceHommeMachine)
    - [Classes participantes](#classesParticipantes)

<!-- /MarkdownTOC -->

<span id="specificationFonctionnelles" >
# Spécifications fonctionnelles 
Ce chapitre fournit la description de l’implémentation fournie par Maarch Bundle Digital Resource.

<span id="modeleStatique" >
## Modèle statique
Le diagramme ci-dessous représente les classes statiques qui entrent en jeu dans les différentes activités liées à la gestion des ressources numériques.

![Static Model](img/staticModel.jpg)

<span id="classeOrganization" >
### Classe organization
Cette classe représente une organisation.

|Propriété                |Description                                        |Type    |Requis |
|-------------------------|---------------------------------------------------|--------|:-----:|
|**orgId               ** |Clé primaire. Identifiant unique de l'organisation |id      |O      |
|**orgName             ** |Nom de l'organisation                              |string  |O      |
|**otherOrgName        ** |Autre nom de l'organisation                        |string  |N      |
|**displayName         ** |Nom tel qu'il sera affiché dans l'application      |string  |O      |
|**orgTypeCode         ** |Code du type d'organisation                        |id      |N      |
|**orgRoleCode         ** |Code du rôle de l'organisation                     |id      |N      |
|**legalClassification ** |Forme juridique de l'organisation                  |string  |N      |
|**taxIdentifier       ** |Numéro de TVA intraco                              |string  |N      |
|**registrationNumber  ** |SIREN / SIRET / Numéro de collectivité             |string  |N      |

<span id="classeOrgUnit" >
### Classe orgUnit
Cette classe représente une unité organisationnelle (UO) qui sont les divisions d'une organisation. Les UO serviront à structurer les organisations.

|Propriété           |Description                                    |Type    |Requis |
|--------------------|-----------------------------------------------|--------|:-----:|
|**orgUnitId       **|Clé primaire. Identifiant unique de l'UO       |id      |O      |
|**orgUnitName     **|Nom de l'UO                                    |string  |O      |
|**displayName     **|Nom tel qu'il sera afficher dans l'application |string  |O      |
|**orgUnitTypeCode **|Code du type de l'UO                           |string  |N      |
|**ownerOrgId      **|Identifiant de l'organisation mère             |id      |O      |
|**parentOrgUnitId **|Identifiant de l'UO mère                       |id      |N      |


<span id="classeOrgPerson" >
### Classe orgPerson
Cette classe représente les personnes appartenant à la hierarchie des organisations.

|Propriété       |Description                                      |Type    |Requis |
|----------------|-------------------------------------------------|--------|:-----:|
|**orgPersonId **|Clé primaire. Identifiant unique de la personne  |id      |O      |

<span id="classeAbstractPerson" >
### Classe abstractPerson
Cette classe contient les informations nécésaire à la représentation d'un personne. Toutes les personnes de l'application doivent avoir cette même structure de base afin de pouvoir être identifié en tant que personne. 

|Propriété        |Description                                      |Type    |Requis |
|-----------------|-------------------------------------------------|--------|:-----:|
|**firstName    **|Prénom de la personne                            |string  |N      |
|**lastName     **|Nom de la personne                               |string  |N      |
|**displayName  **|Nom tel qu'il sera afficher dans l'application   |string  |O      |
|**birthName    **|Nom de jeune fille                               |string  |N      |
|**dateOfBirth  **|Date de naissance                                |string  |N      |
|**gender       **|Sexe de la personne (Male ou Female)             |string  |N      |


<span id="classePersonPosition" >
### Classe personPosition
Le lien entre une UO et les personnes qui y appartiennent est fait grâce à la classe personPosition. Une information sur la fonction qu'exerce la personne dans l'UO peut y être renseigné.  

|Propriété              |Description                                    |Type       |Requis |
|-----------------------|-----------------------------------------------|-----------|:-----:|
|**personnePositionId **|Clé primaire. Identifiant de la relation       |id         |O      |
|**personId           **|Identifiant de la personne impliqué            |id         |O      |
|**personClass        **|Classe de la personne                          |string     |O      |
|**orgUnitId          **|Identifiant de l'UO                            |id         |O      |
|**function           **|Fonction de la personne de l'UO                |id         |N      |

<span id="classeOrgType" >
### Classe orgType
Repertoire des types des organisations.

|Propriété        |Description                                |Type       |Requis |
|-----------------|-------------------------------------------|-----------|:-----:|
|**orgTypeCode  **|Identifiant du type d'organisation         |id         |O      |
|**orgTypeName  **|Nom du type                                |string     |O      |

<span id="classeOrgRole" >
### Classe orgRole
Repertoire des rôles des organisations.

|Propriété       |Description                               |Type    |Requis |
|----------------|------------------------------------------|--------|:-----:|
|**orgRoleCode **|Identifiant du rôle d'organisation        |id      |O      |
|**orgRoleName **|Nom du rôle                               |string  |O      |

<span id="classeOrgUnitType" >
### Classe orgUnitType
Repertoire des types des UO.

|Propriété       |Description                               |Type    |Requis |
|----------------|------------------------------------------|--------|:-----:|
|**orgTypeCode **|Identifiant du type d'UO                  |id      |O      |
|**orgTypeName **|Nom du type                               |string  |O      |


## Messages
Ce chapitre décrit la structure des objets échangés dans les interactions entre les composants, notamment entre l’interface homme-machine et le contrôle métier au travers des couches d’interprétation et de présentation.

Il ne décrit que les membres de messages de types complexes qui n'appartiennent pas au modèle statique. Les arguments simples sont échangés tels quels entre les composants et ne requièrent pas de description particulière autre que celle des APIs.

<span id="creationEtModificationDuneOrganisation" >
### Création et modification d'une organisation
Le message contient un objet qui représente l'oganisation. 

Exemple en notation JSON:

    {
        'organisation' : {
            'orgId' : 'MyOrganizationId,
            'orgName' : 'Maarch',
            'otherOrgName' : '',
            'displayName' : 'Maarch',
            'orgTypeCode' : '',
            'orgRoleCode' : '',
            'legalClassification' : 'SAS',
            'taxIdentifier' : '',
            'registration Number' : ''
        }
    }

<span id="creationEtModificationDuneUniteOrganisationnelle" >
### Création et modification d'une unité organisationnelle
Le message contient un objet qui représente l'unité organisationnelle.

Exemple en notation JSON:

    { 
        'orgUnit': {
            'orgUnitId' : 'myOrgUnitId',
            'orgUnitName' : 'Finance',
            'displayName' : 'Finance',
            'orgUnitTypeCode' : '',
            'ownerOrgUnitId' : 'MyOrganizationId',
            'parentOrgUnitId' : ''
        }
    }


<span id="creationEtModificationDunePersonne" >
### Création et modification d'une personne
Le message contient un objet qui représente la personne.

Exemple en notation JSON:

    { 
        'orgPerson': {
            'orgPersonId' : 'myPersonId',
            'firstName' : 'John',
            'lastName' : 'Doe',
            'displayName' : 'John Doe',
            'birthName' : '',
            'dateOfBirth' : '10/10/1990',
            'gender' : 'Male'
        }
    }

<span id="creationDunePosition" >
### Création d'une position
Le message contient un objet qui représente la position de la personne dans une UO.

Exemple en notation JSON:

    { 
        'personPostion': {
            'personPostionId' : '',
            'personId' : 'myPersonId',
            'personClass' : 'organization/orgPerson',
            'orgUnitId' : 'myrgUnitId',
            'function' : 'agent',
        }
    }

 
<span id="creationEtModificationDunTypeDorganisation" >
### Création et modification d'un type d'organisation
Le message contient l'objet qui représente un type d'organisation.

Exemple en notation JSON:

    { 
        'orgType': {
            'orgTypeCode' : 'PME',
            'orgTypeCode' : 'Petite et moyenne entreprise'
        }
    }


<span id="creationEtModificationDunRoleDorganisation" >
### Création et modification d'un rôle d'organisation
Le message contient l'objet qui représente un rôle d'organisation.

Exemple en notation JSON:

    { 
        'orgRole': {
            'orgRoleCode' : 'ST',
            'orgRoleCode' : 'Sous traitant'
        }
    }


<span id="creationEtModificationDunTypeDUO" >
### Création et modification d'un type d'UO
Le message contient l'objet qui représente un type d'UO.

Exemple en notation JSON:

    { 
        'orgUnitType': {
            'orgUnitType' : 'INT',
            'orgUnitType' : 'interne'
        }
    }

  
<span id="interfaceHommeMachine" >
## Interface Homme-Machine
Ce chapitre décrit le modèle d'interface homme-machine, c'est-à-dire la présentation faire à l'utilisateur des messages réponse de l'application ainsi que les opérations de navigation possibles à partir de cette présentation.

<span id="classesParticipantes" >
## Classes participantes
Ce chapitre effectue la jonction entre la modélisation (les cas d'utilisation, le modèle du domaine et l'IHM) et la conception logicielle utilisée pour l'implémentation mais non détaillée dans ce document (classes de conception, diagrammes d'interaction).

![Involved Model](img/involvedModel.jpg)

### Contrôleur adminOrgTree
### Contrôleur userPosition
### Contrôleur userOrganization
### Contrôleur adminOrgType
### Contrôleur adminOrgRole
### Contrôleur adminOrgUnitType

### Package dependency/sdo
Ce package fournit les classes de service nécessaire à la gestion du stockage persistant des données  structurées. 

Il est utilisé par les contrôleurs du package digitalResource pour toutes les opérations de création, de lecture, de modification et de suppression des objets de données et de leurs propriétés, à l’exclusion des ressources et de leurs métadonnées qui ne sont pas vues dans le package comme des informations structurées mais des paquets de données.

Il s’agit de la couche d’abstraction qui utilise le modèle de données de l’application pour gérer les opérations de base sur le stockage persistant indépendamment de la technologie de stockage : moteur de base de données relationnelle, base noSQL, XML, fichier à plat, etc.

Le service principalement utilisé est Factory et les méthodes associées aux opérations de base sur la persistance des données structurées :
*   Create : crée une nouvelle représentation d’un objet dans le stockage persistant
*   Read : lit un objet à partir du stockage persistant
*   Update : modifie un objet dans le stockage persistant
*   Delete : supprime un objet du stockage persistant


