-- Schema: organization

DROP SCHEMA IF EXISTS organization CASCADE;

CREATE SCHEMA organization
  AUTHORIZATION postgres;

-- Table: organization."orgPerson"

-- DROP TABLE organization."orgPerson";

CREATE TABLE organization."orgPerson"
(
  "orgPersonId" text NOT NULL,

  "firstName" text,
  "lastName" text,
  "title" text,
  "displayName" text NOT NULL,
  "birthName" text,
  "gender" text,
  "dateOfBirth" date,
  "picture" bytea,
  
  CONSTRAINT "orgPerson_pkey" PRIMARY KEY ("orgPersonId")
)
WITH (
  OIDS=FALSE
);

-- Table: organization."orgRole"

-- DROP TABLE organization."orgRole";

CREATE TABLE organization."orgRole"
(
  "orgRoleCode" text NOT NULL,
  "orgRoleName" text,
  CONSTRAINT "orgRole_pkey" PRIMARY KEY ("orgRoleCode")
)
WITH (
  OIDS=FALSE
);

-- Table: organization."orgType"

-- DROP TABLE organization."orgType";

CREATE TABLE organization."orgType"
(
  "orgTypeCode" text NOT NULL,
  "orgTypeName" text,
  CONSTRAINT "orgTypeCode" PRIMARY KEY ("orgTypeCode")
)
WITH (
  OIDS=FALSE
);

-- Table: organization.organization

-- DROP TABLE organization.organization;

CREATE TABLE organization.organization
(
  "orgId" text NOT NULL,
  "orgName" text NOT NULL,
  "displayName" text NOT NULL,

  "orgTypeCode" text,
  "orgRoleCode" text,
  "legalClassification" text,
  "taxIdentifier" text,
  "registrationNumber" text,
  "otherOrgName" text,
  "picture" bytea,
  CONSTRAINT organization_pkey PRIMARY KEY ("orgId"),
  CONSTRAINT "organization_orgType_fkey" FOREIGN KEY ("orgTypeCode")
      REFERENCES organization."orgType" ("orgTypeCode") MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
)
WITH (
  OIDS=FALSE
);


-- Table: organization."orgUnitType"

-- DROP TABLE organization."orgUnitType";

CREATE TABLE organization."orgUnitType"
(
  "orgUnitTypeCode" text NOT NULL,
  "orgUnitTypeName" text,
  CONSTRAINT "orgUnitType_pkey" PRIMARY KEY ("orgUnitTypeCode")
)
WITH (
  OIDS=FALSE
);


-- Table: organization."orgUnit"

-- DROP TABLE organization."orgUnit";

CREATE TABLE organization."orgUnit"
(
  "orgUnitId" text NOT NULL,
  "orgUnitName" text,
  "displayName" text NOT NULL,

  "orgUnitTypeCode" text,
  "ownerOrgId" text,
  "parentOrgUnitId" text,
  "picture" bytea,
  
  CONSTRAINT "orgUnit_pkey" PRIMARY KEY ("orgUnitId"),
  CONSTRAINT "orgUnit_ownerOrgId_fkey" FOREIGN KEY ("ownerOrgId")
      REFERENCES organization.organization ("orgId") MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT "orgUnit_parentOrgUnitId_fkey" FOREIGN KEY ("parentOrgUnitId")
      REFERENCES organization."orgUnit" ("orgUnitId") MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
)
WITH (
  OIDS=FALSE
);

-- Table: organization."personPosition"

-- DROP TABLE organization."personPosition";

CREATE TABLE organization."personPosition"
(
  "personPositionId" text NOT NULL,
  "objectId" text NOT NULL,
  "objectClass" text NOT NULL,
  "orgUnitId" text NOT NULL,
  "function" text NOT NULL,
  CONSTRAINT "personPosition_pkey" PRIMARY KEY ("personPositionId"),
  CONSTRAINT "personPosition_orgUnitId_fkey" FOREIGN KEY ("orgUnitId")
      REFERENCES "organization"."orgUnit" ("orgUnitId") MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
)
WITH (
  OIDS=FALSE
);
