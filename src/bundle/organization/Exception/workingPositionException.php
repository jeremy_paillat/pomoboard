<?php

namespace bundle\organization\Exception;

class workingPositionException
    extends \Exception
{
    public $errors = array();
}
