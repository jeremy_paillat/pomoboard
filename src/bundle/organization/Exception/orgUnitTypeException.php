<?php

namespace bundle\organization\Exception;

class orgUnitTypeException
    extends \Exception
{
    public $errors = array();
}
