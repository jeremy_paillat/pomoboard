<?php

namespace bundle\organization\Exception;

class orgTypeException
    extends \Exception
{
    public $errors = array();
}
