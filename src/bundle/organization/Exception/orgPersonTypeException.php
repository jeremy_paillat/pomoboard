<?php

namespace bundle\organization\Exception;

class orgPersonTypeException
    extends \Exception
{
    public $errors = array();
}
