<?php

namespace bundle\organization\Model;
/**
 * Organization 
 *
 * @package Organization
 * substitution organization/organization
 *
 * @pkey [orgId]
 * @fkey [orgRole] organization/orgRole[orgRoleCode]
 * @fkey [orgType] organization/orgType[orgTypeCode]
 * @key [registrationNumber]
 */
class organization
    extends \bundle\abstractDirectory\Model\abstractOrganization
{
    /**
     * The organization identifier
     *
     * @var id
     */
    public $orgId;

    /**
     * The organization type code
     *
     * @var string
     */
    public $orgTypeCode;

    /**
     * The organization role code
     *
     * @var string
     */
    public $orgRoleCode;

    /**
     * The organization legal classification
     *
     * @var string
     */
    public $legalClassification;

    /**
     * The organization's tax identifier
     *
     * @var string
     */
    public $taxIdentifier;

    /**
     * The organization's registration number
     *
     * @var string
     */
    public $registrationNumber;

    /**
     * The organization's picture
     *
     * @var resource
     */
    public $picture;

}