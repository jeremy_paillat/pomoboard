<?php

namespace bundle\organization\Model;
/**
 * An organization Unit 
 *
 * @package Organization
 * substitution organization/orgUnit
 *
 * @pkey [orgUnitId]
 * @fkey [ownerOrgId] organization/organization[orgId]
 * @fkey [parentOrgUnitId] organization/orgUnit[orgUnitId]
 */
class orgUnit
    extends \bundle\abstractDirectory\Model\abstractGroup
{
    /**
     * The orgUnit's identifier
     *
     * @var id
     */
    public $orgUnitId;

    /**
     * The orgUnit's name
     *
     * @var string
     */
    public $orgUnitName;

    /**
     * The orgUnit's type code
     *
     * @var string
     */
    public $orgUnitTypeCode;

    /**
     * The identifier of the orgUnit's owner organization
     *
     * @var id
     */
    public $ownerOrgId;

    /**
     * The identifier of the orgUnit's parent orgUnit
     *
     * @var id
     */
    public $parentOrgUnitId;

}