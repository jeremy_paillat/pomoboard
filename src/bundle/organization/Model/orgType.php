<?php

namespace bundle\organization\Model;
/**
 * Organization's type 
 *
 * @package organization
 *
 * @pkey [orgTypeCode]
 */
class orgType
{
    /**
     * The organization's type identifier
     *
     * @var string
     */
    public $orgTypeCode;  

    /**
     * The organization's type name
     *
     * @var string
     */
    public $orgTypeName;   
}