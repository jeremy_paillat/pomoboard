<?php

namespace bundle\organization\Model;
/**
 * An organization Unit 
 *
 * @package Organization
 *
 * @substitution organization/orgUnit
 * @pkey [objectId]
 */
class orgUnitGroup
    extends \bundle\abstractDirectory\Model\abstractGroup
{
    /**
     * The group's identifier
     *
     * @var id
     * @substitution orgUnitId
     * @notempty
     */
    public $objectId;

    /**
     * The person type
     *
     * @var string
     */
    protected $objectClass = 'organization/orgUnitGroup';
}