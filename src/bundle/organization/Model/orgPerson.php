<?php

namespace bundle\organization\Model;
/**
 * An organization's person 
 *
 * @package organization
 * substitution organization/orgPerson
 *
 * @pkey [orgPersonId]
 * @fkey [orgUnitId] organization/orgUnit[orgUnitId]
 */
class orgPerson
    extends \bundle\abstractDirectory\Model\abstractPerson
{
    /**
     * The orgPerson's identifier
     *
     * @var id
     */
    public $orgPersonId;

    /**
     * The person's picture
     *
     * @var resource
     */
    public $picture;

}