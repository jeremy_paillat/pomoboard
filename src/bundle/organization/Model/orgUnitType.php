<?php

namespace bundle\organization\Model;
/**
 * Organization's type 
 *
 * @package organization
 *
 * @pkey [orgUnitTypeCode]
 */
class orgUnitType
{
    /**
     * The organization unit's type identifier
     *
     * @var string
     */
    public $orgUnitTypeCode;  

    /**
     * The organization unit's type name
     *
     * @var string
     */
    public $orgUnitTypeName;   
}