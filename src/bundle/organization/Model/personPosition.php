<?php

namespace bundle\organization\Model;

/**
 * Model of the organization
 *
 * @package Organization
 *
 * @pkey [personPositionId]
 * @fkey [orgUnitId] organization/orgUnit[orgUnitId]
 */
class personPosition
{

    /**
     * @var id
     */
    public $personPositionId;

    /**
     * @var id
     */
    public $objectId;

    /**
     * @var string
     */
    public $objectClass;

    /**
     * @var id
     */
    public $orgUnitId;

    /**
     * @var string
     */
    public $function;

} // END class 
