<?php

namespace bundle\organization\Model;
/**
 * Organization party
 *
 * @package Organization
 * substitution organization/organization
 *
 */
class orgParty
    extends \bundle\abstractDirectory\Model\abstractOrganization
{
    /**
     * The party identifier
     *
     * @var id
     */
    public $objectId;

    /**
     * The party identifier
     *
     * @var id
     */
    public $objectClass = "organization/orgParty";   

    /**
     * The org unit name
     *
     * @var string
     */
    public $orgUnitName;


    /**
     * The function
     *
     * @var string
     */
    public $function;

}