<?php

namespace bundle\organization\Model;
/**
 * Organization's role 
 *
 * @package organization
 *
 * @pkey [orgRoleCode]
 */
class orgRole
{
    /**
     * The organization's role identifier
     *
     * @var string
     */
    public $orgRoleCode;  

    /**
     * The organization's role name
     *
     * @var string
     */
    public $orgRoleName;   
}