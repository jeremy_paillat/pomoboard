<?php

namespace bundle\organization\Model;
/**
 * An organization's person  
 *
 * @package Organization
 *
 * @substitution organization/orgPerson
 */
class person
    extends \bundle\abstractDirectory\Model\abstractPerson
{
    /**
     * The party's identifier
     *
     * @var id
     * @substitution orgPersonId
     * @notempty
     */
    public $objectId;

    /**
     * The person type
     *
     * @var string
     */
    protected $objectClass = 'organization/orgPerson'; 

}