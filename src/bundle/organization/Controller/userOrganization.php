<?php

namespace bundle\organization\Controller;

/**
 * Control of the organization types
 *
 * @package Organization
 */
class userOrganization
{
    protected $orgModel;

    /**
     * Constructor
     * @param object $orgModel The model for organization
     *
     * @return void
     */
    public function __construct(\bundle\organization\organization $orgModel)
    {
        $this->orgModel = $orgModel;
    }

    /**
     * Get all user's organizations
     * @return array The list of organizations
     */
    public function getOrganizations()
    {
        return $this->orgModel->getUserOrganizations($_SESSION['user']['user']->userId, 'user');
    }

    /**
     * Get all user's organization units
     * @return array The list of organization units
     */
    public function getOrgUnits()
    {
        return $this->orgModel->getUserOrgUnits($_SESSION['user']['user']->userId, 'user');
    }

    /**
     * Get all persons of user's organisation
     * @return object The list of persons
     */
    public function getOrgPerson()
    {
        return $this->orgModel->getUserOrgPerson($_SESSION['user']['user']->userId, 'user');
    }
    
}