<?php

namespace bundle\organization\Controller;

/**
 * Control of the organization unit
 *
 * @package Organization
 */
trait adminOrgUnitTrait
{
    /**
     * Create a new organiazation unit object
     *
     * @return bundle\organization\Model\orgUnit The orgUnit object
     */
    public function newOrgUnit()
    {  
        return  \laabs::newInstance('organization/orgUnit');
    }

    /**
     * Add an organiazation unit
     * @param object $orgUnit The organization unit object to add
     *
     * @return string the organization unit's Id 
     */
    public function addOrgUnit($orgUnit)
    {
        if ($orgUnit->displayName == '') {
            throw new \bundle\organization\Exception\orgUnitTypeException('Missing organization unit\'s display name.');
        }

        $orgUnit->orgUnitId = \laabs::newId();;
        $this->sdoFactory->create($orgUnit);
        
        if (\laabs::hasBundle('audit')) {
           $auditEntry = \laabs::newInstance('audit/entry');
           $auditEntry->entryType = "organization/orgUnitCreation";
           $auditEntry->objectClass = "organization/orgUnit";
           $auditEntry->objectId = $orgUnit->orgUnitId;
           $auditEntry->message = "Organization unit created";
           $auditEntry->action = 'organization/adminOrgUnitTrait/addOrgUnit';

           \laabs::notify(\bundle\audit\AUDIT_ENTRY, $auditEntry);
       }

        return $orgUnit->orgUnitId;
    }

    /**
     * Edit an organiazation unit
     * @param string $orgUnitId The Id of the organization unit to edit
     *
     * @return object the organization unit
     */
    public function editOrgUnit($orgUnitId)
    {
        return $this->sdoFactory->read("organization/orgUnit", $orgUnitId);
    }

    /**
     * Update an organization unit
     * @param object $orgUnit The organization unit object to update
     *
     * @return boolean
     */
    public function modifyOrgUnit($orgUnit)
    {
        if ($orgUnit->displayName == '') {
            throw new \bundle\organization\Exception\orgUnitTypeException('Missing organization unit\'s display name.');
        }

        $result = $this->sdoFactory->update($orgUnit);
        
        if (\laabs::hasBundle('audit')) {
           $auditEntry = \laabs::newInstance('audit/entry');
           $auditEntry->entryType = "oranization/orgUnitModification";
           $auditEntry->objectClass = "organization/orgUnit";
           $auditEntry->objectId = $orgUnit->orgUnitId;
           $auditEntry->message = "Organization unit updated";
           $auditEntry->action = 'organization/adminOrgUnitTrait/modifyOrgUnit';

           \laabs::notify(\bundle\audit\AUDIT_ENTRY, $auditEntry);
       }
       
       return $result;
    }

    /**
     * Get the list of element where a given orgUnit can be moved
     * @param string $orgUnitId The identifier of the orgUnit to move
     *
     * @return array
     */
    public function getElementsForMove($orgUnitId) 
    {
        $orgUnit = $this->sdoFactory->read('organization/orgUnit', $orgUnitId);

        $orgQuery = "";
        $ogUnitQuery = "";

        if($orgUnit->parentOrgUnitId == null) {
            $orgQuery = "orgId != '$orgUnit->ownerOrgId'";
            $ogUnitQuery = "orgUnitId != '$orgUnit->orgUnitId'";
        } else {
            $ogUnitQuery = "orgUnitId != '$orgUnit->orgUnitId' AND orgUnitId != '$orgUnit->parentOrgUnitId'";
        }

        $organizationList = $this->sdoFactory->find('organization/organization', $orgQuery);
        $orgUnitList = $this->sdoFactory->find('organization/orgUnit', $ogUnitQuery);

        $elementlist = array("organization"=>$organizationList, "orgUnit"=>$orgUnitList);

        return $elementlist;
    }


    /**
     * Move an orgUnit either to a new ownerOrg or to a new parentOrgUnit that can be held by another org
     * @param string $orgUnitId          The org unit id
     * @param string $newParentOrgUnitId The new parent organization unit id 
     * @param string $newOwnerOrgId      The new owner organization id 
     *
     * @return boolean
     */
    public function moveOrgUnit($orgUnitId, $newParentOrgUnitId, $newOwnerOrgId)
    {
        if (!$newParentOrgUnitId) {
            $newParentOrgUnitId = null;
        }

        $orgUnit = $this->sdoFactory->read("organization/orgUnit", $orgUnitId);

        if ($orgUnit->ownerOrgId == $newOwnerOrgId && $orgUnit->parentOrgUnitId == $newParentOrgUnitId) {
            return;
        }

        // Move to another parent org unit in same org
        if (!$newOwnerOrgId) {
        }

        // Move descendant ower org if requested
        if ($orgUnit->ownerOrgId != $newOwnerOrgId) {
            //$descendantOrgUnits = $this->getDescendantOrgUnits($orgUnit);
            $descendantOrgUnits = $this->sdoFactory->readDescendants("organization/orgUnit", $orgUnit);
            foreach ($descendantOrgUnits as $descendantOrgUnit) {
                $descendantOrgUnit->ownerOrgId = $newOwnerOrgId;
                $this->modifyOrgUnit($descendantOrgUnit);
            }

            $orgUnit->ownerOrgId = $newOwnerOrgId;
        }

        $orgUnit->parentOrgUnitId = $newParentOrgUnitId;

        return $this->modifyOrgUnit($orgUnit);
    }

    /**
     * Delete an orgUnit
     * @param string $orgUnitId The orgUnit id
     *
     * @return boolean 
     */
    public function deleteOrgUnit($orgUnitId)
    {
        $orgUnit = $this->sdoFactory->read("organization/orgUnit", $orgUnitId);
        $this->sdoFactory->deleteChildren("organization/personPosition", $orgUnit);

        $childrenOrgUnits=$this->sdoFactory->readChildren("organization/orgUnit", $orgUnit);
        foreach ($childrenOrgUnits as $childrenOrgUnit) {
            $this->sdoFactory->deleteChildren("organization/personPosition", $childrenOrgUnit);
            $this->deleteOrgUnit($childrenOrgUnit);
        }

        $result = $this->sdoFactory->delete($orgUnit);
        
        if (\laabs::hasBundle('audit')) {
           $auditEntry = \laabs::newInstance('audit/entry');
           $auditEntry->entryType = "organization/orgUnitDeletion";
           $auditEntry->objectClass = "organization/orgUnit";
           $auditEntry->objectId = $orgUnit->orgUnitId;
           $auditEntry->message = "Organization unit deleted";
           $auditEntry->action = 'organization/adminOrgUnitTrait/deleteOrgUnit';

           \laabs::notify(\bundle\audit\AUDIT_ENTRY, $auditEntry);
       }
       
       return $result;
    }

    /**
     * Get the list of persons of a given organization unit
     * @param string $orgUnitId The identifier organization unit
     *
     * @return array The list of person
     */
    public function getOrgUnitPersons($orgUnitId)
    {
        $persons = array();

        $personPosition = $this->sdoFactory->find('organization/personPosition', "orgUnitId = '$orgUnitId'");
        foreach ($personPosition as $position) {
            $person = parent::read('organization/orgPerson', $position->objectClass, $position->objectId);
            if($person) {
                $position->displayName = $person->displayName;
                $persons[] = $position;
            }
            
        }

        return $persons;   
    }
}