<?php

namespace bundle\organization\Controller;

/**
 * Control of the organizations
 *
 * @package Organization
 */
class personDirectory
    implements \bundle\abstractDirectory\Controller\directoryInterface
{
    protected $personId;


    /**
     * Constructor
     * @param string $personId   The person Id for read, update, delete
     * @param object $sdoFactory The model for organization
     *
     * @return void
     */
    public function __construct($personId=null, \dependency\sdo\Factory $sdoFactory)
    {
        $this->sdoFactory = $sdoFactory;

        $this->personId = $personId;
    }

    /**
     * Get the list of orgPersons
     * @param string $objectClass The class of the object
     * @param string $query
     *
     * @return array The list of orgPersons
     */
    public function find($objectClass, $query=false)
    {
        $queryTokens = \laabs\explode(" ", $query);
        $queryTokens = array_unique($queryTokens);

        $personQueryProperties = array("displayName", "firstName", "lastName");
        $personQueryPredicats = array();
        foreach ($personQueryProperties as $personQueryProperty) {
            foreach ($queryTokens as $queryToken) {
                $personQueryPredicats[] = $personQueryProperty. "=" . "'*" . $queryToken . "*'";
            }
        }
        $personQueryString = implode(" OR ", $personQueryPredicats);

        return $this->sdoFactory->find($objectClass, $personQueryString);

    }

    /**
     * Create a new instance of an object
     * 
     * @return mixed The new instance of the object
     */
    public function instance()
    {

    }

    /**
     * Create an object on persistance
     * @param mixed $object The object
     * 
     * @return mixed The object
     */
    public function add($object)
    {

    }

    /**
     * Display an object with its id
     * @param string $objectClass The class of the object
     * 
     * @return mixed The object
     */
    public function display($objectClass)
    {
        return $this->sdoFactory->read($objectClass, $this->personId);
    }

    /**
     * Read an object with its id
     * @param mixed $objectId The object identifier
     * 
     * @return mixed The object
     */
    public function edit($objectId)
    {

    }

    /**
     * Update an object with its id
     * @param mixed $object The object
     * 
     * @return bool
     */
    public function modify($object)
    {

    }

    /**
     * Update an object with its id
     * @param mixed $object The object or its id
     * 
     * @return bool
     */
    public function remove($object)
    {

    }

    
}
