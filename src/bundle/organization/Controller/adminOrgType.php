<?php

namespace bundle\organization\Controller;

/**
 * Control of the organization types
 *
 * @package Organization
 */
class adminOrgType
{
    protected $sdoFactory;

    /**
     * Constructor
     * @param object $orgModel The model for organization
     *
     * @return void
     */
    public function __construct(\dependency\sdo\Factory $sdoFactory)
    {
        $this->sdoFactory = $sdoFactory;
    }

    /**
     * Get the organization's types
     *
     * @return array The list of organization's types
     */
    public function index()
    {
        return $this->sdoFactory->find('organization/orgType');
    }

    /**
     * Create a new organization type object
     *
     * @return bundle\organization\Model\orgType The orgType object
     */
    public function newOrgType()
    {
        return \laabs::newInstance('organization/orgType');
    }

    /**
     * Add an organization type
     * @param object $orgType the orgType to add
     *
     * @return bool
     */
    public function addOrgType($orgType)
    {
        if($this->sdoFactory->exists('organization/orgType', $orgType->orgTypeCode)){
            $exception = \laabs::Bundle('organization')->newException('orgTypeException', 'The organization type code already exists.');
            throw $exception;
        }
        $result = $this->sdoFactory->create($orgType, 'organization/orgType');

        if (\laabs::hasBundle('audit')) {
           $auditEntry = \laabs::newInstance('audit/entry');
           $auditEntry->entryType = "organization/orgTypeCreation";
           $auditEntry->objectClass = "organization/orgType";
           $auditEntry->objectId = $orgType->orgTypeCode;
           $auditEntry->message = "Organization type created";
           $auditEntry->action = 'organization/adminOrgType/addOrgType';

           \laabs::notify(\bundle\audit\AUDIT_ENTRY, $auditEntry);
       }
       
        return $result;
    }

    /**
     * Edit an organization type
     * @param string $orgTypeId The id of the orgType to edit
     *
     * @return object The orgType object to add
     */
    public function editOrgType($orgTypeId)
    {
        return $this->sdoFactory->read('organization/orgType', $orgTypeId);
    }

    /**
     * Update an organization type
     * @param object $orgType The orgType to update
     *
     * @return bool
     */
    public function modifyOrgType($orgType)
    {
        $result = $this->sdoFactory->update($orgType, 'organization/orgType');
        
        if (\laabs::hasBundle('audit')) {
           $auditEntry = \laabs::newInstance('audit/entry');
           $auditEntry->entryType = "organization/orgTypeModification";
           $auditEntry->objectClass = "organization/orgType";
           $auditEntry->objectId = $orgType->orgTypeCode;
           $auditEntry->message = "Organization type updated";
           $auditEntry->action = 'organization/adminOrgType/modifyOrgType';

           \laabs::notify(\bundle\audit\AUDIT_ENTRY, $auditEntry);
       }
       
       return $result;
    }
    
}