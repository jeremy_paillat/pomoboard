<?php

namespace bundle\organization\Controller;

/**
 * Control of the organization's person
 *
 * @package Organization
 */
trait adminOrgPersonTrait
{
    /**
     * Create a new orgPerson object
     *
     * @return bundle\organization\Model\orgPerson The orgPerson object
     */
    public function newOrgPerson()
    {  
        return  \laabs::newInstance('organization/orgPerson');
    }

    /**
     * Add a person
     * @param object $orgPerson The person object to add
     *
     * @return string the person's Id 
     */
    public function addOrgPerson($orgPerson)
    {
        if ($orgPerson->displayName == '') {
            throw new \bundle\organization\Exception\orgPersonTypeException('Missing organization person\'s display name.');
        }
        
        $orgPerson->orgPersonId = \laabs::newId();
        $this->sdoFactory->create($orgPerson);
        
        if (\laabs::hasBundle('audit')) {
           $auditEntry = \laabs::newInstance('audit/entry');
           $auditEntry->entryType = "organization/orgPersonCreation";
           $auditEntry->objectClass = "organization/orgPerson";
           $auditEntry->objectId = $orgPerson->orgPersonId;
           $auditEntry->message = "Organization person created";
           $auditEntry->action = 'organization/adminOrgPersonTrait/addOrgPerson';

           \laabs::notify(\bundle\audit\AUDIT_ENTRY, $auditEntry);
       }

        return $orgPerson->orgPersonId;
    }

    /**
     * Edit a person
     * @param string $orgPersonId The identifier of the orgPerson
     *
     * @return object the organization unit
     */
    public function editOrgPerson($orgPersonId)
    {
        return $this->sdoFactory->read("organization/orgPerson", $orgPersonId);
    }

    /**
     * Update a person
     * @param object $orgPerson The person object to update
     *
     * @return boolean
     */
    public function modifyOrgPerson($orgPerson)
    {
        if ($orgPerson->displayName == '') {
            throw new \bundle\organization\Exception\orgPersonTypeException('Missing organization person\'s display name.');
        }

        $result = $this->sdoFactory->update($orgPerson, "organization/orgPerson");
        
        if (\laabs::hasBundle('audit')) {
           $auditEntry = \laabs::newInstance('audit/entry');
           $auditEntry->entryType = "organization/orgPersonModification";
           $auditEntry->objectClass = "organization/orgPerson";
           $auditEntry->objectId = $orgPerson->orgPersonId;
           $auditEntry->message = "Organization person updated";
           $auditEntry->action = 'organization/adminOrgPersonTrait/modifyOrgPerson';

           \laabs::notify(\bundle\audit\AUDIT_ENTRY, $auditEntry);
       }
       
       return $result;
    }

    /**
     * Delete an orgPerson
     * @param string $orgPersonId The identifier of the orgPerson
     *
     * @return boolean 
     */
    public function deleteOrgPerson($orgPersonId)
    {
        $orgPerson = $this->sdoFactory->read("organization/orgPerson", $orgPersonId);

        $result = $this->sdoFactory->delete($orgPerson);
        
        if (\laabs::hasBundle('audit')) {
           $auditEntry = \laabs::newInstance('audit/entry');
           $auditEntry->entryType = "organization/orgPersonDeletion";
           $auditEntry->objectClass = "organization/orgPerson";
           $auditEntry->objectId = $orgPerson->orgPersonId;
           $auditEntry->message = "Organization person deleted";
           $auditEntry->action = 'organization/adminOrgPersonTrait/deleteOrgPerson';

           \laabs::notify(\bundle\audit\AUDIT_ENTRY, $auditEntry);
       }
       
       return $result;
    }


    /**
     * Add a position to a person
     * @param object $personPosition The position of the person
     *
     * @return string The new identifier
     */
    public function addPersonPosition($personPosition)
    {
        $personPosition->personPositionId = \laabs::newId();;

        $this->sdoFactory->create($personPosition);
        
        if (\laabs::hasBundle('audit')) {
           $auditEntry = \laabs::newInstance('audit/entry');
           $auditEntry->entryType = "organization/personPositionCreation";
           $auditEntry->objectClass = "organization/personPosition";
           $auditEntry->objectId = $personPosition->personPositionId;
           $auditEntry->message = "Person position created";
           $auditEntry->action = 'organization/adminOrgPersonTrait/addPersonPosition';

           \laabs::notify(\bundle\audit\AUDIT_ENTRY, $auditEntry);
       }
        
        return $personPosition->personPositionId;
    }

    /**
     * Remove a person's position
     * @param object $personPosition The position of the person
     *
     * @return boolean
     */
    public function deletePersonPosition($personPositionId)
    {
        $personPosition = $this->sdoFactory->read("organization/personPosition", $personPositionId);
        $result = $this->sdoFactory->delete($personPosition);
        
        if (\laabs::hasBundle('audit')) {
           $auditEntry = \laabs::newInstance('audit/entry');
           $auditEntry->entryType = "organization/personPositionDeletion";
           $auditEntry->objectClass = "organization/personPosition";
           $auditEntry->objectId = $personPosition->personPositionId;
           $auditEntry->message = "Person position deleted";
           $auditEntry->action = 'organization/adminOrgPersonTrait/deletePersonPosition';

           \laabs::notify(\bundle\audit\AUDIT_ENTRY, $auditEntry);
       }
       
       return $result;
    }

    /**
     * Get the list of available persons
     * @param string $query A query string of tokens
     * 
     * @return array
     */
    public function queryPersons($query)
    {
        return parent::query('organization/orgPerson', $query);
    }
}