<?php

namespace bundle\organization\Controller;

/**
 * Control of the organization types
 *
 * @package Organization
 */
class adminOrgUnitType
{
    protected $orgUnitTypeCode;
    protected $sdoFactory;

    /**
     * Constructor
     * @param \dependency\sdo\Factory $sdoFactory The dependency Sdo Factory object
     *
     * @return void
     */
    public function __construct($orgUnitTypeCode=null, \dependency\sdo\Factory $sdoFactory)
    {
        $this->orgUnitTypeCode = $orgUnitTypeCode;
        $this->sdoFactory = $sdoFactory;
    }

    /**
     * Get the organization unit's types
     *
     * @return array The list of organization unit's types
     */
    public function index()
    {
        return $this->sdoFactory->find('organization/orgUnitType');
    }

    /**
     * Create a new orgUnittype object
     *
     * @return bundle\organization\Model\orgUnitType The orgUnitType object
     */
    public function newOrgUnitType()
    {
        return \laabs::newInstance('organization/orgUnitType');
    }

    /**
     * Add an orgUnit type
     * @param object $orgUnitType the orgUnitType to add
     *
     * @return bool
     */
    public function addOrgUnitType($orgUnitType)
    {
        if($this->sdoFactory->exists('organization/orgUnitType', $orgUnitType->orgUnitTypeCode)){
            $exception = \laabs::Bundle('organization')->newException('orgUnitTypeException', 'The organization init type code already exists.');
        throw $exception;
            
        }

        $result = $this->sdoFactory->create($orgUnitType, 'organization/orgUnitType');
        
        if (\laabs::hasBundle('audit')) {
           $auditEntry = \laabs::newInstance('audit/entry');
           $auditEntry->entryType = "organization/orgUnitTypeCreation";
           $auditEntry->objectClass = "organization/orgUnitType";
           $auditEntry->objectId = $orgUnitType->orgUnitTypeCode;
           $auditEntry->message = "Organization unit type created";
           $auditEntry->action = 'organization/adminOrgUnitType/addOrgUnitType';

           \laabs::notify(\bundle\audit\AUDIT_ENTRY, $auditEntry);
       }
       
        return $result;
    }

    /**
     * Edit an orgUnit type 
     *
     * @return object The orgUnitType object to add
     */
    public function editOrgUnitType($orgUnitTypeCode)
    {
        return $this->sdoFactory->read('organization/orgUnitType', $orgUnitTypeCode);
    }

    /**
     * Update an orgUnit type
     * @param object $orgUnitType The orgUnitType to update
     *
     * @return bool
     */
    public function modifyOrgUnitType($orgUnitType)
    {
        $result = $this->sdoFactory->update($orgUnitType, 'organization/orgUnitType');
        
        if (\laabs::hasBundle('audit')) {
           $auditEntry = \laabs::newInstance('audit/entry');
           $auditEntry->entryType = "organization/orgUnitTypeModification";
           $auditEntry->objectClass = "organization/orgUnitType";
           $auditEntry->objectId = $orgUnitType->orgUnitTypeCode;
           $auditEntry->message = "Organization unit type updated";
           $auditEntry->action = 'organization/adminOrgUnitType/modifyOrgUnitType';

           \laabs::notify(\bundle\audit\AUDIT_ENTRY, $auditEntry);
       }
       
        return $result;
    }
}