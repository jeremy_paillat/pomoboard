<?php

namespace bundle\organization\Controller;

/**
 * Control of the organization and orgUnit tree
 *
 * @package Organization
 */
class adminOrgTree
    extends \bundle\abstractDirectory\Controller\abstractDirectory
{
    use adminOrganizationTrait;
    use adminOrgUnitTrait;
    use adminOrgPersonTrait;

    protected $orgPersonId;
    protected $personPositionId;

    /**
     * Constructor
     * @param object $sdoFactory The model for organization
     *
     * @return void
     */
    public function __construct(\dependency\sdo\Factory $sdoFactory)
    {
        parent::__construct($sdoFactory);
    }

    /**
     * Get the organizations' index
     *
     * @return array The list of file plans with their position
     */
    public function index()
    {
        $organizations = $this->sdoFactory->find("organization/organization");
        foreach ($organizations as $organization) {
            $organization->orgUnit = $this->readOrgUnits($organization->orgId);
        }

        return $organizations;
    }

    /**
     * Get the list of a given organization units
     * @param string $orgId The identifier of the organization that owns the units 
     *
     * @return array The list of organization units
     */
    protected function readOrgUnits($orgId)
    {
        $orgUnits = $this->sdoFactory->readTree("organization/orgUnit", "ownerOrgId = '$orgId'");
        
        foreach ($orgUnits as $orgUnit) {
            $orgUnit = $this->readOrgPersons($orgUnit);
        }
        
        return $orgUnits;
    }

    /**
     * Get the list of persons of a given organization unit and his children
     * @param string $parentOrgUnit The organization units that owns the persons 
     *
     * @return object The orgUnit with his persons
     */
    protected function readOrgPersons($parentOrgUnit) 
    {
        if (isset($parentOrgUnit->orgUnit)) {
            foreach ($parentOrgUnit->orgUnit as $orgUnit) {
                $orgUnit = $this->readOrgPersons($orgUnit);
            }
        }
        $persons = array();

        $personPosition = $this->sdoFactory->find('organization/personPosition', "orgUnitId = '$parentOrgUnit->orgUnitId'");
        foreach ($personPosition as $position) {
            $person = parent::read('organization/orgPerson', $position->objectClass, $position->objectId);
            if($person)  {
                $persons[] = $person;
            }
        }
        $parentOrgUnit->orgPerson = $persons;

        return $parentOrgUnit;
    }
}
