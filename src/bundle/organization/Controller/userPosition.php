<?php

namespace bundle\organization\Controller;

/**
 * Control of the organization types
 *
 * @package Organization
 */
class userPosition
{
    protected $sdoFactory;

    /**
     * Constructor
     * @param object $sdoFactory The model for organization
     *
     * @return void
     */
    public function __construct(\dependency\sdo\Factory $sdoFactory)
    {
        $this->sdoFactory = $sdoFactory;
    }

    /**
     * Get my all positions
     * @return array The list of my position's
     */
    public function getMyPositions()
    {
        $objectId = $_SESSION['user']['user']->userId;
        
        $personPositions = $this->sdoFactory->find('organization/personPosition', "objectId = '$objectId' AND objectClass = 'user/userPerson'");
        
        $orgUnits = array();
        $orgs = array();

        foreach ($personPositions as $position) {
            $orgUnit = $this->sdoFactory->read('organization/orgUnit', $position->orgUnitId); 
            
            $orgUnits[(string) $position->orgUnitId] = $orgUnit;
            $position->orgUnit = $orgUnit;
            
            if (!isset($orgs[(string) $orgUnit->ownerOrgId])) {
                $org = $this->sdoFactory->read('organization/organization', $orgUnit->ownerOrgId);
                $orgs[(string) $orgUnit->ownerOrgId] = $org;
            }

            $position->orgUnit->orgName = $org->displayName;
        }

        if (count($orgs) == 1) {
            $_SESSION['organization']['currentOrganization'] = reset($orgs);
        }

        if (count($orgUnits) == 1) {
            $_SESSION['organization']['currentOrgUnit'] = reset($orgUnits);
        }

        return $personPositions;
    }

    /**
     * Get the list of user orgUnits
     * @return array The list of my positions
     */
    public function getMyOrgUnits()
    {
        $objectId = $_SESSION['user']['user']->userId;
        
        $personPosition = $this->sdoFactory->find('organization/personPosition', "objectId = '$objectId' AND objectClass = 'user/userPerson'");
        
        $orgUnits = array();

        foreach ($personPosition as $position) {
            array_push($orgUnits, $this->sdoFactory->read('organization/orgUnit', $position->orgUnitId)); 
        }

        return $orgUnits;
    }

    /**
     * Set my working positions
     * @param object $orgUnitId The orgUnitId 
     * 
     * @return bool
     */
    public function setCurrentPosition($orgUnitId)
    {
        if ($orgUnit = $this->sdoFactory->read('organization/orgUnit', $orgUnitId)) {
            $_SESSION['organization']['currentOrgUnit'] = $orgUnit;
            $_SESSION['organization']['currentOrganization'] = $this->sdoFactory->read('organization/organization', $orgUnit->ownerOrgId);
            
            return 1;
        }
        $exception = \laabs::Bundle('organization')->newException('workingPositionException', 'This position is not defined for this user');
        throw $exception;
    }

    /**
     * Get my working positions
     *
     * @return object The working position
     */
    public function getCurrentPosition()
    {
        if (isset($_SESSION['organization']['currentOrgUnit'])) {
            
            return $_SESSION['organization']['currentOrgUnit'];
        } else {
            
            return null;
        }
    }

    /**
     * List user positions orgUnits ids
     *
     * @return array The list of orgUnit ids
     */
    public function listMyOrgUnitIds()
    {
        $objectId = $_SESSION['user']['user']->userId;
        $personPositions = $this->sdoFactory->find('organization/personPosition', "objectId = '$objectId' AND objectClass = 'user/userPerson'");
        $list = array();

        foreach ($personPositions as $personPosition) {
            $list[] = $personPosition->orgUnitId;
        }
        
        return $list;        
    }
    
    /**
     * List user positions orgUnits ids as well as theirs childrens
     *
     * @return array The list of orgUnit ids
     */
    public function listMyDescendantsOrgUnitIds()
    {
        $list = $this->listMyOrgUnitIds();
        
        foreach ($list as $orgUnitId) {
            $list = array_merge($list, $this->getOrgUnitDescendantsIds($orgUnitId));
            $list = array_unique($list);
        }

        return $list;        
    }
    
    /**
     * Get the descendant of a given orgUnit
     * @param sting $parentId The id of the parent orgUnit
     *
     * @return array The list of orgUnit ids
     */
    protected function getOrgUnitDescendantsIds($parentId)
    {
        $parent = $this->sdoFactory->read('organization/orgUnit', $parentId);
        $children = $this->sdoFactory->readChildren('organization/orgUnit', $parent, 'organization/orgUnit');
        $list = array();
        
        foreach ($children as $child) {
            $list[] = (string) $child->orgUnitId;
            $list = array_merge($list, $this->getOrgUnitDescendantsIds($child->orgUnitId));
        }
        
        return $list;
    }
    
    /**
     * List user positions orgUnits ids as well as theirs sisters
     *
     * @return array The list of orgUnit ids
     */
    public function listMySistersOrgUnitIds()
    {
        $list = $this->listMyOrgUnitIds();
        $sistersList = array();
        
        foreach ($list as $orgUnitId) {
            $orgUnit = $this->sdoFactory->read('organization/orgUnit', $orgUnitId);
            
            if (isset($orgUnit->parentOrgUnitId)) {
                $parent = $this->sdoFactory->read('organization/orgUnit', $orgUnit->parentOrgUnitId);
                $sisters = $this->sdoFactory->readChildren('organization/orgUnit', $parent, 'organization/orgUnit'); 
            } else {
                $sisters = $this->sdoFactory->find('organization/orgUnit', "ownerOrgId='$orgUnit->ownerOrgId' AND parentOrgUnitId=null");
            }
            
            foreach ($sisters as $sister) {
                $sistersList[] = (string) $sister->orgUnitId;
                $sistersList = array_merge($this->getOrgUnitDescendantsIds((string) $sister->orgUnitId), $sistersList);
                $sistersList = array_unique($sistersList);
            }
        }

        return $sistersList;   
    }
    
    /**
     * Get organization ids of user
     * 
     * @return array The list of organization ids
     */
    public function listMyOrgIds()
    {
        $orgUnitIds = $this->listMyOrgUnitIds();
        
        $orgIds = array();
        
        foreach ($orgUnitIds as $orgUnitId) {
            $orgUnit = $this->sdoFactory->read('organization/orgUnit', $orgUnitId);
            $orgIds[] = $orgUnit->ownerOrgId;
        }
        $orgIds = array_unique($orgIds);
        
        return $orgIds;
    }
    
    /**
     * Get organization of user
     * 
     * @return array The list of organization/organization object
     */
    public function listMyOrg()
    {
        $orgIds = $this->listMyOrgIds();
        $org = array();
        
        foreach ($orgIds as $orgId) {
            $org[] = $this->sdoFactory->read('organization/organization', $orgId);
        }
        
        return $org;
    }
}