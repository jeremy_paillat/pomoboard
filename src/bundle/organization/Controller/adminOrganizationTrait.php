<?php

namespace bundle\organization\Controller;

/**
 * Control of the organization
 *
 * @package Organization
 */
trait adminOrganizationTrait
{
    /**
     * Create a new organiazation object
     *
     * @return bundle\organization\Model\organization The organization object
     */
    public function newOrganization()
    {   
        return \laabs::newInstance('organization/organization');
    }

    /**
     * Add an organiazation
     * @param object $organization The organization object to add
     *
     * @return string the organization's Id 
     */
    public function addOrganization($organization)
    {
        if ($organization->displayName == '') {
            throw new \bundle\organization\Exception\orgTypeException('Missing organization\'s display name.');
        }

        $organization->orgId = \laabs::newId();;
        $this->sdoFactory->create($organization);
        
        if (\laabs::hasBundle('audit')) {
           $auditEntry = \laabs::newInstance('audit/entry');
           $auditEntry->entryType = "organization/organizationCreation";
           $auditEntry->objectClass = "organization/organization";
           $auditEntry->objectId = $organization->orgId;
           $auditEntry->message = "Organization created";
           $auditEntry->action = 'organization/adminOrganizationTrait/addOrganization';

           \laabs::notify(\bundle\audit\AUDIT_ENTRY, $auditEntry);
        }

        return $organization->orgId;
    }

    /**
     * Edit an organiazation
     * @param string $orgId The Identifier of the organization to edit
     *
     * @return object the organization 
     */
    public function editOrganization($orgId)
    {
        return $this->sdoFactory->read("organization/organization", $orgId);        
    }

    /**
     * Get an organiazation by regitration number
     * @param string $registrationNumber The registration number of the organization
     *
     * @return object the organization 
     */
    public function getOrgByRegNumber($registrationNumber)
    {
        return $this->sdoFactory->read("organization/organization", array('registrationNumber' => $registrationNumber));        
    }

    /**
     * Update an organization
     * @param object $organization The organization object to update
     *
     * @return boolean
     */
    public function modifyOrganization($organization)
    {
        if ($organization->displayName == '') {
            throw new \bundle\organization\Exception\orgTypeException('Missing organization\'s display name.');
        }
        //var_dump($organization);
        $result = $this->sdoFactory->update($organization, 'organization/organization');
        
        if (\laabs::hasBundle('audit')) {
           $auditEntry = \laabs::newInstance('audit/entry');
           $auditEntry->entryType = "organization/organizationModification";
           $auditEntry->objectClass = "organization/organization";
           $auditEntry->objectId = $organization->orgId;
           $auditEntry->message = "Organization updated";
           $auditEntry->action = 'organization/adminOrganizationTrait/modifyOrganization';

           \laabs::notify(\bundle\audit\AUDIT_ENTRY, $auditEntry);
        }
       
       return $result;
    }

    /**
     * Delete an organization
     * @param string $orgId The organization id
     *
     * @return boolean
     */
    public function deleteOrganization($orgId)
    {
        $organization = $this->sdoFactory->read("organization/organization", $orgId);

        $orgUnits=$this->sdoFactory->readChildren("organization/orgUnit", $organization);
        foreach ($orgUnits as $orgUnit) {
            $this->deleteOrgUnit($orgUnit);
        }

        $result = $this->sdoFactory->delete($organization);
        
        if (\laabs::hasBundle('audit')) {
           $auditEntry = \laabs::newInstance('audit/entry');
           $auditEntry->entryType = "organization/organizationDeletion";
           $auditEntry->objectClass = "organization/organization";
           $auditEntry->objectId = $organization->orgId;
           $auditEntry->message = "Organization deleted";
           $auditEntry->action = 'organization/adminOrganizationTrait/deleteOrganization';

           \laabs::notify(\bundle\audit\AUDIT_ENTRY, $auditEntry);
        }
        
        return $result;
    }
}