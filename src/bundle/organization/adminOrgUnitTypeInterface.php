<?php

namespace bundle\organization;
/**
 *
 */
interface adminOrgUnitTypeInterface {

    /**
     * Get the organization unit types
     *
     * @return array Array of organization/orgUnitType objects
     * 
     * @request READ organization/adminOrgUnitType/index
     */
    public function index();

    /**
     * Create a new organiazation object
     *
     * @return organization/orgUnitType The organization unit type object
     * 
     * @request READ organization/adminOrgUnitType/newOrgUnitType
     */
    public function newOrgUnitType();

    /**
     * Add an orgUnit type
     * @param object $orgUnitType the orgUnitType to add
     *
     * @return bool
     * 
     * @request CREATE organization/adminOrgUnitType/addOrgUnitType
     */
    public function addOrgUnitType($orgUnitType);
    
    /**
     * Edit an orgUnit type 
     * @param string $orgUnitTypeCode
     *
     * @return object The orgUnitType object to add
     * 
     * @request READ organization/adminOrgUnitType/editOrgUnitType/(.*)
     */
    public function editOrgUnitType($orgUnitTypeCode);
    
    /**
     * Update an orgUnit type
     * @param object $orgUnitType The orgUnitType to update
     *
     * @return bool
     * 
     * @request UPDATE organization/adminOrgUnitType/modifyOrgUnitType
     */
    public function modifyOrgUnitType($orgUnitType);
}
