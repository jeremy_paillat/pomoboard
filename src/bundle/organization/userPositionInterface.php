<?php

namespace bundle\organization;
/**
 *
 */
interface userPositionInterface 
{

    /**
     * Set the user current org unit
     * @param string $orgUnitId
     *
     * @return bool
     * 
     * @request UPDATE organization/currentPosition
     */
    public function setCurrentPosition($orgUnitId);

    /**
     * Set the user current org unit
     * @param string $orgUnitId
     *
     * @return bool
     * 
     * @request READ organization/currentPosition
     */
    public function getMyPositions();
}
