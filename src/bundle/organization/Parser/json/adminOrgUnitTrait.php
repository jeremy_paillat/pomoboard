<?php

namespace bundle\organization\Parser\json;

/**
 * adminOrgTree parser
 *
 * @package Organization
 */
trait adminOrgUnitTrait
{
    /**
     * Parse data for creating an orgUnit
     * @param Object $orgUnitJson JSON data of creation form
     * 
     * @return array
     */
    public function addOrgUnit($orgUnitJson)
    {
        $this->jsonObject->loadJson($orgUnitJson, 'organization/orgUnit');
        $orgUnit = $this->jsonObject->export();
        if($orgUnit->parentOrgUnitId == '') {
            $orgUnit->parentOrgUnitId = null;
        }
        return array('orgUnit' => $orgUnit);
    }

    /**
     * Parse data for updating an orgUnit
     * @param Object $orgUnitJson JSON data of creation form
     * 
     * @return array
     */
    public function modifyOrgUnit($orgUnitJson)
    {
        $this->jsonObject->loadJson($orgUnitJson, 'organization/orgUnit');
        $orgUnit = $this->jsonObject->export();
        if($orgUnit->parentOrgUnitId == '') {
            $orgUnit->parentOrgUnitId = null;
        }
        
        return array('orgUnit' => $orgUnit);
    }

    /**
     * Parse data for moving an orgUnit
     * @param object $position The information to move the orgUnit
     * 
     * @return array
     */
    public function moveOrgUnit($position)
    {
        $position = json_decode($position);

        return array('orgUnitId' => $position->orgUnitId,
                     'newParentOrgUnitId' => $position->newParentOrgUnitId, 
                     'newOwnerOrgId'=> $position->newOwnerOrgId);
    }
}