<?php

namespace bundle\organization\Parser\json;

/**
 * adminOrgTree parser
 *
 * @package Organization
 */
trait adminOrganizationTrait
{
    /**
     * Parse data for creating an organization
     * @param Object $organizationJson JSON data of creation form
     * 
     * @return array
     */
    public function addOrganization($organizationJson)
    {
        $this->jsonObject->loadJson($organizationJson, 'organization/organization');
        $this->jsonObject->picture = $this->decodeImage($this->jsonObject->picture);

        return array('organization' => $this->jsonObject->export());

    }

    /**
     * Parse data for updating an organization
     * @param Object $organizationJson JSON data of creation form
     * 
     * @return array
     */
    public function modifyOrganization($organizationJson)
    {
        $this->jsonObject->loadJson($organizationJson, 'organization/organization');
        $this->jsonObject->picture = $this->decodeImage($this->jsonObject->picture);
        //var_dump($this->jsonObject->export());
        return array('organization' => $this->jsonObject->export());

    }
}