<?php

namespace bundle\organization\Parser\json;

/**
 * organization unit type parser
 *
 * @package Organization
 */
class adminOrgUnitType
{
    public $jsonObject;

    /**
     * Constructor
     * @param \dependency\json\JsonObject $jsonObject The Json utility
     */
    public function __construct(\dependency\json\JsonObject $jsonObject)
    {
        $this->jsonObject = $jsonObject;
    }

    /**
     * Parse data for create
     * @param Object $orgUnitTypeJson JSON data of creation form
     * 
     * @return object
     */
    public function addOrgUnitType($orgUnitTypeJson)
    {
        $this->jsonObject->loadJson($orgUnitTypeJson, 'organization/orgUnitType');

        return array('orgUnitType' => $this->jsonObject->export());
    }

    /**
     * Parse data for update
     * @param Object $orgUnitTypeJson JSON data of creation form
     * 
     * @return object
     */
    public function modifyOrgUnitType($orgUnitTypeJson)
    {
        $this->jsonObject->loadJson($orgUnitTypeJson, 'organization/orgUnitType');

        return array('orgUnitType' => $this->jsonObject->export());
    }
}