<?php

namespace bundle\organization\Parser\json;

/**
 * adminOrgTree parser
 *
 * @package Organization
 */
class adminOrgTree
{
    use adminOrganizationTrait;
    use adminOrgUnitTrait;
    use adminOrgPersonTrait;
    
    public $jsonObject;

    /**
     * Constructor
     * @param \dependency\json\JsonObject $jsonObject The Json utility
     */
    public function __construct(\dependency\json\JsonObject $jsonObject)
    {
        $this->jsonObject = $jsonObject;
    }

    /**
     * Converting a base64 image to binary
     * @param string $imageBase64 The image
     * 
     * @return string
     */
    public function decodeImage($imageBase64)
    {
        if($imageBase64 == '' || sizeof($imageBase64) == 1) {
            return NULL;
        }

        $imageBase64 = substr($imageBase64, strpos($imageBase64, "base64,")+7);
        $imageBinary = base64_decode($imageBase64);

        return \laabs::createMemoryStream($imageBinary);
    }
}