<?php

namespace bundle\organization\Parser\json;

/**
 * adminOrgTree parser
 *
 * @package Organization
 */
trait adminOrgPersonTrait
{
    /**
     * Parse data for creating an orgPerson
     * @param Object $orgPersonJson JSON data of creation form
     * 
     * @return array
     */
    public function addOrgPerson($orgPersonJson)
    {

        $this->jsonObject->loadJson($orgPersonJson, 'organization/orgPerson');
        
        $this->jsonObject->picture = $this->decodeImage($this->jsonObject->picture);
        if ($this->jsonObject->dateOfBirth == '') {
            $this->jsonObject->dateOfBirth = null;
        }

        return array('orgPerson' => $this->jsonObject->export());
    }

    /**
     * Parse data for updating an orgPerson
     * @param Object $orgUnitJson JSON data of creation form
     * 
     * @return array
     */
    public function modifyOrgPerson($orgPersonJson)
    {
        $this->jsonObject->loadJson($orgPersonJson, 'organization/orgPerson');
        
        $this->jsonObject->picture = $this->decodeImage($this->jsonObject->picture);
        if ($this->jsonObject->dateOfBirth == '') {
            $this->jsonObject->dateOfBirth = null;
        }

        return array('orgPerson' => $this->jsonObject->export());
    }

    /**
     * Parse data for adding a person position
     * @param Object $personPostionJson JSON data of creation form
     * 
     * @return array
     */
    public function addPersonPosition($personPostionJson)
    {
        $this->jsonObject->loadJson($personPostionJson, 'organization/personPosition');
        return array('personPosition' => $this->jsonObject->export());
    }    

    /**
     * Parse data for deleting a person position
     * @param Object $personPostionJson JSON data of creation form
     * 
     * @return array
     */
    public function deletePersonPosition($personPostionJson)
    {
        $this->jsonObject->loadJson($personPostionJson, 'organization/personPosition');

        return array('personPosition' => $this->jsonObject->export());
    }
}