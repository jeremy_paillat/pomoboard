<?php

namespace bundle\abstractDirectory\Model;
/**
 * Abstract class to represent a party
 *
 * @package DirectoryService
 * 
 */
abstract class abstractParty
{
    
    use partyTrait, objectTrait;

    public function __get($name)
    {
        if(isset($this->{$name})) {
            return $this->$name;
        }
    }
}
