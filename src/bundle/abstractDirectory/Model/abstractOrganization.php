<?php

namespace bundle\abstractDirectory\Model;
/**
 * Abstract class to define an organization 
 *
 * @package DirectoryService
 *
 */
abstract class abstractOrganization
{
    use organizationTrait, objectTrait;
    
    public function __get($name)
    {
        if(isset($this->{$name})) {
            return $this->$name;
        }
    }
}