<?php

namespace bundle\abstractDirectory\Model;
/**
 * Abstract class to represent a person
 *
 * @package DirectoryService
 * 
 */
abstract class abstractPerson
{
    
    use personTrait, objectTrait;

    public function __get($name)
    {
        if(isset($this->{$name})) {
            return $this->$name;
        }
    }
}
