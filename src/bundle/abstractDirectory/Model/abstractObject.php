<?php

namespace bundle\abstractDirectory\Model;
/**
 * Class that defines the base objects for directory
 *
 * @package DirectoryService
 * 
 * @pkey [objectId]
 */
abstract class abstractObject
{
    use objectTrait;
    
    public function __get($name)
    {
        if(isset($this->{$name})) {
            return $this->$name;
        }
    }
}