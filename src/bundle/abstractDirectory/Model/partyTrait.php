<?php

namespace bundle\abstractDirectory\Model;
/**
 * Trait that defines a third party org or person as a party
 *
 * @package DirectoryService
 */
trait partyTrait
{
    use personTrait, organizationTrait;

    /**
     * The party's type (person or org)
     *
     * @var string
     * @enumeration [person, organization]
     */
    public $partyType;
}