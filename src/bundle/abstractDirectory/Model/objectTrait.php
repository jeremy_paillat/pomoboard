<?php

namespace bundle\abstractDirectory\Model;
/**
 * Class that defines a directory object traits
 *
 * @package DirectoryService
 */
trait objectTrait
{
    /**
     * The displayed name of the object
     *
     * @var string
     * @notempty
     */
    public $displayName;

    /**
     * The object creation timestamp
     *
     * @var timestamp
     */
    //public $created;

    /**
     * The object creator identifier
     * 
     * @var id
     */
    //public $createdBy;

    /**
     * The object last modification
     *
     * @var timestamp
     */
    //public $modified;

    /**
     * The object modifier identifier
     * 
     * @var id
     */
    //public $modifiedBy;
}