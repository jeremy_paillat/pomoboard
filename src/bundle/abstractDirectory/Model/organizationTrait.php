<?php

namespace bundle\abstractDirectory\Model;
/**
 * Trait to define an organization properties and behaviour
 *
 * @package DirectoryService
 *
 */
trait organizationTrait
{
  
    /**
     * The organization name (legal)
     *
     * @var string
     */
    public $orgName;

    /**
     * Another organization legal or known form of name
     *
     * @var string
     */
    public $otherOrgName;

    
}