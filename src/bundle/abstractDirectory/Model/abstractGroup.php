<?php

namespace bundle\abstractDirectory\Model;
/**
 * Class that defines a group of persons (access, role, family)
 *
 * @package DirectoryService
 */
abstract class abstractGroup
{
    use groupTrait, objectTrait;
    
    public function __get($name)
    {
        if(isset($this->{$name})) {
            return $this->$name;
        }
    }
}