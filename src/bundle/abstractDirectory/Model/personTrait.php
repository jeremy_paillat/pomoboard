<?php

namespace bundle\abstractDirectory\Model;
/**
 * Trait to define a person properties and behaviour
 *
 * @package DirectoryService
 */
trait personTrait
{
    /**
     * The person fisrt name (given name)
     *
     * @var string
     */
    public $firstName;

    /**
     * The person last name (family name, surname)
     *
     * @var string
     */
    public $lastName;

    /**
     * The person gender (male of female)
     *
     * @var string
     * @enumeration [male, female]
     */
    public $gender;

    /**
     * The person birth name (for married females)
     *
     * @var string
     */
    public $birthName;

    /**
     * The person title (civility)
     *
     * @var string
     */
    public $title;

    /**
     * The person birth date
     *
     * @var date
     */
    public $dateOfBirth;
}
