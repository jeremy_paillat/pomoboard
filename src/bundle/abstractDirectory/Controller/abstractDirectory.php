<?php

namespace bundle\abstractDirectory\Controller;

/**
 * class for direcotory control
 *
 * @package AbstractDirectory
 */
abstract class abstractDirectory
{

    protected $sdoFactory;
    protected $relationships;

    /**
     * Constructor of access control class
     * @param \dependency\sdo\Factory $sdoFactory    The factory
     */
    public function __construct( \dependency\sdo\Factory $sdoFactory)
    {
        $this->sdoFactory = $sdoFactory;
        $this->relationships = \laabs::configuration('abstractDirectory')['relationships'];
    }

    /**
     * Get the list of available persons related to the given parent class
     * @param string $parentClass   The name of the parent class
     * @param string $query         A query string
     * 
     * @return array The result of the request
     */
    public function query($parentClass, $query=false)
    {
        $relatedClasses =  array();
        if(isset($this->relationships[$parentClass])) {
            $relatedClasses = $this->relationships[$parentClass];
        }
        $persons = array();
        foreach ($relatedClasses as $class) {
            $bundle = explode('/', $class)[0];
            if(\laabs::hasBundle($bundle)) {
                $controller = \laabs::newController($bundle."/directory");
                $persons = array_merge($persons, $controller->find($class, $query));
            }
        }

        return $persons;
    }

    /**
     * Get an object by his identifier
     * @param string $parentClass    The name of the parent class
     * @param string objectClass     The name of the object class to read
     * @param string objectId        The identifier of the object 
     *
     * @return mixed The object 
     */
    public function read($parentClass, $objectClass, $objectId)
    {
        $relatedClasses =  array();
        if(isset($this->relationships[$parentClass])) {
            $relatedClasses = $this->relationships[$parentClass];
        }
        
        if(!in_array($objectClass, $relatedClasses)) {
            return false;
        }
        $bundle = explode('/', $objectClass)[0];
        $controller = \laabs::newController($bundle."/directory");

        return $controller->read($objectId, $objectClass);
    }
}