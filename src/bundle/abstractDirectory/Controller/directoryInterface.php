<?php

namespace bundle\abstractDirectory\Controller;

/**
 * Interface for direcotory control
 *
 * @package AbstractDirectory
 */
interface directoryInterface
{
    /**
     * Find objects
     * @param string $queryString The query string
     * 
     * @return array The objects
     */
    public function find($objectClass, $queryString);

    /**
     * Read an object with its id
     * @param string $objectId The id of the object
     * 
     * @return mixed The object
     */
    public function read($objectId, $objectClass);
}
