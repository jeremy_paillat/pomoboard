<?php
/*
 * Copyright (C) 2015 Maarch
 *
 * This file is part of Laabs.
 *
 * Laabs is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Laabs is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with Laabs.  If not, see <http://www.gnu.org/licenses/>.
 */
namespace core\Kernel;

class StaticKernel
    extends AbstractKernel
{

    /* Methods */
    public static function run()
    {
        self::$instance->getRessource();

        self::$instance->sendResponse();

    }

    /**
     * undocumented function
     *
     * @return void
     * @author 
     **/
    protected function getRessource()
    {
        $publicRouter = new \core\Route\PublicRouter($this->request->uri);
        $resource = $publicRouter->resource;

        $this->response->setBody($resource->getContents());

        if ($this->response->mode == 'http') {
            $this->response->setContentType($resource->getMimetype());

            $this->response->setHeader("X-Laabs-Static-Resource", $resource->getPath());

            if (!$cacheControl = \core\Globals\Server::getCacheControl()) {
                $cacheControl = "public, max-age=3600";
            }

            $this->response->setCacheControl($cacheControl);
        }

    }

    /**
     * Send response to client
     * @access protected
     */
    protected function sendResponse()
    {
        // Buffer will return void if "LAABS_CLEAN_BUFFER" directive set for app
        $this->useBuffer();
        
        if (!is_scalar($this->response->body)) {
            throw new \Exception("Response content can not be displayed");
        }
        
        $this->response->send();
    }

}