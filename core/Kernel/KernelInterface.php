<?php
/*
 * Copyright (C) 2015 Maarch
 *
 * This file is part of Laabs.
 *
 * Laabs is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Laabs is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with Laabs.  If not, see <http://www.gnu.org/licenses/>.
 */
namespace core\Kernel;

interface KernelInterface
{

    /**
     * Start a new Kernel instance singleton
     * @param string $requestMode
     * @param string $requestType
     * @param string $responseType
     * @param string $responseLanguage
     * 
     * @throws Exception if a Kernel has already been started
     */
    public static function start($requestMode, $requestType, $responseType, $responseLanguage);

    /**
     * Get the Kernel instance singleton
     * @return object The Kernel object started
     * @throws Exception if no Kernel has been started yet
     */
    public static function get();

    /**
     * Run the kernel to process request, send headers if http mode and print resource
     */
    public static function run();

    /**
     * End the Kernel instance singleton to allow a new start (for batch process that will run several kernels)
     */
    public static function end();

}