<?php
/*
 * Copyright (C) 2015 Maarch
 *
 * This file is part of Laabs.
 *
 * Laabs is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Laabs is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with Laabs.  If not, see <http://www.gnu.org/licenses/>.
 */
namespace core\Kernel;
/**
 * Class Laabs Dynamic Kernel
 *
 * @extends core\Kernel\AbstractKernel
 */
class DynamicKernel
    extends AbstractKernel
{
    /* Constants */

    /* Properties */
    /**
     * The Route definition if found in Definitions
     * @var \core\Reflection\Route
     */
    public $requestRoute;

    /**
     * The Action router
     * @var \core\Route\ActionRouter
     */
    public $actionRouter;

    /**
     * The Input Router
     * @var \core\Route\InputRouter
     */
    public $inputRouter;

    /**
     * The Output Router
     * @var \core\Route\OutputRouter
     */
    public $outputRouter;

    /**
     * The arguments to pass to action during call
     * @var array
     */
    public $actionArguments = array();

    /**
     * The Return of the action
     * @var mixed
     */
    public $actionReturn;


    /* Methods */
    /**
     * Run the kernel to process request
     */
    public static function run()
    {
        /* Start Instance */
        \core\Instance::start();

        /* Start session */
        \core\Globals\Session::start();

        /* Initalize components app/dependecy/bundle */
        self::$instance ? self::$instance->initPackages() : null;

        /* Set event dispatcher and attach observers */
        self::$instance ? self::$instance->attachObservers() : null;

        \core\Observer\Dispatcher::notify(LAABS_REQUEST, self::$instance->request);

        \core\Observer\Dispatcher::notify(LAABS_RESPONSE, self::$instance->response);

        /* Establish routes (input, action, output) */
        self::$instance ? self::$instance->setRoutes() : null;

        // Parse request to get action call arguments from request content
        self::$instance ? self::$instance->parseRequest() : null;

        /* Call Action */
        try {
            self::$instance ? self::$instance->callAction() : null;
        } catch (\Exception $exception) {
            if (self::$instance) {
                if (!self::$instance->handleException($exception)) {
                    throw $exception;
                    //self::$instance->sendResponse();
                }
            } else {
                return;
            }
        }

        // Serialize response or exception
        self::$instance ? self::$instance->serializeResponse() : null;

        //Send response;
        self::$instance ? self::$instance->sendResponse() : null;

        // End instance
        \core\Instance::write_close();

    }

    /**
     * Set the action router for kernel
     * @access protected
     */
    protected function setRoutes()
    {
        // Find route in route definitions or return magic route based on fixed length uri (bundle/controller/action/parameters)
        $this->requestRoute = \laabs::route($this->request->method, $this->request->uri);

        \core\Observer\Dispatcher::notify(LAABS_REQUEST_ROUTE, $this->requestRoute);

        if (isset($this->requestRoute->prompt)) {
            $this->setOutputRoute();
        } else {
            $this->setInputRoute();
            $this->setActionRoute();
            $this->setOutputRoute();
        }

    }

    protected function setActionRoute()
    {
        if (isset($this->requestRoute->action)) {
            $this->actionRouter = new \core\Route\ActionRouter($this->requestRoute->action);
        } else {
            $this->actionRouter = new \core\Route\ActionRouter($this->requestRoute->getName());
        }

        \core\Observer\Dispatcher::notify(LAABS_ACTION_ROUTE, $this->actionRouter);
    }

    protected function setInputRoute()
    {
        if (isset($this->requestRoute->input)) {
            $this->inputRouter = new \core\Route\InputRouter($this->requestRoute->input, $this->request->type);
        } elseif (isset($this->requestRoute->action)) {
            try {
                $this->inputRouter = new \core\Route\InputRouter($this->requestRoute->action, $this->request->type);
            } catch (\Exception $e) {

            }
        } else {
            try {
                $this->inputRouter = new \core\Route\InputRouter($this->requestRoute->getName(), $this->request->type);
            } catch (\Exception $e) {

            }
        }

        \core\Observer\Dispatcher::notify(LAABS_INPUT_ROUTE, $this->inputRouter, $this->request);
    }

    protected function setOutputRoute()
    {
        try {
            $this->outputRouter = \laabs::getOutputRouter($this->requestRoute, $this->response->type);
        } catch (\Exception $e) {

        }

        \core\Observer\Dispatcher::notify(LAABS_OUTPUT_ROUTE, $this->outputRouter, $this->response);
    }

    /**
     * Parse current request body with parser
     * @access protected
     */
    protected function parseRequest()
    {
        if (isset($this->requestRoute->prompt)) {
            return;
        }

        if (isset($this->inputRouter)) {
            if ($this->response->mode == 'http') {
                $this->response->setHeader("X-Laabs-Parsed-By", $this->inputRouter->uri . "; type=" . $this->request->type);
            }
            $parser = $this->inputRouter->parser->newInstance();
            $requestArguments = array_merge(
                $this->request->arguments,
                $this->inputRouter->input->parse($parser, $this->request->body)
            );
        } else {
            if ($this->response->mode == 'http') {
                $this->response->setHeader("X-Laabs-Parsed-By", "php; type=" . $this->request->type);
            }
            $requestArguments = $this->request->arguments;
        }

        // Order arguments using route definition OR action parameters
        $parameters = $this->actionRouter->action->getParameters();

        foreach ($parameters as $parameter) {
            $paramType = $parameter->getType();
            $value = null;
            if (isset($this->requestRoute->variables[$parameter->name])) {
                $value = $this->requestRoute->variables[$parameter->name];
            } elseif (isset($requestArguments[$parameter->name])) {
                $value = $requestArguments[$parameter->name];
            }

            $this->actionArguments[$parameter->name] = $value;
        }
    }

    /**
     * Call current action with controller and set "return" property
     * @access protected
     */
    protected function callAction()
    {
        if ($this->requestRoute->prompt) {
            return;
        }
        
        if ($this->response->mode == 'http') {
            $this->response->setHeader("X-Laabs-Controlled-By", $this->actionRouter->uri);
        }

        $controller = $this->actionRouter->controller->newInstance();
        $this->actionReturn = $this->actionRouter->action->call($controller, $this->actionArguments);
    }

    /**
     * Handle Exception sent by action
     * @param \Exception $exception The Exception thrown by the Action
     *
     * @return bool
     */
    public function handleException(\Exception $exception)
    {

        \core\Observer\Dispatcher::notify(LAABS_BUSINESS_EXCEPTION, $exception);

        // Manage specific exception handler
        $exceptionClass = get_class($exception);
        $exceptionName = \laabs\basename($exceptionClass);
        if ($this->response->mode == 'http') {
            $this->response->setHeader("X-Laabs-Exception", $exceptionClass . "; " . str_replace("\n", " ", $exception->getMessage()));
        }

        // Try to find serializer output for the raised exception else send exception as string as response content
        try {
            if (isset($this->outputRouter)) {
                $outputUri = substr_replace($this->outputRouter->uri, $exceptionName, strrpos($this->outputRouter->uri, LAABS_URI_SEPARATOR)+1);
                $this->outputRouter = new \core\Route\OutputRouter($outputUri, $this->response->type);
                $this->actionReturn = $exception;
                
                return true;
            } else {
                $this->response->setBody((string) $exception);
            }
            
        } catch (\Exception $outputRouterException) {
            $this->response->setBody((string) $exception);

            return false;
        }
    }

    /**
     * Serialize current return to set response body with serializer
     * @access protected
     */
    protected function serializeResponse()
    {
        
        if (isset($this->outputRouter)) {
            if ($this->response->mode == 'http') {
                $this->response->setHeader("X-Laabs-Serialized-By", $this->outputRouter->uri . "; type=" . $this->response->type);
            }

            $serializer = $this->outputRouter->serializer->newInstance();

            $content = $this->outputRouter->output->serialize($serializer, $this->actionReturn, $this->response->language);

            $this->response->setBody($content);
        } else {
            if ($this->response->mode == 'http') {
                $this->response->setHeader("X-Laabs-Serialized-By", "*; type=" . $this->response->type);
            }
            $this->response->setBody($this->actionReturn);
        }
    }

    /**
     * Send response to client
     * @access protected
     */
    protected function sendResponse()
    {
        // Buffer will return void if "LAABS_CLEAN_BUFFER" directive set for app
        $this->useBuffer();

        if (!is_scalar($this->response->body)) {
            throw new \core\Exception("Response content can not be displayed");
        }

        $this->response->send();
    }

}