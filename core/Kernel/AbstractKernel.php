<?php
/*
 * Copyright (C) 2015 Maarch
 *
 * This file is part of Laabs.
 *
 * Laabs is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Laabs is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with Laabs.  If not, see <http://www.gnu.org/licenses/>.
 */
namespace core\Kernel;
/**
 * Abstract Class for Laabs Kernel Abstract
 *
 * @uses core\ReadonlyTrait
 */
abstract class AbstractKernel
    implements KernelInterface
{
    use \core\ReadonlyTrait;
    /* Constants */

    /* Properties */
    /**
     * The instanciated Kernel singleton
     * @access protected
     * @var object $instance
     */
    protected static $instance;

    /**
     * The associated log
     * @access protected
     * @var resource $log
     */
    protected static $log;

    /**
     * The laabs Request object
     * @var object $request
     */
    public $request;

    /**
     * The laabs Response object
     * @var object $response
     */
    public $response;

    /* Methods */
    /**
     * Start a new Kernel instance singleton
     * @param string $requestMode
     * @param string $requestType
     * @param string $responseType
     * @param string $responseLanguage
     *
     * @return object instance
     *
     * @throws Exception if a Kernel has already been started
     */
    public static function start($requestMode=false, $requestType=false, $responseType=false, $responseLanguage=false)
    {
        if (self::started()) {
            throw new Exception("Kernel already started.");
        }

        $log = \core\Globals\Server::getLog();
        if ($log == 'syslog') {
            openlog("LAABS " . \core\Globals\Server::getApp(), LOG_ODELAY, LOG_USER);
        } else {
            self::$log = fopen($log, "w");
        }

        $class = get_called_class();
        self::$instance = new $class($requestMode, $requestType, $responseType, $responseLanguage);

        return self::$instance;

    }

    /**
     * Check the Kernel instance singleton
     *
     * @return bool
     */
    public static function started()
    {
        return self::$instance != null;
    }

    /**
     * End the Kernel instance singleton to allow a new start (for batch process that will run several kernels)
     */
    public static function end()
    {
        if (!self::started()) {
            //throw new Exception("Kernel is not started.");
            return;
        }

        static::$instance = null;

        switch(\core\Globals\Server::getBufferMode()) {
            case LAABS_BUFFER_NONE:
                break;

            case LAABS_BUFFER_GET:
                ob_end_flush();
                break;

            case LAABS_BUFFER_CLEAN:
                ob_end_clean();
                break;
        }

        if (self::$log) {
            fclose(self::$log);
        } else {
            closelog();
        }
    }

    /**
     * Get the Kernel instance singleton
     * @return object The Kernel object started
     * @throws Exception if no Kernel has been started yet
     */
    public static function get()
    {
        if (self::started()) {
            return self::$instance;
        }

        throw new Exception("Kernel has no been started.");
    }

    /**
     * Write into log
     * 
     * WIN : 1 error, 2.. Warning, other::info
     * 
     * @param string $message  The message
     * @param int    $priority The proprity 
     */
    public static function log($message, $priority=6)
    {
        if (isset(self::$log)) {
            fwrite(self::$log, "[" . \laabs\date() . "] (" . $priority . ') ' . $message . PHP_EOL);
        } else {
            syslog($priority, $message);
        }
    }



    /**
     * Constructor for a new Kernel instance singleton
     *  * Initalise PHP
     *  * Instanciates the Request object
     *  * Instanciates the Response object
     * @param string $requestMode
     * @param string $requestType
     * @param string $responseType
     * @param string $responseLanguage
     */
    protected function __construct($requestMode=false, $requestType=false, $responseType=false, $responseLanguage=false)
    {
        // Change current dir to src
        chdir('../src');
        
        /* Start buffer output if clean buffer requested */
        if (\core\Globals\Server::getBufferMode() != LAABS_BUFFER_NONE) {
            $callback = \core\Globals\Server::getBufferCallback();
            ob_start($callback);
        }

        /* Define constants from definitions */
        $this->defineConstants();

        /* Initialize PHP */
        $this->initPhp();

        $this->getRequest($requestMode, $requestType);

        $this->getResponse($responseType, $responseLanguage);
    }

    /**
     *  Check existence of a constants.php file at root of app/dependecies/bundles
     */
    protected function defineConstants()
    {
        $appFile = ".." . DIRECTORY_SEPARATOR . LAABS_APP . DIRECTORY_SEPARATOR . \core\Globals\Server::getApp() . DIRECTORY_SEPARATOR . 'constants.php';
        if (is_file($appFile)) {
            require_once($appFile);
        }
        
        foreach (\core\Globals\Server::getDependencies() as $dependency) {
            $dependencyFile = ".." . DIRECTORY_SEPARATOR . LAABS_DEPENDENCY . DIRECTORY_SEPARATOR . $dependency . DIRECTORY_SEPARATOR . 'constants.php';
            if (is_file($dependencyFile)) {
                require_once($dependencyFile);
            }
        }

        foreach (\core\Globals\Server::getBundles() as $bundle) {
            $bundleFiles = \core\Reflection\Extensions::extendedPath($bundle . DIRECTORY_SEPARATOR . 'constants.php', $unique = false);
            foreach ($bundleFiles as $bundleFile) {
                require_once($bundleFile);
            }
        }
    }

    /**
     *  Sets PHP initialization directives from optional ini file
     *  @throws Exception if one of the directives can not be set
     */
    protected function initPhp()
    {
        if (!($phpIni = \core\Globals\Server::getPhpConfiguration())) {
            return;
        }

        if (!is_file($phpIni)) {
            throw new \core\Exception("Php runtime configuration file $phpIni not found");
        }

        $phpConf = parse_ini_file($phpIni, false);

        if (count($phpConf) == 0) {
            return;
        }

        foreach ($phpConf as $varname => $newvalue) {
            if (\ini_set($varname, $newvalue) === false) {
                throw new \core\Exception("Could not set directive '$varname' to '$newvalue'. Possible reasons are : variable name is invalid, new value is invalid or the variable doesn't accept this change mode. Check PHP documentation at www.php.net/manual/en/ini.list.php");
            }
        }
    }

    /**
     *  Check existence of a config.inc.php file at root of app/dependecies/bundles
     */
    protected function initPackages()  
    {
        $appFile = ".." . DIRECTORY_SEPARATOR . LAABS_APP . DIRECTORY_SEPARATOR . \core\Globals\Server::getApp() . DIRECTORY_SEPARATOR . 'init.php';
        if (is_file($appFile)) {
            require_once($appFile);
        }
        
        foreach (\core\Globals\Server::getDependencies() as $dependency) {
            $dependencyFile = ".." . DIRECTORY_SEPARATOR . LAABS_DEPENDENCY . DIRECTORY_SEPARATOR . $dependency . DIRECTORY_SEPARATOR . 'init.php';
            if (is_file($dependencyFile)) {
                require_once($dependencyFile);
            }
        }

        foreach (\core\Globals\Server::getBundles() as $bundle) {
            $bundleFiles = \core\Reflection\Extensions::extendedPath($bundle . DIRECTORY_SEPARATOR . 'init.php', $unique = false);
            foreach ($bundleFiles as $bundleFile) {
                require_once($bundleFile);
            }
        }
    }

    /**
     * Create the Request object
     * @param string $requestMode The code of request mode to create (http/cli)
     * @param string $requestType The contentType definition code used in request
     */
    protected function getRequest($requestMode=false, $requestType=false)
    {
        /* Get Request Mode cli / http */
        if (!$requestMode) {
            $requestMode = $this->guessRequestMode();
        }

        switch($requestMode) {
            case 'http':
                $this->request = new \core\Request\HttpRequest();
                break;

            case 'cli':
                $this->request = new \core\Request\CliRequest();
                break;
        }

    }

    /**
     * Create the response object
     * @param string $responseType     The contentType definition code used for response
     * @param string $responseLanguage The ContentLanguage definition code for response
     */
    protected function getResponse($responseType=false, $responseLanguage=false)
    {
        switch($this->request->mode) {
            case 'http':
                $this->response = new \core\Response\HttpResponse();

                break;
            case 'cli':
                $this->response = new \core\Response\CliResponse();
                break;
        }

        /* Get response type */
        if (!$responseType) {
            $this->guessResponseType();
        } else {
            $this->response->setType($responseType);
            $mimeType = array_search($responseType, \core\Globals\Server::getContentTypes());
        }

        /* Get response lang */
        if (!$responseLanguage) {
            $responseLanguage = $this->guessResponseLanguage();
        }
        $this->response->setLanguage($responseLanguage);

        \core\Observer\Dispatcher::notify(LAABS_RESPONSE, $this->response);
    }

    /**
     * Guess the request mode (Http/Cli)
     * @return string The request mode "cli" or "http"
     */
    protected function guessRequestMode()
    {
        if (php_sapi_name() == 'cli'
        || array_key_exists('SHELL', $_ENV)
        || !isset($_SERVER['REQUEST_METHOD'])
        || defined('STDIN')
        ) {
            return 'cli';
        } else {
            return 'http';
        }
    }


    /**
     * Guess the response content type from the request "Accept"
     * @return string The ContentType code or "text" as a default value
     */
    protected function guessResponseType()
    {
        $contentTypes = \core\Globals\Server::getContentTypes();
        $httpAccept = \core\Globals\Server::getHttpAccept();

        foreach ($httpAccept as $mimetype => $priority) {
            if (isset($contentTypes[$mimetype])) {
                $this->response->setType($contentTypes[$mimetype]);
                if ($this->response->mode == 'http') {
                    $this->response->setContentType($mimetype);
                }

                return;
            }
        }
    }

    /**
     * Guess the response content language from the request "AcceptLanguage"
     * @return string The ContentLanguage code or "en" as a default value
     */
    protected function guessResponseLanguage()
    {
        $contentLanguages = \core\Globals\Server::getContentLanguages();
        $httpAcceptLang = \core\Globals\Server::getHttpAcceptLanguage();
        foreach ($httpAcceptLang as $locale => $proprity) {
            if (isset($contentLanguages[$locale])) {
                return $contentLanguages[$locale];
            }
        }

        return 'en';
    }

    /**
     * Use the output buffer if requested
     *
     * @return void
     * @author
     **/
    protected function useBuffer()
    {
        $buffer = false;
        switch(\core\Globals\Server::getBufferMode()) {
            case LAABS_BUFFER_NONE:
                break;

            case LAABS_BUFFER_GET:
                $this->response->setBody(ob_get_clean() . $this->response->body);
                break;

            case LAABS_BUFFER_CLEAN:
                ob_clean();
                break;
        }
    }

    /**
     * Attach the observers to event dispatcher
     * @access protected
     */
    protected function attachObservers()
    {
        foreach (\laabs::observers() as $observer) {

            $observerObject = $observer->newInstance();
            
            foreach ($observer->getHandlers() as $handler) {
                \core\Observer\Dispatcher::attach(
                    $observerObject,
                    $handler->name,
                    $handler->subject
                );
            }
        }
    }

}