<?php
/*
 * Copyright (C) 2015 Maarch
 *
 * This file is part of Laabs.
 *
 * Laabs is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Laabs is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with Laabs.  If not, see <http://www.gnu.org/licenses/>.
 */
namespace core\Route;
/**
 * undocumented class
 *
 * @package Core
 * @author  Cyril Vazquez <cyrilvazquez@maarch.org>
 **/ 
class ResourceRouter
    extends ContainerRouter
{
    /* Constants */
    const PREFIX = null;

    /* Properties */
    public $resource;

    /* Methods */
    /**
     * undocumented function
     * @param string $uri The uri of resource
     * 
     * @return void
     **/
    public function __construct($uri) 
    {
        parent::__construct($uri);

        $path = array();
        if (static::PREFIX) {
            $path[] = static::PREFIX;
        }
        while ($step = array_shift($this->steps)) {
            $path[] = $step;
        }

        $filename = implode(LAABS_URI_SEPARATOR, $path);

        $this->resource = $this->container->getResource($filename);
    }

    /**
     * undocumented function
     *
     * @return \core\Resource
     * @author 
     **/
    public function getResource()
    {
        return $this->resource;
    }

}