<?php
/*
 * Copyright (C) 2015 Maarch
 *
 * This file is part of Laabs.
 *
 * Laabs is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Laabs is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with Laabs.  If not, see <http://www.gnu.org/licenses/>.
 */
namespace core\Route;

class ActionRouter
    extends BundleRouter
{
    /* Properties */
    public $controller;

    public $action;

    public $parameters;

    /* Methods */
    public function __construct($uri)
    {
        parent::__construct($uri);

        if (!$controller = array_shift($this->steps)) {
            throw new Exception("Invalid action route: no controller name");
        }
        $this->setController($controller);

        if (!$action = array_shift($this->steps)) {
            throw new Exception("Invalid action route: no action name");
        }

        $this->setAction($action);
    }

    public function setController($name)
    {
        if ($this->bundle->hasController($name)) {
            $this->controller = $this->bundle->getController($name);
        } else {
            throw new Exception("Undefined controller '$name' in action route '$this->uri'");
        }
    }

    public function setAction($name) 
    {
        if ($this->controller->hasAction($name)) {
            $this->action = $this->controller->getAction($name);
        } else {
            throw new Exception("Undefined action '$name' in action route '$this->uri'");
        }
    }

}