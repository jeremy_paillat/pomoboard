<?php
/*
 * Copyright (C) 2015 Maarch
 *
 * This file is part of Laabs.
 *
 * Laabs is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Laabs is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with Laabs.  If not, see <http://www.gnu.org/licenses/>.
 */
namespace core\Route;

class ObserverRouter
    extends ContainerRouter
{
    /* Properties */

    public $observer;

    public $handler;

    /* Methods */
    public function __construct($uri)
    {
        parent::__construct($uri);

        $handler = array_pop($this->steps);

        if($this->container instanceof \core\Reflection\Bundle) {
            $this->setObserver(LAABS_OBSERVER . LAABS_URI_SEPARATOR . implode(LAABS_URI_SEPARATOR, $this->steps));
        } else {
            $this->setObserver(implode(LAABS_URI_SEPARATOR, $this->steps));
        }

        $this->setHandler($handler);
    }

    protected function setObserver($name)
    {
        if ($this->container->hasService($name)) {
            $this->observer = $this->container->getService($name);
        } else {
            throw new Exception("Undefined observer '$name' in observation route '$this->uri'");
        }
    }

    protected function setHandler($name)
    {
        if ($this->observer->hasMethod($name)) {
           $this->handler = $this->observer->getMethod($name);
        } else {
            throw new Exception("Undefined handler '$name' in observation route '$this->uri'");
        }
    }

}