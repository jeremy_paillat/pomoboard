<?php
/*
 * Copyright (C) 2015 Maarch
 *
 * This file is part of Laabs.
 *
 * Laabs is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Laabs is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with Laabs.  If not, see <http://www.gnu.org/licenses/>.
 */
namespace core\Route;

class ServiceRouter
    extends ContainerRouter
{
    /* Properties */
    public $service;

    /* Methods */
    public function __construct($uri)
    {
        parent::__construct($uri);

        $this->setService(implode(LAABS_URI_SEPARATOR, $this->steps));
    }

    public function setService($name)
    {
        if ($this->container->hasService($name)) {
            $this->service = $this->container->getService($name);
        } else {
            throw new Exception("Undefined service '$name' in service route '$this->uri'");
        }

    }

}