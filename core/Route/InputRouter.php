<?php
/*
 * Copyright (C) 2015 Maarch
 *
 * This file is part of Laabs.
 *
 * Laabs is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Laabs is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with Laabs.  If not, see <http://www.gnu.org/licenses/>.
 */
namespace core\Route;

class InputRouter
    extends BundleRouter
{
    /* Properties */
    public $parser;
    
    public $input;

    /* Methods */
    public function __construct($route, $type)
    {
        parent::__construct($route);

        if (!$parser = array_shift($this->steps)) {
            throw new Exception("Invalid input route: no parser name");
        }
        $this->setParser($parser, $type);

        if (!$input = array_shift($this->steps)) {
            throw new Exception("Invalid input route: no input name");
        }
        $this->setInput($input);

    }

    public function setParser($name, $type)
    {
        if ($this->bundle->hasParser($name, $type)) {
            $this->parser = $this->bundle->getParser($name, $type);
        } else {
            throw new Exception("Undefined parser '$name' in input route '$this->uri'");
        }
    }

    public function setInput($name)
    {
        if ($this->parser->hasInput($name)) {
            $this->input = $this->parser->getInput($name);
        } else {
            throw new Exception("Undefined input '$name' in input route '$this->uri'");
        }
    }

}