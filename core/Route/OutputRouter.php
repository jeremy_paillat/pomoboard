<?php
/*
 * Copyright (C) 2015 Maarch
 *
 * This file is part of Laabs.
 *
 * Laabs is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Laabs is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with Laabs.  If not, see <http://www.gnu.org/licenses/>.
 */
namespace core\Route;

class OutputRouter
    extends BundleRouter
{
    /* Properties */
    public $serializer;
    public $output;

    /* Methods */
    public function __construct($route, $type)
    {
        parent::__construct($route);

        if (!$serializer = array_shift($this->steps)) {
            throw new Exception("Invalid output route: no serializer name");
        }
        $this->setSerializer($serializer, $type);

        if (!$output = array_shift($this->steps)) {
            throw new Exception("Invalid output route: no output name");
        }
        $this->setOutput($output);
    }

    public function setSerializer($name, $type)
    {
        if ($this->bundle->hasSerializer($name, $type)) {
            $this->serializer = $this->bundle->getSerializer($name, $type);
        } else {
            throw new Exception("Undefined serializer '$type/$name' in output route '$this->uri'");
        }

    }

    public function setOutput($name) 
    {
        if ($this->serializer->hasOutput($name)) {
            $this->output = $this->serializer->getOutput($name);
        } else {
            throw new Exception("Undefined output '$name' in output route '$this->uri'");
        }
    }

}