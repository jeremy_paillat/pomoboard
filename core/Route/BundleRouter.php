<?php
/*
 * Copyright (C) 2015 Maarch
 *
 * This file is part of Laabs.
 *
 * Laabs is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Laabs is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with Laabs.  If not, see <http://www.gnu.org/licenses/>.
 */
namespace core\Route;

class BundleRouter
    extends AbstractRouter
{
    /* Properties */
    public $bundle;

    /* Methods */
    public function __construct($uri)
    {
        parent::__construct($uri);

        $bundle = array_shift($this->steps);
        $this->setBundle($bundle);
    }

    public function setBundle($name)
    {
        if (\core\Globals\Server::hasBundle($name)) {
            $this->bundle = \core\Reflection\Bundle::getInstance($name);
        } else {
            throw new Exception("Undefined bundle '$name' in uri '$this->uri'");
        }
    }

}