<?php
/*
 * Copyright (C) 2015 Maarch
 *
 * This file is part of Laabs.
 *
 * Laabs is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Laabs is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with Laabs.  If not, see <http://www.gnu.org/licenses/>.
 */
namespace core\Route;

abstract class AbstractRouter
{
    use \core\ReadonlyTrait;

    /* Properties */
    public $uri;

    public $reroutedUri;

    protected $steps;

    /* Methods */
    public function __construct($uri)
    {
        $this->uri = $uri;
        $this->steps = \laabs\explode(LAABS_URI_SEPARATOR, $uri);
    }

    public function reroute($uri)
    {
        $this->reroutedUri = $this->uri;

        $this->__construct($uri);
    }

}