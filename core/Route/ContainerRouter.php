<?php
/*
 * Copyright (C) 2015 Maarch
 *
 * This file is part of Laabs.
 *
 * Laabs is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Laabs is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with Laabs.  If not, see <http://www.gnu.org/licenses/>.
 */
namespace core\Route;

class ContainerRouter
    extends AbstractRouter
{
    /* Properties */
    public $container;

    /* Methods */
    public function __construct($uri)
    {
        parent::__construct($uri);

        $root = array_shift($this->steps);

        switch ($root) {
            case LAABS_DEPENDENCY :
                $dependency = array_shift($this->steps);
                if (!\core\Globals\Server::hasDependency($dependency)) {
                    throw new \core\Exception("Error on service container route '$uri ': Dependency $dependency is not activated.");
                }
                $container = \core\Reflection\Dependency::getInstance($dependency);
                break;

            case LAABS_APP :
                $app = array_shift($this->steps);
                if (\core\Globals\Server::getApp() != $app) {
                    throw new \core\Exception("Error on service container route '$uri ': $app is not the currently configured app.");
                }
                $container = \core\Reflection\App::getInstance();
                break;

            default:
                $bundle = $root;
                if (!\core\Globals\Server::hasBundle($bundle)) {
                    throw new \core\Exception("Error on service container route '$uri ': Bundle $bundle is not activated.");
                }
                $container = \core\Reflection\Bundle::getInstance($bundle);
        }

        // Forward container (bundle/container/app) to the next in uri (if dependency only)
        while (($step = array_shift($this->steps)) == LAABS_DEPENDENCY) {
            $dependency = array_shift($this->steps);
            if (!\core\Globals\Server::hasDependency($dependency)) {
                throw new \core\Exception("Error on service container route '$uri ': Dependency $dependency is not activated.");
            }
            $container = \core\Reflection\Dependency::getInstance($dependency, $container);
        }

        $this->container = $container;

        // Unshift the last non dependency step
        array_unshift($this->steps, $step);
    }

}