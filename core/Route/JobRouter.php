<?php
/*
 * Copyright (C) 2015 Maarch
 *
 * This file is part of Laabs.
 *
 * Laabs is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Laabs is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with Laabs.  If not, see <http://www.gnu.org/licenses/>.
 */
namespace core\Route;

class JobRouter
    extends ContainerRouter
{
    /* Properties */

    public $job;

    /* Methods */
    public function __construct($uri)
    {
        parent::__construct($uri);

        $jobName = array_shift($this->steps);

        $this->setJob($jobName);
    }

    protected function setJob($name)
    {
        if ($this->container->hasJob($name)) {
            $this->job = $this->container->getJob($name);
        } else {
            throw new Exception("Undefined job '$name' in route '$this->uri'");
        }
    }

}