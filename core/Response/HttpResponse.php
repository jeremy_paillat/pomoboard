<?php
/*
 * Copyright (C) 2015 Maarch
 *
 * This file is part of Laabs.
 *
 * Laabs is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Laabs is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with Laabs.  If not, see <http://www.gnu.org/licenses/>.
 */
namespace core\Response;
/**
 * Http response 
 */
class HttpResponse
    extends AbstractResponse
{
    /* -------------------------------------------------------------------------
    - Constants
    ------------------------------------------------------------------------- */
    /* Status 1 - Information */
    /* Status 2 - Success */
    /* Status 3 - Redirection */
    /* Status 4 - Client Error */
    /* Status 5 - Server Error */

    /* -------------------------------------------------------------------------
    - Properties
    ------------------------------------------------------------------------- */

    /* -------------------------------------------------------------------------
    - Methods
    ------------------------------------------------------------------------- */
    public function __construct()
    {
        $this->mode = 'http';
        $this->type = 'html';
    }

    /* Http Response Code */
    public function getHeader($name)
    {
        if (isset($this->headers[$name])) {
            return $this->headers[$name];
        }
    }

    public function setHeader($name, $value=null, $replace=true) 
    {
        if (is_null($value)) {
            if (isset($this->headers[$name])) {
                unset($this->headers[$name]);
            }

            return true;
        }

        if (!$replace && isset($this->headers[$name])) {
            return false;
        }

        $this->headers[$name] = $value;

        return true;
    }

    public function setContentType($contentType)
    {
        $this->setHeader('Content-Type', $contentType);
    }

    public function getContentType()
    {
        return $this->getHeader('Content-Type');
    }

    public function guessContentType()
    {
        $finfo = new \finfo();
        $type = $finfo->buffer($this->body, FILEINFO_MIME_TYPE);

        if (strtok($type, "/") == "text") {
            switch(strtolower($this->type)) {
                case 'css':
                case 'less':
                    $type = "text/css";
                    break;

                case 'js':
                    $type = "application/javascript";
                    break;

                case 'csv':
                    $type = "text/csv";
                    break;
            }
        }


        $encoding = $finfo->buffer($this->body, FILEINFO_MIME_ENCODING);
        $contentType = $type . "; charset=" . $encoding;
        $this->setContentType($contentType);
    }

    public function setGzip($bool)
    {
        if ($bool) {
            $this->setHeader('Content-Encoding', 'gzip');
        } else {
            $this->setHeader('Content-Encoding');
        }
    }

    public function getGzip()
    {
        return $this->getHeader('Content-Encoding');
    }

    public function setCacheControl($control, $maxAge = null, $mustRevalidate = true)
    {
        $value = $control;
        if ($maxAge !== null) {
            $value .= ", max-age=$max-age";
        }
        if ($mustRevalidate) {
            $value .= ", must-revalidate";
        }

        $this->setHeader('Cache-Control', $value);
    }

    public function getCacheControl()
    {
        return $this->getHeader('Cache-Control');
    }

    /* Send */
    public function send()
    {
        http_response_code($this->code);

        if(!headers_sent()) {
            $this->setHeader('Content-Length', mb_strlen($this->body));
        
            if (!isset($this->headers['Content-Type'])) {
                $this->guessContentType();
            }
            $this->setHeader('X-Laabs-Content-Type', $this->type);
            $this->setHeader('X-Laabs-Content-Language', $this->language);
        
            foreach ($this->headers as $field => $value) {
                header($field . ": " . $value);
            }
        }
        echo $this->body . PHP_EOL;
    }

}