<?php
/*
 * Copyright (C) 2015 Maarch
 *
 * This file is part of Laabs.
 *
 * Laabs is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Laabs is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with Laabs.  If not, see <http://www.gnu.org/licenses/>.
 */
namespace core\Response;

abstract class AbstractResponse
    implements ResponseInterface
{
    use \core\ReadonlyTrait;

    /* -------------------------------------------------------------------------
    - Constants
    ------------------------------------------------------------------------- */

    /* -------------------------------------------------------------------------
    - Properties
    ------------------------------------------------------------------------- */
    public $mode;

    public $code;

    public $type;

    public $language;

    public $headers;

    public $body;

    /* -------------------------------------------------------------------------
    - Methods
    ------------------------------------------------------------------------- */
    public function setMode($mode) 
    {
        $this->mode = $mode;
    }

    public function setCode($code) 
    {
        $this->code = $code;
    }

    public function setType($type)
    {
        $this->type = $type;
    }

    public function setLanguage($language)
    {
        $this->language = $language;
    }

    public function setBody($body)
    {
        $this->body = $body;
    }

    public function send()
    {
        echo $this->body . PHP_EOL;
    }

}