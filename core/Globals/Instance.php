<?php
/*
 * Copyright (C) 2015 Maarch
 *
 * This file is part of Laabs.
 *
 * Laabs is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Laabs is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with Laabs.  If not, see <http://www.gnu.org/licenses/>.
 */
namespace core\Globals;

class Instance
    extends AbstractGlobal
{
    /* Constants */
    const NAME = 'INSTANCE';

    /* Properties */

    /* Methods */
    public static function start($init = false) 
    {
        \core\Instance::start();

        if ($init) {
            self::init();
        }
    }

    public static function init()
    {
        $GLOBALS['INSTANCE'] = array();
    }

    public static function setClass($class)
    {
        $GLOBALS['INSTANCE']['laabs']['class'][$class->getName()] = $class;
        $GLOBALS['INSTANCE']['laabs']['property'][$class->getName()] = $class->getProperties();
    }

    public static function getClass($classname)
    {
        if (isset($GLOBALS['INSTANCE']['laabs']['class'][$classname])) {
            return $GLOBALS['INSTANCE']['laabs']['class'][$classname];
        }
    }

    public static function hasClass($classname)
    {
        return (isset($GLOBALS['INSTANCE']['laabs']['class'][$classname]));
    }

    public static function getProperty($classname, $propertyname)
    {
        if (isset($GLOBALS['INSTANCE']['laabs']['property'][$classname][$propertyname])) {
            return $GLOBALS['INSTANCE']['laabs']['property'][$classname][$propertyname];
        }
    }

    public static function hasProperties($classname)
    {
        return (isset($GLOBALS['INSTANCE']['laabs']['property'][$classname]));
    }

    public static function getProperties($classname)
    {
        if (isset($GLOBALS['INSTANCE']['laabs']['property'][$classname])) {
            return $GLOBALS['INSTANCE']['laabs']['property'][$classname];
        }
    }
}