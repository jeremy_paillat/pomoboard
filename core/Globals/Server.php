<?php
/*
 * Copyright (C) 2015 Maarch
 *
 * This file is part of Laabs.
 *
 * Laabs is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Laabs is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with Laabs.  If not, see <http://www.gnu.org/licenses/>.
 */
namespace core\Globals;
/**
 * This class provides static methods to access PHP superglobal $_SERVER and parse specific values
 * 
 * @package Core
 */
class Server
    extends AbstractGlobal
{

    /* Constants */
    const NAME = "_SERVER";

    /* Properties */
    protected static $_SERVER = array();

    /* Methods */
    /* Boolean value retrieval
        <<< name
        >>> bool
    */
    /**
     * Checks if the app instance storage is disabled in server configuration
     *
     * @return bool
     */
    public static function instanceDisable()
    {
        return isset($_SERVER['LAABS_INSTANCE_DISABLE']);
    }

    /**
     * Checks if the php session is disabled in server configuration
     *
     * @return bool
     */
    public static function sessionDisable()
    {
        return isset($_SERVER['LAABS_SESSION_DISABLE']);
    }

    /**
     * Checks if the output buffer should be cleaned up before the kernel sends the output
     *
     * @return bool
     */
    public static function getBufferMode()
    {
        if (isset($_SERVER['LAABS_BUFFER_MODE'])) {
            $_SERVER['LAABS_BUFFER_MODE'];
        }

        return LAABS_BUFFER_NONE;
    }

    /* Simple value retrieval
        <<< value
        >>> value
    */
    /**
     * Get the name of the app
     *
     * @return string
     * @throws \core\Exception if no name of app provided on the configuration
     */
    public static function getApp()
    {
        if (isset($_SERVER['LAABS_APP'])) {
            return $_SERVER['LAABS_APP'];
        }

        throw new \core\Exception("Invalid host configuration: no application name provided");
    }

    /**
     * Get the http method used (GET | POST | PUT | DELETE)
     *
     * @return string
     */
    public static function getHttpMethod()
    {
        if (isset($_SERVER['REQUEST_METHOD'])) {
            return $_SERVER['REQUEST_METHOD'];
        }

        return 'GET';
    }

    /**
     * Get the additional php configuration file for runtime directives
     *
     * @return string
     */
    public static function getPhpConfiguration()
    {
        if (isset($_SERVER['LAABS_PHP_INI'])) {
            return $_SERVER['LAABS_PHP_INI'];
        }
    }

    /**
     * Get the configuration file name
     *
     * @return string
     */
    public static function getConfiguration()
    {
        if (isset($_SERVER['LAABS_CONFIGURATION'])) {
            return $_SERVER['LAABS_CONFIGURATION'];
        }

        return ".." . DIRECTORY_SEPARATOR . LAABS_APP . DIRECTORY_SEPARATOR . self::getApp() . DIRECTORY_SEPARATOR . 'configuration.ini';
    }

    /**
     * Get the authentication information 
     * @return array
     */
    public static function getAuthentication()
    {
        if (isset($_SERVER['PHP_AUTH_USER'])) {
            return new \core\Request\basicAuthentication($_SERVER['PHP_AUTH_USER'], $_SERVER['PHP_AUTH_PW']);
        }

        if (isset($_SERVER['PHP_AUTH_DIGEST'])) {
            $neededParts = array('nonce'=>1, 'nc'=>1, 'cnonce'=>1, 'qop'=>1, 'username'=>1, 'uri'=>1, 'response'=>1);
            $data = array();
            $keys = implode('|', array_keys($neededParts));
         
            preg_match_all('@(' . $keys . ')=(?:([\'"])([^\2]+?)\2|([^\s,]+))@', $_SERVER['PHP_AUTH_DIGEST'], $matches, PREG_SET_ORDER);

            foreach ($matches as $match) {
                $data[$match[1]] = $match[3] ? $match[3] : $match[4];
                unset($neededParts[$match[1]]);
            }

            return new \core\Request\digestAuthentication($data['username'], $data['nonce'], $data['uri'], $data['response'], $data['qop'], $data['nc'], $data['cnonce']);
        }

        if (isset($_COOKIE['LAABS_AUTH_TOKEN'])) {
            return new \core\Request\tokenAuthentication();
        }

        //setcookie('LAABS_AUTH', 'value', $expire=0, $path="/", $domain=$_SERVER['HTTP_HOST'], $secure=false, $httponly=true);
        //var_dump($_COOKIE);

        return new \core\Request\appAuthentication();
    }

    /**
     * Checks if a bundle is enabled or not
     * @param string $bundle The name of bundle to check
     *
     * @return bool
     */
    public static function hasBundle($bundle)
    {
        if (in_array($bundle, self::getBundles())) {
            return true;
        }
    }

    /**
     * Checks if a dependency is enabled or not
     * @param string $dependency The name of dependency to check
     *
     * @return bool
     */
    public static function hasDependency($dependency)
    {
        if (in_array($dependency, self::getDependencies())) {
            return true;
        }
    }

    /**
     * Get the Http cache control default value
     *
     * @return string
     */
    public static function getCacheControl()
    {
        if (isset($_SERVER['LAABS_CACHE_CONTROL'])) {
            return $_SERVER['LAABS_CACHE_CONTROL'];
        }
    }

    /**
     * Get the last request time
     * @param bool $asFloat Get the time as float or int
     *
     * @return mixed The request time
     */
    public static function getRequestTime($asFloat=false)
    {
        if ($asFloat) {
            return $_SERVER['REQUEST_TIME_FLOAT'];
        } else {
            return $_SERVER['REQUEST_TIME'];
        }
    }

    /**
     * Get the app instance storage path
     * 
     * @return string
     */
    public static function getInstanceSavePath()
    {
        if (isset($_SERVER['LAABS_INSTANCE_SAVE_PATH'])) {
            return $_SERVER['LAABS_INSTANCE_SAVE_PATH'];
        }

        if ($sessionSavePath = \session_save_path()) {
            return $sessionSavePath;
        }

        if ($tmpDir = \sys_get_temp_dir()) {
            return $tmpDir;
        }
    }

    /**
     * Get the app instance handler class
     * 
     * @return string
     */
    public static function getInstanceHandler()
    {
        if (isset($_SERVER['LAABS_INSTANCE_HANDLER'])) {
            return $_SERVER['LAABS_INSTANCE_HANDLER'];
        }

        return "\core\Repository\php";
    }
    
    /**
     * Get the buffer control callback function
     * 
     * @return string
     */
    public static function getBufferCallback()
    {
        if (isset($_SERVER['LAABS_BUFFER_CALLBACK'])) {
            return $_SERVER['LAABS_BUFFER_CALLBACK'];
        }
        
        //return "ob_gzhandler";
    }

    /**
     * Get the log path
     * 
     * @return string
     */
    public static function getLog()
    {
        if (isset($_SERVER['LAABS_LOG'])) {
            return $_SERVER['LAABS_LOG'];
        }

        if ($phplog = \ini_get('error_log')) {
            if ($phplog != 'syslog') {
                return dirname($phplog) . DIRECTORY_SEPARATOR . 'laabs_log';
            }
        }

        return 'syslog';
    }

    /**
     * Get the date format
     * 
     * @return string
     */
    public static function getDateFormat()
    {
        if (isset($_SERVER['LAABS_DATE_FORMAT'])) {
            return $_SERVER['LAABS_DATE_FORMAT'];
        }

        return 'Y-m-d';
    }

    /**
     * Get the timestamp format
     * 
     * @return string
     */
    public static function getTimestampFormat()
    {
        if (isset($_SERVER['LAABS_TIMESTAMP_FORMAT'])) {
            return $_SERVER['LAABS_TIMESTAMP_FORMAT'];
        }

        return 'Y-m-d\TH:i:s.u\Z';
    }

    /**
     * Get the decimal positions for number format
     * 
     * @return integer
     */
    public static function getNumberDecimals()
    {
        if (isset($_SERVER['LAABS_NUMBER_DECIMALS'])) {
            return (integer) $_SERVER['LAABS_NUMBER_DECIMALS'];
        }

        return false;
    }

    /**
     * Get the decimal separator for number format
     * 
     * @return string
     */
    public static function getNumberDecimalSeparator()
    {
        if (isset($_SERVER['LAABS_NUMBER_DECIMAL_SEPARATOR'])) {
            return (string) $_SERVER['LAABS_NUMBER_DECIMAL_SEPARATOR'];
        }

        return ".";
    }

    /**
     * Get the thousand separator for number format
     * 
     * @return string
     */
    public static function getNumberThousandsSeparator()
    {
        if (isset($_SERVER['LAABS_NUMBER_THOUSANDS_SEPARATOR'])) {
            return (string) $_SERVER['LAABS_NUMBER_THOUSANDS_SEPARATOR'];
        }

        return "";
    }

    /**
     * Get the XML encoding for xml type
     * 
     * @return string
     */
    public static function getXmlEncoding()
    {
        if (isset($_SERVER['LAABS_XML_ENCODING'])) {
            return (string) $_SERVER['LAABS_XML_ENCODING'];
        }

        return "utf-8";
    }

    /**
     * Get the empty route path for READ /
     * 
     * @return string
     */
    public static function getDefaultUri()
    {
        if (isset($_SERVER['LAABS_DEFAULT_URI'])) {
            return (string) $_SERVER['LAABS_DEFAULT_URI'];
        }
    }

    /**
     * Get the route for uncaught exceptions and errors
     * 
     * @return string
     */
    public static function getErrorUri()
    {
        if (isset($_SERVER['LAABS_ERROR_URI'])) {
            return (string) $_SERVER['LAABS_ERROR_URI'];
        }
    }

    /**
     * Get the tmp dir
     * 
     * @return string
     */
    public static function getTmpDir()
    {
        if (isset($_SERVER['LAABS_TMP_DIR'])) {
            $tmpDir = $_SERVER['LAABS_TMP_DIR'];
        } else {
            $tmpDir = \sys_get_temp_dir();
        }

        if (!is_dir($tmpDir)) {
            mkdir($tmpDir, 755, true);
        }

        return $tmpDir;
    }

    /* Array retrieval
        Get an indexed array of strings from separated string
        <<< item1;item2; ... itemN
        >>> array(
            0 => 'item1',
            1 => 'item2',
            ...
            M => 'itemN'
            )
    */
    /**
     * Utility to retrieve a simple list of separated value
     * @param string $key The name of the server value
     * 
     * @return array
     */
    protected static function getList($key)
    {
        if (!isset(self::$_SERVER[$key])) {
            self::$_SERVER[$key] = array();
            if (isset($_SERVER[$key])) {
                self::$_SERVER[$key] = \laabs\explode(LAABS_CONF_LIST_SEPARATOR, $_SERVER[$key]);
            }
        }

        return self::$_SERVER[$key];
    }

    /**
     * Get the list of bundles
     * 
     * @return array
     */
    public static function getBundles()
    {
        return self::getList('LAABS_BUNDLES');
    }

    /**
     * Get the list of Extensions
     * 
     * @return array
     */
    public static function getExtensions()
    {
        return self::getList('LAABS_EXTENSIONS');
    }

    /**
     * Get the list of Dependencies
     * 
     * @return array
     */
    public static function getDependencies()
    {
        return self::getList('LAABS_DEPENDENCIES');
    }

    /* Assoc retrieval
        Get an associative array from a separated string
        <<< key1:name1;key2:name2...keyN:nameN
        >>> array(
            key1 => name1,
            key2 => name2,
            ...
            keyN => nameN
            );
    */
    
    /**
     * Utility to retrieve an associative array from separated value
     * @param string $key The name of the server value
     * 
     * @return array
     */
    protected static function getAssoc($key)
    {
        if (!isset(self::$_SERVER[$key])) {
            self::$_SERVER[$key] = array();
            if (isset($_SERVER[$key])) {
                foreach (\laabs\explode(LAABS_CONF_LIST_SEPARATOR, $_SERVER[$key]) as $item) {
                    $key = strtok($item, ":");
                    self::$_SERVER[$key][$key] = $value = strtok(":");
                }
            }
        }

        return self::$_SERVER[$key];
    }

    /**
     * Get the list of default parsers by content type
     * 
     * @return array
     */
    public static function getParsers()
    {
        return self::getAssoc('LAABS_PARSERS');
    }

    /**
     * Get the list of default serializers by content type
     * 
     * @return array
     */
    public static function getSerializers()
    {
        return self::getAssoc('LAABS_SERIALIZERS');
    }

    /* Revert Assoc retrieval
        Get an associative array from a separated string where each item becomes an array pair
        <<< name1:key11,key12...key1n;name2:key21,key22...key2m ... nameN:keyN1,keyN2...keyNm
        >>> array(
            key11 => name1,
            key12 => name1,
            ...
            key1m => name1,
            key21 => name2,
            key22 => name2,
            ...
            key2m => name2,
            ...
            keyN1 => nameN,
            keyN2 => nameN,
            ...
            keyNm => nameN,
            );
    */
    /**
     * Utility to retrieve an associative array from separated value, reverting the key/value pairs
     * @param string $key The name of the server value
     * 
     * @return array
     */
    protected static function getRevertAssoc($key)
    {
        if (!isset(self::$_SERVER[$key])) {
            self::$_SERVER[$key] = array();
            if (isset($_SERVER[$key])) {
                foreach (\laabs\explode(LAABS_CONF_LIST_SEPARATOR, $_SERVER[$key]) as $item) {
                    $name = strtok($item, ":");
                    $values = strtok(":");
                    foreach (\laabs\explode(",", $values) as $value) {
                        self::$_SERVER[$key][$value] = $name;
                    }
                }
            }
        }

        return self::$_SERVER[$key];
    }

    /**
     * Get the list of observers/subjects to attach on a pool
     * 
     * @return array
     */
    public static function getObservers()
    {
        return self::getRevertAssoc('LAABS_OBSERVERS');
    }

    /**
     * Get the list of mimetypes/contenttypes to identify parser and serializer adapters
     * 
     * @return array
     */
    public static function getContentTypes()
    {
        return self::getRevertAssoc('LAABS_CONTENT_TYPES');
    }

    /**
     * Get the list of locales/language codes to identify languages
     * 
     * @return array
     */
    public static function getContentLanguages()
    {
        return self::getRevertAssoc('LAABS_CONTENT_LANGUAGES');
    }

    /* Linked list with order/priority retrieval
        For RFC2616 ranges with quality/priority
        <<< item1,item2;item3...itemN,q=0.8;...itemM,q=0.1
        >>> array(
                item1 => 1.0,
                item2 => 1.0,
                item3 => 0.8,
                ...
                itemN => 0.8,
                ...
                itemM => 0.1,
            )
    */
    /**
     * Utility to retrieve an associative array from separated value, with priority/quality index as in RFC2616 
     * @param string $key The name of the server value
     * 
     * @return array
     */
    protected static function getPriorisedList($key)
    {
        if (!isset(self::$_SERVER[$key])) {
            self::$_SERVER[$key] = array();
            if (isset($_SERVER[$key])) {
                $priorised = array();
                foreach (\laabs\explode(LAABS_CONF_LIST_SEPARATOR, $_SERVER[$key]) as $accept) {
                    $q = '1.0';
                    foreach (\laabs\explode(",", $accept) as $acceptItem) {
                        if ($acceptItem[0] == 'q') {
                            $q = substr($acceptItem, 2);
                            continue;
                        }
                        $priorised[$q][] = $acceptItem;
                    }
                }

                krsort($priorised);

                foreach ($priorised as $priority => $items) {
                    foreach ($items as $item) {
                        self::$_SERVER[$key][$item] = $priority;
                    }
                }
            }
        }

        return self::$_SERVER[$key];
    }

    /**
     * Get the ordered list of accepted mimetypes for the http request
     * 
     * @return array
     */
    public static function getHttpAccept()
    {
        return self::getPriorisedList('HTTP_ACCEPT');
    }

    /**
     * Get the ordered list of accepted languages for the http request
     * 
     * @return array
     */
    public static function getHttpAcceptLanguage()
    {
        return self::getPriorisedList('HTTP_ACCEPT_LANGUAGE');
    }

    /**
     * Get the ordered list of accepted encodings for the http request
     * 
     * @return array
     */
    public static function getHttpAcceptEncoding()
    {
        return self::getPriorisedList('HTTP_ACCEPT_ENCODING');
    }

    /**
     * Get the ordered list of accepted character sets for the http request
     * 
     * @return array
     */
    public static function getHttpAcceptCharset()
    {
        return self::getPriorisedList('HTTP_ACCEPT_CHARSET');
    }

    /*
     Other cases
     */
    /**
     * Get the http headers
     *
     * @return array
     */
    public static function getHttpHeaders()
    {
        $httpNonPrefixedHeaders = array(
            "CONTENT_TYPE",
            "CONTENT_LENGTH",
            );

        $httpHeaders = array();
        foreach ($_SERVER as $key => $value) {
            switch(true) {
                case substr($key, 0, 5) == 'HTTP_':
                    $key = substr($key, 5);
                // Continue with http header name
                case in_array($key, $httpNonPrefixedHeaders):
                    $nameParts = explode("_", $key);
                    foreach ($nameParts as $i => $namePart) {
                        $nameParts[$i] = ucfirst(strtolower($namePart));
                    }
                    $name = implode('-', $nameParts);
                    $httpHeaders[$name] = $value;
            }
        }

        return $httpHeaders;
    }

}