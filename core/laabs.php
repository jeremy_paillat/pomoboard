<?php
require_once __DIR__ . DIRECTORY_SEPARATOR . 'constants.php';
require_once __DIR__ . DIRECTORY_SEPARATOR . 'functions.php';
require_once __DIR__ . DIRECTORY_SEPARATOR . 'laabsAppTrait.php';
require_once __DIR__ . DIRECTORY_SEPARATOR . 'laabsModelTrait.php';
/**
 * The main Laabs API class
 */
class laabs
{
    use laabsModelTrait, laabsAppTrait;
    /* Constants */

    /* Properties */
    /**
     * Other autoloads methods registered if base fails
     */
    protected $autoload;

    /* Methods */
    /**
     *  Class autoloader
     *  Requires the source file of a given class based on its namespace\name
     *  @param string $class the name of the class to load
     *
     * @return bool
     */
    public static function autoload($class)
    {
        // Build class file path
        $classfile = str_replace(LAABS_NS_SEPARATOR, DIRECTORY_SEPARATOR, $class) . '.php';

        switch(strtok($class, LAABS_NS_SEPARATOR)) {
            case LAABS_CORE:
                $classfile = '..' . DIRECTORY_SEPARATOR . $classfile;
                break;

            case LAABS_APP:
            case LAABS_DEPENDENCY:
                $classfile = '..' . DIRECTORY_SEPARATOR . $classfile;
                // Continued ...

            case LAABS_BUNDLE:
            case LAABS_EXTENSION:
                if (!file_exists($classfile)) {
                    // When a function checks class file exists (for class_exists, class_uses functions ...)
                    return false;
                }
                break;

            default:
                return false;
                break;
        }

        require_once($classfile);

        return true;
    }

    /**
     *  Fallback exception handler
     *  Catches all uncaught exceptions and sends back a formatted message in requested response format
     *  @param object $exception An uncaught exception to handle
     */
    public static function exceptionHandler($exception)
    {
        \core\Observer\Dispatcher::notify(LAABS_EXCEPTION, $exception);

        if ($errorUri = \core\Globals\Server::getErrorUri()) {
            if ($kernel = self::kernel()) {

                $route = 'READ '. $errorUri;
                $type = $kernel->response->type;
                if (!$type) {
                    $type = 'text';
                }
                try {
                    //$kernel->response->setCode(500);
                    $kernel->response->setBody(\laabs::callOutputRoute($route, $type, $exception));
                    $kernel->response->send();
                    $kernel::end();
                } catch (\Exception $e) {

                }
            }
        } 

        exit;
    }

    /**
     *   Fallback error handler
     *   Catches all errors and sends back a formatted message in requested reponse format
     *   @param int    $errno      The error level (E_STRICT, E_WARNING, etc...)
     *   @param string $errstr     The error message
     *   @param string $errfile    The file where error occured
     *   @param int    $errline    The line where error occured
     *   @param mixed  $errcontext The context of data
     */
    public static function errorHandler($errno, $errstr, $errfile = null, $errline = null, $errcontext = null)
    {
        $error = new \core\Error($errstr, null, $errno, $errfile, $errline, $errcontext);
        \core\Observer\Dispatcher::notify(LAABS_ERROR, $error);
        switch($error->getCode()) {
            case E_ERROR: // 1
            case E_CORE_ERROR: // 16
            case E_COMPILE_ERROR: // 64
            case E_USER_ERROR: // 256
            case E_RECOVERABLE_ERROR: // 4096
            case E_PARSE: // 4
                if ($errorUri = \core\Globals\Server::getErrorUri()) {
                    if ($kernel = self::kernel()) {

                        $route = 'READ '. $errorUri;
                        $type = $kernel->response->type;
                        if (!$type) {
                            $type = 'text';
                        }

                        try {
                            //$kernel->response->setCode(500);
                            $kernel->response->setBody(\laabs::callOutputRoute($route, $type, $error));
                            $kernel->response->send();
                            $response->send();
                        } catch (\Exception $e) {

                        }
                    }
                }
        }
    }

    /**
     * Get the current running Kernel
     * @return object The Kernel
     */
    public static function kernel()
    {
        return \core\Kernel\AbstractKernel::get();
    }

    /**
     * Decode an object serialized on an ini file
     * @param string  $value The ini string to convert
     * @param boolean $assoc Return associative array instead of objects
     * 
     * @return mixed
     */
    public static function decodeIni($value, $assoc=true)
    {
        // Replace single quotes by double (for json compliance) except if escaped
        $value = preg_replace('#(?<![\\\\])\'#', '"', $value);

        // Unescape quotes
        $value = preg_replace("#\\\\'#", "'", $value);

        // escape backslahes
        $value = preg_replace("#\\\\#", "\\\\\\\\", $value);

        // unescape double-quotes
        $value = preg_replace('#\\\\"#', '"', $value);

        // Remove double-quotes enclosing true/false values and convert On/Off
        $value = preg_replace('#"(on|true)"#i', 'true', $value);
        $value = preg_replace('#"(off|false)"#i', 'false', $value);

        $decodedValue = json_decode($value, $assoc);
        
        if (is_null($decodedValue)) {
            switch (json_last_error()) {
                /*case JSON_ERROR_NONE:       
                    return $decodedValue;*/
                case JSON_ERROR_DEPTH:
                    $error = 'Maximal depth reached';
                    break;

                /*case JSON_ERROR_STATE_MISMATCH:
                    $error = 'State mismatch error'; 
                    break;

                case JSON_ERROR_CTRL_CHAR:
                    $error = 'Control character error'; 
                    break;*/

                case JSON_ERROR_SYNTAX:
                    $error = 'Syntax error: malformed JSON';
                    break;

                case JSON_ERROR_UTF8:
                    $error = 'Invalid UTF-8 characters';
                    break;

                default:
                    $error = 'Unknown error';
            }

            $displayValue = $value;
            if (strlen($displayValue) > 1000) {
                $displayValue = substr($displayValue, 0, 997) . "...";
            }
            throw new \Exception("Error decoding complex ini directive [$error] '$displayValue'");
        }

        return $decodedValue;
    }

    /**
     * Generates a Laabs debug context
     * @param bool $traceCore Trace code components or only source and dependencies
     *
     * @return array The backtrace of routes
     */
    public static function backtrace($traceCore = false)
    {
        $phpTrace = debug_backtrace(0);
        $trace = array();

        /*
            (array) of
                (string) function
                (int)    line
                (string) file
                (array)  args
                if OOP:
                (string) class
                (object) object
                (string) type (:: | ->)
        */

        $phpCurrent = reset($phpTrace);
        while ($phpStep = next($phpTrace)) {
            $step = false;
            if (isset($phpStep['class'])) {
                $step = array();

                if (isset($phpStep['file'])) {
                    $step['file'] = $phpStep['file'];
                }
                if (isset($phpStep['line'])) {
                    $step['line'] = $phpStep['line'];
                }

                $classParser = self::parseClass($phpStep['class']);
                $step = array_merge($step, $classParser);

                if (isset($classParser[LAABS_BUNDLE])) {
                    if (isset($classParser[LAABS_CONTROLLER])) {
                        $step[LAABS_CONTROLLER] = $classParser[LAABS_BUNDLE]
                            . LAABS_URI_SEPARATOR . $classParser[LAABS_CONTROLLER];
                        $methodName = LAABS_ACTION;
                    } elseif (isset($classParser[LAABS_PARSER])) {
                        $step[LAABS_PARSER] = $classParser[LAABS_BUNDLE]
                            . LAABS_URI_SEPARATOR . $classParser[LAABS_PARSER];
                        $methodName = LAABS_INPUT;
                    } elseif (isset($classParser[LAABS_SERIALIZER])) {
                        $step[LAABS_SERIALIZER] = $classParser[LAABS_BUNDLE]
                            . LAABS_URI_SEPARATOR . $classParser[LAABS_SERIALIZER];
                        $methodName = LAABS_OUTPUT;
                    } else {
                        $step[LAABS_SERVICE] = $classParser[LAABS_BUNDLE]
                            . LAABS_URI_SEPARATOR . $classParser[LAABS_SERVICE];
                        $methodName = LAABS_METHOD;
                    }
                } elseif (isset($classParser[LAABS_DEPENDENCY])) {
                    $step[LAABS_SERVICE] = $classParser[LAABS_DEPENDENCY]
                        . LAABS_URI_SEPARATOR . $classParser[LAABS_SERVICE];
                    $methodName = LAABS_METHOD;
                }

                $step[$methodName] = $phpStep['function'];
            } else {
                $step['function'] = $phpStep['function'];
            }
            if ($step) {
                foreach ($phpStep['args'] as $phpArg) {
                    if (is_scalar($phpArg)) {
                        $step['args'][] = $phpArg;
                    } else {
                        $step['args'][] = "__non_scalar_argument__";
                    }
                }
                $trace[] = $step;
            }
        }

        return $trace;

    }

    /**
     * Analyses a class name and returns an associative array of components
     * @param string $name The full name of the class
     *
     * @return array An associative array of component types and names
     */
    public static function parseClass($name)
    {
        if ($name[0] == LAABS_NS_SEPARATOR) {
            $name = substr($name, 1);
        }
        $steps = explode(LAABS_NS_SEPARATOR, $name);
        $parser = array();
        /*
            core/<component>
            core/laabs
            bundle/<bundle>
                /Controller/<controller>
                /Service/<service>
            ext/<ext>
                bundle/...
            dependency/<dependency>
                /<service>
                /Adapter/<adapter>/<service>
        */

        // core/<component> | ext/<ext> | bundle/<bundle> | dependency/<dependency>
        $rootType = array_shift($steps);
        $rootName = array_shift($steps);
        $parser[$rootType] = $rootName;

        switch ($rootType) {
            case LAABS_EXTENSION:
                $bundleName = array_shift($steps);
                $parser[LAABS_BUNDLE] = $bundleName;
                // Continue after extension parsed

            case LAABS_BUNDLE:
                $componentType = array_shift($steps);
                switch ($componentType) {
                    case LAABS_CONTROLLER:
                    case LAABS_MODEL:
                    case LAABS_OBSERVER:
                        $componentName = array_shift($steps);
                        $parser[$componentType] = $componentName;
                        break;

                    case LAABS_PARSER:
                    case LAABS_SERIALIZER:
                        $componentAdapter = array_shift($steps);
                        $componentName = array_shift($steps);
                        $parser[$componentType] = $componentName;
                        $parser[LAABS_ADAPTER] = $componentAdapter;
                        break;

                    default:
                        if (count($steps) > 0) {
                            $parser[LAABS_SERVICE] = $componentType . LAABS_URI_SEPARATOR . \laabs\implode(LAABS_URI_SEPARATOR, $steps);
                        } else {
                            $parser[LAABS_SERVICE] = $componentType;
                        }
                }

                break;

            case LAABS_DEPENDENCY:
                // Adapter/<adapter>/<service> |  <service>
                $next = array_shift($steps);
                if ($next == LAABS_ADAPTER) {
                    $componentAdapter = array_shift($steps);
                    $parser[$next] = $componentAdapter;
                    $parser[LAABS_SERVICE] = implode(LAABS_URI_SEPARATOR, $steps);
                } else {
                    if (count($steps) > 0) {
                        $parser[LAABS_SERVICE] = $next . LAABS_URI_SEPARATOR . \laabs\implode(LAABS_URI_SEPARATOR, $steps);
                    } else {
                        $parser[LAABS_SERVICE] = $next;
                    }
                }
                break;

            case LAABS_CORE:
                if (count($steps)) {
                    $parser['Class'] = implode(LAABS_URI_SEPARATOR, $steps);
                }
        }

        return $parser;
    }

    /**
     * Returns the delay between server request and current time
     * @return float The delay in microseconds
     */
    public static function requestDelay()
    {
        return (float) number_format(( microtime(true) - \core\Globals\Server::getRequestTime(true) ), 3);
    }

    /**
     * Checks whether the Instance is available for get/set or not
     * @return bool
     */
    public static function instanceReady()
    {
        if (!class_exists("\core\Instance\Instance", $autoload = false)) {
            return false;
        }
        if (!class_exists("\core\Globals\Instance", $autoload = false)) {
            return false;
        }
        if (\core\Globals\Instance::status() == \core\Globals\Instance::STS_NONE) {
            return false;
        }

        return true;
    }

    /**
     * Creates a public resource on the web root directory if not already there.
     * The new file name will be a hash of the content, allowing a pre-check and avoiding copy of content if file already exists
     * @param string $content The content of the file to be created
     * @param string $path    The relative path to create the file (from web root).
     *
     * @return string The uri to access the resource from the web
     */
    public static function createPublicResource($content, $path = LAABS_TMP)
    {
        $uid = hash('md5', $content);

        $dir = ".." . DIRECTORY_SEPARATOR . LAABS_WEB;
        if ($path) {
            $dir .= DIRECTORY_SEPARATOR . $path;
        }

        if (!is_dir($dir)) {
            if (!mkdir($dir, 0777)) {
                throw new Exception("Unable to make directory '$dir'");
            }
        }

        $file = $dir . DIRECTORY_SEPARATOR . $uid;

        $pathUri = str_replace(DIRECTORY_SEPARATOR, LAABS_URI_SEPARATOR, $path);
        $uri = LAABS_URI_SEPARATOR . $pathUri . LAABS_URI_SEPARATOR . $uid;

        if (!is_file($file)) {
            if (!file_put_contents($file, $content)) {
                throw new Exception("Unable to copy resource to '$file'");
            }
        }

        return $uri;
    }

    /**
     * Check if a public resource exist on the web root directory.
     * The new file name will be a hash of the content, allowing a pre-check and avoiding copy of content if file already exists
     * @param string $path    The relative path to create the file (from web root).
     *
     * @return string The uri to access the resource from the web
     */
    public static function hasPublicResource($path = LAABS_TMP)
    {
        $dir = ".." . DIRECTORY_SEPARATOR . LAABS_WEB;
        if ($path) {
            $dir .= DIRECTORY_SEPARATOR . $path;
        }

        if (!file_exists($dir)) {
            return false;
        }

        return true;
    }

    /**
     * Create a stream resource in memory with a given content
     * @param string $contents
     *
     * @return resource
     * @author
     **/
    public static function createMemoryStream($contents)
    {
        $stream = fopen('php://memory', 'r+');
        fwrite($stream, $contents);
        rewind($stream);

        return $stream;
    }

    /**
     * Export a value into a serializable value
     * @param mixed   $sourceValue The value to export
     * @param integer $depth       The max depth
     *
     * @return mixed The export
     */
    public static function export($sourceValue, $depth=false)
    {
        if ($depth > 99) {
            return;
        }
        switch (gettype($sourceValue)) {
            case 'resource':
                return null;

            case 'object':
                if (self::isScalar($sourceValue)) {
                    return (string) $sourceValue;
                }

                $reflection = new \ReflectionObject($sourceValue);

                $targetObject = new \StdClass();
                foreach ($reflection->getProperties() as $property) {
                    $name = $property->getName();
                    $property->setAccessible(true);
                    $value = $property->getValue($sourceValue);

                    $targetObject->$name = self::export($value, $depth);
                }
                $depth++;

                return $targetObject;

            case 'array':
                $targetArray = array();
                foreach ($sourceValue as $key => $sourceRow) {
                    $targetArray[$key] = self::export($sourceRow, $depth);
                }
                $depth++;

                return $targetArray;

            // Scalar values + NULL
            default:
                return $sourceValue;
        }
    }

    /**
     * Get a new type object
     * @param string $typename The name of the derived type
     * @param mixed  $value    The initial value
     * 
     * @return object
     */
    public static function newType($typename, $value=null)
    {
        switch($typename) {
            case 'id':
                return self::newId($value);

            case 'date':
                return self::newDate($value);

            case 'datetime':
                return self::newDateTime($value);

            case 'timestamp':
                return self::newTimestamp($value);

            case 'duration':
                return self::newDuration($value);

            case 'number':
                return self::newNumber($value);

            case 'name':
                return self::newName($value);

            case 'qname':
                return self::newQName($value);

            case 'xml':
                return self::newXml($value);
        }
    }

    /**
     * Get a new Id object
     * @param string $id
     * @param string $prefix
     * 
     * @return Id
     */
    public static function newId($id=null, $prefix=null)
    {
        return new \core\Type\Id($id, $prefix);
    }

    /**
     * Get a new name object
     * @param string $name
     * 
     * @return Name
     */
    public static function newName($name)
    {
        return new \core\Type\Name($name);
    }

    /**
     * Get a new qname object
     * @param string $qname
     * 
     * @return QName
     */
    public static function newQName($qname)
    {
        return new \core\Type\QName($qname);
    }

    /**
     * Get a new Date object
     * @param string $date
     * @param string $format
     * 
     * @return Date
     */
    public static function newDate($date=null, $format=null)
    {
        return new \core\Type\Date($date, $format);
    }

    /**
     * Get a new Datetime object
     * @param string $date
     * @param string $timezone
     * 
     * @return DateTime
     */
    public static function newDateTime($date=null, $timezone=null)
    {
        return new \core\Type\DateTime($date, $timezone);
    }

    /**
     * Get a new Timestamp object
     * @param string $timestamp
     * 
     * @return Timestamp
     */
    public static function newTimestamp($timestamp=null)
    {
        return new \core\Type\Timestamp($timestamp);
    }

    /**
     * Get a new Duration object
     * @param string $duration
     * 
     * @return Duration
     */
    public static function newDuration($duration=null)
    {
        return new \core\Type\Duration($duration);
    }

    /**
     * Get a new Number object
     * @param mixed $number
     * 
     * @return Number
     */
    public static function newNumber($number=null)
    {
        return new \core\Type\Number((float) $number);
    }

    /**
     * Get a new Xml object
     * @param string $xml
     * @param string $encoding
     * 
     * @return Xml
     */
    public static function newXml($xml=null, $encoding=null)
    {
        return new \core\Type\Xml($xml, $encoding);
    }

    /**
     * Log an event into laabs log
     * @param string  $message
     * @param integer $level
     * 
     * @return void
     */
    public static function log($message, $level=null)
    {
        \core\Kernel\AbstractKernel::get()->log($message, $level);
    }


    /**
     * Store a value in globals
     * @param mixed $ref   The reference
     * @param mixed $value The value
     * 
     * @return string The hash
     */
    public static function setCache($ref, $value)
    {
        $hash = md5(print_r($ref, true));
        if (!isset($_GLOBALS[$hash])) {
            $_GLOBALS[$hash] = $value;
        }

        return $hash;
    }

    /**
     * Store a value in globals
     * @param mixed $ref The reference
     * 
     * @return mixed The value 
     */
    public static function getCache($ref)
    {
        $hash = md5(print_r($ref, true));
        if (isset($_GLOBALS[$hash])) {
            return $_GLOBALS[$hash];
        }
    }
}
