<?php
/**
 * Trait for app source management
 */
trait laabsAppTrait
{
    /* Properties */
    protected static $bundles = array();

    protected static $dependencies = array();

    protected static $classes = array();

    protected static $observers = array();

    protected static $routes = array();

    protected static $routesByDomain = array();

    /* Methods */
    /**
     * Get the current app name
     * @return string
     */
    public static function app()
    {
        return \core\Reflection\App::getInstance();
    }

    /**
     * Check if the app has dependency activated
     * @param string $name The name of the dependency to check
     *
     * @return bool
     */
    public static function hasDependency($name)
    {
        return \core\Globals\Server::hasDependency($name);
    }

    /**
     * Get dependency injection
     * @param string $name   The name of the dependency instance
     * @param string $caller The uri of the caller instance
     *
     * @return object The Dependency Injection
     */
    public static function dependency($name, $caller = false)
    {
        if (!\core\Globals\Server::hasDependency($name)) {
            throw new \core\Exception("Dependency $name is not activated. Check configuration of the instance.");
        }

        if ($caller) {
            $containerRouter = new \core\Route\ContainerRouter($caller);
            $caller = $containerRouter->container;
        } else {
            $caller = null;
        }

        return \core\Reflection\Dependency::getInstance($name, $caller);
    }

    /**
     * Get dependency injections
     *
     * @return object The Dependency Injection
     */
    public static function dependencies()
    {
        if (empty(self::$dependencies)) {
            foreach (\core\Globals\Server::getDependencies() as $dependency) {
                self::$dependencies[$dependency] = \core\Reflection\Dependency::getInstance($dependency);
            }
        }

        return self::$dependencies;
    }

    /**
     * Get all the bundles
     * @return array The bundle objects
     */
    public static function bundles()
    {
        if (empty(self::$bundles)) {
            foreach (\core\Globals\Server::getBundles() as $bundle) {
                self::$bundles[$bundle] = \core\Reflection\Bundle::getInstance($bundle);
            }
        }

        return self::$bundles;
    }

    /**
     * check a bundle
     * @param string $name The name of the bundle. If null current buendle i returned
     *
     * @return bool
     */
    public static function hasBundle($name)
    {
        return \core\Globals\Server::hasBundle($name);
    }

    /**
     * Get a bundle
     * @param string $name The name of the bundle. If null current buendle i returned
     *
     * @return \core\Reflection\Bundle The bundle object
     */
    public static function bundle($name)
    {
        if (!\core\Globals\Server::hasBundle($name)) {
            throw new \core\Exception("Bundle $name is not activated. Check configuration of the instance.");
        }

        if (empty(self::$bundles)) {
            self::bundles();
        }

        return self::$bundles[$name];
    }

    /**
     * Get the available action routes
     * @param string $domain The namespace of routes
     *
     * @return array
     */
    public static function routes($domain=false)
    {
        
        if (empty(self::$routes) && empty(self::$routesByDomain)) {
            $app = self::app();

            foreach ($app->getAPIs() as $API) {

                foreach ($API->getRoutes() as $route) {
                    self::$routes[] = $route;
                    self::$routesByDomain['app' . LAABS_URI_SEPARATOR . $app->name][] = $route;
                }
            }

            foreach (self::bundles() as $bundle) {
                foreach ($bundle->getAPIs() as $API) {
                    foreach ($API->getRoutes() as $route) {
                        self::$routes[] = $route;
                        self::$routesByDomain[$bundle->getName()][] = $route;
                    }
                }
            }
        }
        

        if (!$domain) {
            return self::$routes;
        } else {
            if (isset(self::$routesByDomain[$domain])) {
                return self::$routesByDomain[$domain];
            }
        }

        return array();
    }

    /**
     * Get the action associated with the given action uri
     * @param string $method The CRUD method
     * @param string $uri    The uri received
     *
     * @return string The bundle/controller/action triplet
     */
    public static function route($method, $uri)
    {
        if ($method == "READ" && empty($uri)) {
            $uri = \core\Globals\Server::getDefaultUri();
        }

        // Try domain
        $bundle = strtok($uri, LAABS_URI_SEPARATOR);
        foreach (self::routes($bundle) as $route) {
            if ($route->match($method, $uri)) {
                return $route;
            }
        }

        // Try all routes
        foreach (self::routes() as $route) {
            if ($route->match($method, $uri)) {
                return $route;
            }
        }

        throw new \core\Exception("Undefined route $method $uri");
    }

    /**
     * Get the available types
     *
     * @return array
     */
    public static function classes()
    {
        if (empty(self::$classes)) {
            foreach (self::bundles() as $bundle) {
                foreach ($bundle->getClasses() as $class) {
                    self::$classes[] = $class;
                }
            }
        }
        
        return self::$classes;
    }

    /**
     * Attach an observer
     * @param string $methoduri
     * @param string $subject
     */
    public static function attachObserver($methoduri, $subject)
    {
        $methodRouter = new \core\Route\MethodRouter($methoduri);
        $observer = $methodRouter->service;
        $handler = $methodRouter->method;
        
        $observerObject = $observer->newInstance();
            
        \core\Observer\Dispatcher::attach(
            $observerObject,
            $handler->name,
            $subject
        );
    }

    /**
     * Call an action by route   
     * @param string $actionRequest The uri of action (METHOD + route or default budnle/controller/action). All additional parameters will be considered as action arguments
     * 
     * @return mixed
     */
    public static function callActionRoute($actionRequest)
    {
        $passedArgs = func_get_args();
        array_shift($passedArgs);

        $method = strtok($actionRequest, ' ');
        $path = strtok(' ');
    
        $route = self::route($method, $path);

        if (isset($route->action)) {
            $actionRouter = new \core\Route\ActionRouter($route->action);
        } else {
            $actionRouter = new \core\Route\ActionRouter($route->getName());
        }

        // Order arguments using message definition OR action parameters
        $parameters = $actionRouter->action->getParameters();
        $actionArgs = array();
        foreach ($parameters as $i => $parameter) {
            $paramType = $parameter->getType();
            $value = null;
            if (isset($route->variables[$parameter->name])) {
                $value = $route->variables[$parameter->name];
            } elseif (isset($passedArgs[$i])) {
                $value = $passedArgs[$i];
            }

            $actionArgs[$parameter->name] = $value;
        }

        $controller = $actionRouter->controller->newInstance();
        $actionReturn = $actionRouter->action->call($controller, $actionArgs);

        return $actionReturn;
    }


    /**
     * Get an input router from route definition
     * @param \core\Reflection\Route $route
     * @param string                 $type
     * 
     * @return \core\Route\inputRouter
     */
    public static function getInputRouter($route, $type='url')
    {
        if (isset($route->input)) {
            return new \core\Route\InputRouter($route->input, $type);
        } 

        if (isset($route->action)) {
            try {
                return new \core\Route\InputRouter($route->action, $type);
            } catch (\Exception $e) {
                return;
            }
        } 

        return new \core\Route\InputRouter($route->getName(), $type);
    }

    /**
     * Call an input by route   
     * @param string $inputRequest The uri of input (METHOD + route or default bundle/controller/action). All additional parameters will be considered as action arguments
     * @param string $type         The type of input (html, json...)
     * @param mixed  $data         The data to parse
     * 
     * @return mixed
     */
    public static function callInputRoute($inputRequest, $type='url', $data)
    {
        $method = strtok($inputRequest, ' ');
        $path = strtok(' ');
    
        $route = self::route($method, $path);

        $inputRouter = self::getInputRouter($route, $type);

        $parser = $inputRouter->parser->newInstance();

        $input = $inputRouter->input->parse($parser, $data);

        return $input;
    }

    /**
     * Get an output router from route definition
     * @param \core\Reflection\Route $route
     * @param string                 $type
     * 
     * @return \core\Route\outputRouter
     */
    public static function getOutputRouter($route, $type='txt')
    {
        if (isset($route->output)) {
            return new \core\Route\OutputRouter($route->output, $type);
        } 

        if (isset($route->action)) {
            try {
                return new \core\Route\OutputRouter($route->action, $type);
            } catch (\Exception $e) {
                return;
            }
        } 

        return new \core\Route\OutputRouter($route->getName(), $type);
    }

    /**
     * Call an output by route   
     * @param string $outputRequest The uri of output (METHOD + route or default budnle/controller/action). All additional parameters will be considered as action arguments
     * @param string $type          The type of output (html, json...)
     * @param mixed  $data          The data to serializes
     * 
     * @return mixed
     */
    public static function callOutputRoute($outputRequest, $type='txt', $data)
    {
        $method = strtok($outputRequest, ' ');
        $path = strtok(' ');
    
        $route = self::route($method, $path);

        $outputRouter = self::getOutputRouter($route, $type);

        $serializer = $outputRouter->serializer->newInstance();

        $output = $outputRouter->output->serialize($serializer, $data);

        return $output;
    }

    /**
     * Call a controller action   
     * @param string $actionUri The uri of action (METHOD + route or default budnle/controller/action). All additional parameters will be considered as action arguments
     * 
     * @return mixed
     */
    public static function callAction($actionUri)
    {
        $passedArgs = func_get_args();
        array_shift($passedArgs);

        $steps = \laabs\explode(LAABS_URI_SEPARATOR, $actionUri);
        $actionRouter = new \core\Route\ActionRouter(implode(LAABS_URI_SEPARATOR, array_splice($steps, 0, 3)));
        $actionArgs = $passedArgs;

        $controller = $actionRouter->controller->newInstance();
 
        return $actionRouter->action->call($controller, $actionArgs);
    }

    /**
     * Instanciate a new controller. All additional arguments will be passed to constructor
     * @param string $controllerName The controller name bundle/controller
     *
     * @return object
     */
    public static function newController($controllerName)
    {
        $constructorArgs = func_get_args();
        $argsHash = md5(serialize($constructorArgs));
        
        if (!isset($GLOBALS[$argsHash])) {

            $bundleName = strtok($controllerName, LAABS_URI_SEPARATOR);
            $controllerName = strtok(LAABS_URI_SEPARATOR);      
            array_shift($constructorArgs);

            $controller = self::bundle($bundleName)->getController($controllerName);

            $GLOBALS[$argsHash] = $controller->newInstance($constructorArgs);

        } 

        return $GLOBALS[$argsHash];
    }

    /**
     * Instanciate a new parser. All additional arguments will be passed to constructor
     * @param string $name The parser name bundle/parser
     * @param string $type The type of data to parse
     *
     * @return object
     */
    public static function newParser($name, $type)
    {
        $bundleName = strtok($name, LAABS_URI_SEPARATOR);
        $parserName = strtok(LAABS_URI_SEPARATOR);

        $constructorArgs = func_get_args();
        array_shift($constructorArgs);
        array_shift($constructorArgs);

        $parser = self::bundle($bundleName)->getParser($parserName, $type);

        return $parser->newInstance($constructorArgs);
    }

    /**
     * Instanciate a new serializer. All additional arguments will be passed to constructor
     * @param string $name The serializer name bundle/serializer
     * @param string $type The type of data to serialize
     *
     * @return object
     */
    public static function newSerializer($name, $type)
    {
        $bundleName = strtok($name, LAABS_URI_SEPARATOR);
        $serializerName = strtok(LAABS_URI_SEPARATOR);

        $constructorArgs = func_get_args();
        array_shift($constructorArgs);
        array_shift($constructorArgs);

        $serializer = self::bundle($bundleName)->getSerializer($serializerName, $type);

        return $serializer->newInstance($constructorArgs);
    }

    /**
     * Instanciate a new exception. All additional arguments will be passed to constructor
     * @param string $exceptionName The exception name bundle/exception
     *
     * @return object
     */
    public static function newException($exceptionName)
    {
        $bundleName = strtok($exceptionName, LAABS_URI_SEPARATOR);
        $exceptionName = strtok(LAABS_URI_SEPARATOR);

        $constructorArgs = func_get_args();
        array_shift($constructorArgs);

        $exception = self::bundle($bundleName)->getException($exceptionName);

        return $exception->newInstance($constructorArgs);
    }

    /**
     * Call a service
     * @param string $serviceName The service name bundle|dependency / service|class
     *
     * @return object
     */
    public static function newService($serviceName)
    {
        $serviceRouter = new \core\Route\ServiceRouter($serviceName);

        $constructorArgs = func_get_args();
        array_shift($constructorArgs);

        return $serviceRouter->service->newInstance($constructorArgs);
    }

    /**
     * Call a service method
     * @param string $methodName  The service name bundle|dependency / service|class
     * @param array  $methodArgs  The arguments for method call
     * @param array  $serviceArgs The arguments to pass to constructor of object
     *
     * @return mixed
     */
    public static function callServiceMethod($methodName, array $methodArgs = null, array $serviceArgs = null)
    {
        $methodRouter = new \core\Route\MethodRouter($methodName);

        $serviceObject = $methodRouter->service->newInstance($serviceArgs);

        return $methodRouter->method->callArgs($serviceObject, $methodArgs);
    }

    /**
     * Get the available observers
     *
     * @return array
     */
    public static function observers()
    {
        if (empty(self::$observers)) {
            foreach (self::bundles() as $bundle) {
                foreach ($bundle->getObservers() as $observer) {
                    self::$observers[] = $observer;
                }
            }
        }

        return self::$observers;
    }

    /**
     * Send an event to observers
     * @param string $subject The subject for event
     * @param mixed  &$object The observed object
     * @param array  &$info   The associated data
     *
     * @return array An associative array for class::method => return of notified observers
     */
    public static function notify($subject, &$object, array &$info=null)
    {
        return \core\Observer\Dispatcher::notify($subject, $object, $info);
    }

    /**
     * Checks if a type name is a valid service name
     * @param string $type The name of the type
     *
     * @return bool
     */
    public static function isServiceType($type)
    {
        if (self::isScalarType($type)
            || $type == 'resource'
            || $type == 'NULL'
            || substr($type, -2) == "[]"
        ) {
            return false;
        }

        if ($type[0] == LAABS_NS_SEPARATOR) {
            $type = substr($type, 1);
        }

        $root = strtok($type, LAABS_NS_SEPARATOR);
        switch($root) {
            case LAABS_CORE:
            case LAABS_DEPENDENCY:
            case LAABS_BUNDLE:
            case LAABS_EXTENSION:
                return true;

            default:
                return false;
        }
    }

    /**
     * Get configuration section
     * @param string $section
     * 
     * @return mixed
     */
    public static function configuration($section)
    {
        $conf = \core\Configuration\Configuration::getInstance();

        $section = str_replace(LAABS_URI_SEPARATOR, ".", $section);

        if (isset($conf[$section])) {
            return $conf[$section];
        }
    }

}
