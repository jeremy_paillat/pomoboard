<?php
/*
 * Copyright (C) 2015 Maarch
 *
 * This file is part of Laabs.
 *
 * Laabs is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Laabs is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with Laabs.  If not, see <http://www.gnu.org/licenses/>.
 */
namespace core;
/**
 * Trait Readonly
 * Brings magic method __set that triggers error, forcing the use of setter methods
 * @package core
 */
trait ReadonlyTrait
{
    /**
     * Magic method __set that triggers error, forcing the use of setter methods
     * @param string $name  The name of the property
     * @param mixed  $value The new value of the property
     * 
     * @return void
     * 
     * @access public
     */
    public function __set($name, $value=null)
    {
        if (!in_array($name, get_object_vars($this))) {
            trigger_error("Undeclared property " . __CLASS__ . "::" . $name);
        }

        trigger_error("Can not modify read only property " . __CLASS__ . "::" . $name);
    }

}

