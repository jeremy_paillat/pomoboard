<?php
/*
 * Copyright (C) 2015 Maarch
 *
 * This file is part of Laabs.
 *
 * Laabs is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Laabs is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with Laabs.  If not, see <http://www.gnu.org/licenses/>.
 */
namespace laabs;

/**
 *  Symbolic links creation for Windows systems
 *  @param string $target The path to target file/dir
 *  @param string $link   The path of the symlink to create
 * 
 *  @return bool True if creation of link succeeded
 */
function symlink($target, $link)
{
    if (DIRECTORY_SEPARATOR == "\\") {
        $target = str_replace('/', DIRECTORY_SEPARATOR, $target);
        $link = str_replace('/', DIRECTORY_SEPARATOR, $link);
        $param = false;
        if (is_dir($target)) {
            $param = "/D";
        }
        $output = array();
        $return = false;
        $cmd = 'mklink ' . $param . ' "' . $link . '" "' . $target . '"';
        
        exec($cmd, $output, $return);

        if ($return > 0) {
            return false;
        } else {
            return true;
        }
    } else {
        return \symlink($target, $link);
    }
}

/**
 * Generates a local unique identifier
 * Improvements : the use of base conversion to get short ids with good entropy
 * @param string $prefix      A prefix for the generated id, if a specific class of characters is needed (XML ids must start with alpha)
 * @param bool   $moreEntropy Use more entropy
 * 
 * @return string The unique id
 */
function uniqid($prefix = "", $moreEntropy = true)
{
    $base64 = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz-_";

    /*$ipParts = \explode('.', gethostbyname(gethostname()));
    $hostIp = str_pad(decbin($ipParts[3] + $ipParts[2] * 0x100 + $ipParts[1] * 0x10000 + $ipParts[0] * 0x1000000), 32, "0", STR_PAD_LEFT);

    $pid = str_pad(decbin(getmypid()), 32, "0", STR_PAD_LEFT);*/

    $timeParts = \explode(' ', microtime());
    $sec = $timeParts[1];
    $sec64 = \laabs\base_convert($sec, 10, 64);

    $msec = substr($timeParts[0], 2, 6);
    $msec64 = \laabs\base_convert($msec, 10, 64);

    if ($moreEntropy) {
        $rand = mt_rand();
        $rand64 = \laabs\base_convert($rand, 10, 64);
    } else {
        $rand64 = '';
    }

    $uniqId = $sec64 . $msec64 . $rand64;

    return $prefix . $uniqId;  
}

/**
 * Explode a string into an array
 * Improvements : Always returns an array and removes empty items
 * @param string $delimiter The delimiter string
 * @param string $string    The string to explode
 * @param bool   $noEmpty   If true, empty strings will not be included
 * 
 * @return array The array of exploded values, empty array if empty string
 */
function explode($delimiter, $string, $noEmpty = true)
{
    $array = \explode($delimiter, $string);

    if (!$array) {
        $array = array();
    }
    
    if ($noEmpty) {
        foreach ($array as $index => $value) {
            $value = trim($value);
            if ($value === "") {
                unset($array[$index]);
            } else {
                $array[$index] = $value;
            }
        }
    }
    
    return array_values($array);
}

/**
 * Implode an array into a string
 * Improvements : Empty rows can be excludedto
 * @param string $glue    The glue string
 * @param array  $array   The array to implode
 * @param bool   $noEmpty If true, empty items will not be included into string
 * 
 * @return string The string of imploded values, empty string if empty array
 */
function implode($glue, array $array, $noEmpty = true)
{
    $arrayValues = array_values($array);
    
    if ($noEmpty) {
        foreach ($arrayValues as $index => $value) {
            if (trim($value) == "") {
                unset($arrayValues[$index]);
            }
        }
    }
            
    return \implode($glue, $arrayValues);
}



/**
 * Returns the basename of a path including class names and namespaces
 *
 * Improvements : works on file system path and php namespaces
 * @param string $name   a name
 * @param string $suffix a suffix
 *
 * @return string The base name
 */
function basename($name, $suffix = null)
{
    if (strpos($name, LAABS_NS_SEPARATOR) === false) {
        return \basename($name, $suffix);
    }

    $basename = end(@explode(LAABS_NS_SEPARATOR, $name));
    if ($suffix) {
        $basename = substr($basename, 0, strrpos($basename, $suffix));
    }
        
    return $basename;
}

/**
 * Returns the dirname of a path including class names and namespaces
 * Improvements : works on file system path and php namespaces
 * @param string $name
 * 
 * @return string the directory or namespace
 */
function dirname($name)
{
    if (strpos($name, LAABS_NS_SEPARATOR) !== false) {
        return implode(LAABS_NS_SEPARATOR, array_splice(@explode(LAABS_NS_SEPARATOR, $name), 0, -1));
    }

    return \dirname($name);
}

/**
 * Tokenizes a string
 * Improvements : tokenizes all strings even non php (no open tag), removes open tag
 * @param string $string The string to tokenize
 * 
 * @return array The tokens
 */
function token_get_all($string)
{
    if (strpos("<?php ", $string) === false) {
        $string = "<?php " . $string;
    }

    $tokens = \token_get_all($string);

    // Ignore php open tag
    $phpOpenTag = array_shift($tokens);

    // Force token array structure and set offset
    $offset = 0;
    foreach ($tokens as $i => $token) {
        if (is_scalar($token)) {
            $tokens[$i] = array(false, $token, false);
        } else {
            $tokens[$i][3] = $offset;
        }
    }

    return $tokens;
}

/**
 * List the traits used by a given class
 * Improvements : Ability to go deep and return traits used by traits and traits used by parent classes with no limitation
 * @param string $class    The class to list traits of
 * @param bool   $autoload Use autoload or not
 * @param bool   $deep     List traits of traits and traits of ancestors
 * 
 * @return array An array of the unique traits used by the class
 */
function class_uses($class, $autoload = true, $deep = true)
{
    if (!$deep) {
        return \class_uses($class);
    }

    $traits = array();
    if (is_object($class)) {
        $class = get_class($class);
    }

    // Get traits of all ancestor classes
    do {
        $traits = array_merge(\class_uses($class, $autoload), $traits);
    } while ($class = get_parent_class($class));

    // Get traits of all ancestor traits
    $ancestorTraits = $traits;
    while (!empty($ancestorTraits)) {
        $traitTraits = \class_uses(array_pop($ancestorTraits), $autoload);
        $traits = array_merge($traitTraits, $traits);
        $ancestorTraits = array_merge($traitTraits, $ancestorTraits);
    };

    foreach ($traits as $trait => $same) {
        $traits = array_merge(\class_uses($trait, $autoload), $traits);
    }

    return array_unique($traits);
}

/**
 * Returns the type of a variable
 * Improvements : Returns the class of var is an object and $class set true
 * @param mixed  $var   The variable to type check
 * @param string $class Return the class of object instead of php base type 'object'
 * 
 * @return string The type of the variable
 * Possible values are
 *  * "boolean"
 *  * "integer"
 *  * "double"
 *  * "string"
 *  * "array"
 *  * "object" or the object class if claa name requested
 *  * "resource"
 *  * "NULL"
 *  * "unknown type"
 */
function gettype($var, $class = false)
{
    $type = \gettype($var);

    if ($class && $type=='object') {
        $type = get_class($var);
    } elseif ($type == 'double') {
        $type = 'float';
    } elseif ($type == 'NULL') {
        $type = 'null';
    }

    return $type;
}

/**
 * Checks if an array is associative
 * @param array $array The array to check
 * 
 * @return bool
 */
function is_assoc($array)
{
    return (bool) count(array_filter(array_keys($array), 'is_string'));
}

/**
 * Get constans value from its name
 * Improvements : Checks constant is defined before returning value. If not, return constant name
 * @param string $name The name of the constant
 * 
 * @return mixed The value of the defined constant or the name if not defined
 */
function constant($name)
{
    if (defined($name)) {
        return \constant($name);
    }
        
    return $name;
}

/**
 * Calculate the MD5 hash value for a string
 * Get a 25 bit MD5 instead of 32 (base 36)
 * @param string $str       The string to hash
 * @param bool   $rawOutput Return as raw string
 * @param bool   $short     Return short 25 MD5 instead of 32
 * 
 * @return $string The MD5
 */
function md5($str, $rawOutput=false, $short=true)
{
    // If short not requested, return classic MD5
    if (!$short) {
        return \md5($str, $rawOutput);
    }

    $md5 = \md5($str);
    $rawMd5 = \md5($str, true);

    $shortMd5 = \base_convert($md5, 16, 36);

    // If raw output requested, re-convert new hash to its character representation (base36 to chr)
    if ($rawOutput) {
        $base36Chunks = str_split($shortMd5, 2);
        $base36Chrs = array();
        foreach ($base36Chunks as $base36Chunk) {
            $base36Chrs[] = chr(\base_convert($base36Chunk, 36, 10));
        }

        $shortMd5 = implode('', $base36Chrs);
    }

    return $shortMd5;
}

/**
 * Base conversion that accepts 64 also
 * @param string  $number   The number to convert
 * @param integer $frombase The original encoding
 * @param integer $tobase   The target encoding
 *
 * @return string
 */
function base_convert($number, $frombase, $tobase)
{
    if ($tobase != 64 && $frombase != 64 ) {
        return \base_convert($number, $frombase, $tobase);
    }

    $base64 = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz-_";
    
    if ($tobase == 64) {
        $b64 = "";

        if ($frombase != 2) {
            $bin = \base_convert($number, $frombase, 2);
        } else {
            $bin = $number;
        }

        foreach (str_split($bin, 6) as $bword) {
            $dword = \base_convert($bword, 2, 10);
            while ($dword > 0) {
                $mod = $dword % 64;
                $dword = ($dword - $mod) / 64;     
                $b64 .= $base64[$mod];
            };
        }

        return $b64;
    } 

    if ($frombase == 64) {
        $bin = "";

        foreach (str_split($number) as $b64word) {
            $dword = strpos($base64, $b64word);
            $bin .= \base_convert($dword, 10, 2);
        }

        return \base_convert($bin, 2, $tobase);
    }
}


/**
 * Format a local date/time
 * Improvements: supports microseconds
 * @param string  $format    The format for the date/time. See php.net . Default is ISO format
 * @param integer $timestamp The timestamp to format, if null the current timestamp is used
 * 
 * @return string
 */
function date($format="Y-m-d\TH:i:s.uP", $timestamp=false)
{
    if (!$timestamp) {
        $timestamp = \date('Y-m-d\TH:i:s') . substr(microtime(), 1, 9);
    }
    
    $datetime = new \DateTime($timestamp);
    
    return $datetime->format($format);
}

/**
 * Format a gmt date/time
 * Improvements: supports microseconds
 * @param string  $format    The format for the date/time. See php.net . Default is ISO format
 * @param integer $timestamp The timestamp to format, if null the current timestamp is used
 * 
 * @return string
 */
function gmdate($format="Y-m-d\TH:i:s.u\Z", $timestamp=false)
{
    if (!$timestamp) {
        $timestamp = \gmdate('Y-m-d\TH:i:s') . substr(microtime(), 1, 9);
    }
    
    $datetime = new \DateTime($timestamp);
    
    return $datetime->format($format);
}

/**
 * Coalesce empty values
 * @param mixed $value       The input value
 * @param mixed $replacement The value to return if value is empty
 * 
 * @return mixed
 */
function coalesce($value, $replacement)
{
    if (empty($value)) {
        return $replacement;
    }

    return $value;
}

/**
 * Encode data into json
 * Improvements: stringifies laabs data types 
 * @param mixed   $value   The input value
 * @param integer $options A bitmask of json options
 * 
 * @return string
 */
function json_encode($value, $options=0)
{
    //var_dump($value);
    $value = \laabs::export($value);

    $json = \json_encode($value, $options);

    return $json;
}

/**
 * Decode data from json
 * @param string  $json    The json value
 * @param integer $options A bitmask of json options
 * 
 * @return mixed
 * 
 * @todo  encode laabs data types 
 */
function json_decode($json, $options=0)
{
    $value = \json_decode($json, $options);

    return $value;
}

/**
 * Create a new tmp file opened in w+ (read+write) and return handler
 * Improvements : creates directory, uses laabs tmp directory
 * 
 * @return resource
 */
 function tmpfile()
 {
    $dir = \core\Globals\Server::getTmpDir();
    if (!is_dir($dir)) {
        mkdir($dir, 0755);
    }

    $uid = \laabs\uniqid();

    $filename = $dir . DIRECTORY_SEPARATOR . $uid;

    return fopen($filename, "w+");
 }

/**
 * Create a new tmp file and return filename
 * Improvements : creates directory, uses laabs tmp directory
 * @param string $dir
 * @param string $prefix
 * 
 * @return string
 */
function tempnam($dir=false, $prefix=false)
{
    if (!$dir) {
        $dir = \core\Globals\Server::getTmpDir();
    }

    if (!is_dir($dir)) {
        mkdir($dir, 0755);
    }

    return \tempnam($dir, $prefix);
}

/**
 * Create a new tmp dir and return path
 * Improvements : Not a php function
 * @param string $prefix
 * 
 * @return string
 */
function tempdir($prefix="")
{
    $dir = \core\Globals\Server::getTmpDir();

    $dirname = $dir . DIRECTORY_SEPARATOR . \laabs\uniqid($prefix);
    
    while (is_dir($dirname)) {
        $dirname = $dir . DIRECTORY_SEPARATOR . \laabs\uniqid($prefix);
    }

    mkdir($dirname, 1755);

    return $dirname;
}


