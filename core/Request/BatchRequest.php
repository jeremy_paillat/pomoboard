<?php
/*
 * Copyright (C) 2015 Maarch
 *
 * This file is part of Laabs.
 *
 * Laabs is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Laabs is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with Laabs.  If not, see <http://www.gnu.org/licenses/>.
 */
namespace core\Request;

class BatchRequest
    extends AbstractRequest
{
    use \core\ReadonlyTrait;

    public function __construct()
    {
        /* Batch.php METHOD jobName|repoName arg1=val1 arg2=val2 */
        $this->mode = 'cli';
        
        // Batch.php or any frontal
        $this->script = \laabs\basename(reset($_SERVER['argv']));

        $this->authentication = \core\Globals\Server::getAuthentication();
        
        // Action : run job, restart job, list repo
        $this->method = strtoupper(next($_SERVER['argv']));

        // Job name | Repo name
        $this->uri = next($_SERVER['argv']);

        while ($arg = next($_SERVER['argv'])) {
            $this->arguments[strtok($arg, LAABS_CLI_ARG_OPERATOR)] = strtok(LAABS_CLI_ARG_OPERATOR);
        }
    }
}