<?php
/*
 * Copyright (C) 2015 Maarch
 *
 * This file is part of Laabs.
 *
 * Laabs is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Laabs is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with Laabs.  If not, see <http://www.gnu.org/licenses/>.
 */
namespace core\Request;

class CliRequest
    extends AbstractRequest
{
    
    public function __construct()
    {
        /* Cli.php METHOD /bundle/controller/action/param1/param2 arg1=val1 arg2=val2 */
        $this->mode = 'cli';
        
        $this->script = \laabs\basename(reset($_SERVER['argv']));

        $this->authentication = \core\Globals\Server::getAuthentication();
        
        $this->method = next($_SERVER['argv']);
        
        $cmd = next($_SERVER['argv']);
        if ($cmd && $cmd[0] == LAABS_URI_SEPARATOR) {
            $cmd = substr($cmd, 1);
        }

        $this->uri = $cmd;
        
        $this->type = "url";
        
        while ($arg = next($_SERVER['argv'])) {
            if (preg_match("#^(?<type>\w+):\\/\\/(?<body>.*)$#", $arg, $matches)) {
                $this->type = $matches['type'];
                $this->body = $matches['body'];
            } else {
                $this->arguments[strtok($arg, LAABS_CLI_ARG_OPERATOR)] = strtok(LAABS_CLI_ARG_OPERATOR);
            }
        }
    }
}