<?php
/*
 * Copyright (C) 2015 Maarch
 *
 * This file is part of Laabs.
 *
 * Laabs is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Laabs is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with Laabs.  If not, see <http://www.gnu.org/licenses/>.
 */
namespace core\Request;

class HttpRequest
    extends AbstractRequest
{

    public $headers;

    public function __construct()
    {
        $this->mode = 'http';

        $this->headers = \core\Globals\Server::getHttpHeaders();

        if (!isset($this->headers['Content-Type'])) {
            $this->type = "url";
        } else {
            $this->type = $this->guessRequestType();
        }

        $this->authentication = \core\Globals\Server::getAuthentication();

        $this->body = @file_get_contents('php://INPUT');

        $this->arguments = $_REQUEST;

        switch(\core\Globals\Server::getHttpMethod()) {
            case 'POST':
                $this->method = 'CREATE';
                break;

            case 'PUT':
                $this->method = 'UPDATE';
                $this->parseUrlBody();
                break;

            case 'DELETE':
                $this->method = 'DELETE';
                $this->parseUrlBody();
                break;

            case 'OPTIONS':
                $this->method = 'PROMPT';
                $this->parseUrlBody();
                break;
                
            case 'GET':
            default:
                $this->method = 'READ';
                break;
        }

        /* (/MyFrontal.php)/bundle/controller/action/param1/param2?GET... */
        $this->script = \laabs\basename($_SERVER['SCRIPT_FILENAME']); 

        $this->parseUrl();
    }

    public function getHeader($name)
    {
        if (isset($this->headers[$name])) {
            return $this->headers[$name];
        }
    }

    public function getContentType()
    {
        return $this->getHeader('Content-Type');
    }

    /**
     * Guess the request content type
     */
    protected function guessRequestType()
    {
        $contentTypes = \core\Globals\Server::getContentTypes();
        $contentType = $this->getContentType();
        $mimeType = strtok($contentType, ";");

        if (isset($contentTypes[$mimeType])) {
            return $contentTypes[$mimeType];
        }

        throw new \Exception("Could not find a request content handler for the request content type '$contentType'");
    }

    protected function parseUrlBody()
    {
        if ($this->type == 'url' && !empty($this->body)) {
            $bodyArguments = array();
            \parse_str($this->body, $bodyArguments);
            $this->arguments = array_merge($bodyArguments, $this->arguments);
        }
    }

    protected function parseUrl()
    {
        $url = $_SERVER['SCRIPT_URL'];

        // Remove frontal script
        if ($this->script) {
            $url = str_replace(LAABS_URI_SEPARATOR . $this->script, "", $url);
        }

        // Remove leading slash
        if ($url && $url[0] == LAABS_URI_SEPARATOR) {
            $url = substr($url, 1);
        }

        $this->uri = $url;
    }

}
