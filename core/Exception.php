<?php
/*
 * Copyright (C) 2015 Maarch
 *
 * This file is part of Laabs.
 *
 * Laabs is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Laabs is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with Laabs.  If not, see <http://www.gnu.org/licenses/>.
 */
namespace core;

/**
 * The Exception class
 * 
 * @package Core
 * @see     \Exception
 */
class Exception 
    extends \Exception
{
    /* Propriétés */
    /*
    protected string $message;
    protected int $code;
    protected string $file;
    protected int $line;
    */
    protected $variables = array();
    
    /* Méthodes */
    /**
     * Constructor for a new Exception
     * Sends an event type LAABS_EXCEPTION to observers
     * @param string $message   The message of Exception
     * @param int    $code      The code of Exception
     * @param object $previous  The previous Exception for backtrace
     * @param array  $variables
     * 
     * @return void 
     */
    public function __construct ($message = "", $code = 0, \Exception $previous = null, $variables=array())
    {
        parent::__construct($message, $code, $previous);

        $this->variables = $variables;
        
        \core\Observer\Dispatcher::notify(LAABS_EXCEPTION, $this);
    }

    /**
     * Magic method for string conversion
     * @return string
     */
    public function __toString()
    {
        $string = parent::__toString();

        foreach (get_object_vars($this) as $name => $value) {
            if (!in_array($name, array_keys(get_class_vars('Exception')))) {
                $string .= PHP_EOL . $name . ": " . print_r($value, true);
            }
        }

        return $string;
    }

    /**
     * Add a variable
     * @param string $name
     * @param mixed $value
     */
    public function setVariable($name, $value)
    {
        $this->variables[$name] = $value;
    }

    /*
    final public string getMessage ( void )
    final public Exception getPrevious ( void )
    final public mixed getCode ( void )
    final public string getFile ( void )
    final public int getLine ( void )
    final public array getTrace ( void )
    final public string getTraceAsString ( void )
    public string __toString ( void )
    final private void __clone ( void )
    */
}
