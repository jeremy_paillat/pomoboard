<?php
/*
 * Copyright (C) 2015 Maarch
 *
 * This file is part of Laabs.
 *
 * Laabs is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Laabs is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with Laabs.  If not, see <http://www.gnu.org/licenses/>.
 */
namespace core\Configuration;

/**
 * Class that defines the configuration singleton object
 */
class Configuration
    extends Section
{
    /* Constants */

    /* Properties */
    /**
     * The object storage for the singleton instance
     * @var array
     * @static
     * @access protected
     */
    protected static $instance;

    /**
     * The set of variables defined on configurations
     * @var array
     */
    public $variables;

    /* Methods */
    /**
     * Get the configuration instance. It can be
     *  * from configuration files
     *  * from Instance of application if loaded
     *  * from the already instantiated instance during the same call to app
     * @return \core\Configuration\Configuration object
     */
    public static function getInstance()
    {
        if (!isset(self::$instance)) {
            if (\core\Globals\Instance::exists('Configuration')) {
                self::$instance = \core\Globals\Instance::get('Configuration');
            } else {
                self::$instance = new Configuration();
                \core\Globals\Instance::set('Configuration', self::$instance);
                $confFile = \core\Globals\Server::getConfiguration();
                self::$instance->loadFile($confFile);
            }
        }

        return self::$instance;
    }

    /**
     * Registers a variable on the global scope
     * @param string $name  The name of the variable
     * @param mixed  $value The value of the variable
     * 
     * @return void
     */
    public function registerVariable($name, $value)
    {
        $this->variables[$name] = $value;
    }

    /**
     * Retrieves a variable on the global scope
     * @param string $name the name of the variable
     * 
     * @return mixed $value The value of the variable
     */
    public function getVariable($name)
    {
        //var_dump("getVariable($name)");
        if (isset($this->variables[$name])) {
            return $this->variables[$name];
        }
    }

}