<?php
/*
 * Copyright (C) 2015 Maarch
 *
 * This file is part of Laabs.
 *
 * Laabs is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Laabs is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with Laabs.  If not, see <http://www.gnu.org/licenses/>.
 */
namespace core\Reflection;

/**
 * Class for Bundle definitions
 * 
 * @extends \core\Reflection\abstractContainer
 */
class Bundle
    extends abstractContainer
{

    /* Constants */

    /* Properties */

    /* Methods */
    /**
     * Get a bundle instance singleton.
     * The instance singleton is identified by the name of bundle
     * @param string $name The name of the bundle to get instance of
     * 
     * @return \core\Reflection\Bundle object
     */
    public static function getInstance($name)
    {
        if (!isset(self::$instances[$name])) {
            self::$instances[$name] = new Bundle($name);
        }

        return self::$instances[$name];
    }

    /**
     * Constructs a new dependency bundle
     * @param string $name The name of the bundle
     * 
     * @return void
     * 
     * @access protected
     */
    protected function __construct($name)
    {
        $this->name = $name;

        $this->uri = $name;

        $this->instance = $name;

        if (!$extendedPath = Extensions::extendedPath($this->name)) {
            throw new \Exception("Bundle '$name' not found");
        }

        /* Load Configuration */
        $configuration = \core\Configuration\Configuration::getInstance();
        $this->configuration = $configuration->export();
        $this->configuration->import($configuration->export($this->name));
    }

    /* Routines */
    /**
     * Return the name of bundle
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Returns the class to use for a given service name, searching the extensions
     * @param string $uri The uri of the service to find on the bundle. Can be a simple local name or a qualified name
     * 
     * @return string The full qualified extended class
     */
    public function getClassName($uri)
    {
        $class = $this->name . LAABS_NS_SEPARATOR . str_replace(LAABS_URI_SEPARATOR, LAABS_NS_SEPARATOR, $uri);
        if ($extendedClass = Extensions::extendedClass($class)) {
            return $extendedClass;
        }
    }

    /**
     * Returns the interface to use for a given service name, searching the extensions
     * @param string $uri The uri of the service to find on the bundle. Can be a simple local name or a qualified name
     * 
     * @return string The full qualified extended interface
     */
    public function getInterface($uri)
    {
        $interface = $this->name . LAABS_NS_SEPARATOR . str_replace(LAABS_URI_SEPARATOR, LAABS_NS_SEPARATOR, $uri);
        if ($extendedInterface = Extensions::extendedInterface($interface)) {
            return $extendedInterface;
        }
    }

    /**
     * Return the extended path for a given filename
     * @param string $uri The uri of resource to find on the bundle. Can be a simple local name or a qualified name
     * 
     * @return string The full qualified extended path
     */
    public function getPath($uri)
    {
        $path = $this->name . DIRECTORY_SEPARATOR . str_replace(LAABS_URI_SEPARATOR, DIRECTORY_SEPARATOR, $uri);
        $extendedPath = Extensions::extendedPath($path);

        return $extendedPath;
    }

    /* Route API */
    /**
     * Checks if an API is defined on the bundle
     * @param string $name The name of the API
     * 
     * @return bool
     */
    public function hasAPI($name)
    {
        return ($this->getAPI($name) != false);
    }

    /**
     * Get a API definition
     * @param string $name The name of the API
     * 
     * @return object the \core\Reflection\API object
     * 
     * @throws core\Reflection\Exception if the API is unknown or can not be instantiated
     */
    public function getAPI($name)
    {
        $uri = $name . "Interface";
        $interface = $this->getInterface($uri);

        if (!$interface) {
            throw new \Exception("API '$this->name/$name' not found");
        }

        $API = new API($uri, $interface, $this);

        return $API;
    }

    /**
     * Get all the controllers defined on the bundle
     * 
     * @return array An array of all the \core\Reflection\Controller objects
     */
    public function getAPIs()
    {
        $APIs = array();

        $interfaces = Extensions::extendedInterfaces($this->name);
        foreach ($interfaces as $interface) {
            $name = $this->name . LAABS_URI_SEPARATOR . \laabs\basename($interface);
            $APIs[] = new API($name, $interface, $this);
        }

        return $APIs;
    }

    /* Controller */
    /**
     * Checks if a controller is defined on the bundle
     * @param string $name The name of the controller
     * 
     * @return bool
     */
    public function hasController($name)
    {
        return ($this->getController($name) != false);
    }

    /**
     * Get a controller definition
     * @param string $name The name of the controller
     * 
     * @return object the \core\Reflection\Controller object
     * 
     * @throws core\Reflection\Exception if the controller is unknown or can not be instantiated
     */
    public function getController($name)
    {
        $uri = LAABS_CONTROLLER . LAABS_URI_SEPARATOR . $name;
        $class = $this->getClassName($uri);
        if (!$class) {
            throw new \Exception("Controller '$this->name/$name' not found");
        }

        $controller = new Controller($uri, $class, $this);

        if (!$controller->isInstantiable()) {
            throw new \Exception("Controller '$this->name/$name' is not instantiable");
        }

        return $controller;
    }

    /**
     * Get all the controllers defined on the bundle
     * 
     * @return array An array of all the \core\Reflection\Controller objects
     */
    public function getControllers()
    {
        $controllers = array();

        $controllerClasses = Extensions::extendedClasses($this->name . LAABS_NS_SEPARATOR . LAABS_CONTROLLER);
        foreach ($controllerClasses as $controllerClass) {
            $controllerName = \laabs\basename($controllerClass);
            $controller = new Controller(LAABS_CONTROLLER . LAABS_URI_SEPARATOR . $controllerName, $controllerClass, $this);
            if ($controller->isInstantiable()) {
                $controllers[] = $controller;
            }
        }

        return $controllers;
    }

    /* Parser */
    /**
     * Checks if a parser is defined on the bundle
     * @param string $name The name of the parser
     * @param string $type The type/adapter of the parser. Represents the contentType of the request
     * 
     * @return bool
     */
    public function hasParser($name, $type)
    {
        return ($this->getParser($name, $type) != false);
    }

    /**
     * Get a parser definition
     * @param string $name The name of the parser
     * @param string $type The type/adapter of the parser. Represents the contentType of the request
     * 
     * @return object the \core\Reflection\Parser object
     * @throws core\Reflection\Exception if the parser is unknown or can not be instantiated
     */
    public function getParser($name, $type)
    {
        $uri = LAABS_PARSER . LAABS_URI_SEPARATOR . $type . LAABS_URI_SEPARATOR . $name;
        $class = $this->getClassName($uri);

        if (!$class) {
            throw new \Exception("Parser '$this->name::$name' not found for content type $type");
        }

        $parser = new Parser($uri, $class, $this);

        if (!$parser->isInstantiable()) {
            throw new \Exception("Parser '$this->name::$name' is not instantiable");
        }

        return $parser;
    }

    /**
     * Get all the parsers defined on the bundle for a given name or type
     * @param string $name The name of the parsers to find
     * @param string $type The type/adapter of the parsers to list
     * 
     * @return array An array of all the \core\Reflection\Parser objects matching the name or type
     */
    public function getParsers($name=false, $type=false)
    {
        $parsers = array();
        $parserDirs = Extensions::extendedContents($this->name . DIRECTORY_SEPARATOR . LAABS_PARSER);
        foreach ($parserDirs as $parserDir) {
            $parserType = \laabs\basename($parserDir);
            if (!$type || $type == $parserType) {
                $parserTypeClasses = Extensions::extendedClasses($this->name . LAABS_NS_SEPARATOR . LAABS_PARSER . LAABS_NS_SEPARATOR . $parserType);
                foreach ($parserTypeClasses as $parserTypeClass) {
                    $parserName = \laabs\basename($parserTypeClass);
                    if (!$name || $parserName == $name) {
                        $parser = new Parser(LAABS_PARSER . LAABS_URI_SEPARATOR . $type . LAABS_URI_SEPARATOR . $name, $parserTypeClass, $this);
                        if ($parser->isInstantiable()) {
                            $parsers[] = $parser;
                        }
                    }
                }
            }
        }

        return $parsers;
    }


    /* Serializer */
    /**
     * Checks if a serializer is defined on the bundle
     * @param string $name The name of the serializer
     * @param string $type The type/adapter of the serializer. Represents the contentType of the response
     *
     * @return bool
     */
    public function hasSerializer($name, $type)
    {
        return ($this->getSerializer($name, $type) != false);
    }

    /**
     * Get a serializer definition
     * @param string $name The name of the serializer
     * @param string $type The type/adapter of the serializer. Represents the contentType of the response
     * 
     * @return object the \core\Reflection\Serializer object
     * 
     * @throws core\Reflection\Exception if the serializer is unknown or can not be instantiated
     */
    public function getSerializer($name, $type)
    {
        $uri = LAABS_SERIALIZER . LAABS_URI_SEPARATOR . $type . LAABS_URI_SEPARATOR . $name;
        $class = $this->getClassName($uri);

        if (!$class) {
            throw new \Exception("Serializer '$this->name::$name' not found for content type $type");
        }

        $serializer = new Serializer($uri, $class, $this);

        if (!$serializer->isInstantiable()) {
            throw new \Exception("Serializer '$this->name::$name' is not instantiable");
        }

        return $serializer;
    }

    /**
     * Get all the serializers defined on the bundle for a given name or type
     * @param string $name The name of the serializers to find
     * @param string $type The type/adapter of the serializers to list
     * 
     * @return array An array of all the \core\Reflection\Serializer objects matching the name or type
     */
    public function getSerializers($name=false, $type=false)
    {
        $serializers = array();
        $serializerDirs = Extensions::extendedContents($this->name . DIRECTORY_SEPARATOR . LAABS_SERIALIZER);
        foreach ($serializerDirs as $serializerDir) {
            $serializerType = \laabs\basename($serializerDir);
            if (!$type || $type == $serializerType) {
                $serializerTypeClasses = Extensions::extendedClasses($this->name . LAABS_NS_SEPARATOR . LAABS_SERIALIZER . LAABS_NS_SEPARATOR . $type);
                foreach ($serializerTypeClasses as $serializerTypeClass) {
                    $serializerName = \laabs\basename($serializerTypeClass);
                    if (!$name || $serializerName == $name) {
                        $serializer = new Serializer(LAABS_SERIALIZER . LAABS_URI_SEPARATOR . $type . LAABS_URI_SEPARATOR . $name, $serializerTypeClass, $this);
                        if ($serializer->isInstantiable()) {
                            $serializers[] = $serializer;
                        }

                    }
                }
            }
        }

        return $serializers;
    }

    /* Resource */
    /**
     * Checks if a resource is available on the bundle
     * @param string $name The name/uri of the resource
     * 
     * @return bool
     */
    public function hasResource($name)
    {
        $uri = LAABS_RESOURCE . LAABS_URI_SEPARATOR . $name;

        return (($this->getPath($uri)) != false);
    }

    /**
     * Get a ressource definition
     * @param string $name The name of the resource
     * 
     * @return object the \core\Reflection\Resource object
     * @throws core\Reflection\Exception if the resource is unknown
     */
    public function getResource($name)
    {
        $uri = LAABS_RESOURCE . LAABS_URI_SEPARATOR . $name;
        $path = $this->getPath($uri);

        if (!$path) {
            throw new \Exception("Resource '$this->name/$name' not found");
        }

        return new Resource($name, $path, $this->instance);
    }

    /* Model */
    /**
     * Checks if a class is defined on the bundle
     * @param string $name The name of the class
     * 
     * @return bool
     */
    public function hasClass($name)
    {
        return ($this->getClass($name) != false);
    }

    /**
     * Get a class definition
     * @param string $name The name of the class
     * 
     * @return object the \core\Reflection\Type object
     * @throws core\Reflection\Exception if the class is unknown or can not be instantiated
     */
    public function getClass($name)
    {
        $uri = LAABS_MODEL . LAABS_URI_SEPARATOR . $name;
        $classname = $this->getClassName($uri);
        if (!$classname) {
            throw new \Exception("Type '$this->name/$name' not found");
        }

        $class = new Type($uri, $classname, $this);

        return $class;
    }

    /**
     * Get all the types defined on the bundle
     * @return array An array of all the \core\Reflection\Type objects
     */
    public function getClasses()
    {
        $classes = array();
        $classnames = Extensions::extendedClasses($this->name . LAABS_NS_SEPARATOR . LAABS_MODEL);
        foreach ($classnames as $classname) {
            $class = new Type(LAABS_MODEL . LAABS_URI_SEPARATOR . \laabs\basename($classname), $classname, $this);
            $classes[$class->getName()] = $class;
        }

        return $classes;
    }
    
    /* Exception */
    /**
     * Checks if an exception is defined on the bundle
     * @param string $name The name of the exception
     * 
     * @return bool
     */
    public function hasException($name)
    {
        return (($this->getClassName(LAABS_EXCEPTION . LAABS_URI_SEPARATOR . $name)) != false);
    }

    /**
     * Get an exception definition
     * @param string $name The name of the exception
     * 
     * @return object the \core\Reflection\Exception object
     * 
     * @throws core\Reflection\Exception if the exception is unknown or can not be instantiated
     */
    public function getException($name)
    {
        $uri = LAABS_EXCEPTION . LAABS_URI_SEPARATOR . $name;
        $class = $this->getClassName($uri);
        if (!$class) {
            throw new \Exception("Exception '$this->name/$name' not found");
        }

        $exception = new Exception($uri, $class, $this);

        if (!$exception->isInstantiable()) {
            throw new \Exception("Exception '$this->name/$name' is not instantiable");
        }

        return $exception;
    }

    /**
     * Get all the exceptions defined on the bundle
     * 
     * @return array An array of all the \core\Reflection\Exception objects
     */
    public function getExceptions()
    {
        $exceptions = array();
        $exceptionClasses = Extensions::extendedClasses($this->name . LAABS_NS_SEPARATOR . LAABS_EXCEPTION);
        foreach ($exceptionClasses as $exceptionClass) {
            $exceptions[] = new Exception(LAABS_EXCEPTION . LAABS_URI_SEPARATOR . \laabs\basename($exceptionClass), $exceptionClass, $this);
        }

        return $exceptions;
    }

    /**
     * Instantiate a new exception object from bundle exception component
     * @param string $name The name of the exception to instantiate
     * 
     * @return object
     */
    public function newException($name)
    {
        $exception = $this->getException($name);

        $args = func_get_args();
        array_shift($args);

        return $exception->newInstance($args);
    }

        /**
     * Indicates whether the container has a batch job or not
     * @param string $jobname The name of the job
     * 
     * @return bool
     */
    public function hasJob($jobname)
    {
        return ($this->getClassName(LAABS_BATCH . LAABS_URI_SEPARATOR . $jobname) !== null);
    }

    /**
     * Returns a core reflection job object
     * @param string $jobname The name of the job
     * 
     * @return \core\Reflection\Job The job object
     * 
     * @throws Exception if the job is not declared by the container
     */
    public function getJob($jobname)
    {
        $class = $this->getClassName(LAABS_BATCH . LAABS_URI_SEPARATOR . $jobname);
        if (!$class) {
            throw new \core\Exception("Undefined job '$this->uri/$jobname'");
        }

        $job = new Job($jobname, $class, $this);

        return $job;
    }

    /**
     * List the jobs
     * 
     * @return \core\Reflection\Job[] An array of jobs classes
     */
    public function getJobs()
    {
        $jobs = array();

        $jobClasses = Extensions::extendedClasses($this->name . LAABS_NS_SEPARATOR . LAABS_BATCH);
        foreach ($jobClasses as $jobClass) {
            $jobName = \laabs\basename($jobClass);
            $job = new Job(LAABS_BATCH . LAABS_URI_SEPARATOR . $jobName, $jobClass, $this);
            if ($job->isInstantiable()) {
                $jobs[] = $job;
            }
        }

        return $jobs;
    }

    /**
     * Indicates whether the container has an observer or not
     * @param string $observername The name of the observer
     * 
     * @return bool
     */
    public function hasObserver($observername)
    {
        return ($this->getClassName(LAABS_OBSERVER . LAABS_URI_SEPARATOR . $observername) !== null);
    }

    /**
     * Returns a core reflection observer object
     * @param string $observername The name of the observer
     * 
     * @return \core\Reflection\Observer The observer object
     * 
     * @throws Exception if the observer is not declared by the container
     */
    public function getObserver($observername)
    {
        $class = $this->getClassName(LAABS_OBSERVER . LAABS_URI_SEPARATOR . $observername);
        if (!$class) {
            throw new \core\Exception("Undefined observer '$this->uri/$service'");
        }

        $observer = new Observer($observername, $class, $this);

        return $observer;
    }

    /**
     * List the observers
     * 
     * @return \core\Reflection\Observer[] An array of observers classes
     */
    public function getObservers()
    {
        $observers = array();

        $observerClasses = Extensions::extendedClasses($this->name . LAABS_NS_SEPARATOR . LAABS_OBSERVER);
        foreach ($observerClasses as $observerClass) {
            $observerName = \laabs\basename($observerClass);
            $observer = new Observer(LAABS_OBSERVER . LAABS_URI_SEPARATOR . $observerName, $observerClass, $this);
            if ($observer->isInstantiable()) {
                $observers[] = $observer;
            }
        }

        return $observers;
    }

}
