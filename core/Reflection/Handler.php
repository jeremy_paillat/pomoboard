<?php
/*
 * Copyright (C) 2015 Maarch
 *
 * This file is part of Laabs.
 *
 * Laabs is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Laabs is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with Laabs.  If not, see <http://www.gnu.org/licenses/>.
 */
namespace core\Reflection;

/**
 * Class Laabs Handler
 * 
 * @extends \core\Reflection\Method
 */
class Handler
    extends Method
{

    /* Constants */

    /* Properties */
    public $subject;

    /* Methods */
    /**
     * Constructs a new handler instance
     * @param string $name             The name of handler
     * @param string $class            The class of the handler
     * @param string $serviceContainer The service container
     */
    public function __construct($name, $class, $serviceContainer)
    {
        parent::__construct($name, $class, $serviceContainer);

        // Set observed subject
        $docComment = $this->getDocComment();
        if (preg_match("#@subject (?<subject>[^\s]+)#", $docComment, $matches)) {
            $subject = trim($matches['subject']);

            if (defined($subject)) {
                $subject = constant($subject);
            }
            $this->subject = $subject;
        }
    }

    /**
     * Call the handler
     * @param object $observerObject Observer of the handler
     * @param array  $passedArgs     Arguments array for the handler
     * 
     * @return mixte 
     */
    public function notify($observerObject = null, array $passedArgs = null)
    {
        $response = $this->invokeArgs($observerObject, $passedArgs);

        return $response;
    }

}
