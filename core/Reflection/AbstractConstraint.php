<?php
/*
 * Copyright (C) 2015 Maarch
 *
 * This file is part of Laabs.
 *
 * Laabs is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Laabs is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with Laabs.  If not, see <http://www.gnu.org/licenses/>.
 */
namespace core\Reflection;

/**
 * Class for abstract constraint
 * 
 */
abstract class AbstractConstraint
{
    use \core\ReadonlyTrait;

    /* Constants */

    /* Properties */
    /**
     * constraint name
     * @var string
     */
    public $name;

    /**
     * constraint type
     * @var string
     */
    public $type;

    /**
     * The declaring class name
     * @var string
     */
    public $class;



    /* Methods */
    /**
     * Constructor of the constraint
     * @param string $name  The uri of the service
     * @param string $class The declaring class
     */
    public function __construct($name, $class)
    {
        $this->name = $name;

        $this->class = $class;
    }

    /**
     * Get the name
     * 
     * @return string
     */
    public function getName()
    {
        return $this->class . LAABS_URI_SEPARATOR . $this->name;
    }

    /**
     * Get the type
     * 
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Get the class
     * 
     * @return object
     */
    public function getClass()
    {
        return $this->class;
    }

    /**
     * Get the schema
     * 
     * @return object
     */
    public function getSchema()
    {
        return strtok($this->class, LAABS_URI_SEPARATOR);
    }  

}
