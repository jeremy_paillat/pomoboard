<?php
/*
 * Copyright (C) 2015 Maarch
 *
 * This file is part of Laabs.
 *
 * Laabs is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Laabs is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with Laabs.  If not, see <http://www.gnu.org/licenses/>.
 */
namespace core\Reflection;

/**
 * Class for reflection API
 * 
 * @extends \core\Reflection\Service
 */
class API
    extends \ReflectionClass
{
    
    use \core\ReadonlyTrait, docCommentTrait;

    /* Constants */

    /* Properties */
    /**
     * The name of the interface
     * @var string
     */
    public $interface;

    /**
     * The uri of the service
     * @var string
     */
    public $uri;

    /**
     * The container domain
     * @var string
     */
    public $domain;

    /* Methods */
    /**
     * Constructor of the API
     * @param string $uri       The uri of the API
     * @param string $class     The class of the API
     * @param object $container The API container object
     */
    public function __construct($uri, $class, $container)
    {
        $this->interface = \laabs\basename($uri, 'Interface');

        $this->uri = $uri;

        $this->domain = $container->instance;

        parent::__construct($class); 
    }

    /**
     * Getter for component name
     * 
     * @return string The value of the property
     */
    public function getName() 
    {
        return $this->domain . LAABS_URI_SEPARATOR . $this->interface;
    }

    /**
     * Check if route exists
     * @param string $name the name of the route
     * 
     * @return boolean
     */
    public function hasRoute($name)
    {
        return parent::hasMethod($name);
    }

    /**
     * Returns the API routes
     * @param int $filter
     * 
     * @return array An array of route objects declared for the API
     */
    public function getRoutes($filter=null)
    {
        $reflectionMethods = parent::getMethods(Method::IS_PUBLIC);
        $routes = array();

        for ($i=0, $l=count($reflectionMethods); $i<$l; $i++) {
            $reflectionMethod = $reflectionMethods[$i];
            if ($reflectionMethod->isConstructor()
                || $reflectionMethod->isDestructor()
                || $reflectionMethod->isStatic()
            ) {
                continue;
            }
            $routes[] = new Route($reflectionMethod->name, $this->name, $this->domain);
        }

        return $routes;
    }

    /**
     * Get a API route declaration from its name
     * @param string $name The name of the route
     * 
     * @return object The Method object
     * 
     * @throws Exception if the route is not declared by the API
     */
    public function getRoute($name)
    {
        if (!parent::hasMethod($name)) {
            throw new \core\Exception("Undefined route '$this->domain/$this->interface/$name'");
        }

        $route = new Route($name, $this->name, $this->domain);

        if (!$route->isPublic()) {
            throw new \core\Exception("Route '$name' is not public");
        }

        return $route;
    }
}
