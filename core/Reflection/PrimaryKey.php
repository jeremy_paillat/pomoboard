<?php
/*
 * Copyright (C) 2015 Maarch
 *
 * This file is part of Laabs.
 *
 * Laabs is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Laabs is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with Laabs.  If not, see <http://www.gnu.org/licenses/>.
 */
namespace core\Reflection;

/**
 * Class for PK definitions
 * 
 */
class PrimaryKey
    extends Key
{

    /* Constants */

    /* Properties */


    /* Methods */
    /**
     * Constructor of the injection service
     * @param string $name   The uri of the service     
     * @param string $class  The declaring class
     * @param array  $fields The array of fields, i.e. property names
     */
    public function __construct($name, $class, $fields)
    {
        parent::__construct($name, $class, $fields);
        
        $this->type = "PRIMARY KEY";
    }

}
