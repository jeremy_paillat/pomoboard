<?php
/*
 * Copyright (C) 2015 Maarch
 *
 * This file is part of Laabs.
 *
 * Laabs is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Laabs is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with Laabs.  If not, see <http://www.gnu.org/licenses/>.
 */
namespace core\Reflection;
/**
 * Class that defines a service
 * 
 * @uses \core\ReadonlyTrait
 */
class Service
    extends \ReflectionClass
{
    use \core\ReadonlyTrait, docCommentTrait;

    /* Constants */

    /* Properties */
    /**
     * The name of the service
     * @var string
     */
    public $component;

    /**
     * The uri of the service
     * @var string
     */
    public $uri;

    /**
     * The class of the service
     * @var string
     */
    public $phpclass;

    /**
     * The uri of the container instance holding the service
     * @var string
     */
    public $container;

    /**
     * The configuration section of the service
     * @var core\Configuration\Section
     */
    public $configuration;

    /* Methods */
    /**
     * Constructor of the injection service
     * @param string $uri       The uri of the service
     * @param string $class     The class of the service
     * @param object $container The service container object
     */
    public function __construct($uri, $class, $container)
    {
        $this->component = \laabs\basename($uri);

        $this->uri = $uri;

        $this->phpclass = $class;

        $this->container = $container->instance;

        parent::__construct($class);

        $configuration = $container->configuration->export();
        $configuration->import($container->configuration->export($this->uri));

        $this->configuration = $configuration;
    }

    /**
     * Getter for component name
     * 
     * @return string The value of the property
     */
    public function getName() 
    {
        return $this->component;
    }

    /**
     * Returns the service methods
     * @param int $filter
     * 
     * @return array An array of Method objects declared for the service
     */
    public function getMethods($filter=null)
    {
        $reflectionMethods = parent::getMethods(Method::IS_PUBLIC);
        $methods = array();

        for ($i=0, $l=count($reflectionMethods); $i<$l; $i++) {
            $reflectionMethod = $reflectionMethods[$i];
            if ($reflectionMethod->isConstructor()
                || $reflectionMethod->isDestructor()
                || $reflectionMethod->isStatic()
            ) {
                continue;
            }
            $methods[] = new Method($reflectionMethod->name, $this->name, $this->container);
        }

        return $methods;
    }

    /**
     * Get a service method declaration from its name
     * @param string $name The name of the method
     * 
     * @return object The Method object
     * 
     * @throws Exception if the method is not declared by the service
     */
    public function getMethod($name)
    {
        if (!$this->hasMethod($name)) {
            throw new \core\Exception("Undefined method '$this->container/$this->component/$name'");
        }

        $method = new Method($name, $this->name, $this->container);

        if (!$method->isPublic()) {
            throw new \core\Exception("Method '$name' is not public");
        }

        return $method;
    }

    /**
     * Get a service construction method declaration from its name
     * 
     * @return object The construction method object
     * 
     * @throws Exception if the method is not declared by the service
     */
    public function getConstructor()
    {
        if (($constructor = parent::getConstructor()) && $constructor->isPublic()) {
            return $this->getMethod($constructor->name);
        }
    }

    /**
     * Call the service with an array of parameters
     * Send a LAABS_SERVICE_CALL before call and a LAABS_SERVICE_OBJECT after call to observers
     * param array $passedArgs An indexed or associative array of arguments to be passed to the service
     * 
     * @return object The Service object
     * 
     * @see newInstance();
     */
    public function call()
    {
        $callArgs = func_get_args();
        
        return $this->callArgs($callArgs);
    }

    /**
     * Call the service with parameters
     * Send a LAABS_SERVICE_CALL before call and a LAABS_SERVICE_OBJECT after call to observers
     * @param array $passedArgs An indexed or associative array of arguments to be passed to the service
     * 
     * @return object The Service object
     * 
     * @see newInstance();
     */
    public function callArgs(array $passedArgs=null)
    {
        \core\Observer\Dispatcher::notify(LAABS_SERVICE_CALL, $this, $passedArgs);

        $serviceObject = $this->newInstance($passedArgs);

        \core\Observer\Dispatcher::notify(LAABS_SERVICE_OBJECT, $serviceObject);

        return $serviceObject;
    }

    /**
     * Instantiate the service object for the service declaration
     * @param array $passedArgs An indexed or associative array of arguments to be passed to the service
     * 
     * @return object The service object
     */
    public function newInstance($passedArgs=array())
    {
        // Get construction method
        if ($constructor = $this->getConstructor()) {
            
            $contructorArgs = $constructor->getCallArgs($passedArgs, $this->configuration);
            $serviceObject = parent::newInstanceArgs($contructorArgs);

        } elseif ($this->hasMethod($this->component) && ($method = $this->getMethod($this->component)) && $method->isStatic() && $method->isPublic()) {
            
            $staticFactory = $this->getMethod($this->component);
            $staticFactoryArgs = $factory->getCallArgs($passedArgs, $this->configuration);
            $serviceObject = $staticFactory->callArgs(null, $staticFactoryArgs);
        
        } else {
            
            $serviceObject = parent::newInstanceWithoutConstructor();
        
        }

        $this->useTraits($serviceObject, $passedArgs);

        return $serviceObject;
    }

    /**
     * Triggers the use of traits for the service
     * If trait has a method that has the same name, the method will be called
     * @param object $serviceObject The service object
     * @param array  $passedArgs    An indexed or associative array of arguments to be passed to the trait invocation method
     * 
     * @return void
     * 
     * @access protected
     */
    protected function useTraits($serviceObject, $passedArgs)
    {
        foreach (\laabs\class_uses($serviceObject) as $trait) {
            $traitName = \laabs\basename($trait);
            $traitOrigin = strtok($trait, LAABS_NS_SEPARATOR);
            if ($traitOrigin == LAABS_DEPENDENCY && method_exists($serviceObject, $traitName)) {
                $useMethod = new Method($traitName, $trait, $this->container);
                $callArgs = $useMethod->getCallArgs($passedArgs, $this->configuration, $onlyAssoc = true);

                if ($useMethod->isStatic()) {
                    call_user_func_array($this->name . "::" . $traitName, $callArgs);
                } else {
                    call_user_func_array(array($serviceObject, $traitName), $callArgs);
                }
            }
        }
    }

}