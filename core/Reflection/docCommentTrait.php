<?php
/*
 * Copyright (C) 2015 Maarch
 *
 * This file is part of Laabs.
 *
 * Laabs is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Laabs is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with Laabs.  If not, see <http://www.gnu.org/licenses/>.
 */
namespace core\Reflection;

trait docCommentTrait
{

    public $docComment;

    /**
     * Parse the doc comment into an associative array
     * 
     * @return array
     */
    public function parseDocComment()
    {
        $this->docComment = new docComment();
        $this->docComment->_source = parent::getDocComment();
        $lines = \laabs\explode("\n", $this->docComment->_source);
        $texts = array();

        array_shift($lines);
        array_pop($lines);
        foreach ($lines as $line) {
            $line = trim(substr($line, 1));
            if (!$line) {
                continue;
            }
            if ($line[0] == '@') {
                $tag = substr(strtok($line, ' '), 1);
                $value = strtok("\n");

                switch($tag) {
                    // Method
                    case 'param':
                        $this->parseParam($value);
                        break;

                    case 'return':
                        $this->parseReturn($value);
                        break;

                    case 'request':
                        $this->parseRequest($value);
                        break;

                    case 'action':
                        $this->parseAction($value);
                        break;
                }

            } else {
                if (isset($this->docComment->text)) {
                    $this->docComment->text .= ' ';
                }
                $this->docComment->text .= $line;
            }

        }

        var_dump($this->docComment);
    }

    protected function parseParam($value)
    {
        if (!isset($this->docComment->param)) {
            $this->docComment->param = array();
        }

        $param = new docComment();

        $param->type = strtok($value, ' ');
        $param->name = strtok(' ');
        $param->text = strtok("\n");
    
        $this->docComment->param[] = $param;
    }

    protected function parseReturn($value)
    {
        $return = new docComment();

        $return->type = strtok($value, ' ');
        $return->text = strtok("\n");
    
        $this->docComment->return = $return;
    }

    protected function parseRequest($value)
    {
        $request = new docComment();

        $request->method = strtok($value, ' ');
        $request->uri = strtok("\n");
    
        $this->docComment->request = $request;
    }

    protected function parseAction($value)
    {
        $action = new docComment();

        $action->uri = $value;
    
        $this->docComment->action = $action;
    }


}