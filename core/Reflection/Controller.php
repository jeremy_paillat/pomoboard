<?php
/*
 * Copyright (C) 2015 Maarch
 *
 * This file is part of Laabs.
 *
 * Laabs is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Laabs is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with Laabs.  If not, see <http://www.gnu.org/licenses/>.
 */
namespace core\Reflection;

/**
 * Class for Controller definitions
 * 
 * @extends \core\Reflection\Service
 */
class Controller
    extends Service
{

    /* Constants */

    /* Properties */

    /* Methods */
    /**
     * Checks if action is defined on the controller
     * @param string $name The name of the action
     * 
     * @return bool
     */
    public function hasAction($name)
    {
        return $this->hasMethod($name);
    }

    /**
     * Get all actions defined on the controller
     * 
     * @return array An array of all the \core\Reflection\Action objects
     */
    public function getActions()
    {
        $reflectionMethods = $this->getMethods(\ReflectionMethod::IS_PUBLIC);
        $actions = array();
        for ($i=0, $l=count($reflectionMethods); $i<$l; $i++) {
            $reflectionMethod = $reflectionMethods[$i];
            if ($reflectionMethod->isConstructor()
                || $reflectionMethod->isDestructor()
                || $reflectionMethod->isStatic()
                || $reflectionMethod->isAbstract()
            ) {
                continue;
            }

            $actions[] = new Action($reflectionMethod->name, $this->name, $this->container);
        }

        return $actions;
    }

    /**
     * Get a controller definition
     * @param string $name The name of the action
     * 
     * @return object the \core\Reflection\Action object
     * 
     * @throws core\Reflection\Exception if the action is unknown or not public
     */
    public function getAction($name)
    {
        if (!$this->hasAction($name)) {
            throw new \Exception("Undefined action '$this->container/$this->name/$name'");
        }

        $action = new Action($name, $this->name, $this->container);

        if (!$action->isPublic()) {
            throw new \Exception("Action '$name' is not public");
        }

        return $action;
    }

}
