<?php
/*
 * Copyright (C) 2015 Maarch
 *
 * This file is part of Laabs.
 *
 * Laabs is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Laabs is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with Laabs.  If not, see <http://www.gnu.org/licenses/>.
 */
namespace core\Reflection;

/**
 * Class for Type definitions
 * 
 * @extends \core\Reflection\Service
 */
class Type
    extends \ReflectionClass
{
    use \core\ReadonlyTrait, docCommentTrait;

    /* Constants */

    /* Properties */
    /**
     * The name of the service
     * @var string
     */
    public $component;

    /**
     * The uri of the service
     * @var string
     */
    public $uri;

    /**
     * The class of the service
     * @var string
     */
    public $phpclass;

    /**
     * The uri of the container instance holding the service
     * @var string
     */
    public $container;

    /* Properties */
    /**
     * Pkey definition
     * @var \core\Reflection\PrimaryKey
     */
    public $pkey;

    /**
     * key definitions
     * @var \core\Reflection\Key[]
     */
    public $key = array();

    /**
     * fkey definitions
     * @var \core\Reflection\ForeignKey[]
     */
    public $fkey = array();

    /**
     * @var string
     */
    public $extension;

    /**
     * @var string
     */
    public $substitution;

    /* Methods */
    /**
     * Constructor of the injection service
     * @param string $uri       The uri of the service
     * @param string $class     The class of the service
     * @param object $container The service container object
     */
    public function __construct($uri, $class, $container)
    {
        $this->component = \laabs\basename($uri);

        $this->uri = $uri;

        $this->phpclass = $class;

        $this->container = $container->instance;

        parent::__construct($class);

        if ($parentClass = $this->getParentClass()) {
            $this->importDocComments($parentClass->getName());
            $this->extension = \laabs::getClassName($parentClass->name);
        }

        $docComment = $this->getDocComment();

        if (preg_match("#@substitution (.*)#", $docComment, $substitutionMatch)) {
            $this->substitution = $substitutionMatch[1];
        }

        if (preg_match("#@pkey (.*)#", $docComment, $keyMatch)) {
            $keyInfo = $keyMatch[1];
            if (preg_match("#(?<name>\w+)?\s*\[(?<fields>[^\)]+)\]#", $keyInfo, $parser)) {

                $name = $parser['name'];
                if (!$name) {
                    $name = "pkey";
                }

                $fields = \laabs\explode(", ", $parser['fields']);
                foreach ($fields as $pos => $field) {
                    $fields[$pos] = trim($field);
                }

                $this->pkey = new PrimaryKey($name, $this->container . LAABS_URI_SEPARATOR .$this->component, $fields);
            }
        }

        if (preg_match_all("#@key (.*)#", $docComment, $keyMatches, PREG_SET_ORDER)) {
            foreach ($keyMatches as $pos => $keyMatch) {
                $keyInfo = $keyMatch[1];
                preg_match("#(?<name>\w+)?\s*\[(?<fields>[^\)]+)\]#", $keyInfo, $parser);

                $fields = \laabs\explode(", ", $parser['fields']);
                foreach ($fields as $pos => $field) {
                    $fields[$pos] = trim($field);
                }

                $name = $parser['name'];
                if (!$name) {
                     $name = "unique" . LAABS_URI_SEPARATOR . count($this->key);
                }

                $this->key[] = new Key($name, $this->container . LAABS_URI_SEPARATOR .$this->component, $fields);
            }
        }

        if (preg_match_all("#@fkey (.*)#", $docComment, $fkeyMatches, PREG_SET_ORDER)) {
            foreach ($fkeyMatches as $pos => $fkeyMatch) {
                $keyInfo = $fkeyMatch[1];
                
                preg_match("#(?<name>\w+)?\s*\[(?<fields>[^\)]+)\]\s*(?<refname>[\w\/]+)\s*\[(?<reffields>[^\)]+)\]#", $keyInfo, $parser);

                $fields = \laabs\explode(", ", $parser['fields']);
                foreach ($fields as $pos => $field) {
                    $fields[$pos] = trim($field);
                }

                $reffields = \laabs\explode(", ", $parser['reffields']);
                foreach ($reffields as $pos => $reffield) {
                    $refields[$pos] = trim($reffield);
                }

                $refname = $parser['refname'];

                $name = $parser['name'];
                if (!$name) {
                     $name = "fkey" . LAABS_URI_SEPARATOR . count($this->fkey);
                }

                $this->fkey[] = new ForeignKey($name, $this->container . LAABS_URI_SEPARATOR .$this->component, $fields, $refname, $reffields);
            }
        }

    }

    public function __wakeup()
    {
        parent::__construct($this->uri, $this->phpclass, $this->container);
    }

    protected function importDocComments($className)
    {
        $typeName = \laabs::getClassName($className);
        $type = \laabs::getClass($typeName);
        
        if ($parentPKey = $type->getPrimaryKey()) {
            $this->pkey = new PrimaryKey($parentPKey->getName(), $this->container . LAABS_URI_SEPARATOR .$this->component, $parentPKey->getFields());
        }
        
        $parentKeys = $type->getKeys("UNIQUE");
        foreach ($parentKeys as $parentKey) {
            $this->key[] = new Key($parentKey->getName(), $this->container . LAABS_URI_SEPARATOR .$this->component, $parentKey->getFields());
        }
        
        $parentFKeys = $type->getForeignKeys();
        foreach ($parentFKeys as $parentFKey) {
            $this->fkey[] = new ForeignKey($parentFKey->getName(), $this->container . LAABS_URI_SEPARATOR .$this->component, $parentFKey->getFields(), $parentFKey->getRefClass(), $parentFKey->getRefFields());
        }
    }

    /**
     * Instantiate the type object for the service declaration
     * @param array $passedArgs An indexed or associative array of arguments to be passed to the service
     * 
     * @return object The new object
     */
    public function newInstance($passedArgs=array())
    {
        // Get construction method
        if ($constructor = $this->getConstructor()) {
            $object = parent::newInstanceArgs($passedArgs);
        } else {
            $object = parent::newInstanceWithoutConstructor();        
        }

        return $object;
    }

    /** 
     * Get the parent type name
     * @return string
     */
    public function getExtensionName()
    {
        return $this->extension;
    }

    /** 
     * Get the base type name
     * @return string
     */
    public function getBaseName()
    {
        if ($this->substitution) {
            $baseType = \laabs::getClass($this->substitution);

            return $baseType->getBaseName();
        }

        return $this->container . LAABS_URI_SEPARATOR . $this->component;
    }

    /** 
     * Get the base type
     * @return type
     */
    public function getBaseType()
    {
        if ($this->substitution) {
            $baseType = \laabs::getClass($this->substitution);

            return $baseType->getBaseType();
        }

        return $this;
    }

    /**
     * Get the fully qualified name
     * @return string
     */
    public function getName()
    {
        return $this->container . LAABS_URI_SEPARATOR . $this->component;
    }

    /**
     * Get the schema (bundle)
     * @return \core\Reflection\bundle
     */
    public function getSchema()
    {
        return $this->container;
    }

    /**
     * Get a property definition
     * @param string $name The name of the property to get
     * 
     * @return \core\Reflection\Property The property definition object
     */
    public function getProperty($name)
    {
        if (isset($this->properties[$name])) {
            return $this->properties[$name];
        }

        $rProperty = parent::getProperty($name);
        
        return new Property($rProperty->name, $this->name, $this);
    }

    /**
     * Get all property definitions
     * @param int $filter A bitmask of property modifiers
     * 
     * @return array of \core\Reflection\Property The property definition objects
     */
    public function getProperties($filter=null)
    {
        if(\core\Globals\Instance::hasProperties($this->getName())) {
            return \core\Globals\Instance::getProperties($this->getName());
        } else {
            $properties = array();

            foreach ((array) parent::getProperties() as $rProperty) {
                $properties[$rProperty->name] = new Property($rProperty->name, $this->name, $this);
            }

            return $properties;
        }
    }

    /**
     * Check keys
     * @return boolean
     */
    public function hasKey() 
    {
        return (isset($this->pkey) || isset($this->key));
    }
    
    /**
     * Get all key definitions
     * @param string $filter The type of key (primary or unique)
     * 
     * @return array of \core\Reflection\Key The key definition objects
     */
    public function getKeys($filter=false)
    {
        $keys = array();

        if ((!$filter || $filter == "PRIMARY KEY") && $this->pkey) {
            $keys[] = $this->pkey;
        }

        if (!$filter || $filter == "UNIQUE") {
            return array_merge($keys, $this->key);
        }

        return $this->key;
    }

    /**
     * Check primary key
     * @return boolean
     */
    public function hasPrimaryKey() 
    {
        return isset($this->pkey);
    }

    /**
     * Retrieve the Sdo primary Key
     *  @return object the key object
     */
    public function getPrimaryKey() 
    {
        return $this->pkey;
    }

    /* Foreign Key methods */
    /**
     * Retrieve the Sdo Foreign Keys
     *  @param string $refClass filter on reference class (unqualified or qualified name)
     * 
     *  @return array Sdo Foreign Key
     */
    public function getForeignKeys($refClass=null)
    {       
        if ($refClass) {
            $fkeys = array();
            foreach ($this->fkey as $fkey) {
                if ($fkey->refClass == $refClass) {
                    $fkeys[] = $fkey;
                }
            }

            return $fkeys;
        } else {
            return $this->fkey;
        }
        
    }


    /**
     * List properties available on the given data object VS class properties
     * @param object $object The object holding the data 
     *
     * @return array
     * @author 
     */
    public function getObjectProperties($object)
    {
        $properties = $this->getProperties();
        reset($properties);
        $usableProperties = array();
        do {
            $property = current($properties);
            
            $propertyName = $property->getName();
            if ($property->isPublic() && $property->isSimple() && property_exists($object, $propertyName)) {
                $usableProperties[] = $property;
            }
        
        } while (next($properties));

        return $usableProperties;
    }

    /**
     * Checks if the type is a descendant of another class
     * @param string $typename
     * 
     * @return boolean
     */
    public function isSubtypeOf($typename)
    {
        $ancestor = \laabs::getClass($typename);

        return $this->isSubclassOf($ancestor->name);
    }

   

}
