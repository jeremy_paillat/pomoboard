<?php
/*
 * Copyright (C) 2015 Maarch
 *
 * This file is part of Laabs.
 *
 * Laabs is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Laabs is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with Laabs.  If not, see <http://www.gnu.org/licenses/>.
 */
namespace core\Reflection;
/**
 * Class that defines a service or method parameter
 * 
 * @uses \core\ReadonlyTrait
 */
class Parameter
    extends \ReflectionParameter
{
    use \core\ReadonlyTrait;

    /* Properties */
    /**
     * @var string The type of the param
     */
    public $type;

    /**
     * @var string The documentation
     */
    public $doc;    

    /* Methods */
    /**
     * Constructor of the injection service method parameter
     * @param string $name   The name of the parameter
     * @param string $method The name of the method
     * @param string $class  The class of the service that declares the method
     * @param string $type   The type of parameter retrieved from method doc comments
     * @param string $doc    The documentation retrieved from method doc comments 
     */
    public function __construct($name, $method, $class, $type=null, $doc=false)
    {
        parent::__construct(array($class, $method), $name);

        $this->doc = $doc;

        $this->type = $type;

        // Unknown array type
        if ($this->isArray() && !$type) {
            $this->type = 'array';
        }

        // Class
        if (preg_match('/\\[\s\<\w+?>\s([\w\\\\]+)/s', $this->__toString(), $matches)) {
            if (isset($matches[1])) {
                $this->type = $matches[1];  
            } 
        }
       
    }

    /**
     * Get the type of the parameter
     * @return string A class name, 'array' or null
     */
    public function getType()
    {
        return $this->type;
    }

}
