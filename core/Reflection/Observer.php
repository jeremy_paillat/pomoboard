<?php
/*
 * Copyright (C) 2015 Maarch
 *
 * This file is part of Laabs.
 *
 * Laabs is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Laabs is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with Laabs.  If not, see <http://www.gnu.org/licenses/>.
 */
namespace core\Reflection;

/**
 * Class for reflection observer
 * 
 * @extends \core\Reflection\Service
 */
class Observer
    extends Service
{

    /* Constants */

    /* Properties */

    /* Methods */

    /**
     * Getter for component name
     * 
     * @return string The value of the property
     */
    public function getName() 
    {
        return $this->component;
    }

    /**
     * Check if handler exists
     * @param string $name the name of the handler
     * 
     * @return boolean
     */
    public function hasHandler($name)
    {
        return parent::hasMethod($name);
    }

    /**
     * Returns the observer handlers
     * @param int $filter
     * 
     * @return array An array of handler objects declared for the observer
     */
    public function getHandlers($filter=null)
    {
        $reflectionMethods = parent::getMethods(Method::IS_PUBLIC);
        $handlers = array();

        for ($i=0, $l=count($reflectionMethods); $i<$l; $i++) {
            $reflectionMethod = $reflectionMethods[$i];
            if ($reflectionMethod->isConstructor()
                || $reflectionMethod->isDestructor()
                || $reflectionMethod->isStatic()
                || !preg_match("#@subject (?<subject>[^\s]+)#", $reflectionMethod->getDocComment())
            ) {
                continue;
            }

            $handlers[] = new Handler($reflectionMethod->name, $this->name, $this->container);
        }

        return $handlers;
    }

    /**
     * Get a observer handler declaration from its name
     * @param string $name The name of the handler
     * 
     * @return object The Method object
     * 
     * @throws Exception if the handler is not declared by the observer
     */
    public function getHandler($name)
    {
        if (!parent::hasMethod($name)) {
            throw new \core\Exception("Undefined handler '$this->container/$this->component/$name'");
        }

        $handler = new Handler($name, $this->name, $this->container);

        if (!$handler->isPublic()
            || $handler->isConstructor()
            || $handler->isDestructor()
            || $handler->isStatic()
            || !$handler->subject
        ) {
            throw new \core\Exception("Method '$name' is not a valid observer handler");
        }

        return $handler;
    }
}
