<?php
/*
 * Copyright (C) 2015 Maarch
 *
 * This file is part of Laabs.
 *
 * Laabs is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Laabs is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with Laabs.  If not, see <http://www.gnu.org/licenses/>.
 */
namespace core\Reflection;

/**
 * Class Laabs Step
 * 
 * @extends \core\Reflection\Method
 */
class Step
    extends Method
{

    /* Constants */

    /* Properties */
    public $returnName;

    /* Methods */
    /**
     * Constructor of the batch step
     * @param string $step      The name of the step
     * @param string $class     The class of the job that declares the step
     * @param string $container The name of the service container for context transmission to other service calls
     */
    public function __construct($step, $class, $container)
    {
        parent::__construct($step, $class, $container);

        $docComment = $this->getDocComment();
        if (preg_match('#@return (?<type>[^\s]+)\s+\$(?<name>[\w_]+)#', $docComment, $matches)) {
            $this->returnType = trim($matches['type']);
            $this->returnName = trim($matches['name']);
        }

    }
    /**
     * Call the step
     * @param object $jobObject  Job of the step
     * @param array  $passedArgs Arguments array for the step
     * 
     * @return mixte 
     */
    public function exec($jobObject = null, array $passedArgs = null)
    {
        \core\Observer\Dispatcher::notify(LAABS_STEP_EXEC, $this, $passedArgs);

        $result = $this->invokeArgs($jobObject, $passedArgs);

        \core\Observer\Dispatcher::notify(LAABS_STEP_RESULT, $response);

        return $result;
    }

}
