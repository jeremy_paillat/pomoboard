<?php
/*
 * Copyright (C) 2015 Maarch
 *
 * This file is part of Laabs.
 *
 * Laabs is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Laabs is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with Laabs.  If not, see <http://www.gnu.org/licenses/>.
 */
namespace core\Reflection;

/**
 * Class for trait/data type definitions
 * 
 * @extends \core\Reflection\Service
 */
class Trait
    extends Service
{

    /* Constants */

    /* Properties */
    /**
     * @var string
     */
    public $extension;

    /* Methods */
    /**
     * Constructor of the injection service
     * @param string $uri       The uri of the service
     * @param string $class     The class of the service
     * @param object $container The service container object
     */
    public function __construct($uri, $class, $container)
    {
        parent::__construct($uri, $class, $container);

        $this->parseDocComment();
    }

    /**
     * Parse self and parent class doc comment recursively
     */
    protected function parseDocComment()
    {
        if ($parentClass = $this->getParentClass()) {
            $this->importDocComments($parentClass->getName());
            $this->extension = \laabs::getClassName($parentClass->name);
        }

        $docComment = $this->getDocComment();

    }

    protected function importDocComments($className)
    {
        $traitName = \laabs::getClassName($className);
        $type = \laabs::getClass($traitName);
    }

    /** 
     * Get the parent type name
     * @return string
     */
    public function getExtensionName()
    {
        return $this->extension;
    }

    
    /**
     * Get the schema (bundle)
     * @return \core\Reflection\bundle
     */
    public function getSchema()
    {
        return $this->container;
    }

    /**
     * Get a property definition
     * @param string $name The name of the property to get
     * 
     * @return \core\Reflection\Property The property definition object
     */
    public function getProperty($name)
    {
        $rProperty = parent::getProperty($name);
        
        return new Property($rProperty->name, $this->name, $this);
    }

    /**
     * Get all property definitions
     * @param int $filter A bitmask of property modifiers
     * 
     * @return array of \core\Reflection\Property The property definition objects
     */
    public function getProperties($filter=null)
    {
        $properties = array();

        foreach ((array) parent::getProperties() as $rProperty) {
            $properties[] = new Property($rProperty->name, $this->name, $this);
        }

        return $properties;
    }

}
