<?php
/*
 * Copyright (C) 2015 Maarch
 *
 * This file is part of Laabs.
 *
 * Laabs is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Laabs is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with Laabs.  If not, see <http://www.gnu.org/licenses/>.
 */
namespace core\Reflection;

/**
 * Class for Serializer definitions
 */
class Serializer
    extends Service
{

    /* Constants */

    /* Properties */

    /* Methods */
    /**
     * Checks if input is defined on the serializer
     * @param string $name The name of the input
     * 
     * @return bool
     */
    public function hasOutput($name)
    {
        return $this->hasMethod($name);
    }

    /**
     * Get all outputs defined on the serializer
     * 
     * @return array An array of all the \core\Reflection\Output objects
     */
    public function getOutputs()
    {
        $reflectionMethods = $this->getMethods(\ReflectionMethod::IS_PUBLIC);
        $outputs = array();
        for ($i=0, $l=count($reflectionMethods); $i<$l; $i++) {
            $reflectionMethod = $reflectionMethods[$i];
            if ($reflectionMethod->isConstructor()
                || $reflectionMethod->isDestructor()
                || $reflectionMethod->isStatic()
                || $reflectionMethod->isAbstract()
            ) {
                continue;
            }

            $outputs[] = new Output($reflectionMethod->name, $this->name, $this->container);
        }

        return $outputs;
    }

    /**
     * Get a output definition
     * @param string $name The name of the output
     * 
     * @return object the \core\Reflection\Output object
     * 
     * @throws core\Reflection\Exception if the output is unknown or not public
     */
    public function getOutput($name)
    {
        if (!$this->hasOutput($name)) {
            throw new \Exception("Undefined output '$this->name::$name'");
        }

        $output = new Output($name, $this->name, $this->container);

        if (!$output->isPublic()) {
            throw new \Exception("Output '$name' is not public");
        }

        return $output;
    }

}
