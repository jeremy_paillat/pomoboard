<?php
/*
 * Copyright (C) 2015 Maarch
 *
 * This file is part of Laabs.
 *
 * Laabs is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Laabs is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with Laabs.  If not, see <http://www.gnu.org/licenses/>.
 */
namespace core\Reflection;

/**
 * Class Laabs Route
 * 
 * @extends \core\Reflection\Method
 */
class Route
    extends Method
{

    /* Constants */

    /* Properties */
    /**
     * The name of the domain
     * @var string
     */
    public $domain;

    /**
     * The name of the interface
     * @var string
     */
    public $interface;

    /**
     * The name of the action <bundle>/<component>/<method>
     * @var string
     */
    public $action;

    /**
     * The name of the input <bundle>/<component>/<method>
     * @var string
     */
    public $input;

    /**
     * The name of the output <bundle>/<component>/<method>
     * @var string
     */
    public $output;

    /**
     * The request method CRETE READ UPDATE DELETE
     * @var string
     */
    public $method;

    /**
     * The requested path
     * @var string
     */
    public $path;

    /**
     * The variables contained in path
     * @var string
     */
    public $variables;

    /**
     * Route is prompt only, input and action will be ignored by kernel
     * @var boolean
     */
    public $prompt;

    /* Methods */
    /**
     * Constructs a new action instance
     * @param string $name   The name of action
     * @param string $class  The class of the action
     * @param string $domain The service container
     */
    public function __construct($name, $class, $domain)
    {
        parent::__construct($name, $class, $domain);

        $this->domain = $domain;
        
        $this->interface = \laabs\basename($class, 'Interface');

        $docComment = $this->getDocComment();
        
        // Set mapping between the requested route and the action
        if (preg_match("#@request\s+(?<method>(CREATE|READ|UPDATE|DELETE|PROMPT))(\s(?<uri>[^\s]+))?#", $docComment, $matches)) {
            $this->method = trim($matches['method']);
            if (isset($matches['uri'])) {
                $this->path = trim($matches['uri']);
            }
        } else {
            $this->method = $this->name;
            $this->path = $this->domain . LAABS_URI_SEPARATOR . $this->interface . LAABS_URI_SEPARATOR . $this->name;
        }

        if (preg_match("#@prompt#", $docComment, $matches)) {
            $this->prompt = true;
        } else {
            $this->prompt = null;
        }

        if (preg_match("#@action\s+(?<action>[^\s]+)#", $docComment, $matches)) {
            $this->action = trim($matches['action']);
        } else {
            $this->action = null;
        }

        if (preg_match("#@input\s+(?<input>[^\s]+)#", $docComment, $matches)) {
            $this->input = trim($matches['input']);
        } else {
            $this->input = null;
        }


        if (preg_match("#@output\s+(?<output>[^\s]+)#", $docComment, $matches)) {
            $this->output = trim($matches['output']);
        } else {
            $this->output = null;
        }

    }

    /**
     * Match a route with method and uri
     * @param string $method
     * @param string $uri
     * 
     * @return object The route with variables
     */
    public function match($method='READ', $uri='')
    {
        $this->variables = null;

        // Check method
        if (isset($this->method) && strtoupper($this->method) != strtoupper($method)) {
            return;
        }
        
        // Check uri VS path pattern
        if (isset($this->path) && !preg_match("#^" . str_replace("/", "\\/", $this->path) . "$#", $uri, $args)) {
            return;
        }

        // Check empty uri
        if (!isset($this->path) && !empty($uri)) {
            return;
        } 

        // Check uri == action
        if (!isset($this->path) && !empty($uri) && $uri !== $this->action) {
            return;
        }

        if (!empty($args)) {
            // Remove complete regexp match at offset 0
            array_shift($args);

            // Check if args are assoc (if at least one is named, other indexed will be ignored)
            $assoc = \laabs\is_assoc($args);

            $parameters = parent::getParameters();
            foreach ($parameters as $pos => $parameter) {
                if (isset($args[$parameter->name])) {
                    $this->variables[$parameter->name] = $args[$parameter->name];
                } elseif (isset($args[$pos])) {
                    $this->variables[$parameter->name] = $args[$pos];
                }
            }
        }

        return $this;
    }

    /**
     * Get path
     * 
     * @return string
     */
    public function getName()
    {
        return $this->domain . LAABS_URI_SEPARATOR . $this->interface . LAABS_URI_SEPARATOR . $this->name;
    } 

    /**
     * Reroute
     * @param \core\Reflection\Route $newRoute
     * 
     * @return route
     */
    public function reroute($newRoute)
    {
        $this->__construct($newRoute->name, $newRoute->class, $newRoute->domain);

        return $this;
    } 

}
