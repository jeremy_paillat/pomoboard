<?php
/*
 * Copyright (C) 2015 Maarch
 *
 * This file is part of Laabs.
 *
 * Laabs is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Laabs is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with Laabs.  If not, see <http://www.gnu.org/licenses/>.
 */
namespace core\Reflection;

/**
 * Class Laabs Action
 * 
 * @extends \core\Reflection\Method
 */
class Action
    extends Method
{

    /* Constants */

    /* Properties */
    public $bundle;

    public $controller;

    /* Methods */
    /**
     * Constructs a new action instance
     * @param string $name              The name of action
     * @param string $class             The class of the action
     * @param string $serviceContainer The service container
     */
    public function __construct($name, $class, $serviceContainer)
    {
        parent::__construct($name, $class, $serviceContainer);

        $this->bundle = \laabs\basename(\laabs\dirname(\laabs\dirname($class)));

        $this->controller = \laabs\basename($class);
    }

    /**
     * Call the action
     * @param object $controllerObject Controller of the action
     * @param array  $passedArgs       Arguments array for the action
     * 
     * @return mixte 
     */
    public function call($controllerObject = null, array $passedArgs = null)
    {
        \core\Observer\Dispatcher::notify(LAABS_ACTION_CALL, $this, $passedArgs);

        $response = $this->invokeArgs($controllerObject, $passedArgs);

        \core\Observer\Dispatcher::notify(LAABS_ACTION_RESPONSE, $response);

        return $response;
    }

}
