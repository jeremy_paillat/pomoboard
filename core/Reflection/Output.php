<?php
/*
 * Copyright (C) 2015 Maarch
 *
 * This file is part of Laabs.
 *
 * Laabs is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Laabs is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with Laabs.  If not, see <http://www.gnu.org/licenses/>.
 */
namespace core\Reflection;

/**
 * Class for Output definitions
 * 
 * @extends \core\Reflection\Method
 */
class Output
    extends Method
{

    /* Constants */

    /* Properties */
    public $bundle;

    public $serializer;
    
    /* Methods */
    /**
     * Constructs a new output instance
     * @param string $name             The name of output
     * @param string $class            The class of the output
     * @param string $serviceContainer The service container name
     */
    public function __construct($name, $class, $serviceContainer)
    {
        parent::__construct($name, $class, $serviceContainer);

        $this->bundle = \laabs\basename(\laabs\dirname(\laabs\dirname($class)));

        $this->serializer = \laabs\basename($class);
    }
    /**
     * Serialize the output
     * @param object $serializer The serializer of the output
     * @param object $data       The data to serialize
     * @param object $language   The language
     * 
     * @return string response body
     */
    public function serialize($serializer = null, $data = null, $language = null)
    {
        $args = array($data, $language);
        \core\Observer\Dispatcher::notify(LAABS_OUTPUT_SERIALIZE, $this, $args);

        $output = $this->invokeArgs($serializer, array($data, $language));

        \core\Observer\Dispatcher::notify(LAABS_OUTPUT_STREAM, $output);

        return $output;
    }

}
