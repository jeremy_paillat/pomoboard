<?php
/*
 * Copyright (C) 2015 Maarch
 *
 * This file is part of Laabs.
 *
 * Laabs is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Laabs is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with Laabs.  If not, see <http://www.gnu.org/licenses/>.
 */
namespace core\Reflection;

/**
 * Class for Parser definitions
 * 
 * @extends \core\Reflection\Service
 */
class Parser
    extends Service
{
    use \core\ReadonlyTrait;

    /* Constants */

    /* Properties */

    /* Methods */
    /**
     * Checks if input is defined on the parser
     * @param string $name The name of the input
     * 
     * @return bool
     */
    public function hasInput($name)
    {
        return $this->hasMethod($name);
    }

    /**
     * Get all inputs defined on the parser
     * 
     * @return array An array of all the \core\Reflection\Input objects
     */
    public function getInputs()
    {
        $reflectionMethods = $this->getMethods(\ReflectionMethod::IS_PUBLIC);
        $inputs = array();
        for ($i=0, $l=count($reflectionMethods); $i<$l; $i++) {
            $reflectionMethod = $reflectionMethods[$i];
            if ($reflectionMethod->isConstructor()
                || $reflectionMethod->isDestructor()
                || $reflectionMethod->isStatic()
                || $reflectionMethod->isAbstract()
            ) {
                continue;
            }

            $inputs[] = new Input($reflectionMethod->name, $this->name, $this->container);
        }

        return $inputs;
    }

    /**
     * Get a input definition
     * @param string $name The name of the action
     * 
     * @return object the \core\Reflection\Input object
     * 
     * @throws core\Reflection\Exception if the input is unknown or not public
     */
    public function getInput($name)
    {
        if (!$this->hasInput($name)) {
            throw new \Exception("Undefined input '$this->container/$this->name/$name'");
        }

        $input = new Input($name, $this->name, $this->container);

        if (!$input->isPublic()) {
            throw new \Exception("Input '$this->container/$this->name/$name' is not public");
        }

        return $input;
    }

}
