<?php
/*
 * Copyright (C) 2015 Maarch
 *
 * This file is part of Laabs.
 *
 * Laabs is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Laabs is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with Laabs.  If not, see <http://www.gnu.org/licenses/>.
 */
namespace core\Reflection;

/**
 * Class for Input definitions
 * 
 * @extends \core\Reflection\Method
 */
class Input
    extends Method
{

    /* Constants */

    /* Properties */
    public $bundle;

    public $parser;
    
    /* Methods */
    /**
     * Constructs a new input instance
     * @param string $name             The name of input
     * @param string $class            The class of the input
     * @param string $serviceContainer The service container name
     */
    public function __construct($name, $class, $serviceContainer)
    {
        parent::__construct($name, $class, $serviceContainer);

        $this->bundle = \laabs\basename(\laabs\dirname(\laabs\dirname($class)));

        $this->parser = \laabs\basename($class);
    }

    /**
     * Parse the input
     * @param object $parser      The parser of the input
     * @param object $requestBody The request to parse
     * 
     * @return string Request body
     */
    public function parse($parser = null, $requestBody)
    {
        \core\Observer\Dispatcher::notify(LAABS_INPUT_PARSING, $this, $requestBody);

        $input = $this->invokeArgs($parser, array($requestBody));

        \core\Observer\Dispatcher::notify(LAABS_INPUT_ARGUMENTS, $input);

        return $input;
    }

}
