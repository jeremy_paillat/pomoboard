<?php
/*
 * Copyright (C) 2015 Maarch
 *
 * This file is part of Laabs.
 *
 * Laabs is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Laabs is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with Laabs.  If not, see <http://www.gnu.org/licenses/>.
 */
namespace core\Observer;

class Pool
    extends \SplObjectStorage
{

    /* Constants */

    /* Properties */
    protected $subject;

    /* Methods */
    public function __construct($subject)
    {
        $this->subject = $subject;
    }

    /**
     * Notify a pool of observers
     * @param object &$object The subject of observation
     * @param mixed  &$info   The data context
     * 
     * @return mixed
     */
    public function notify(&$object, &$info=null) 
    {
        $return = array();

        $this->rewind();

        while ($this->valid()) {
            $observer = $this->current();
            $observerInfo = $this->getInfo();
            $key = $observerInfo['class'] . "::" . $observerInfo['method'];
            $return[$key] = call_user_func(array($observer, $observerInfo['method']), $object, $info);
            $this->next();
        }

        return $return;
    }

}