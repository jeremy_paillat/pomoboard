<?php
/*
 * Copyright (C) 2015 Maarch
 *
 * This file is part of Laabs.
 *
 * Laabs is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Laabs is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with Laabs.  If not, see <http://www.gnu.org/licenses/>.
 */
namespace core\Language;

/**
 * Class which represents an expression token
 */
class Token
{
    /**
     * The token code
     *
     * @var int
     */
    private $code = null;
    /**
     * The token value
     *
     * @var string
     */
    private $value = null;
    /**
     * The token offset
     *
     * @var int
     */
    private $offset = null;
    /**
     * The class constructor
     *
     * @param int    $code
     * @param string $value
     * @param int    $offset
     */
    public function __construct($code, $value, $offset)
    {
        $this->code = $code;
        $this->value = $value;
        $this->offset = $offset;
    }
    /**
     * Returns the token code
     *
     * @return int
     */
    public function getCode()
    {
        return $this->code;
    }
    /**
     * Returns the token value
     *
     * @return string
     */
    public function getValue()
    {
        return $this->value;
    }
    /**
     * Returns thr token offset
     *
     * @return int
     */
    public function getOffset()
    {
        return $this->offset;
    }
}
