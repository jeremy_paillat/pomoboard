<?php
/*
 * Copyright (C) 2015 Maarch
 *
 * This file is part of Laabs.
 *
 * Laabs is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Laabs is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with Laabs.  If not, see <http://www.gnu.org/licenses/>.
 */
namespace core\Language;
/**
 * Represents a binary operator node, a binary operator has a left and a right operand
 */
abstract class AbstractBinaryOperation
{
    use \core\ReadonlyTrait;
    /**
     * The operator
     * @var mixed
     */
    public $code;

    /**
     * The left operand
     * @var mixed
     */
    public $left;

    /**
     * The right operand
     * @var mixed
     */
    public $right;

    /**
     * The class constructor
     * @param mixed $code
     * @param mixed $left
     * @param mixed $right
     */
    public function __construct($code, $left, $right)
    {
        $this->code = $code;
        $this->left = $left;
        $this->right = $right;
    }

    /**
     * Get the op code
     * @return mixed The var
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * Get the left op
     * @return mixed The var
     */
    public function getLeftOperator()
    {
        return $this->left;
    }

    /**
     * Get the right op
     * @return mixed The var
     */
    public function getRightOperator()
    {
        return $this->right;
    }

}
