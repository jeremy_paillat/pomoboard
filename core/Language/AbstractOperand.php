<?php
/*
 * Copyright (C) 2015 Maarch
 *
 * This file is part of Laabs.
 *
 * Laabs is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Laabs is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with Laabs.  If not, see <http://www.gnu.org/licenses/>.
 */
namespace core\Language;
/**
 * Represents an operand in an operation
 */
abstract class AbstractOperand
{
    use \core\ReadonlyTrait;
    /**
     * The type
     *
     * @var mixed
     */
    public $code;
    
    /**
     * The value
     *
     * @var mixed
     */
    public $value;
    
    
    /**
     * The class constructor
     * @param mixed $code
     * @param mixed $value
     */
    public function __construct($code, $value)
    {
        $this->code = $code;
        
        $this->value = $value;
    }

    /**
     * Get the op code
     * @return mixed The var
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * Get the op value
     * @return mixed The var
     */
    public function getValue()
    {
        return $this->value;
    }
}
