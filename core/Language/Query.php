<?php
/*
 * Copyright (C) 2015 Maarch
 *
 * This file is part of Laabs.
 *
 * Laabs is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Laabs is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with Laabs.  If not, see <http://www.gnu.org/licenses/>.
 */
namespace core\Language;
/**
 * class for create, read, update end delete queries on model
**/
class Query
{
    /* Constants */

    /* Properties */
    protected $code;

    protected $class;

    protected $properties;

    protected $asserts;

    protected $sortings;

    protected $offset;

    protected $length;
    
    protected $summarise = false;

    protected $lock = false;

    protected $returns;
    
    protected $params = array();

    /* Methods */
    /**
     * Construct a new query from LQL
     * @param string $queryString A Laabs Query Language query string to parse
     * 
     * @return a new instance of query object
     */ 
    public static function parse($queryString)
    {
        $parser = new Parser();
        $query = $parser->parseQuery($queryString);

        return $query;
    }

    /**
     * 
     */
    public function setCode($code)
    {
        $this->code = $code;
    }

    /**
     * 
     */
    public function getCode()
    {
        return $this->code;
    }


    /* Class */
    /**
     * 
     */
    public function setClass($class)
    {
        $this->class = $class;
    }

    /**
     * 
     */
    public function getClass()
    {
        return $this->class;
    }

    /* Properties */
    /**
     * 
     */
    public function setProperties(array $properties)
    {
        $this->properties = $properties;
    }

    /**
     * 
     */
    public function addProperty($property)
    {
        $this->properties[] = $property;
    }

    /**
     * 
     */
    public function getProperties()
    {
        return $this->properties;
    }
    
    /**
     * 
     */
    public function setAsserts(array $asserts)
    {
        $this->asserts = $asserts;
    }
    /**
     * 
     */
    public function addAssert($assert)
    {
        $this->asserts[] = $assert;
    }

    /**
     * 
     */
    public function getAsserts()
    {
        return $this->asserts;
    }
    
    /**
     * 
     */
    public function summarise($do=null)
    {
        if (is_null($do)) {
            return $this->summarise;
        }
        
        $this->summarise = $do;
    }

    /**
     * 
     */
    public function lock($do=null)
    {
        if (is_null($do)) {
            return $this->lock;
        }
        
        $this->lock = $do;
    }

    /**
     * 
     */
    public function isSummary()
    {
        return $this->summarise;
    }

    /* Sortings */
    /**
     * 
     */
    public function addSorting(Sorting $sorting)
    {
        $this->sortings[] = $sorting;
    }

    /**
     * 
     */
    public function setSortings(array $sortings)
    {
        $this->sortings = $sortings;
    }


    /**
     * 
     */
    public function getSortings()
    {
        return $this->sortings;
    }

    /**
     * 
     */
    public function setLength($length)
    {
        $this->length = (int) $length;
    }

    /**
     * 
     */
    public function getLength()
    {
        return $this->length;
    }

    /**
     * 
     */
    public function setOffset($offset)
    {
        $this->offset = (int) $offset;
    }

    /**
     * 
     */
    public function getOffset()
    {
        return $this->offset;
    }

    /**
     * 
     */
    public function setReturns(array $returns)
    {
        $this->returns = $returns;
    }
    /**
     * 
     */
    public function addReturn($return)
    {
        $this->returns[] = $return;
    }

    /**
     * 
     */
    public function getReturns()
    {
        return $this->returns;
    }

    /**
     * 
     */
    public function addParam($name, $type='string', $length=null)
    {
        $value = null;
        $this->params[$name] = new Param($name, $value, $type, $length);
    }


    /**
     * 
     */
    public function bindParam($name, &$value=null, $type=null)
    {
        $this->params[$name] = new Param($name, $value, $type);
    }

    /**
     * 
     */
    public function getParams()
    {
        return $this->params;
    }

    

}