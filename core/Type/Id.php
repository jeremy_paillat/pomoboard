<?php
/*
 * Copyright (C) 2015 Maarch
 *
 * This file is part of Laabs.
 *
 * Laabs is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Laabs is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with Laabs.  If not, see <http://www.gnu.org/licenses/>.
 */
namespace core\Type;
/**
 * Class for unique ids
 */
class Id
{
    
    public $id;

    /**
     * Construct a new date object
     * @param string $id
     * @param string $prefix
     */
    public function __construct($id=null, $prefix=null)
    {
        if (is_null($id)) {
            $this->generate($prefix);
        } else {
            $this->set($id);
        }
    }

    /**
     * Generate a new id
     * @param string $prefix
     * 
     * @return string The new id
     */
    public function generate($prefix)
    {
        $this->id = \laabs\uniqid($prefix);
    }

    /**
     * Get string
     * @return string
     */
    public function __toString()
    {
        return $this->id;
    }

    /**
     * Call value
     * @param string $id The new id to set. If omitted the current value is returned
     * 
     * @return Id
     */
    public function set($id)
    {
        if (!preg_match('#^[A-Za-z0-9_\-]+$#', $id)) {
            throw new \core\Exception("Invalid id '$id': Ids must contain only alphanumeric, underscores and dashes");
        }

        $this->id = $id;
    }

    /**
     * Call value
     * @param string $id The new id to set. If omitted the current value is returned
     * 
     * @return Id
     */
    public function __invoke($id=null)
    {
        if ($id) {
            return $this->set($id);
        } else {
            return $this->id;
        }        
    }


}