<?php
/*
 * Copyright (C) 2015 Maarch
 *
 * This file is part of Laabs.
 *
 * Laabs is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Laabs is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with Laabs.  If not, see <http://www.gnu.org/licenses/>.
 */
namespace core\Type;
/**
 * Class for integer type
 */
class Date
    extends DateTime
{

    /**
     * Construct a new date object
     * @param string $value  The date value
     * @param string $format The default format for __toString
     */
    public function __construct($value=null, $format=false)
    {
        // Default value for date is year+month+day
        // In UTC timezone
        if (!$value) {
            $value = date("Y-m-d");
        }

        // Useless (no time)
        $timezone = null;

        // Default format for date is given by server directive
        if (!$format) {
            $format = \core\Globals\Server::getDateFormat();
        }

        parent::__construct($value, $timezone, $format);
    }

}