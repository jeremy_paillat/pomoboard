<?php
/*
 * Copyright (C) 2015 Maarch
 *
 * This file is part of Laabs.
 *
 * Laabs is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Laabs is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with Laabs.  If not, see <http://www.gnu.org/licenses/>.
 */
namespace core\Type;
/**
 * Class for names
 */
class Name
{
    
    public $name;

    /**
     * Construct a new name object
     * @param string $name
     */
    public function __construct($name)
    {
        if (!preg_match('#^[A-Za-z_][A-Za-z0-9_]*$#', $name)) {
            throw new \core\Exception("Invalid name '$name': Names must start with alphabetic characters or underscore and contain only alphanumeric or underscores");
        }

        $this->name = $name;
    }

    /**
     * Get string
     * @return string
     */
    public function __toString()
    {
        return $this->name;
    }


}