<?php
/*
 * Copyright (C) 2015 Maarch
 *
 * This file is part of Laabs.
 *
 * Laabs is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Laabs is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with Laabs.  If not, see <http://www.gnu.org/licenses/>.
 */
namespace core\Type;
/**
 * Class for timestamp type
 * Timestamp is a date + time of day + microseconds
 * No timezone ! Timestamp is always given from UNIX epoch UTC
 */
class Timestamp
    extends DateTime
{
    protected $format;

    /**
     * Construct a new date object
     * @param string $value
     * @param string $format
     */
    public function __construct($value=null, $format=false)
    {
        // Default value for timestamp is year+month+day hour+minute+second + microsecond
        // In UTC timezone
        if (empty($value)) {
            $value = \gmdate('Y-m-d\TH:i:s') . "," . substr(microtime(), 2, 6) . "+0";
        } else {
            $value = str_replace(",", ".", $value);
        }

        $timezone = null;

        // Default format for timestamp is given by server directive
        if (!$format) {
            $format = \core\Globals\Server::getTimestampFormat();
        }

        parent::__construct($value, $timezone, $format);
    }

}