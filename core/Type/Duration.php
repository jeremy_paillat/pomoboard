<?php
/*
 * Copyright (C) 2015 Maarch
 *
 * This file is part of Laabs.
 *
 * Laabs is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Laabs is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with Laabs.  If not, see <http://www.gnu.org/licenses/>.
 */
namespace core\Type;
/**
 * Class for duration type
 */
class Duration
    extends \DateInterval
{
    
    /**
     * Construct a new duration object
     * @param string $duration
     */
    public function __construct($duration)
    {
        if ($duration[0] == '-') {
            $invert = 1;
            $duration = substr($duration, 1);
        } else {
            $invert = false;
        }

        parent::__construct($duration);

        if ($invert) {
            $this->invert = 1;
        }
    }
    /**
     * Magic method to retrieve value
     * @return string
     */
    public function __toString()
    {
        $str = "";

        if ($this->invert) {
            $str .= "-";
        }

        $str .= "P";
        
        if ($this->y) {
            $str .= $this->y . "Y";
        }
        if ($this->m) {
            $str .= $this->m . "M";
        }
        if ($this->d) {
            $str .= $this->d . "D";
        }

        if ($this->h || $this->i || $this->s) {
            $str .= "T";
        }

        if ($this->h) {
            $str .= $this->h . "H";
        }
        if ($this->i) {
            $str .= $this->i . "M";
        }
        if ($this->s) {
            $str .= $this->s . "S";
        }
        
        return $str;
    }
}