<?php
/*
 * Copyright (C) 2015 Maarch
 *
 * This file is part of Laabs.
 *
 * Laabs is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Laabs is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with Laabs.  If not, see <http://www.gnu.org/licenses/>.
 */
namespace core\Type;
/**
 * Class for Xml documents
 */
class Xml
    extends \DOMDocument
{
    
    /**
     * Construct a new xml object
     * @param string $xml
     * @param string $encoding
     */
    public function __construct($xml=false, $encoding=false)
    {
        if (!$encoding) {
            $encoding = \core\Globals\Server::getXmlEncoding();
        }

        parent::__construct("1.0", $encoding);

        if ($xml) {
            $this->loadXml($xml);
        }

    }

    /**
     * Get string
     * @return string
     */
    public function __toString()
    {
        return $this->saveXml();
    }

    /**
     * Query xpath
     * @param string $path
     * 
     * @return mixed
     */
    public function path($path)
    {
        $xpath = new \DOMXPath($this);

        
    }
        


}