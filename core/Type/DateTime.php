<?php
/*
 * Copyright (C) 2015 Maarch
 *
 * This file is part of Laabs.
 *
 * Laabs is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Laabs is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with Laabs.  If not, see <http://www.gnu.org/licenses/>.
 */
namespace core\Type;
/**
 * Class for integer type
 */
class DateTime
    extends \DateTime
{
    
    protected $format;

    /**
     * Construct a new date object
     * @param mixed  $datetime Either a date string or an existing datetime object
     * @param mixed  $timezone Either a timezone name or a timezone object
     * @param string $format   The default format for __toString
     */
    public function __construct($datetime=null, $timezone=null, $format=false)
    {
        // Default value for datetime is year+month+day hour+minute+second
        if (!$datetime) {
            $datetime = date("y-m-d H:i:s");
        } else {
            $datetime = str_replace("/", "-", $datetime);
            $datetime = str_replace(",", ".", $datetime);
        }

        if (!$timezone) {
            $timezone = new TimeZone();
        } elseif (is_string($timezone)) {
            $timezone = new TimeZone($timezone);
        }

        if (!$format) {
            $format = \core\Globals\Server::getTimestampFormat();
        }

        $this->format = $format;

        parent::__construct($datetime, $timezone);
    }

    /**
     * Magic method to retrieve value
     * @return string
     */
    public function __toString()
    {
        return (string) $this->format();
    }

    /**
     * Format date
     * @param string $format
     * 
     * @return string
     */
    public function format($format=null)
    {
        if (is_null($format)) {
            $format = $this->format;
        }

        return parent::format($format);
    }

    /**
     * Calculate the difference between the date and another
     * @param \DateTimeInterface $dateTime The date to compare
     * @param boolean            $absolute Calc in absolute or relative mode
     * 
     * @return \core\Type\duration
     */
    public function diff($dateTime, $absolute=null)
    {
        $interval = parent::diff($dateTime, $absolute);

        $duration = new Duration($interval->format("P%YY%MM%DDT%HH%IM%SS"));

        $duration->invert = $interval->invert;
        $duration->days = $interval->days;

        return $duration;
    }

    /**
     * Get the timezone object 
     * @return TimeZone
     */
    public function getTimeZone()
    {
        $timezone = parent::getTimeZone();

        return new TimeZone($timezone->getName());
    }

    /**
     * Add a duration to the date annd return a new datetime object as the result
     * @param \duration $duration The duration to add
     * 
     * @return mixed The new datetime object of same class
     */
    public function shift($duration)
    {
        $newDate = clone($this);

        return $newDate->add($duration);
    }

    /**
     * Add a duration to the date annd return a new datetime object as the result
     * @param \duration $duration The duration to add
     * 
     * @return mixed The new datetime object of same class
     */
    public function unshift($duration)
    {
        $newDate = clone($this);

        return $newDate->sub($duration);
    }

}