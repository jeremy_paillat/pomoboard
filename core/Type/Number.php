<?php
/*
 * Copyright (C) 2015 Maarch
 *
 * This file is part of Laabs.
 *
 * Laabs is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Laabs is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with Laabs.  If not, see <http://www.gnu.org/licenses/>.
 */
namespace core\Type;
/**
 * Class for Numbers
 */
class Number
{
    
    public $value;

    protected $decimals = 0;

    protected $decimalSep=".";

    protected $thousandsSep=",";

    /**
     * Construct a new number object
     * @param string $value
     * @param int    $decimals
     * @param string $decimalSep
     * @param string $thousandsSep
     */
    public function __construct($value, $decimals=false, $decimalSep=false, $thousandsSep=false)
    {
        if (!is_numeric($value)) {
            throw new \core\Exception("Invalid number '$value'");
        }

        $this->value = $value;

        if (!$decimals) {
            $decimals = \core\Globals\Server::getNumberDecimals();
        }
        $this->decimals = $decimals;

        if (!$decimalSep) {
            $decimalSep = \core\Globals\Server::getNumberDecimalSeparator();
        }
        $this->decimalSep = $decimalSep;

        if (!$thousandsSep) {
            $thousandsSep = \core\Globals\Server::getNumberthousandsSeparator();
        }
        $this->thousandsSep = $thousandsSep;
    }

    /**
     * Get string
     * @return string
     */
    public function __toString()
    {
        return $this->format();
    }

    /**
     * Get integer
     * @return string
     */
    public function __toInt()
    {
        return (int) $this->value;
    }

    /**
     * Get float
     * @return string
     */
    public function __toFloat()
    {
        return (float) $this->value;
    }

    /**
     * Format the number
     * @param int    $decimals
     * @param string $decimalSep
     * @param string $thousandsSep
     * 
     * @return string
     */
    public function format($decimals=false, $decimalSep=false, $thousandsSep=false)
    {
        if ($decimals === false) {
            $decimals = (integer) $this->decimals;
        }
        if ($decimals === false) {
            $parts = explode('.', $this->value);
            if (isset($parts[1])) {
                $decimals = strlen($parts[1]);
            }
        }

        if ($decimalSep === false) {
            $decimalSep = $this->decimalSep;
        }

        if ($thousandsSep === false) {
            $thousandsSep = $this->thousandsSep;
        }

        return \number_format($this->value, $decimals, $decimalSep, $thousandsSep);
    }

    /** 
     * Add a value
     * @param mixed $value The value to add
     * 
     * @return float The new value
     */
    public function add($value)
    {
        $this->value += $value;

        return $this->value;
    }

    /** 
     * Substract a value
     * @param mixed $value The value to sub
     * 
     * @return float The new value
     */
    public function sub($value)
    {
        $this->value -= $value;

        return $this->value;
    }

    /** 
     * Multiply a value
     * @param mixed $value The value to multiply
     * 
     * @return float The new value
     */
    public function mul($value)
    {
        $this->value *= $value;

        return $this->value;
    }

    /** 
     * Divide by a value
     * @param mixed $value The value to divide value with
     * 
     * @return float The new value
     */
    public function div($value)
    {
        $this->value /= $value;

        return $this->value;
    }

    /** 
     * Calc mod of the value
     * @param mixed $value The value for mod
     * 
     * @return float The new value
     */
    public function mod($value)
    {
        $this->value %= $value;

        return $this->value;
    }

}