<?php
/*
 * Copyright (C) 2015 Maarch
 *
 * This file is part of Laabs.
 *
 * Laabs is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Laabs is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with Laabs.  If not, see <http://www.gnu.org/licenses/>.
 */
namespace core\Type;
/**
 * Class for names
 */
class QName
{
    
    public $qname;

    /**
     * Construct a new name object
     * @param string $qname
     */
    public function __construct($qname)
    {
        if (!preg_match('#^[A-Za-z_][A-Za-z0-9_]*(\/[A-Za-z_][A-Za-z0-9_]*)*$#', $qname)) {
            throw new \core\Exception("Invalid qualified name '$name': It must start with a name and contain only uri separators and names");
        }

        $this->qname = $qname;
    }

    /**
     * Get the namespace
     * @return string
     */
    public function getNamespace()
    {
        return \laabs\dirname($this->qname);
    }

    /**
     * Get the base name
     * @return string
     */
    public function getBasename()
    {
        return \laabs\basename($this->qname);
    }

    /**
     * Get string
     * @return string
     */
    public function __toString()
    {
        return $this->qname;
    }


}