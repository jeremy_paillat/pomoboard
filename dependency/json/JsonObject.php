<?php
namespace dependency\json;

class JsonObject
{

    /**
     * @var int A bitmask of json options
     */
    protected $Options;

    /**
     * @var mixed The storage for data to encode
     */
    protected $storage;

    /**
     * Constructor 
     * @param string                                       $type       The type (scalar, class, array, typed array) of data to store
     * @param int                                          $Options    A bitmask of json options
     */
    public function __construct($type=false, $Options=0)
    {
        $this->Options = $Options;

        if ($type) {
            $this->storage = \laabs::cast(null, $type);
        } else {
            $this->storage = new \StdClass();
        }

    }

    /**
     * Load data into storage 
     * @param data $data The data to load
     */
    public function load($data)
    {
        $this->storage = $data;
    }

    /**
     * Load json data into storage 
     * @param mixed  $data The data to load
     * @param string $type The type of data to store
     */
    public function loadJson($data, $type=null)
    {
        $object = json_decode($data);

        if ($type) {
            $this->storage = \laabs::cast($object, $type);
        } else {
            $this->storage = $object;
        }
    }

    /**
     * Export storage 
     * @return mixed The value of storage
     */
    public function export()
    {
        return $this->storage;
    }

    /**
     * Save data from storage to json string 
     * @return string The json data string
     */
    public function save()
    {
        $object = \laabs::export($this->storage);

        $jsonString = json_encode($object, $this->Options);

        if ($jsonString === false) {
            return false;
        }

        switch (json_last_error()) {
            case JSON_ERROR_NONE:
                return $jsonString;
            break;
            case JSON_ERROR_DEPTH:
                $message = 'The maximum stack depth has been exceeded';
                break;
            case JSON_ERROR_STATE_MISMATCH:
                $message = 'Invalid or malformed JSON';
                break;
            case JSON_ERROR_CTRL_CHAR:
                $message = 'Control character error, possibly incorrectly encoded';
                break;
            case JSON_ERROR_SYNTAX:
                $message = 'Syntax error';
                break;
            case JSON_ERROR_UTF8:
                $message = 'Malformed UTF-8 characters, possibly incorrectly encoded';
                break;
            case JSON_ERROR_RECURSION:
                $message = 'One or more recursive references detected in the value to be encoded';
                break;
            case JSON_ERROR_INF_OR_NAN:
                $message = 'One or more NAN or INF values in the value to be encoded';
                break;
            case JSON_ERROR_UNSUPPORTED_TYPE:
                $message = 'A value of a type that cannot be encoded was given';
                break;
            default:
                $message = 'Unknown error';
        }

        trigger_error("Error encoding JSON: " . $message, E_USER_ERROR);
    }

    /**
     * Set value
     * @param string $name  The name of property
     * @param mixed  $value The value to set
     */
    public function __set($name, $value)
    {
        $this->storage->$name = $value;
    }

    /**
     * Get value
     * @param string $name The name of property
     * 
     * @return mixed The value
     */
    public function __get($name)
    {
        return $this->storage->$name;
    }

    /**
     * Call local or storage method
     * @param string $method The name of method
     * @param array  $args   The method args
     * 
     * @return mixed The return of called method
     */
    public function __call($method, array $args=array())
    {
        if (method_exists($this, $method)) {
            return call_user_func_array(array($this, $method), $args);
        }

        return call_user_func_array(array($this->storage, $method), $args);
    }

    /**
     * Translate storage string values
     * @param string $catalog The name of the catalog to use
     */
    public function translate($catalog)
    {
        $this->translator->setCatalog($catalog);

        $this->recursiveTranslate($this->storage, $depth=0);

    }

    protected function recursiveTranslate(&$value, $depth)
    {
        if ($depth >= 100) {
            return;
        } else {
            $depth++;
        }
        switch (gettype($value)) {
            case 'string':
                $value = $this->translator->getText($value);
                break;

            case 'object':
                $reflectionObject = new \ReflectionObject($value);
                foreach ($reflectionObject->getProperties() as $reflectionProperty) {
                    $reflectionProperty->setAccessible(true);
                    
                    $propertyValue = $reflectionProperty->getValue($value);
                    
                    $this->recursiveTranslate($propertyValue, $depth);
                    
                    $reflectionProperty->setValue($value, $propertyValue);
                }
                break;

            case 'array':
                foreach ($value as $key => &$rowValue) {
                    $this->recursiveTranslate($rowValue, $depth);

                    $value[$key] = $rowValue;
                }
                break;

            default:
        }
    }

}