<?php
namespace dependency\localisation\Adapter\Gettext;
 /**
 * Language translator for Personalization Objects / Machine Objects (po/mo) adapter
 *
 * @package Localisation
 * @author  Cyril Vazquez <cyril.vazquez@maarch.org>
 */ 
class Translator
    implements \dependency\localisation\TranslatorInterface
{
    /* Constants */
    /* Properties */
    /* Methods */
    /**
     * Construct a new translator
     * @param string $locale  The default locale
     * @param string $codeset The codeset for texts
     * @param string $domain  The default domain name
     *
     * @return void
     **/
    public function __construct($locale, $codeset, $domain="messages") 
    {
        $this->setLocale($locale, $codeset);
    }

    /**
     * Sets the localisation
     * @param string $lang The lang code
     * 
     * @return void
     */
    public function setLang($lang) 
    {
        $GLOBALS['locale'] = $lang;
        setlocale(LC_ALL, $lang);
        putenv("LC_ALL=" . $lang);
    }

    /**
     * Open a localisation source
     * @param string $catalog The bundle name, i.e. source and domain file name
     * 
     * @return void
     */
    public function setCatalog($catalog) 
    {
        $bundle = strtok($catalog, LAABS_URI_SEPARATOR);
        $domain = strtok(LAABS_URI_SEPARATOR);

        $this->bindDomain($bundle, $domain);

        return textdomain($domain);
    }

    /**
     * Bind a text domain
     *
     * @return void
     * @author 
     **/
    protected function bindDomain($bundle, $domain)
    {
        
        $catalogDir = $bundle . DIRECTORY_SEPARATOR . LAABS_RESOURCE . DIRECTORY_SEPARATOR . \dependency\localisation\LocalisationInterface::LOCALE_DIR .

        $catalogPath = DIRECTORY_SEPARATOR . $GLOBALS['locale'] . DIRECTORY_SEPARATOR . 'LC_MESSAGES' . DIRECTORY_SEPARATOR . $domain . ".mo";

        $catalogFile = \core\Reflection\Extensions::extendedPath($catalogDir . $catalogPath);

        $realpath = bindtextdomain($domain, $catalogDir);
    }

    /**
     * Get a translated text from dictionaries
     * @param string $msgid   The message identifier, the original text
     * @param string $msgctxt The context of message, to search the translation for a specific use
     * @param string $catalog The catalog to use for this translation (does not modify the current catalog)
     *  
     * @return string The translated text or the original text if no translation was found
     */
    public function getText($msgid, $msgctxt=false, $catalog=false) 
    {
        if ($msgctxt) {
            $msgid = $msgctxt . chr(4) . $msgid;
        }

        if ($catalog) {
            $bundle = strtok($catalog, LAABS_URI_SEPARATOR);
            $domain = strtok(LAABS_URI_SEPARATOR);
            $this->bindDomain($bundle, $domain);

            return dgettext($domain, $msgid);
        } else {
            return gettext($msgid);
        }
    }

    /**
     * Get a translated format for a plural form or a formatted message
     * @param string $msgid   The message identifier
     * @param array  $args    The variables for format
     * @param string $msgctxt The context of message, to search the translation for a specific use
     * @param string $catalog The catalog to use for this translation (does not modify the current catalog)
     * 
     * @return string The translated text with the merged values, or the original text if no translation was found
     */
    public function getFormattedText($msgid, array $args, $msgctxt=false, $catalog=false) 
    {
        if ($msgctxt) {
            $msgid = $msgctxt . chr(4) . $msgid;
        }

        if(isset($args['count'])) {
        }

        if ($catalog) {
            $bundle = strtok($catalog, LAABS_URI_SEPARATOR);
            $domain = strtok(LAABS_URI_SEPARATOR);
            $this->bindDomain($bundle, $domain);

            return dgettext($domain, $msgid);
        } else {
            return gettext($msgid);
        }

        return ngettext($msgid, $nmsgid, $count);
    }
}
