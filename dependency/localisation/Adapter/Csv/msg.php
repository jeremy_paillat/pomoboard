<?php

namespace dependency\localisation\Adapter\Csv;
/**
 * Language message for CSV adapter
 *
 * @package Localisation
 * @author  Cyril Vazquez <cyril.vazquez@maarch.org>
 */ 
class msg
{
    use \core\ReadonlyTrait;

    public $msgid;

    public $msgctxt;

    public $msgstr;

    /**
     * Constructor
     * @param array $data The csv line. The format depends on the number of columns in the data
     * 2 stands for  msgid [msgctxt], msgstr
     * 4 or more for msgid [msgctxt], nmsgid, msgstr, nmsgtxt [n..m], mmsgtxt [y..z]
     *
     * @return void
     */
    public function __construct(array $data)
    {
        // Get msg id. If context given , extract context and msgid
        $msgid = array_shift($data);
        if (preg_match("#(.+)\s\[([^\]]+)\]$#", $msgid, $matches)) {
            $this->msgid = trim($matches[1]);
            $this->msgctxt = trim($matches[2]);
        } else {
            $this->msgid = trim($msgid);
        }
        
        while ($msgstr = array_shift($data)) {
            $this->msgstr[] = trim($msgstr);
        } 
    }

}