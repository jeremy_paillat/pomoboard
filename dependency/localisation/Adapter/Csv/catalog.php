<?php

namespace dependency\localisation\Adapter\Csv;
/**
 * Language message catalog for CSV adapter
 *
 * @package Localisation
 * @author  Cyril Vazquez <cyril.vazquez@maarch.org>
 */ 
class catalog
{

    public $pluralFormCount;
    public $pluralFormAssert;
    public $charset = 'UTF-8';

    public $messages = array();

    /**
     * Constructor
     * @param string $catalogUri The uri of catalog
     * @param string $lang       The language code to load
     *
     * @return void
     * @author 
     **/
    public function __construct($catalogUri, $lang)
    {
        ini_set('auto_detect_line_endings', true);

        $appDefaultCatalogFile = ".." . DIRECTORY_SEPARATOR . LAABS_APP . DIRECTORY_SEPARATOR . \core\Globals\Server::getApp()
            . DIRECTORY_SEPARATOR . LAABS_RESOURCE . DIRECTORY_SEPARATOR 
            . \dependency\localisation\LocalisationInterface::LOCALE_DIR . DIRECTORY_SEPARATOR 
            . $lang . DIRECTORY_SEPARATOR . 'messages.csv';

        if (file_exists($appDefaultCatalogFile)) {
            $this->loadFile($appDefaultCatalogFile);
        }

        $root = strtok($catalogUri, LAABS_URI_SEPARATOR);

        if ($root == LAABS_DEPENDENCY) {
            $dependency = strtok(LAABS_URI_SEPARATOR);
            $domain = strtok(LAABS_URI_SEPARATOR);

            $appDependencyCatalogFile = ".." . DIRECTORY_SEPARATOR . LAABS_APP . DIRECTORY_SEPARATOR . \core\Globals\Server::getApp()
                . DIRECTORY_SEPARATOR . LAABS_RESOURCE . DIRECTORY_SEPARATOR 
                . \dependency\localisation\LocalisationInterface::LOCALE_DIR . DIRECTORY_SEPARATOR 
                . $lang . DIRECTORY_SEPARATOR . $root . "#" . $dependency . "#" . $domain . ".csv";

            if (file_exists($appDependencyCatalogFile)) {
                $this->loadFile($appDependencyCatalogFile);
            }
        } else {
            $bundle = $root;
            $domain = strtok(LAABS_URI_SEPARATOR);

            $appBundleCatalogFile = ".." . DIRECTORY_SEPARATOR . LAABS_APP . DIRECTORY_SEPARATOR . \core\Globals\Server::getApp()
                . DIRECTORY_SEPARATOR . LAABS_RESOURCE . DIRECTORY_SEPARATOR 
                . \dependency\localisation\LocalisationInterface::LOCALE_DIR . DIRECTORY_SEPARATOR 
                . $lang . DIRECTORY_SEPARATOR . $bundle . "#" . $domain . ".csv";

            if (file_exists($appBundleCatalogFile)) {
                $this->loadFile($appBundleCatalogFile);
            }

            /* Search for resources on extensions */
            $catalogFile = $bundle . DIRECTORY_SEPARATOR 
                . LAABS_RESOURCE . DIRECTORY_SEPARATOR 
                . \dependency\localisation\LocalisationInterface::LOCALE_DIR . DIRECTORY_SEPARATOR 
                . $lang . DIRECTORY_SEPARATOR
                . $domain . ".csv";


            $catalogFiles = \core\Reflection\Extensions::extendedPath($catalogFile, false);

            if (count($catalogFiles) == 0) {
                return;
                throw new \dependency\localisation\Exception("Catalog $catalogUri not found for language $lang");
            }

            foreach ($catalogFiles as $catalogFile) {
                $this->loadFile($catalogFile);
            }
        }

        ini_set('auto_detect_line_endings', false);
    }

    /**
     * Load a source file for translation catalo
     * @param string $catalogFile The uri to a ressource to load
     *
     * @return void
     * @author 
     **/
    public function loadFile($catalogFile) 
    {
        $handle = fopen($catalogFile, 'r');

        while ( ($data = fgetcsv($handle) ) !== false ) {
            if (count($data) == 1) {
                $headerName = strtok($data[0], ":");
                $headerValue = trim(strtok(""));
                switch (strtolower($headerName)) {
                    case 'plural-forms':
                        $vars = \laabs\explode(";", $headerValue);
                        $this->pluralFormCount = \laabs\explode("=", trim($vars[0]))[1];
                        $this->pluralFormAssert = \laabs\explode("=", trim($vars[1]))[1];
                        break;
                    case 'charset':
                        $this->charset = $headerValue;
                }

            } else {
                $msg = new msg($data);
                
                if ($msg->msgctxt) {
                    $qmsgid = (string) $msg->msgctxt . "/" . (string) $msg->msgid;
                } else {
                    $qmsgid = (string) $msg->msgid;
                }

                $qmsgid = preg_replace('/[^[:print:]]/', '', $qmsgid);
                                
                if (!array_key_exists($qmsgid, $this->messages)) {
                    $this->messages[$qmsgid] = $msg;
                } 
            }

            
        }
    }

}