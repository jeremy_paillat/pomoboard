<?php
namespace dependency\localisation;
/**
 * Localisation exception.
 */
class Exception
    extends \Exception
{
}
