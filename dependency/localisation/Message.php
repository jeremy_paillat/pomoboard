<?php

namespace dependency\localisation;

class Message
    implements \dependency\localisation\MessageInterface
{
    /* Constants */

    /* properties */
    protected $msgtxt;
    protected $msgvars = array();
    protected $binds = array();

    /* Methods */
    public function __construct($msgtxt) {
        $this->msgtxt = $msgtxt;

        if (preg_match_all("#\:(\w+)#", ' ' . $msgtxt . ' ', $msgvars))
            $this->msgvars = $msgvars[1];
    }

    public function bindValue($name, $value) {
        $this->binds[$name] = $value;
    }

    public function bindVariable($name, &$variable){
        $this->binds[$name] = $variable;
    }

    public function getText(array $msgvals=null) {
        $msgtxt = $this->msgtxt;
        foreach ($this->msgvars as $msgvar) {
            if (isset($msgvals[$msgvar]))
                $msgval = $msgvals[$msgvar];
            elseif (isset($this->binds[$msgvar]))
                $msgval = $this->binds[$msgvar];

            $msgtxt = str_replace(":" . $msgvar, $msgval, $msgtxt);
        }
        return $msgtxt;
    }

}