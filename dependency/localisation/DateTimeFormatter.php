<?php
namespace dependency\localisation;
/**
 * Date formatter
 * 
 * @package Localisation
 * @author  Cyril Vazquez <cyril.vazquez@maarch.org>
 */ 
class DateTimeFormatter
{
    /* Constants */

    /* Properties */
    protected $dateFormat;

    protected $locale;


    /* Methods */
    /**
     * Constructor
     * @param string $outputFormat The default output format for dates
     * @param string $locale       The target locale
     *
     * @return void
     * @author 
     **/
    public function __construct($dateFormat=false, $locale=false)
    {
        $this->dateFormat = $dateFormat;

        if ($locale) {
            $this->setLocale($locale);
        }
    }

    /**
     * Set the target locale for date times
     * @param string $locale The locale indentifer
     */
    public function setLocale($locale) 
    {
        setlocale(\LC_TIME, $locale);
        $this->locale = $locale;
    }

    /**
     * Get a formatted date/time
     * @param string $time         A valid date/time string. 
     * @param string $inputFormat  A PHP date() format to use
     * 
     * @return string The formatted time
     */
    public function format($time, $inputFormat=false) 
    {
        //var_dump("format " . $time . " with format " . $dateFormat);
        /*if ($timezone) {
            $datetimeZone = new DateTimeZone($timezone);
        } else {
            $datetimeZone = null;
        }*/
        if ($inputFormat) {
            $datetime = date_create_from_format($inputFormat, $time);
        } else {
            $datetime = date_create($time);
        }

        if ($datetime) {
            return $datetime->format($this->dateFormat);
        }

        return $time;
    }   

}

