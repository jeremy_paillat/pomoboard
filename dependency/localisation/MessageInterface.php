<?php
namespace dependency\localisation;
interface MessageInterface
{
    public function bindValue($name, $value);
    public function bindVariable($name, &$variable);
    public function getText(array $msgvals=null);
}
