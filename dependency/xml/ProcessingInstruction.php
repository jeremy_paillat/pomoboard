<?php
namespace dependency\xml;
class ProcessingInstruction
    extends \DOMProcessingInstruction
{
    /* -------------------------------------------------------------------------
    - Properties
    ------------------------------------------------------------------------- */
    /* -------------------------------------------------------------------------
    - Methods
    ------------------------------------------------------------------------- */
    public function __call($method, $args) {
        if (($extension = $this->ownerDocument->getNodeExtension($this)) && is_callable("$extension::$method"))
            return call_user_func_array("$extension::$method", array_merge(array($this), $args));

        return parent::$method();
    }
}