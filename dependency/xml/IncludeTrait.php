<?php
namespace dependency\xml;

trait IncludeTrait
{

    /*************************************************************************/
    /* INCLUDE processing instructions                                       */
    /*************************************************************************/
    public function xinclude($node=null)
    {
        if ($pis = $this->XPath->query("descendant-or-self::processing-instruction('xinclude')", $node)) {
            foreach ($pis as $pi) {
                $this->includeXml($pi);
            }
        }
    }

    public function includeXml($pi)
    {
        $includeFragment = $this->createDocumentFragment();
        $includeFragment->appendFile(trim($pi->data));
        if (!$includeFragment) {
            throw new \Exception("Error including Xml fragment: fragment '$pi->data' could not be parsed");
        }

        return $pi->parentNode->replaceChild($includeFragment, $pi);
    }

}