<?php
namespace dependency\xml;

trait TemplateTrait
{
    use TemplateDataTrait,
        TemplateParserTrait;

    /* Properties */
    protected $parsedPis = array();
    protected $parsedTexts = array();

    protected $mergedNodes;

    /* Methods */
    public function templateTrait()
    {
        $this->mergedNodes = new \SplObjectStorage();

        $this->bindVariable("_SESSION", $_SESSION);
    }


    /* -------------------------------------------------------------------------
    - MERGE processing instructions
    ------------------------------------------------------------------------- */
    public function merge($node=null, $source=null)
    {
        // Avoid garbage nodes merge
        if (!isset($this->mergedNodes)) {
            $this->mergedNodes = new \SplObjectStorage();
        }
        
        if ($node && $this->mergedNodes->contains($node)) {
            return;
        }

        $this->mergePis($node, $source);

        $this->mergeTextNodes($node, $source);
    }

    protected function mergePis($node=null, $source=null)
    {
        $pis = $this->XPath->query("descendant-or-self::processing-instruction('merge')", $node);

        foreach ($pis as $i => $pi) {
            if (!isset($this->parsedPis[$pi->data])) {
                $this->parsedPis[$pi->data] = $this->parse($pi->data);
            }

            if ($discardedNode = $this->mergePi($pi, $this->parsedPis[$pi->data], $source)) {
                $this->mergedNodes->attach($discardedNode);
            }
            $pi->parentNode->removeChild($pi);
        }
    }

    protected function mergePi($pi, $instr, $source=null)
    {
        // Get value by reference
        $value = &$this->getData($instr, $source);

        // Use value with selected target
        if (isset($instr->params['var'])) {
            $this->addVar($instr->params['var'], $value);

            return false;
        }

        // Get type of value
        $type = gettype($value);
        //var_dump($type);
        //if (isset($instr->params['source']))
        //    var_dump($instr->params['source']);

        switch(true) {
            // If value is scalar, merge text before Pi
            case $type == 'string':
            case $type == 'integer':
            case $type == 'double':
                return $this->mergeText($pi, $instr, $value);

            // Value is bool, remove target sibling if false
            case $type == 'boolean':
                return $this->mergeBool($pi, $instr, $value);

            // Value is null, no action
            case $type == 'NULL':
                return false;

            // Value is array, merge target by iterating over array
            case $type == 'array':
                return $this->mergeArray($pi, $instr, $value);

            case ($type == 'object'
                && $value instanceof \ArrayAccess
                && $value instanceof \Iterator) :
                return $this->mergeArray($pi, $instr, $value);

            case ($type == 'object'
                && $value instanceof \DOMNode) :
                return $this->mergeNode($pi, $instr, $value);

            // If value is an object ???
            case ($type == 'object'
                && method_exists($value, '__toString')) :
                return $this->mergeText($pi, $instr, (string) $value);

            // If value is an object ???
            case $type == 'object':
                return $this->mergeObject($pi, $instr, $value);

        }

    }

    protected function mergeTextNodes($node=null, $source=null)
    {
        $textNodes = $this->XPath->query("descendant-or-self::text()[contains(., '[?merge')] | descendant-or-self::*/@*[contains(., '[?merge')]", $node);

        for ($i=0, $l=$textNodes->length; $i<$l; $i++) {
            $this->mergeTextNode($textNodes->item($i), $source);
        }
    }

    protected function mergeTextNode($textNode, $source=null)
    {
        //$nodeXml = $this->saveXml($textNode);
        $nodeValue = $textNode->nodeValue;
        if (isset($this->parsedTexts[$nodeValue])) {
            $instructions = $this->parsedTexts[$nodeValue];
        } else {
            preg_match_all("#(?<pi>\[\?merge (?<instr>(?:(?!\?\]).)*)\?\])#", $nodeValue, $pis, PREG_SET_ORDER);
            $instructions = array();
            foreach ($pis as $i => $pi) {
                $instructions[$pi['pi']] = $this->parse($pi['instr']);
            }
            $this->parsedTexts[$nodeValue] = $instructions;
        }

        foreach ($instructions as $pi => $instr) {
            $value = $this->getData($instr, $source);
            if (is_scalar($value) || is_null($value) || (is_object($value) && method_exists($value, '__toString'))) {
                $mergedValue = str_replace($pi, (string) $value, $textNode->nodeValue);
                $mergedValue = htmlentities($mergedValue);
                $textNode->nodeValue = str_replace($pi, $value, $mergedValue);
            }
        }
    }

    public function mergeText($pi, $instr, $value)
    {
        $params = $instr->params;
        switch(true) {
        case isset($params['attr']):
            if (!$targetNode = $this->XPath->query("following-sibling::*", $pi)->item(0)) {
                return;
            }
            $targetNode->setAttribute($params['attr'], $value);
            break;

        case isset($params['render']):
            if (!$params['render']) {
                $fragment = $value;
            } else { 
                $fragment = $params['render'];
            }
            if (!isset($this->fragments[$fragment])) {
                return;
            }
            $targetNode = $this->fragments[$fragment]->cloneNode(true);
            $this->merge($targetNode);
            $pi->parentNode->insertBefore($targetNode, $pi);
            break;

        default:
            $targetNode = $this->createTextNode($value);
            $pi->parentNode->insertBefore($targetNode, $pi);
        }
    }

    public function mergeArray($pi, $instr, &$array)
    {
        $params = $instr->params;
        //var_dump($array);
        if (!$targetNode = $this->XPath->query("following-sibling::*", $pi)->item(0)) {
            return;
        }

        reset($array);
        if (count($array)) {
            do {
                $itemNode = $targetNode->cloneNode(true);
                $itemData = current($array);
                if (isset($params['source'])) {
                    $this->setSource($params['source'], $itemData);
                }

                $this->merge($itemNode, $itemData);
                $pi->parentNode->insertBefore($itemNode, $pi);
            } while (
                @next($array)
            );
        }
        // Delete targetNode (row template)
        return $targetNode->parentNode->removeChild($targetNode);
    }

    public function mergeObject($pi, $instr, $object)
    {
        $params = $instr->params;
        if (!$targetNode = $this->XPath->query("following-sibling::*", $pi)->item(0)) {
            return;
        }

        if (isset($params['source'])) {
            $this->setSource($params['source'], $object);
        }

        $this->mergeObjectProperties($targetNode, $oname = false, $object, $params);

    }

    protected function mergeObjectProperties($targetNode, $oname=false, $object, $params)
    {
        foreach ($object as $pname => $pvalue) {
            if ($oname) {
                $pname = $oname . "." . $pname;
            }
            //var_dump("merge $pname");
            if (\laabs::isScalar($pvalue)) {
                $this->mergeObjectProperty($targetNode, $pname, $pvalue, $params);
            }
            /*elseif (is_object($pvalue))
                $this->mergeObjectProperties($targetNode, $pname, $pvalue, $params);
            elseif (is_array($pvalue))
                foreach ($pvalue as $key => $item)
                    $this->mergeObjectProperties($targetNode, $pname . "[$key]", $pvalue, $params);*/
        }
    }

    protected function mergeObjectProperty($targetNode, $name, $value, $params)
    {
        $elements = $this->XPath->query("descendant-or-self::*[@name='$name']", $targetNode);
        for ($i=0, $l=$elements->length; $i<$l; $i++) {
            $element = $elements->item($i);
            switch (strtolower($element->nodeName)) {
                // Form Input
                case 'input':
                    switch($element->getAttribute('type')) {
                        case 'checkbox':
                            if (is_bool($value)) {
                                if ($value) {
                                    $element->setAttribute('checked', 'true');
                                } else {
                                    $element->removeAttribute('checked');
                                }
                            } else {
                                if ($element->getAttribute('value') == $value) {
                                    $element->setAttribute('checked', 'true');
                                } else {
                                    $element->removeAttribute('checked');
                                }
                            }
                            
                            break;

                        case 'radio':
                            if ($element->getAttribute('value') == $value) {
                                $element->setAttribute('checked', 'true');
                            } else {
                                $element->removeAttribute('checked');
                            }
                            break;

                        default:
                            $element->setAttribute('value', $value);
                    }
                    break;

                // Select
                case 'select':
                    $value = $this->XPath->quote($value);
                    if ($option = $this->XPath->query(".//option[@value=$value]", $element)->item(0)) {
                        $option->setAttribute('selected', 'true');
                        if ($optGroup = $this->XPath->query("parent::optgroup", $option)->item(0)) {
                            $optGroup->removeAttribute('disabled');
                        }
                    }
                    break;

                // Textareas
                case 'textarea':
                    $element->nodeValue = $value;
                    break;
            }
        }
    }

    public function mergeBool($pi, $instr, $bool)
    { 
        $params = $instr->params;
        if (!$targetNode = $this->XPath->query("following-sibling::*", $pi)->item(0)) {
            return;
        }

        if (isset($params['attr'])) {
            if ($bool == false) {
                $targetNode->removeAttribute($params['attr']);
            } else {
                $targetNode->setAttribute($params['attr'], $params['attr']);
            }
        } else {
            if (isset($params['source'])) {
                $this->setSource($params['source'], $bool);
            }
            if ($bool == false) {
                return $targetNode->parentNode->removeChild($targetNode);
            }
        }
    }

    public function mergeNode($pi, $instr, $DOMNode)
    {
        $pi->parentNode->insertBefore($DOMNode, $pi);
    }

}