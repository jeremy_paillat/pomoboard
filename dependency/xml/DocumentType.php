<?php
namespace dependency\xml;
class DocumentType
    extends \DOMDocumentType
{
    protected $elementClasses;
    public function registerElementClass($elementName, $elementClass) {
        $this->elementClasses[$elementName] = $elementClass;
    }
    public function getCallbackClass($elementName) {
        if (isset($this->elementClasses[$elementName]))
            return $this->elementClasses[$elementName];
        return "\dependency\xml\Element";
    }
}