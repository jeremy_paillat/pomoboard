<?php
namespace dependency\xml;
class Element
    extends \DOMElement
{
    /* -------------------------------------------------------------------------
    - Properties
    ------------------------------------------------------------------------- */
    /* -------------------------------------------------------------------------
    - Methods
    ------------------------------------------------------------------------- */
    public function __call($method, $args) {
        // If method exists magic method is not called by DOM
        if (($extension = $this->ownerDocument->getNodeExtension($this)) && is_callable("$extension::$method"))
            return call_user_func_array("$extension::$method", array_merge(array($this), $args));

        return parent::$method($this);
    }
}