<?php
namespace dependency\xml;
class DocumentFragment
    extends \DOMDocumentFragment
{
    public function __call($method, $args)
    {
        // If method exists magic method is not called by DOM
        if (($extension = $this->ownerDocument->getNodeExtension($this)) && is_callable("$extension::$method"))
            return call_user_func_array("$extension::$method", array_merge(array($this), $args));

        return parent::$method();
    }
    
    public function appendXml($source)
    {
        $source = preg_replace('#\<\?xml [^\>]*\>#', '', $source);

        $result = parent::appendXml($source);
        $this->ownerDocument->xinclude($this);
        
        return $result;
    }

    public function appendFile($resource)
    {
        $Router = new \core\Route\ResourceRouter($resource);
        $path = $Router->getResource()->getRealPath();
        $source = file_get_contents($path);

        return $this->appendXml($source);
    }
}