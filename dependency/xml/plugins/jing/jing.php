<?php
namespace dependency\Xml\plugins\jing;
/**
 * Plugin for RelaxNg validation tool jing
 */
class jing
{

    protected $executable;

    /**
     * Construct environment
     *
     * @return void
     * @author 
     */
    public function __construct()
    {
        $this->executable = "java -jar " . __DIR__ . DIRECTORY_SEPARATOR . 'bin' . DIRECTORY_SEPARATOR . "jing.jar";
    }

    /**
     * Validate file
     * @param string $schema A schema file
     * @param mixed  $xml    An xml file or an array of filenames
     * 
     * @return boolean
     */
    public function validate($schema, $xml)
    {
        $tokens = array();
        $tokens[] = $this->executable;
        $tokens[] = '"'.$schema.'"';
        $xmls = (array) $xml;
        foreach ($xmls as $xml) {
            $tokens[] = '"'.$xml.'"';
        }
        
        $command = implode(' ', $tokens);

        $output = array();
        $return = null;

        exec($command, $output, $return);
        var_dump($command);
        //var_dump($output);
        //var_dump($return);

        if ($return !== 0) {
            foreach ($output as $line) {
                $sep = strpos($line, ": ");
                $position = substr($line, 0, $sep);
                $message = substr($line, $sep+2);
                
                $posparts = explode(':', $position);
                $offset = array_pop($posparts);
                $line = array_pop($posparts);
                $filename = implode(':', $posparts);
                
                $this->errors[$filename] = $message;
            }

            return false;
        }

        return true;
    }

    /**
     * Get errors
     * 
     * @return array The validation errors
     */
    public function getErrors()
    {
        return $this->errors;
    }

}