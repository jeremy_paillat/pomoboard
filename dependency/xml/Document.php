<?php
namespace dependency\xml;
class Document
    extends \DOMDocument
{
    use IncludeTrait,
        TemplateTrait;

    /* Constants */
    /* -------------------------------------------------------------------------
    - Properties
    ------------------------------------------------------------------------- */
    protected $extensions;
    public $XPath;
    /* -------------------------------------------------------------------------
    - Methods
    ------------------------------------------------------------------------- */
    /* -------------------------------------------------------------------------
    - DOMDocument methods overrides
    ------------------------------------------------------------------------- */
    public function __construct($version="1.0", $encoding=null, $extensions=null)
    {
        parent::__construct($version, $encoding);
        $this->registerNodeClass('DOMElement'                 , '\dependency\xml\Element');
        $this->registerNodeClass('DOMAttr'                    , '\dependency\xml\Attr');
        $this->registerNodeClass('DOMText'                    , '\dependency\xml\Text');
        $this->registerNodeClass('DOMComment'                 , '\dependency\xml\Comment');
        $this->registerNodeClass('DOMCdataSection'            , '\dependency\xml\CdataSection');
        $this->registerNodeClass('DOMEntityReference'         , '\dependency\xml\EntityReference');
        $this->registerNodeClass('DOMProcessingInstruction'   , '\dependency\xml\ProcessingInstruction');
        $this->registerNodeClass('DOMDocument'                , '\dependency\xml\Document');
        $this->registerNodeClass('DOMDocumentType'            , '\dependency\xml\DocumentType');
        $this->registerNodeClass('DOMDocumentFragment'        , '\dependency\xml\DocumentFragment');
        $this->registerNodeClass('DOMNotation'                , '\dependency\xml\Notation');
        $this->nodeExtensions = $extensions;
        $this->XPath = new XPath($this);
    }
    /* -------------------------------------------------------------------------
    - Xml\Document methods
    ------------------------------------------------------------------------- */
    public function getNodeExtension($node)
    {
        switch($node->nodeType) {
        case \XML_ELEMENT_NODE:
            $type = 'ELEMENT';
            $name = $node->nodeName;
            break;
        case \XML_ATTRIBUTE_NODE:
            $type = 'ATTRIBUTE';
            $name = $node->name;
            break;
        case \XML_PI_NODE:
            $type = 'PI';
            $name = $node->target;
            break;
        default:
            return;
        }

        if (isset($this->nodeExtensions[$type][$name])) {
            return $this->nodeExtensions[$type][$name];
        }
    }

    /**
     * Load a ressource
     * @param string $uri
     * 
     */
    public function loadResource($uri)
    {
        $router = new \core\Route\ResourceRouter($uri);
        $path = $router->getResource()->getRealPath();

        $this->load($path);

        $this->XPath = new XPath($this);
    }

    /**
     * Export XML as object
     * @param DOMNode $node
     * 
     * @return mixed
     */
    public function export($node=null)
    {
        if (!$node) {
            $node = $this->documentElement;
            $this->XPath = new XPath($this);
        }
        
        $childNodes = $this->XPath->query("./* | ./@*", $node);
        if ($childNodes->length > 0) {
            $value = new \stdClass();
            foreach ($childNodes as $childNode) {
                $propertyName = $childNode->nodeName;
                $propertyValue = $childNode->nodeValue;
                switch($childNode->nodeType) {
                    case XML_ATTRIBUTE_NODE:
                        $value->{$propertyName} = $childNode->value;
                        break;

                    case XML_ELEMENT_NODE:
                        $value->{$propertyName} = $this->export($childNode);
                        break;
                }
            }
        } else {
            $value = $node->nodeValue;
        }

        return $value;
    }
}