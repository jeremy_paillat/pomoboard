<?php
namespace dependency\html\plugins\DataTree;
class DataTree
    extends \dependency\html\AbstractHtmlClass
{
    /* -------------------------------------------------------------------------
    - Properties
    ------------------------------------------------------------------------- */
    protected $parameters;
    /* -------------------------------------------------------------------------
    - Methods
    ------------------------------------------------------------------------- */
    public function __construct($element)
    {
        parent::__construct($element);
        $this->parameters = new \StdClass();
    }
    public function saveHtml() {
        $dataTreeId = \laabs\uniqid();
        $this->element->setAttribute('data-tree-id', $dataTreeId);
        $parameters = json_encode($this->parameters);
        $scriptText =
<<<EOS
$(document).ready(function() {
    $('*[data-tree-id="$dataTreeId"]').dataTree($parameters);
});
EOS;
        $script = $this->element->ownerDocument->createElement('script');
        $CdataSection = $this->element->ownerDocument->createCDataSection($scriptText);
        $script->appendChild($CdataSection);
        $this->element->appendChild($script);
    }
    public function setIconBranchClose($icon)
    {
        $this->parameters->iconBranchClose = $icon;
    }
    public function setIconBranchOpen($icon)
    {
        $this->parameters->iconBranchOpen = $icon;
    }
    public function setIconLeaf($icon)
    {
        $this->parameters->iconLeaf = $icon;
    }
    public function setToggleEasing($easing)
    {
        $this->parameters->toggleEasing = $easing;
    }
    public function setToggleDuration($duration)
    {
        $this->parameters->toggleDuration = $duration;
    }
    public function setExpanded($expanded)
    {
        $this->parameters->expanded = $expanded;
    }
}