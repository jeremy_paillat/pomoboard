<?php

namespace dependency\html;

class Factory
    extends \DOMImplementation
{
    /* Constants */

    /* Properties */


    protected $lang;

    protected $extensions;

    protected $classes;

    protected $plugins;

    protected $headers;

    protected $layout;

    protected $view;

    protected $XPath;

    protected $Sources;

    public function __construct($layout, $lang=false, $extensions=null, $plugins=null, $headers=null, $classes=null) {
        $this->layout = $layout;

        $this->lang = $lang;

        $this->plugins    = $plugins;
        $this->extensions = $extensions;

        $this->headers = $headers;
        $this->classes = $classes;
    }

    public function setLayout($layout=false) {
        $this->layout = $layout;
    }

    public function setLang($lang) {
        $this->lang = $lang;
    }

    public function newView($layout=null)
    {
        $view = new \dependency\html\Document($this->extensions, $this->plugins, $this->headers, $this->classes);

        if ($layout === false) {
            // Layout is false make empty document
        } else {
            // Layout is null (not passed) use default layout
            if (is_null($layout)) $layout = $this->layout;
            $LayoutRoute = new \core\Route\ResourceRouter($layout);
            $layoutPath = $LayoutRoute->Resource->getRealPath();
            $layoutFragment = $view->createDocumentFragment();
            $layoutFragment->appendFile($layoutPath);
            $view->getElementsByTagName('body')->item(0)->appendChild($layoutFragment);
        }

        $this->View = $view;
        $this->XPath = new \dependency\html\XPath($this->View);

        return $this->View;
    }

}