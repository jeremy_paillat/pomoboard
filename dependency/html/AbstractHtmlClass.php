<?php
namespace dependency\html;

class AbstractHtmlClass
{
    /* -------------------------------------------------------------------------
    - Properties
    ------------------------------------------------------------------------- */
    protected $element = null;
    /* -------------------------------------------------------------------------
    - Methods
    ------------------------------------------------------------------------- */
    public function __construct($element)
    {
        $this->element = $element;
    }

    public function __get($name) {
        if (property_exists($this, $name))
            return $this->$name;
    }

}