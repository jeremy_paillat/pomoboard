<?php
namespace dependency\html;

trait IncludeTrait
{
    /*************************************************************************/
    /* INCLUDE processing instructions                                       */
    /*************************************************************************/
    public function hinclude($node=null)
    {
        if ($Pis = $this->XPath->query("descendant-or-self::processing-instruction('hinclude')", $node))
            foreach ($Pis as $Pi)
                $this->includeHtml($Pi);
    }
    
    public function includeHtml($Pi)
    {
        $includeFragment = $this->createDocumentFragment();
        $includeFragment->appendHtmlFile(trim($Pi->data));
        if (!$includeFragment) {
            throw new \Exception("Error including Html fragment: fragment '$Pi->data' could not be parsed");
        }
        return $Pi->parentNode->replaceChild($includeFragment, $Pi);
    }
}