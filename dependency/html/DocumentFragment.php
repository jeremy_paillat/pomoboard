<?php
namespace dependency\html;
/**
 * undocumented class
 *
 * @package dependency\html
 * @author  Cyril Vazquez <cyril.vazquez@maarch.org>
 **/
class DocumentFragment
    extends \dependency\xml\DocumentFragment
{
    /**
     * Appendh Html content to the fragment
     * @param string $source The html source content
     * 
     * @return bool
     */
    public function appendHtml($source)
    {
        libxml_use_internal_errors(true);
        $htmlWrapperId = \laabs\uniqid();
        $htmlWrapper = "<div id='$htmlWrapperId'>" . $source . '</div>';
        $hdoc = new \DOMDocument();
        $hdoc->loadHTML($htmlWrapper);
        $children = $hdoc->getElementById($htmlWrapperId)->childNodes;
        foreach ($children as $child) {
            $childHtml = $hdoc->saveXml($child, LIBXML_NOEMPTYTAG + LIBXML_NOXMLDECL);
            $childHtml = str_replace("?>", ">", $childHtml);
            $result = parent::appendXml($childHtml);
        }
        libxml_use_internal_errors(false);

        $this->ownerDocument->addPlugins($this);
        $this->ownerDocument->hinclude($this);

        return $result;
    }
    
    /**
     * Appendh Html resource file contents to the fragment
     * @param string $resource The html source uri
     * 
     * @return bool
     */
    public function appendHtmlFile($resource)
    {
        $router = new \core\Route\ResourceRouter($resource);
        $path = $router->getResource()->getRealPath();
        $source = file_get_contents($path);

        return $this->appendHtml($source);
    }
    
    /**
     * Clone the Html node with its plugins
     * @param bool $deep clone with chil nodes
     * 
     * @return object The cloned node
     */
    public function cloneNode($deep=false)
    {
        $clonedNode = parent::cloneNode($deep);
        $this->ownerDocument->addPlugins($this);
        
        return $clonedNode;
    }
}