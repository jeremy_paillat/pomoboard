<?php
namespace dependency\html;
class Element
    extends \dependency\xml\Element
{
    /* -------------------------------------------------------------------------
    - Properties
    ------------------------------------------------------------------------- */
    public $plugin = array();
    /* -------------------------------------------------------------------------
    - Methods
    ------------------------------------------------------------------------- */
    public function cloneNode($deep=false) {
        $clonedNode = parent::cloneNode($deep);
        $this->ownerDocument->addPlugins($clonedNode);
        return $clonedNode;
    }
    public function addHtmlClass($newHtmlClasses) {
        $htmlClasses = $this->tokenize($this->getAttribute("class"));
        foreach ($this->tokenize($newHtmlClasses) as $newHtmlClass)
            if (!in_array($newHtmlClass, $htmlClasses))
                $htmlClasses[] = $newHtmlClass;
        $result = parent::setAttribute('class', $this->stringify($htmlClasses));
    }
    public function removeHtmlClass($oldHtmlClasses)
    {
        $htmlClasses = $this->tokenize($this->getAttribute("class"));
        foreach ($this->tokenize($oldHtmlClasses) as $oldHtmlClass)
            if ($i = array_search($oldHtmlClass, $htmlClasses))
                unset($htmlClasses[$i]);
        return parent::setAttribute('class', $this->stringify($htmlClasses));
    }
    public function hasHtmlClass($htmlClass)
    {
        return (array_search($htmlClass, $this->tokenize($this->getAttribute("class"))) !== false);
    }
    public function tokenize($string)
    {
        return explode(" ", $string);
    }
    public function stringify(array $array)
    {
        return implode(" ", $array);
    }
}