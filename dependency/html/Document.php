<?php
namespace dependency\html;
/**
 * Represents an entire HTML document; serves as the root of the document tree. 
 *
 * @package Dependency\Html
 * @author  Cyril VAZQUEZ <cyril.vazquez@maarch.org>
 */
class Document
    extends \dependency\xml\Document
{
    use IncludeTrait, 
        LocalisationTrait;

    /* Constants */
    /* -------------------------------------------------------------------------
    - Properties
    ------------------------------------------------------------------------- */
    protected $layout;
    protected $classes;
    protected $plugins;
    public $XPath;
    public $translator;
    public $dateTimeFormatter;
    /**
     *   -- document --
     *   <html>
     *       <head>
     *       <body>
     *   -- body --
     *       -- layout --
     *           <main> or <* role="main">
     */
    /* -------------------------------------------------------------------------
    - Methods
    ------------------------------------------------------------------------- */
    /*************************************************************************/
    /* DOM Document override
    /*************************************************************************/
    /**
     * Construct a new Html document
     * @param string $layout            The uri to a resource for the default layout to use
     * @param string $layoutData        The uri to a service method to merge with the layout
     * @param array  $extensions        An associative array of Html node type+name and Php classes that will extend it
     * @param array  $plugins           An associative array of Html classes and Php classes that will be automatically plugged-in
     * @param array  $headers           Uris of html ressources that will be imported into the <head> tag
     * @param object $translator        The localisation/translator object to translate the texts of the document
     * @param object $dateTimeFormatter The localisation/dateTimeFormatter object to format the dates of the document
     * 
     * @return void
     **/
    public function __construct($layout=null, $layoutData=null, $extensions=null, $plugins=null, $headers=null, \dependency\localisation\TranslatorInterface $translator=null, \dependency\localisation\DateTimeFormatter $dateTimeFormatter=null)
    {
        parent::__construct(null, null, $extensions);
        $this->formatOutput = true;

        // Register node classes for Html
        $this->registerNodeClass('DOMElement', '\dependency\html\Element');
        $this->registerNodeClass('DOMDocument', '\dependency\html\Document');
        $this->registerNodeClass('DOMDocumentFragment', '\dependency\html\DocumentFragment');
        //$this->registerNodeClass('DOMProcessingInstruction'   , '\dependency\html\ProcessingInstruction');

        // Keep plugins and classes (extensions are already managed by Xml\Document)
        $this->plugins = $plugins;
        $this->headers = $headers;
        $this->layout = $layout;
        $this->layoutData = $layoutData;

        $this->XPath = new XPath($this);
        $this->translator = $translator;
        $this->dateTimeFormatter = $dateTimeFormatter;

        // Construct Html base <!--DOCTYPE --><html lang="lang"><head/><body/></html>
        $htmlFragment = $this->createDocumentFragment();
        $htmlFragment->appendHtml('<!--DOCTYPE html -->');
        $this->appendChild($htmlFragment);
        $html = $this->createElement('html');
        $this->appendChild($html);
        
        $head = $this->createElement('head');
        $this->documentElement->appendChild($head);
        $body = $this->createElement('body');
        $this->documentElement->appendChild($body);     

    }

    /**
     * Save the entire Html document or the given node into a Html string; also resolves the plugins to automatically insert necessary data
     * @param object $node The html node to save or null to save entire document
     *
     * @return string The Html string
     */
    public function saveHtml($node = null)
    {
        $this->savePlugins($node);

        return parent::saveHtml($node);
    }
    /*************************************************************************/
    /* DOM Shortcuts
    /*************************************************************************/
    /**
     * Insert header contents onto the <head> tag of the document
     * @param array $headers Uris of html head content ressources or null to use default headers declared at consruction 
     */
    public function addHeaders(array $headers = null)
    {
        $head = $this->getHead();
        if ($headers) {
            foreach ($headers as $header) {
                $this->addContentFile($header, $head);
            }
        } else {
            foreach ((array) $this->headers as $header) {
                $this->addContentFile($header, $head);
            }
        }

        // add css
        $this->addStyle("/public/dependency/html/css/bootstrap-toggle/bootstrap-toggle.css");

        // Add js scripts
        $this->addScript("/public/dependency/html/js/jQuery_1.11.0/jQuery.js");

        $this->addScript("/public/dependency/html/js/flipclock/flipclock.min.js");
	 // Add jquery ui
        $this->addScript("/public/dependency/html/js/jQueryUI_1.10.4/jqueryUI.js");
	// Add js 
        //less compiler
        $this->addScript("/public/dependency/html/js/less_1.7.0/less.js");
        //bootstrap.affix
        $this->addScript("/public/dependency/html/js/bootstrap_3.1.1/affix.js");
        //bootstrap.alert
        $this->addScript("/public/dependency/html/js/bootstrap_3.1.1/alert.js");
        //bootstrap.button
        $this->addScript("/public/dependency/html/js/bootstrap_3.1.1/button.js");
        //bootstrap.carousel
        $this->addScript("/public/dependency/html/js/bootstrap_3.1.1/carousel.js");
        //bootstrap.collapse
        $this->addScript("/public/dependency/html/js/bootstrap_3.1.1/collapse.js");
        //bootstrap.dropdown
        $this->addScript("/public/dependency/html/js/bootstrap_3.1.1/dropdown.js");
        //bootstrap.modal
        $this->addScript("/public/dependency/html/js/bootstrap_3.1.1/modal.js");
        //bootstrap.tooltip
        $this->addScript("/public/dependency/html/js/bootstrap_3.1.1/tooltip.js");
        //bootstrap.popover
        $this->addScript("/public/dependency/html/js/bootstrap_3.1.1/popover.js");
        //bootstrap.scrollspy
        $this->addScript("/public/dependency/html/js/bootstrap_3.1.1/scrollspy.js");
        //bootstrap.tab
        $this->addScript("/public/dependency/html/js/bootstrap_3.1.1/tab.js");
        //bootstrap.transition
        $this->addScript("/public/dependency/html/js/bootstrap_3.1.1/transition.js");
        //metisMenu
        $this->addScript("/public/dependency/html/js/metisMenu_1.0.1/metisMenu.js");
        //dataForm
        $this->addScript("/public/dependency/html/js/dataForm_0.0.1/dataForm.js");
        //gritter
        $this->addScript("/public/dependency/html/js/gritter_1.7.4/gritter.min.js");
        //typeahead
        $this->addScript("/public/dependency/html/js/typeahead_0.10.2/typeahead.js");  

        $this->addScript("/public/dependency/html/js/bootstrap-toggle/bootstrap-toggle.js");

        $this->addScript("/public/dependency/html/js/datePicker/bootstrap-datepicker.js");
        $this->addScript("/public/dependency/html/js/datePicker/locales/bootstrap-datepicker.fr.js");
    }

    /**
     * Deploy a layout on the empty document
     * @param string $layout     The uri of a resource for the layout html content or null to use the default layout declared at construction
     * @param string $layoutData Some data to merge with layout
     */
    public function useLayout($layout=false, $layoutData=null)
    {
        if (!$layout) {
            $layout = $this->layout;
        }
        if (!$layout) {
            return;
        }
        $body = $this->getBody();
        $layoutFragment = $this->createDocumentFragment();
        $layoutFragment->appendHtmlFile($layout);

        if (!$layoutData) {
            if ($this->layoutData) {
                $methodRoute = new \core\Route\MethodRouter($this->layoutData);
                $layoutService = $methodRoute->service->call();
                $layoutData = $methodRoute->method->call($layoutService);
            }
        }
        
        if ($layoutData) {
            $this->merge($layoutFragment, $layoutData);
        } 

        $body->appendChild($layoutFragment);
    }

    /**
     * Add html content to the document. If no container is provided as argument, the method will guess where to append the html
     * @param string $content   The html content to add
     * @param object $container An html node to append the content. If ignored, the method will search for the best place to append content to
     * 
     * @return object The first node appended to the container
     */
    public function addContent($content, $container=false)
    {
        $contentFragment = $this->createDocumentFragment();
        $contentFragment->appendHtml($content);
        if (!$container) {
            $container = $this->getContainer();
        }

        return $container->appendChild($contentFragment);
    }

    /**
     * Add a resource content to the document. If no container is provided as argument, the method will guess where to append the html
     * @param string $contentResource The uri to a resource filr holding the html content to add
     * @param object $container       An html node to append the content. If ignored, the method will search for the best place to append content to
     * 
     * @return object The first node appended to the container
     */
    public function addContentFile($contentResource, $container=false)
    {
        $contentFragment = $this->createDocumentFragment();
        $contentFragment->appendHtmlFile($contentResource);
        if (!$container) {
            $container = $this->getContainer();
        }

        return $container->appendChild($contentFragment);
    }

    /*************************************************************************/
    /* HTML plugins management
    /*************************************************************************/
    /**
     * Add the plugins to a html tag
     * @param object $node An html node to add plugins to
     */
    public function addPlugins($node=null)
    {
        //var_dump("addPlugins($node->nodeType)");
        $elements = $this->XPath->query("descendant-or-self::*[@class]", $node);
        foreach ($elements as $element) {
            foreach (explode(' ', $element->getAttribute('class')) as $htmlClass) {
                if (isset($this->plugins[$htmlClass])) {
                    $pluginClass = $this->plugins[$htmlClass];
                    $element->plugin[$htmlClass] = new $pluginClass($element);
                }
            }
        }
    }

    /**
     * Save the plugins of a html tag
     * @param object $node An html node to save plugins of
     */
    public function savePlugins($node=null)
    {
        $elements = $this->XPath->query("descendant-or-self::*[@class]", $node);
        foreach ($elements as $element) {
            foreach ($element->plugin as $name => $plugin) {
                if (method_exists($plugin, 'saveHtml')) {
                    $plugin->saveHtml();
                }
            }
        }
    }

    /**
     * Get the plugins of a given html class for a given html node
     * @param string $class The class name the plugins are linked with
     * @param object $node  An html node to get plugins
     * 
     * @return array The plugin objects
     */
    public function getPlugins($class, $node=null)
    {
        $elements = $this->getElementsByClass($class, $node);
        $plugins = array();
        foreach ($elements as $element) {
            $plugins[] = $element->plugin[$class];
        }

        return $plugins;
    }

    /*-------------------------------------------------------------------------
    - get Methods
    ------------------------------------------------------------------------- */
    /**
     * Get a node with given id attribute
     * @param string $elementId The id attribute
     * @param object $node      The html node for relative search
     * 
     * @return The element or false
     */
    public function getElementById($elementId, $node=null) 
    {
        if ($nodeList = $this->XPath->query("descendant-or-self::*[@id='$elementId']", $node)) {
            return $nodeList->item(0);
        }
    }

    /**
     * Get a node list of elements having the given name attribute
     * @param string $name The name attribute value
     * @param object $node The html node for relative search
     * 
     * @return The node list of elements
     */
    public function getElementsByName($name, $node=null) 
    {
        return $this->XPath->query("descendant-or-self::*[@name='$name']", $node);
    }

    /**
     * Get a node list of elements having the given class attribute
     * @param string $class The class attribute value
     * @param object $node  The html node for relative search
     * 
     * @return The node list of elements
     */
    public function getElementsByClass($class, $node=null) 
    {
        return $this->XPath->query("descendant-or-self::*[contains(concat(' ', normalize-space(@class), ' '), ' $class ')]", $node);
    }

    /**
     * Get a node list of elements having the given role attribute
     * @param string $role The role attribute value
     * @param object $node The html node for relative search
     * 
     * @return The node list of elements
     */
    public function getElementsByRole($role, $node=null) 
    {
        return $this->XPath->query("descendant-or-self::*[@role='$role']", $node);
    }

    /**
     * Retrieve the <head> element of the document
     * @return The head element
     */
    public function getHead()
    {
        return $this->getElementsByTagName('head')->item(0);
    }

    /**
     * Retrieve the <body> element of the document
     * @return The body element
     */
    public function getBody()
    {
        return $this->getElementsByTagName('body')->item(0);
    }

    /**
     * Retrieve the main container element of the document
     * 
     * @return The main container element
     */
    public function getContainer()
    {
        if ($mainElement = $this->getElementsByTagName("main")->item(0)) {
            return $mainElement;
        }
        if ($mainRole = $this->getElementsByRole("main")->item(0)) {
            return $mainRole;
        }
        if ($containerClass = $this->getElementsByClass("container")->item(0)) {
            return $containerClass;
        }

        return $this->getBody();
    }

    /**
     * Get the script element by its src attribute
     * @param string $src The src of script
     * 
     * @return \DOMElement
     */
    public function getScript($src)
    {
        $script = $this->XPath->query("//script[@src='$src']")->item(0);

        return $script;
    }

    /**
     * Add a script element if not already included
     * @param string $src The src of script
     * 
     * @return \DOMElement The created or already included element
     */
    public function addScript($src)
    {
        if (!($script = $this->getScript($src))) {

            $script = $this->createElement('script');
            $script->setAttribute('src', $src);
            $script->setAttribute('data-auto', '1');

            $head = $this->getHead();

            $autos = $this->XPath->query('./script[@data-auto]', $head);
            if ($autos->length) {
                $lastAuto = $autos->item($autos->length-1);
                $head->insertBefore($script, $lastAuto->nextSibling);
                
                return $script;
            }

            $metas = $this->XPath->query('./meta', $head);
            if ($metas->length) {

                $lastMeta = $metas->item($metas->length-1);

                if ($lastMeta->nextSibling) {
                    $head->insertBefore($script, $lastMeta->nextSibling);
                } else {
                    $head->appendChild($script);
                }
            } else {
                $head->appendChild($script);
            }
        }

        return $script;
    }

    /**
     * Get the style element by its src attribute
     * @param string $href The href of style
     * 
     * @return \DOMElement
     */
    public function getStyle($href)
    {
        $style = $this->XPath->query("//style[@href='$href']")->item(0);

        return $style;
    }

    /**
     * Add a script element if not already included
     * @param string $src The src of script
     * 
     * @return \DOMElement The created or already included element
     */
    public function addStyle($href)
    {
        if (!($style = $this->getStyle($href))) {

            $style = $this->createElement('link');
            $style->setAttribute('href', $href);
            $style->setAttribute('rel', 'stylesheet');
            $style->setAttribute('data-auto', '1');

            $head = $this->getHead();

            $autos = $this->XPath->query('./link[@data-auto]', $head);
            if ($autos->length) {
                $lastAuto = $autos->item($autos->length-1);
                $head->insertBefore($style, $lastAuto->nextSibling);
                
                return $style;
            }

            $metas = $this->XPath->query('./meta', $head);
            if ($metas->length) {
                $lastMeta = $metas->item($metas->length-1);
                if ($lastMeta->nextSibling) {
                    $head->insertBefore($style, $lastMeta->nextSibling);
                } else {
                    $head->appendChild($style);
                }
            } else {
                $head->appendChild($style);
            }
        }

        return $style;
    }

}