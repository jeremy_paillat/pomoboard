<?php
namespace dependency\logger;
require_once __DIR__ . DIRECTORY_SEPARATOR . "log4php" . DIRECTORY_SEPARATOR . "Logger.php";

class Logger 
{

    public function __construct($rootLogger=array(), $appenders=array())
    {
        $this->configure($rootLogger, $appenders);
    }

    public function configure($rootLogger=array(), $appenders=array())
    {
        \Logger::configure(
            array(
                'rootLogger' => $rootLogger,
                'appenders' => $appenders
            )
        );
    }

    public function getLogger($name='main')
    {
        return \Logger::getLogger($name);
    }

}