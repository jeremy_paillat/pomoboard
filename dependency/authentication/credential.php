<?php

namespace dependency\authentication;

/**
 * Class for the authenticated user and authentication info
 */
class credential
{
    use \core\ReadonlyTrait;

    public $user;

    public $app;

    public $timestamp;

    public function __construct($user)
    {
        $this->user = $user;

        $this->app = \core\globals\Server::getApp();

        $this->timestamp = time();
    }

}