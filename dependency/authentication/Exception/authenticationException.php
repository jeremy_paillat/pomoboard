<?php

namespace dependency\authentication\Exception;

class authenticationException
    extends \core\Exception
{
    public function __construct($message = "", $code = 0, \Exception $previous = null)
    {
        parent::__construct($message, $code, $previous);
    }
}