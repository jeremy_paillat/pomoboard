<?php

namespace dependency\authentication\Exception;

class passwordChangeRequired
    extends \core\Exception
{
    public $message = false;

    /**
     * undocumented function
     *
     * @return void
     * @author 
     */
    public function __construct(array $validationErrors=null)
    {
        parent::__construct("Invalid user information");
        $this->message = "Password must be change";
    }

}
