<?php
namespace dependency\authentication;
/**
 * Interface for authentication of a user
 *
 * @package Dependency\Authentication
 * @author  Maarch Cyril  VAZQUEZ <cyril.vazquez@maarch.org>
 */
Interface AuthenticationInterface
{
    /**
     * Authenticate a user with a userName and a password.
     * Check user existence, verify password, user is enabled and not locked
     * Load the authenticated user object in session/dependency/authentication/credential 
     * @param string $userName The user name
     * @param string $password The user password
     * 
     * @return string A connection token 
     */
    public function logIn($userName, $password);

    /**
     * End the session
     * @todo unstore the token with date and validity
     * 
     * @return bool 
     */
    public function logOut();

    /**
     * Get the authentication credential
     * 
     * @return object 
     */
    public function credential();

}