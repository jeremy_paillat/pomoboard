<?php

namespace dependency\authorization;

interface AuthorizationInterface
{
    /* Properties */

    /* Methods */
    /**
     * Get the list of a user privileges, i.e. the bundle/controllers he can use
     *
     * @return array The array of unique (string) routes (bundle/controller) where the user can call actions
     */
    public function getUserPrivileges();

    /**
     * Checks if the user has access to the action
     * @param string $route  The action route to check privilege for
     *
     * @return bool
     */
    public function hasUserPrivilege($route);

    /**
     * Get a user list of access rules
     * @param string $className The class name for objects
     *
     * @return array The list of rules
     */
    public function getUserAccessRule($className);

    //public function hasUserAccess($class, $object);
}