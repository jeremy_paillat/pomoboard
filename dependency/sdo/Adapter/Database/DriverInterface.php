<?php
namespace dependency\sdo\Adapter\Database;
/**
 *
 */
interface DriverInterface
{

    public function setDateFormat($name);

    public function getDateFormat();

}