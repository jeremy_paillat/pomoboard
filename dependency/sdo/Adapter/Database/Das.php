<?php
namespace dependency\sdo\Adapter\Database;

class Das
    extends \dependency\datasource\Adapter\Database\Datasource
    implements \dependency\sdo\DasInterface,
               \dependency\datasource\TransactionalInterface
{
    
    /* Methods */
    public function __construct(
        $Dsn, 
        $Username=null, 
        $Password=null, 
        array $Options=null, 
        array $bindings=array(), 
        $dateFormat=null, 
        $timeFormat=null,
        $datetimeFormat=null,
        $boolFormat=null, 
        $encoding="UTF8")
    {
        parent::__construct($Dsn, $Username, $Password, $Options);

        $this->Pdo->setAttribute(\PDO::ATTR_CASE, \PDO::CASE_NATURAL);
        
        $driverClass = __NAMESPACE__ . LAABS_NS_SEPARATOR . "Driver" . LAABS_NS_SEPARATOR . $this->driver;
        $this->driver = new $driverClass($this, $dateFormat, $timeFormat, $datetimeFormat, $boolFormat, $encoding, $bindings);

    }

    /**
     * Prepare a SDO DAS Statement
     * @param \core\Language\Query $query The Query object
     * 
     * @return object The prepared statement
     */
    public function prepare($query) 
    {
        if (is_string($query)) {
            $this->setStatementClass();

            return parent::prepare($query); 
        } 

        $queryString = $this->driver->getQueryString($query);

        if (!$queryString) {
            throw new Exception("Error when parsing the query");
        }

        $this->setStatementClass('\dependency\sdo\Adapter\Database\Statement', array($query, $this->driver));

        $stmt = parent::prepare($queryString);

        return $stmt;
    }

    /**
     * Query a SDO DAS Statement
     * @param \core\Language\Query $query The Query object
     * 
     * @return object The executed statement result set
     */
    public function query($query) 
    {
        if (is_string($query)) {
            $this->setStatementClass();

            return parent::query($query);
        } 
        
        $queryString = $this->driver->getQueryString($query);

        if (!$queryString) {
            throw new Exception("Error when parsing the query");
        }

        $this->setStatementClass('\dependency\sdo\Adapter\Database\Statement', array($query, $this->driver));

        // LAABS Variables found will be evaluated and replaced as STRING values
        foreach ($this->driver->getVariables() as $param => $value) {
            $queryString = str_replace($param, $this->parseString($value), $queryString);
        }

        $stmt = parent::query($queryString);

        return $stmt;
    }

    /* Schema */
    public function hasSchema($schemaName)
    {
        return $this->driver->hasSchema($schemaName);
    }
    
    public function getSchema($schemaName)
    {
        return $this->driver->getSchema($schemaName);
    }

    public function getSchemas()
    {
        return $this->driver->getSchemas();
    }

    public function getClass($qualifiedClassName)
    {
        $qualifiedClassName = $this->driver->getBinding($qualifiedClassName);
        $schemaName = strtok($qualifiedClassName, ".");
        $className = strtok(".");

        $schema = $this->getSchema($schemaName);

        return $schema->getClass($className);
    }

    public function hasClass($qualifiedClassName)
    {
        $qualifiedClassName = $this->driver->getBinding($qualifiedClassName);
        $schemaName = strtok($qualifiedClassName, ".");
        $className = strtok(".");

        if (!$this->hasSchema($schemaName)) {
            return false;
        }
        
        $schema = $this->getSchema($schemaName);
        
        return $schema->hasClass($className);
    }
    
}
