<?php

namespace dependency\sdo\Adapter\Database;
/**
 * Class for Sdo Database adapter Statements
 *
 * @package Dependency_Sdo
 */
class Statement
    extends \dependency\datasource\Adapter\Database\Statement
    implements \dependency\sdo\StatementInterface
{
    /* Constants */

    /* Properties */
    protected $query;

    protected $driver;

    protected $params = array();

    /* Methods */
    /**
     * Construct a new Sdo Database Statement
     * @param \PDOStatement                                    $pdoStatement The PDO statement object
     * @param \core\Language\Query                             $query        The Query object used to prepare the PDO Statement
     * @param \dependency\sdo\Adapter\Database\DriverInterface $driver       The Sdo dataaccess service
     *
     * @return void
     */

    public function __construct(\PDOStatement $pdoStatement, \core\Language\Query $query, \dependency\sdo\Adapter\Database\DriverInterface $driver) 
    {
        parent::__construct($pdoStatement);

        $this->query = $query;

        $this->driver = $driver;
    }

    /**
     * Bind a Sdo Data Object to the statement
     * @param string $ns      The namespace of object (class or constraint name)
     * @param object &$object The data object
     * @param object $type    The type (class, constraint key /unique / fkey)
     * @param array  $options An associative array of driver-specific options 
     * 
     * @return bool
     */
    public function bindObject($ns, &$object, $type=null, array $options=null)
    {
        //var_dump("bind object $ns");
        //var_dump($type);
        //var_dump($object);
        switch (true) {
            case $type instanceof \core\Reflection\Type :
                return $this->bindTypeProperties($ns, $object, $type->getProperties());

            case is_null($type) :
                return $this->bindObjectProperties($ns, $object);

            default:
                throw new \Exception("Invalid object type for binding: you must provide a Sdo class, key, foreign key");
        }       
    }

    /**
     * Bind a Sdo Data Object key to the statement
     * @param string $ns      The namespace of object (class or constraint name)
     * @param object &$object The data object
     * @param object $key     The key (constraint key /unique)
     * @param array  $options An associative array of driver-specific options 
     * 
     * @return bool
     */
    public function bindKey($ns, &$object, \core\Reflection\Key $key=null, array $options=null)
    {
        //var_dump("bind object $ns");
        //var_dump($type);
        //var_dump($object);
        $class = \laabs::getClass($key->getClass());
        $properties = array();
        foreach ($key->getFields() as $field) {
            $properties[] = $class->getProperty($field);
        }

        return $this->bindTypeProperties($ns, $object, $properties);   
    }

    /**
     * Bind a Sdo Data Object foreign key to the statement
     * @param string $ns         The namespace of object (class or constraint name)
     * @param object &$object    The data object
     * @param object $foreignKey The key (constraint key /unique)
     * @param array  $options    An associative array of driver-specific options 
     * 
     * @return bool
     */
    public function bindForeignKey($ns, &$object, \core\Reflection\ForeignKey $foreignKey, array $options=null)
    {
        //var_dump("bind object $ns");
        //var_dump($type);
        //var_dump($object);
        $refClass = \laabs::getClass($foreignKey->getRefClass());
        $properties = array();
        foreach ($foreignKey->getRefFields() as $refField) {
            $properties[] = $refClass->getProperty($refField);
        }

        return $this->bindTypeProperties($ns, $object, $properties);  
    }

    /* ResultSet methods */
    public function fetch($modelName=false, array $ctor_args=null)
    {
        $object = parent::fetch();
        
        if (!$object) {
            return null;
        }
        
        if ($modelName) {
            return \laabs::castObject($object, $modelName, $ctor_args);
        } else {
            return $object;
        }

        
    }
    
    public function fetchAll($modelName=false, array $ctor_args=null)
    {
        $objects = parent::fetchAll();

        if (empty($objects)) {
            return $objects;
        }

        if ($modelName) {
            return \laabs::castCollection($objects, $modelName);
        } else {
            return $objects;
        }
        
        
    }
    
    public function fetchItem($cursor_offset=0, $modelName=false, array $ctor_args=null)
    {
        $object = parent::fetchItem($cursor_offset);

        if (!$object) {
            return null;
        }

        if ($modelName) {
            return \laabs::castObject($object, $modelName, $ctor_args);
        } else {
            return $object;
        }

        
    }
    
    public function fetchColumn($offset=0, $type=false)
    {
        $value = parent::fetchColumn($offset);
        
        if ($type) {
            return \laabs::cast($value, $type);
        }

        return $value;
    }

    protected function bindTypeProperties($ns, &$object, array $properties)
    {
        foreach ($properties as $property) {
            $propertyName = $property->getName();

            // Only bind existing properties (partial objects)
            if (!property_exists($object, $propertyName) 
                || !$property->isPublic()
                || $property->isComplex()
                || $property->isStatic()
            ) {
                continue;
            }

            $propertyType = $property->getType();           
            
            $bindName = $ns . LAABS_URI_SEPARATOR . $propertyName;

            //var_dump("bind property $bindName");
            //var_dump($propertyType);
            //var_dump($object->$propertyName);
            
            $this->bindProperty($bindName, $object->$propertyName, $propertyType);
        }

        return true;
    }

    protected function bindObjectProperties($ns, &$object) 
    {
        foreach ($object as $name => &$value) {
            $bindName = $ns . LAABS_URI_SEPARATOR . $name;
            $type = gettype($value);
            $this->bindProperty($bindName, $value, $type);
        }

        return true;
    } 

    protected function bindProperty($ns, &$propertyValue, $propertyType=null)
    {
        $bindName = "sdo_" . \laabs\md5($ns, false, true);
        $bindType = null;
        $bindLength = null;

        if (is_null($propertyValue)) {
            if ($propertyType == 'resource') {
                return $this->bindParam($bindName, $propertyValue, \PDO::PARAM_LOB);
            }

            return $this->bindParam($bindName, $propertyValue, \PDO::PARAM_NULL);
        }

        $bindType = $this->driver->getBindType($propertyType);

        return $this->bindParam($bindName, $propertyValue, $bindType, 0, array(), $ns);
    }

    public function dump()
    {
        $dump = "SQL: [ " . $this->getQueryString() . " ]" . "\n"
                . "PARAMS: " . count($this->params) . "\n";
            
        if (count($this->params) > 0) {
            foreach ($this->params as $pos => $param) {
                if (!$param) {
                    $dump .= "Param " . $pos . "\n";
                    continue;
                }
                $value = $param->getValue();
                if (is_scalar($value)) {
                    if (strlen($value) > 128) {
                        $dmpvalue = substr($value, 0, 125) . "...";
                    } else {
                        $dmpvalue = $value;
                    }
                    $dmpvalue = "'" . $dmpvalue . "'";
                } else {
                    $dmpvalue = \laabs\gettype($value);
                }

                switch($type = $param->getType()) {
                    case "0" : 
                        $type = "NULL";
                        break;

                    case "1" : 
                        $type = "NUMBER";
                        break;

                    case "2" : 
                        $type = "STRING";
                        break;

                    case "3" : 
                        $type = "LOB";
                        break;

                    case "5" : 
                        $type = "BOOL";
                        break;
                }

                $dump .= "Param " . $pos . "\n" 
                    . " Name: " . $param->getName() . "\n" 
                    . " Value: " . $dmpvalue . "\n"
                    . " Type: ". $type . "\n"
                    . " length: " . $param->getLength() . "\n";
            }
        }

        return $dump;
    }

}