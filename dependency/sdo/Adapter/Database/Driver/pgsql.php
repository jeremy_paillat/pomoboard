<?php
namespace dependency\sdo\Adapter\Database\Driver;

class pgsql
    extends AbstractDriver
{
    /* Constants */

    /* Properties */
    public static $name = "pgsql";

    public function setDateFormat($dateFormat) 
    {
        if (!$dateFormat) {
            $dateFormat = "dd-mm-yyyy";
        }

        $this->dateFormat = $dateFormat;
    }   

    public function setTimeFormat($timeFormat) 
    {
        if (!$timeFormat) {
            $timeFormat = "hh24:mi:ss";
        }

        $this->timeFormat = $timeFormat;
    }

    public function setDatetimeFormat($datetimeFormat) 
    {
        if (!$datetimeFormat) {
            $datetimeFormat = "YMD";
        }

        $this->datetimeFormat = $datetimeFormat;

        //$this->ds->exec("SET datestyle TO ".$datetimeFormat);
    }

    /**
     * Set the encoding for texts
     * @param string $encoding The sql specific encoding
     */
    public function setEncoding($encoding) 
    {
        $this->ds->exec("SET client_encoding TO '".$encoding."'");
    }


}