<?php
namespace dependency\sdo\Adapter\Database\Driver;
/**
 * Abstract class for database drivers
 */
abstract class AbstractDriver
    extends AbstractCompiler
    implements \dependency\sdo\Adapter\Database\DriverInterface
{
    /* Constants */

    /* Properties */
    public static $name;

    protected $ds;

    protected $schema;

    protected $bindings;

    protected $encoding;

    protected $dateFormat;

    protected $timeFormat;

    protected $datetimeFormat;

    protected $boolFormat;

    /**
     * Constructor
     * @param object $ds             The Sdo datasource, Model or Das
     * @param string $dateFormat     The date format for to_char and to_date convertions 
     * @param string $timeFormat     The time format for to_char and to_time convertions 
     * @param string $datetimeFormat The datetime format for to_char and to_date convertions 
     * @param int    $boolFormat     The bool format for php bool to db representation
     * @param string $encoding       The encoding
     * @param array  $bindings       An array of bindings between bundle/type/property and schema/table/column 
     * 
     * @return void 
     */
    public function __construct(
        \dependency\datasource\Adapter\Database\Datasource $ds, 
        $dateFormat=null, 
        $timeFormat=null,
        $datetimeFormat=null,
        $boolFormat=null, 
        $encoding="UTF8", 
        array $bindings=array())
    {
        $this->ds = $ds;

        $this->boolFormat = $boolFormat;

        $this->bindings = $bindings;

        $this->setDateFormat($dateFormat);
        $this->setTimeFormat($timeFormat);
        $this->setDatetimeFormat($datetimeFormat);
        
        /*$this->encoding = $encoding;
        if ($encoding) {
            $this->setEncoding();
        }*/

    }

    /**
     * Magic method to get the name of the driver
     * 
     * @return string The name of the driver
     */
    public function __toString()
    {
        return self::$name;
    }

    public function setBoolFormat($boolFormat=1) 
    {
        $this->boolFormat = $boolFormat;
    }

    /**
     * Get the date format
     * 
     * @return string
     */
    public function getDateFormat()
    {
        return $this->dateFormat;
    }


    /**
     * Get type bundle/type/property => schema/table/column 
     * @param string $name
     * 
     * @return string The binded type
     */
    public function getBinding($name)
    {
        if (isset($this->bindings[$name])) {           
            return $this->bindings[$name];
        }

        $parts = \laabs\explode(LAABS_URI_SEPARATOR, $name);
        $bind = array();
        switch(count($parts)) {
            case 3:
                $bind[] = array_pop($parts);
                $qTypeName = \laabs\implode(LAABS_URI_SEPARATOR, $parts);
                if (isset($this->bindings[$qTypeName])) {           
                    array_unshift($bind, $this->bindings[$qTypeName]);
                    break;
                }
                // If not found, continue to 2

            case 2:
                array_unshift($bind, array_pop($parts));
                $bundleName = reset($parts);
                if (isset($this->bindings[$bundleName])) {           
                    array_unshift($bind, $this->bindings[$bundleName]);
                    break;
                }

            case 1:
                array_unshift($bind, reset($parts));
        }
        
        return implode(".", $bind);
    }

    public function getBindType($propertyType)
    {
        switch (\laabs::getPhpType($propertyType)) {
            /*
                0 = NULL,
                1 = INT,
                2 = STR
                3 = LOB
                4 = STMT
                5 = BOOL
            */
            case 'null':
                return \PDO::PARAM_NULL;

            case 'resource':
                return \PDO::PARAM_LOB;

            case 'boolean':
                switch ($this->boolFormat) {
                    case 1 : 
                        return \PDO::PARAM_BOOL;

                    case 2 : 
                        return \PDO::PARAM_STR;

                    case 0 : 
                    default:
                        return \PDO::PARAM_INT;
                }

            case 'float':
            case 'integer':
                return \PDO::PARAM_INT;

            case 'string':
            default:
                return \PDO::PARAM_STR;
        }
    }

}