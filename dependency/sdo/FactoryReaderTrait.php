<?php
/**
 * File for Sdo Factory trait Read methods
 * @package dependency\sdo
 */
namespace dependency\sdo;
/**
 * Trait FactoryReader
 * The API for Service Data Object read queries
 */
trait FactoryReaderTrait
{
    
    /**
     * Find a set of objects matching the filter expression, ordered
     * @param string  $className     The class of the objects to find
     * @param string  $queryString   The query (search) expression, encoded in Laabs Query Language
     * @param string  $sortingString The sorting (order) expression, encoded in Laabs Query Language
     * @param integer $offset        The start offset
     * @param integer $length        The max number of results to show
     * @param bool    $lock          Lock rows for update
     *
     * @return array An array of objects matching the query and ordered as requested
     */
    public function find($className, $queryString=false, $sortingString=false, $offset=0, $length=null, $lock=false)
    {
        $lqlString = 'READ';
                
        $lqlString .= ' ' . $className;

        if ($queryString) {
            $lqlString .= "(" . $queryString .")";
        } 

        if ($sortingString) {
            $lqlString .= ' SORT '. $sortingString;
        } else {
            $class = \laabs::getClass($className);
            if ($class->hasPrimaryKey()) {
                $key = $class->getPrimaryKey();
            } elseif ($class->hasKey()) {
                $keys = $class->getKeys();
                $key = reset($keys);
            }

            if ($key) {
                $sortingString = \implode(', ', $key->getFields());
                $lqlString .= ' SORT '. $sortingString;
            }
        }

        if ($offset) {
            $lqlString .= ' OFFSET ' . (int) $offset;
        }

        if ($length) {
            $lqlString .= ' LIMIT ' . (int) $length;
        }

        if ($lock) {
            $lqlString .= 'LOCK';
        }

        $parser = new \core\Language\Parser();
        $query = $parser->parseQuery($lqlString);

        /*
        $query = new \core\Language\Query();
        $query->setCode(LAABS_T_READ);

        $class = \laabs::getClass($className);
        $query->setClass($class);

        $parser = new \core\Language\Parser();
        if ($queryString) {
            $assert = $parser->parseAssert($queryString, $query);
            $query->addAssert($assert);
        }

        if ($sortingString) {
            $sortings = $parser->parseSortingList($sortingString, $query);
            $query->setSortings($sortings);
        } else {
            if ($class->hasPrimaryKey()) {
                $key = $class->getPrimaryKey();
            } elseif ($class->hasKey()) {
                $keys = $class->getKeys();
                $key = reset($keys);
            }

            if ($key) {
                $sortingString = \implode(', ', $key->getFields());
                $sortings = $parser->parseSortingList($sortingString, $query);
                $query->setSortings($sortings);
            }
        }

        if ($length) {
            $query->setLength($length);
            if ($offset) {
                $query->setOffset($offset);
            }
        }

        if ($lock) {
            $query->lock(true);
        }*/

        /* Prepare statement */
        $stmt = $this->das->prepare($query);
        //var_dump($stmt);
        /* Execute statement */
        $result = $this->execute($stmt);
        
        /* Fetch all objects */
        if ($result) {
            $array = $stmt->fetchAll($className);
        }

        return $array;
    }
    
    /**
     * Checks if an object exists in storage
     * @param string $className The class of the objects to find
     * @param mixed  $keyValue  A scalar, associative array, indexed array or object representing the univoque key to use for object retrieval
     *  Passing an associative array key value will allow to guess which key should be used, else the primary key will be used if exists
     * 
     * @return bool The object exists or not
     */
    public function exists($className, $keyValue)
    {
        /* Get Sdo Query 'Read' */
        $query = new \core\Language\Query();
        $query->setCode(LAABS_T_READ);

        $class = \laabs::getClass($className);
        $query->setClass($class);
        
        $query->addProperty(true);
        
        $key = $this->getKey($class, $keyValue);
        if (!$key) {
            throw new \Exception("No key found for existence check on class $className");
        }
        $keyAssert = $key->getAssert();
        $query->addAssert($keyAssert);
        
        /* Prepare statement */
        $stmt = $this->das->prepare($query);

        /* Bind value of key to key parameter */
        $keyObject = $key->getObject($keyValue);
        $stmt->bindKey($key->getClass(), $keyObject, $key);

        $result = $this->execute($stmt);
        
        if ($result) {
            $exists = $stmt->fetchColumn();

            return (bool) $exists;
        } 
    }
    
    /**
     * Read an object from storage
     * @param string $className The class of the objects to find
     * @param mixed  $keyValue  A scalar, associative array, indexed array or object representing the univoque key to use for object retrieval
     * @param bool   $lock      Lock objects in datasource
     * 
     * @return object The requested object
     */
    public function read($className, $keyValue, $lock=false)
    {
        $class = \laabs::getClass($className);
        $key = $this->getKey($class, $keyValue);

        if (!isset($GLOBALS["READ ". $className . " " . $key->getName()])) {
            /* Get Sdo Query 'Read' */
            $query = new \core\Language\Query();
            $query->setCode(LAABS_T_READ);

            $class = \laabs::getClass($className);
            $query->setClass($class);

            $key = $this->getKey($class, $keyValue);
            if (!$key) {
                throw new Exception("No key found to read class $className");
            }
            $keyAssert = $key->getAssert();

            $query->addAssert($keyAssert);

            if ($lock) {
                $query->lock(true);
            }
            
            /* Prepare statement */
            $stmt = $this->das->prepare($query);

            $GLOBALS["READ ". $className . " " . $key->getName()] = $stmt;
        } else {
            $stmt = $GLOBALS["READ ". $className . " " . $key->getName()];  
        }

        /* Bind value of key to key parameter */
        $keyObject = $key->getObject($keyValue);

        $stmt->bindKey($className, $keyObject, $key);

        $result = $this->execute($stmt);
        
        if ($result) {
            $object = $stmt->fetch($className);

            if ($object) {
                return $object;
            }
        } 

        if (is_object($keyValue)) {
            $keyValue = implode(LAABS_URI_SEPARATOR, get_object_vars($keyValue));
        }
        if (is_array($keyValue)) {
            $keyValue = implode(LAABS_URI_SEPARATOR, $keyValue);
        }
        
        throw new Exception\objectNotFoundException("Object of class $className identified by $keyValue was not found");
    }

    /**
     * Find distinct values
     * @param string  $className    The class of the objects to find
     * @param string  $propertyName The name of the property to find distinct values
     * @param string  $queryString  The query (search) expression, encoded in Laabs Query Language
     * @param integer $offset       The start offset
     * @param integer $length       The max number of results to show
     *
     * @return array An array of values matching the query and ordered as requested
     */
    public function summarise($className, $propertyName, $queryString=false, $offset=0, $length=null)
    {
        $lqlString = 'READ UNIQUE';
                
        $lqlString .= ' ' . $className;

        $lqlString .= ' [' . $propertyName . "]";

        if ($queryString) {
            $lqlString .= "(" . $queryString .")";
        } 

        $lqlString .= ' SORT '. $propertyName;

        if ($offset) {
            $lqlString .= ' OFFSET ' . (int) $offset;
        }

        if ($length) {
            $lqlString .= ' LIMIT ' . (int) $length;
        }

        $parser = new \core\Language\Parser();
        $query = $parser->parseQuery($lqlString);

        /* Prepare statement */
        $stmt = $this->das->prepare($query);

        /* Execute statement */
        $result = $this->execute($stmt);
        
        /* Fetch all objects */
        if ($result) {
            $array = $stmt->fetchAll();

            foreach ($array as $i => $object) {
                $array[$i] = $object->{$propertyName};
            }
        }

        return $array;
    }

    /**
     * Read the children objects of a given parent using a foreign key navigation
     * @param string $childClassName  The class of the objects to find
     * @param mixed  $parentObject    The parent object to read children of
     * @param string $parentClassName The class of the parent object provided, if different from object class
     * 
     * @return array An array of objects of requested class related to parent object
     */
    public function readChildren($childClassName, $parentObject, $parentClassName=false)
    {
        /* Get Sdo Query 'Read' */
        $query = new \core\Language\Query();
        $query->setCode(LAABS_T_READ);

        $childClass = \laabs::getClass($childClassName);
        $query->setClass($childClass);

        if (!$parentClassName) {
            $parentClassName = \laabs::getClassName($parentObject);
        }

        $foreignKey = $this->getChildKey($childClass, $parentObject, $parentClassName);

        if (!$foreignKey) {
            throw new \Exception("No foreign key found to read child class $childClassName of $parentClassName");
        }
        $childKeyAssert = $foreignKey->getChildAssert();
        $query->addAssert($childKeyAssert);
        
        /* Prepare statement */
        $stmt = $this->das->prepare($query);
        
        /* Bind value of fkey to fkey parameter */
        $refKeyObject = $foreignKey->getParentObject($parentObject);

        $stmt->bindForeignKey($foreignKey->getRefClass(), $refKeyObject, $foreignKey);

        $result = $this->execute($stmt);

        if ($result) {
            return $stmt->fetchAll($childClassName);
        }
    }
    
    /**
     * Read the parent object of a given child using a foreign key navigation
     * @param string $parentClassName The class of the parent object to read
     * @param mixed  $childObject     An associative array, object, indexed array or scalar value representing the child
     * @param string $childClassName  The class of the child object
     * 
     * @return object The object of requested class related to child object
     */
    public function readParent($parentClassName, $childObject, $childClassName=false)
    {
        /* Get Sdo Query 'Read' */
        $query = new \core\Language\Query();
        $query->setCode(LAABS_T_READ);

        $parentClass = \laabs::getClass($parentClassName);
        $query->setClass($parentClass);
        
        if ($childClassName) {
            $childClass = \laabs::getClass($childClassName);
        } else {
            if (is_object($childObject)) {
                $childClass = \laabs::getClass($childObject);
            } 
            if (!$childClass) {
                throw new \Exception("Can't delete parent: You must provide either a child object of required class or a child class name.");
            }
        }

        $foreignKey = $this->getParentKey($childClass, $childObject, $parentClassName);
        if (!$foreignKey) {
            throw new \Exception("No foreign key found to read parent class $parentClassName of $childClassName");
        }
        $parentKeyAssert = $foreignKey->getParentAssert();
        $query->addAssert($parentKeyAssert);
        
        /* Prepare statement */
        $stmt = $this->das->prepare($query);

        /* Bind value of fkey to fkey parameter */
        $childKeyObject = $foreignKey->getChildObject($childObject);
        $stmt->bindKey($foreignKey->getClass(), $childKeyObject, $foreignKey);

        $result = $this->execute($stmt);
        
        if ($result) {
            return $stmt->fetch();
        }
    }
    
    /**
     * Read a tree of a single class. Roots are objects without parents
     * @param string $className   The class of the object to read
     * @param string $queryString An optional filter on tree items
     * 
     * @return array The array of root objects with their branches
     */
    public function readTree($className, $queryString=false)
    {
        /* Get Sdo Query 'Read' */
        $query = new \core\Language\Query();
        $query->setCode(LAABS_T_READ);

        $class = \laabs::getClass($className);
        $query->setClass($class);
        
        $selfKeys = $class->getForeignKeys($class->getName());
        if (count($selfKeys) == 0) {
            return array();
        }
        $selfKey = reset($selfKeys);
        foreach ($selfKey->getFields() as $keyField) {
            $assert = new \core\Language\ComparisonOperation(LAABS_T_EQUAL, $class->getProperty($keyField), new \core\Language\ConstantOperand(LAABS_T_NULL, null));
            //$assert = new \core\Language\Func(LAABS_T_IS_NULL, $class->getProperty($keyField));
            $query->addAssert($assert);
        }

        /* Add Asserts to statement */
        if ($queryString) {
            $parser = new \core\Language\Parser();
            $assert = $parser->parseAssert($queryString, $query);
            $query->addAssert($assert);
        }

        /* Prepare statement */
        $stmt = $this->das->prepare($query);

        /* Execute statement */
        $result = $this->execute($stmt);
        
        /* Fetch all objects */
        if (!$result) {
            return array();
        }
            
        $roots = $stmt->fetchAll();

        $query = new \core\Language\Query();
        $query->setCode(LAABS_T_READ);
        
        $query->setClass($class);
        
        $selfKeyAssert = $selfKey->getChildAssert();
        $query->addAssert($selfKeyAssert);
        
        /* Prepare statement */
        $stmt = $this->das->prepare($query);

        foreach ($roots as $root) {
            $this->readBranches($class, $selfKey, $root, $stmt);
        }
            
        return $roots;
    }
    
    /**
     * Read branches of a given tree root
     * @param object $class        The class definition
     * @param object $selfKey      The key that references the objects on tree
     * @param object $parentObject The root or branch to read children of
     * @param object $stmt         The statement to select objects
     * @param int    $depth        The depth of tree
     *
     * @return void
     */
    protected function readBranches($class, $selfKey, $parentObject, $stmt, $depth=0)
    {
        /* Bind fkey */
        $refKeyObject = $selfKey->getParentObject($parentObject);

        $stmt->bindForeignKey($selfKey->getRefClass(), $refKeyObject, $selfKey);

        /* Execute statement */
        $result = $this->execute($stmt);

        /* Fetch all objects */
        if (!$result) {
            return;
        }
        
        $branchName = \laabs\basename($class->getName());
        $branchObjects = $stmt->fetchAll();
        $parentObject->$branchName = $branchObjects;

        $depth++;
        foreach ($branchObjects as $branchObject) {
            $this->readBranches($class, $selfKey, $branchObject, $stmt, $depth);
        }
    }

    /**
     * Get the ancestor objects on an homogenic tree
     * @param string $className The class of the object 
     * @param string $object    The reference object
     *
     * @return array 
     */
    public function readAncestors($className, $object)
    {
        $ancestors = array();
        while ($object = $this->readParent($className, $className, $object)) {
            $ancestors[] = $object;
        }

        return $ancestors;
    }

    /**
     * Get the descendant objects in an homogenic hierechical tree
     * @param string $className The class of the object 
     * @param string $object    The reference object
     *
     * @return array The descendant objects
     */
    public function readDescendants($className, $object)
    {
        $class = \laabs::getClass($className);
                
        $selfKeys = $class->getForeignKeys($class->getName());
        if (count($selfKeys) == 0) {
            return;
        }
        $selfKey = reset($selfKeys);

        $query = new \core\Language\Query();
        $query->setCode(LAABS_T_READ);
        $query->setClass($class);
        
        $selfKeyAssert = $selfKey->getChildAssert();
        $query->addAssert($selfKeyAssert);
        
        /* Prepare statement */
        $stmt = $this->das->prepare($query);
        
        $descendants = $this->recursiveReadDescendants($class, $selfKey, $object, $stmt);

        return $descendants;
    }

    /**
     * Recursive getter for children
     * @param object $class   The class definition
     * @param object $selfKey The key that references the objects on tree
     * @param object $self    The current object
     * @param object $stmt    The statement to select objects
     *
     * @return array
     * @author 
     */
    protected function recursiveReadDescendants($class, $selfKey, $self, $stmt)
    {
        /* Bind value of fkey to fkey parameter */
        $selfKeyObject = $selfKey->getParentObject($self);
        $stmt->bindForeignKey($selfKey->getRefClass(), $selfKeyObject, $selfKey);

        $result = $this->execute($stmt);

        if (!$result) {
            return array();
        }

        $descendants = $stmt->fetchAll();

        foreach ($descendants as $descendant) {
            $descendants = array_merge($descendants, $this->recursiveReadDescendants($class, $selfKey, $descendant, $stmt));
        }

        return $descendants;
    }
}
