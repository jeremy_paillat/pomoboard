<?php
namespace dependency\sdo;

interface StatementInterface
    extends \dependency\datasource\StatementInterface
{

    public function bindObject($ns, &$object, $type, array $options=null);

}