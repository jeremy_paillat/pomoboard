<?php
namespace dependency\sdo\Exception;
/**
 * Exception thrown when a read operation returns no object
 * 
 * @package Dependency/sdo
 */
class objectNotFoundException
    extends \core\Exception
{

}