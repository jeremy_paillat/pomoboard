<?php

namespace dependency\sdo\Exception;

class objectDuplicateException
    extends \Exception
{

    public function __construct($message="Object already exists", $code=0, \Exception $previous=null)
    {
        parent::__construct($message, $code, $previous);
    }

}