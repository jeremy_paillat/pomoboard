<?php

namespace dependency\sdo\Exception;

class classNotFoundException
    extends \Exception
{

    public function __construct($message="Class not found", $code=0, \Exception $previous=null)
    {
        parent::__construct($message, $code, $previous);
    }

}