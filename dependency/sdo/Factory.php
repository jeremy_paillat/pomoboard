<?php
namespace dependency\sdo;
/**
 * Factory for Service Data objects
 * Provides basic and advanced methods to create and execute parameterized Queries
 */
class Factory
{
    use \core\ReadonlyTrait,
        FactoryReaderTrait,
        FactoryWriterTrait;
                
    /* Properties */
    /**
     * The Data Access Service object
     * @var object 
     */
    public $das;
            
    /* Methods */
    /**
     * Deploy the trait on the service that uses the trait
     * @param object $das The dependency sdo Das to use
     */
    public function __construct(\dependency\sdo\DasInterface $das) 
    {
        $this->das = $das;
    }
        
    /**
     * Prepare a Laabs Query Language operation
     * @param string $queryString The Laabs Query Language query
     *
     * @return stmt The statement
     */
    public function prepare($queryString)
    {
        $query = \core\Language\Query::parse($queryString);
        
        //var_dump($query);
        
        /* Prepare statement */
        $stmt = $this->das->prepare($query);
                
        return $stmt;
    }
    
    /**
     * Execute a Laabs Query Language operation
     * @param string $queryString The Laabs Query Language query
     *
     * @return mixed The result
     */
    public function query($queryString)
    {
        $query = \core\Language\Query::parse($queryString);
        
        //var_dump($query);
        
        /* Prepare statement */
        $stmt = $this->das->prepare($query);
        
        foreach ($query->getParams() as $param) {
            $stmt->bindObject($param->getName(), $param->getValue(), $param->getType());
        }
        
        //var_dump($stmt->getQueryString());
        //var_dump($stmt->getParams());
        
        /* Execute statement */
        $result = $this->execute($stmt);
        
        return $stmt->fetchAll();
    }

    /**
     * Execute a statement with auto-transaction
     * @param object $stmt The statement
     * @param array  $args The params for exec
     * 
     * @return mixed $result
     */
    public function execute($stmt, array $args=null)
    {       
        $transactional = false;
        try {

            if (!$this->inTransaction()) {
                $transactional = true;
                $this->beginTransaction();
            }

            $result = @$stmt->execute($args);
            
        } catch (\Exception $exception) {
            if ($transactional) {
                $this->rollback();
            }


            if ($error = $stmt->getError()) {
                $sqlMessage = $error->getCode() . ': ' . $error->getMessage();
            } else {
                $sqlMessage = false;
            }
            $execException = new \Exception(
                "An error occured during the execution of the data access statement. " . $sqlMessage . "\n" . $stmt->dump(), 
                null, 
                $exception);

            throw $execException;
        }

        if ($transactional) {
            $this->commit();
        }

        return $result;
    } 
       
    /**
     * Get the last error associated with the current Statement object
     * @return object A laabs Error object
     */
    public function getError()
    {
        if (isset($stmt)) {
            return $stmt->getError();
        } else {
            return new \core\Error('Statement empty');
        }
    }
    
    /**
     * Get the last query string associated with the current Statement object
     * @return string
     */
     
    public function getQueryString()
    {
        return $stmt->getQueryString();
    }
    
    /**
     * Begins a new transaction with Data Access Service
     * 
     * @return bool
     */
    public function beginTransaction()
    {
        return $this->das->beginTransaction();
    }
    
    /**
     * Checks if in transaction with Data Access Service
     * @return bool
     */
    public function inTransaction()
    {
        return $this->das->inTransaction();
    }
    
    /**
     * Commit current transaction with Data Access Service
     * @return bool
     */
    public function commit()
    {
        return $this->das->commit();
    }
    
    /**
     * Rollback current transaction with Data Access Service
     *
     * @return bool
     */
    public function rollback()
    {        
        return $this->das->rollback();
    }
        
    /* ********************************************************************************************
    **
    **      Properties and fields
    ** 
    ******************************************************************************************** */

    /**
     * Generate a key for a new object
     * @param object $class  The class definition
     * @param object $object The data object
     * 
     * @return bool
     * @access protected
     */
    protected function generateKey($class, $object)
    {
        $key = $class->getPrimaryKey();
        if (!$key) {
            return;
        }

        foreach ($key->getFields() as $keyField) {
            if (empty($object->$keyField)) {
                $object->$keyField = \laabs\uniqid();
            }
        }
    }  

    /* ********************************************************************************************
    **
    **      Key navigation
    ** 
    ******************************************************************************************** */

    /**
     * Get the first usable key for a given class, based on the available values versus key fields.
     *  Behaviour depends on the type of key value :
     *      - associative array will use array keys
     *      - object will use properties
     *      - scalar value or indexed array will return primary key if exists
     * @param object $class    The class definition object
     * @param mixed  $keyValue The available values
     * 
     * @return object The first key definition (primary or unique) matching the values
     * @access protected
     */
    protected function getKey($class, $keyValue)
    {
        // Key value is an associative array
        if (is_array($keyValue) && \laabs\is_assoc($keyValue)) {
            $bindedKeyFields = array_keys($keyValue);
        } elseif (is_object($keyValue)) {
            $bindedKeyFields = array_keys(get_object_vars($keyValue));
        } else {
            $bindedKeyFields = false;
        }

        if ($bindedKeyFields) {
            sort($bindedKeyFields);
            $keys = $class->getKeys();
            foreach ($keys as $key) {
                $keyFields = $key->getFields();
                sort($keyFields);
                $intersection = array_intersect($keyFields, $bindedKeyFields);
                if (count($intersection) == count($keyFields)) {
                    return $key;
                }
            }
        }
        
        return $class->getPrimaryKey();
    }
    
    /* ********************************************************************************************
    **
    **      Parent -> Child navigation
    ** 
    ******************************************************************************************** */

    /**
     * Get the first usable foreign key between given child and parent class, based on the available parent values versus ref key fields.
     * Allows a parent to child navigation
     *  Behaviour depends on the type of key value :
     *      - associative array will use array keys
     *      - object will use properties
     *      - scalar value or indexed array not allowed
     * @param object $childClass      The child class definition object
     * @param mixed  $parentValue     The available parent values
     * @param string $parentClassName The class of the parent object provided
     * 
     * @return object The first foreign key definition matching the values
     * @access protected
     */
    protected function getChildKey($childClass, $parentValue, $parentClassName=false)
    {

        // Key value is an associative array
        if (is_array($parentValue) && \laabs\is_assoc($parentValue)) {
            $bindedRefFields = array_keys($parentValue);
        } elseif (is_object($parentValue)) {
            $bindedRefFields = array_keys(get_object_vars($parentValue));
        } else {
            $bindedRefFields = false;
        }

        if ($bindedRefFields) {
            sort($bindedRefFields);
            $foreignKeys = $childClass->getForeignKeys($parentClassName);
            foreach ($foreignKeys as $foreignKey) {
                $refFields = $foreignKey->getRefFields();
                sort($refFields);
                $intersection = array_intersect($refFields, $bindedRefFields);
                if (count($intersection) == count($refFields)) {
                    return $foreignKey;
                }
            }
        }
    }

    /* ********************************************************************************************
    **
    **      Child -> Parent navigation
    ** 
    ******************************************************************************************** */
    /**
     * Get the first usable foreign key between given parent and child class, based on the available child values versus ref key fields.
     * Allows a child to parent navigation
     *  Behaviour depends on the type of key value :
     *      - associative array will use array keys
     *      - object will use properties
     *      - scalar value or indexed array not allowed
     * @param object $childClass      The child class definition object
     * @param mixed  $childValue      The available child values
     * @param object $parentClassName The parent class name
     * 
     * @return object The first foreign key definition matching the values
     * @access protected
     */
    protected function getParentKey($childClass, $childValue, $parentClassName)
    {
        // Key value is an associative array
        if (is_array($childValue) && \laabs\is_assoc($childValue)) {
            $bindedKeyFields = array_keys($childValue);
        } elseif (is_object($childValue)) {
            $bindedKeyFields = array_keys(get_object_vars($childValue));
        } else {
            $bindedKeyFields = false;
        }
        
        $foreignKeys = $childClass->getForeignKeys($parentClassName);
        if (count($foreignKeys) == 0) {
            return;
        }

        // Try to match binded key field names with foreign key references
        if ($bindedKeyFields) {
            sort($bindedKeyFields);
            foreach ($foreignKeys as $foreignKey) {
                $keyFields = $foreignKey->getFields();
                sort($keyFields);
                $intersection = array_intersect($keyFields, $bindedKeyFields);
                if (count($intersection) == count($keyFields)) {
                    return $foreignKey;
                }
            }
        } elseif ($parentClassName == $childClass->getSchema() . LAABS_URI_SEPARATOR . $childClass->getName()) {
            return reset($foreignKeys);
        }
    }

}