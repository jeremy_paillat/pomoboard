<?php
/**
 * File for Sdo Factory trait write queries
 * @package dependency\sdo
 */
namespace dependency\sdo;
/**
 * Trait FactoryWriter
 * The API for Service Data Object Write queries
 */
trait FactoryWriterTrait
{
    /**
     * Create a new Sdo DataObject on the data storage
     * @param object $object    The data object
     * @param string $className The class of the object to be created, if different from object class
     * 
     * @return bool True if object has been created, false if an error occured
     */
    public function create($object, $className=false)
    {
        if ($className) {
            $class = \laabs::getClass($className);
        } else {
            if (is_object($object)) {
                $class = \laabs::getClass($object);
            } 
            if (!$class) {
                throw new \Exception("Can't create object: You must provide either an object of required class or a class name.");
            }
        }

        $className = $class->getName();

        if (!isset($GLOBALS["CREATE " . $className])) {
            /* Get Sdo Query 'Read' */
            $query = new \core\Language\Query();
            $query->setCode(LAABS_T_CREATE);

            
            $query->setClass($class);

            $this->generateKey($class, $object);

            $createProperties = $class->getObjectProperties($object);

            $query->setProperties($createProperties);

            /* Prepare statement */
            $stmt = $this->das->prepare($query);

            $GLOBALS["CREATE " . $className] = $stmt;
        } else {
            $stmt = $GLOBALS["CREATE " . $className];
        }

        /* Bind object for data */
        $stmt->bindObject($className, $object, $class);
        //var_dump($stmt);
        return $this->execute($stmt);
    }
    
    /**
     * Create a batch of DataObject on the data storage
     * @param array  $objects   The array of object
     * @param string $className The class of the object to be created
     * 
     * @return bool True if objects has been created, false if an error occured
     */
    public function createCollection(array $objects, $className)
    {
        $manageTransaction = !$this->inTransaction();

        if ($manageTransaction) {
            $begun = $this->beginTransaction();
        }

        try {
            $query = new \core\Language\Query();
            $query->setCode(LAABS_T_CREATE);

            $class = \laabs::getClass($className);
            $query->setClass($class);

            foreach ($objects as $object) {

                $this->generateKey($class, $object);
                
                $createProperties = $class->getObjectProperties($object);

                $query->setProperties($createProperties);

                /* Prepare statement */
                $stmt = $this->das->prepare($query);

                /* Bind object for data */
                $stmt->bindObject($class->getName(), $object, $class);

                $result = $this->execute($stmt);
            }
            
        } catch (\Exception $exception) {
            if ($manageTransaction) {
                $rollbacked = $this->rollback();
            }
            throw $exception;
        }
        if ($manageTransaction) {
            $committed = $this->commit();
        }

        return true;
    }
    
    /**
     * Update an object from storage 
     * @param string $object    The data object holding data to update
     * @param string $className The class of the objects to update, if different from object class
     * @param mixed  $keyValue  A scalar, associative array, indexed array or object representing the univoque key to use for object retrieval
     *  Passing an associative array key value will allow to guess which key should be used, else the primary key will be used if exists
     * 
     * @return bool The success of failure of operation
     */
    public function update($object, $className=false, $keyValue=false)
    {
        if (!$keyValue) {
            $keyValue = $object;
        }

        $query = new \core\Language\Query();
        $query->setCode(LAABS_T_UPDATE);

        if ($className) {
            $class = \laabs::getClass($className);
        } else {
            if (is_object($object)) {
                $class = \laabs::getClass($object);
            } 
            if (!$class) {
                throw new \Exception("Can't update object: You must provide either an object of required class or a class name.");
            }
        }
        $query->setClass($class);
        
        if (!$keyValue) {
            $keyValue = $object;
        }

        $key = $this->getKey($class, $keyValue);
        if (!$key) {
            throw new \Exception("No key found to update class $className");
        }
        $keyAssert = $key->getAssert();
        $query->addAssert($keyAssert);
        
        $updateProperties = $class->getObjectProperties($object);

        $query->setProperties($updateProperties);
        
        /* Prepare statement */
        $stmt = $this->das->prepare($query);
     
        /* Bind value for data and key */
        $stmt->bindObject($class->getName(), $object, $class);
        
        $keyObject = $key->getObject($keyValue);
        $stmt->bindKey($key->getClass(), $keyObject, $key);

        return $this->execute($stmt);
    }
    
    /**
     * Delete an object from storage
     * @param mixed  $object    The data holding identifiers of what to delete
     * @param string $className The class of the objects to update, if different from object class
     * 
     * @return bool The success of failure of operation
     */
    public function delete($object, $className=false)
    {
        $query = new \core\Language\Query();
        $query->setCode(LAABS_T_DELETE);

        if ($className) {
            $class = \laabs::getClass($className);
        } else {
            if (is_object($object)) {
                $class = \laabs::getClass($object);
            }  
        }

        if (!isset($class)) {
            throw new \Exception("Can't delete object: You must provide either an object of required class or a class name.");
        }

        $query->setClass($class);
        
        $key = $this->getKey($class, $object);
        if (!$key) { 
            throw new \Exception("No key found to delete class $className");
        }
        $keyAssert = $key->getAssert();
        $query->addAssert($keyAssert);
        
        /* Prepare statement */
        $stmt = $this->das->prepare($query);
        
        /* Bind value of key to key parameter */
        $keyObject = $key->getObject($object);
        $stmt->bindKey($key->getClass(), $keyObject, $key);

        $result = $this->execute($stmt);
        
        return $result;
    }
    
    /**
     * Delete a batch of DataObject on the data storage
     * @param mixed  $collection The array of object/arrays standing for key values
     * @param string $className  The class of the object to be deleted
     * 
     * @return bool True if objects has been deleted, false if an error occured
     */
    public function deleteCollection(array $collection, $className)
    {        
        $manageTransaction = !$this->inTransaction();
        
        if ($manageTransaction) {
            $begun = $this->beginTransaction();
        }
        
        try {
            $query = new \core\Language\Query();
            $query->setCode(LAABS_T_DELETE);

            $class = \laabs::getClass($className);
            $query->setClass($class);
            
            foreach ($collection as $object) {
                if (!$this->exists($className, $object)) {
                    continue;
                }
                
                $key = $this->getKey($class, $object);
                if (!$key) {
                    throw new \Exception("No key found to delete class $className");
                }

                $keyAssert = $key->getAssert();
                $query->setAsserts(array($keyAssert));
                
                /* Prepare statement */
                $stmt = $this->das->prepare($query);
                
                $keyObject = $key->getObject($object);
                $stmt->bindKey($key->getClass(), $keyObject, $key);

                $result = $this->execute($stmt);
            }
            
        } catch (\Exception $exception) {
            if ($manageTransaction) {
                $rollbacked = $this->rollback();
            }
            throw $exception;
        }
        if ($manageTransaction) {
            $committed = $this->commit();
        }

        return true;
    }
    
    /**
     * Delete the children objects of a given parent using a foreign key navigation
     * @param string $childClassName  The class of the objects to delete
     * @param mixed  $parentObject    The parent object
     * @param string $parentClassName The class of the parent object provided
     *
     * @return int The number of children deleted
     */
    public function deleteChildren($childClassName, $parentObject, $parentClassName=false)
    {
        $childClass = \laabs::getClass($childClassName);
        
        /* Get Sdo Statement 'Read' */
        $query = new \core\Language\Query();
        $query->setCode(LAABS_T_DELETE);

        $query->setClass($childClass);

        if (!$parentClassName) {
            if (is_object($parentObject)) {
                $parentClassName = \laabs::getClassName($parentObject);
            } 
            if (!$parentClassName) {
                throw new \Exception("Can't delete child objects: You must provide either a parent object of required class or a parent class name.");
            }
        }
        
        $foreignKey = $this->getChildKey($childClass, $parentObject, $parentClassName);
        if (!$foreignKey) {
            throw new \Exception("No foreign key found to read child class $childClassName of $parentClassName");
        }

        $childKeyAssert = $foreignKey->getChildAssert();
        $query->addAssert($childKeyAssert);
        
        /* Prepare statement */
        $stmt = $this->das->prepare($query);
        
        /* Bind value of fkey to fkey parameter */
        $refKeyObject = $foreignKey->getParentObject($parentObject);
        $stmt->bindForeignKey($foreignKey->getRefClass(), $refKeyObject, $foreignKey);
        
        $result = $this->execute($stmt);
        
        if ($result) {
            return $stmt->rowCount();
        }
        
        return false;
    }
    
    /**
     * Delete the parent object of a given child using a foreign key navigation
     * @param string $parentClassName The class of the parent object to delete
     * @param mixed  $childObject     An associative array, object, indexed array or scalar value representing the child
     * @param string $childClassName  The class of the child object
     * 
     * @return bool
     */
    public function deleteParent($parentClassName, $childObject, $childClassName=false)
    {
        /* Get Sdo Model Statement 'ReadByKey' */
        $query = new \core\Language\Query();
        $query->setCode(LAABS_T_DELETE);

        $parentClass = \laabs::getClass($parentClassName);
        $query->setClass($parentClass);

        if ($childClassName) {
            $childClass = \laabs::getClass($childClassName);
        } else {
            if (is_object($childObject)) {
                $childClass = \laabs::getClass($childObject);
            } 
            if (!$childClass) {
                throw new \Exception("Can't delete parent: You must provide either a child object of required class or a child class name.");
            }
        }
        
        $foreignKey = $this->getParentKey($childClass, $childObject, $parentClassName);
        if (!$foreignKey) {
            throw new \Exception("No foreign key found to read parent class $parentClassName of $childClassName");
        }
        $parentKeyAssert = $foreignKey->getParentAssert();
        $query->addAssert($parentKeyAssert);
        
        /* Prepare statement */
        $stmt = $this->das->prepare($query);
        
        /* Bind value of fkey to fkey parameter */
        $keyObject = $foreignKey->getParentObject($childObject);
        $stmt->bindKey($foreignKey->getClass(), $keyObject, $foreignKey);
        
        $result = $this->execute($stmt);
        
        return $result;
    }

    /**
     * Save an entire hierarchical tree of objects
     * Root object may have been created or updated, child objects may have been added, updated or deleted
     * @param string $className The class of object to save
     * @param object $object    The object hodling the data
     * 
     * @return bool
     */
    public function save($className, $object)
    {
        if (!$this->schema->hasClass($className)) {
            return false;
        }
            
        $this->beginTransaction();
        
        if ($this->exists($className, $object)) {
            if (!$this->update($className, $object)) {
                $this->rollback();

                return false;
            }
        } else {
            if (!$this->create($className, $object)) {
                $this->rollback();

                return false;
            }
        }    

        if (!$this->saveRecursive($className, $object)) {
            $this->rollback();

            return false;
        }
        
        return $this->commit();
    }
    
    /**
     * Save a tree of objects
     * @param string $class  The class definitions of object to save
     * @param object $object The object hodling the data
     * 
     * @return bool
     */
    protected function saveRecursive($class, $object)
    {
        foreach ($object as $name => $value) {
            switch(true) {
            case is_scalar($value):
                break;
                
            case is_object($value):
                if (!$this->saveRecursive($name, $value)) {
                    $this->rollback();

                    return false;
                }
                break;
                
            case is_array($value):
                foreach ($value as $childObject) {
                    if (!$this->saveRecursive($name, $value)) {
                        $this->rollback();

                        return false;
                    }
                }
                break;
            }
        }

        return true;
    }
}
