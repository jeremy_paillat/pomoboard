<?php
namespace dependency\datasource;
interface StatementInterface
{
    public function bindParam($parameter, &$variable, $data_type, $length);
    public function bindValue($parameter, $value, $data_type);
    public function getError();
    public function getQueryString();
    public function execute($parameters);
    public function getParams();
}