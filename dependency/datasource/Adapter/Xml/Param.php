<?php

namespace dependency\datasource\Adapter\Xml;

class Param
    implements \dependency\datasource\ParamInterface
{

    /* Properties */
    protected $name;
    protected $value;
    protected $type;
    protected $length;

    /* Methods */
    public function __construct($name, &$value, $type=Xml::PARAM_STR, $length=null) {
        $this->name = $name;
        $this->value = &$value;
        $this->type = $type;
        $this->length = $length;
    }

    public function getName() {
        return $this->name;
    }

    public function getValue() {
        return $this->value;
    }

    public function getType() {
        return $this->type;
    }

    public function getLength() {
        return $this->length;
    }

}