<?php
namespace dependency\datasource\Adapter\Xml;
class Datasource
    extends \DOMDocument
    implements \dependency\datasource\DatasourceInterface
{
    /* Constants */
    const PARAM_NULL = 0;
    const PARAM_STR = 1;
    const PARAM_INT = 2;
    const PARAM_BOOL = 3;
    const QUOTE_STR = "'";
    /* Properties */
    protected $name;
    protected $driver = "Xml";
    protected $statementClass;
    protected $XPath;
    /* Methods */
    public function __construct($Dsn, $Username=null, $Password=null, array $Options=null) {
        $this->name = $Dsn;
        $XmlFile = substr($Dsn, 4);
        $XmlOptions = array_sum($Options);
        parent::__construct();
        $this->load($XmlFile, $XmlOptions);
        $this->setStatementClass();
    }
    /* DatasourceInterface methods */
    public function getDriver() {
        return $this->driver;
    }
    public function getName() {
        return $this->name;
    }
    public function exec($queryString, \DOMNode $context=null) {
        $Statement = $this->newStatement($queryString, $context);
        $Statement->execute();
        return count($Statement->fetchAll());
    }
    public function prepare($queryString, \DOMNode $context=null) {
        return $this->newStatement($queryString, $context);
    }
    public function query($queryString, \DOMNode $context=null, $registerNodeNS=null) {
        $Statement = $this->newStatement($queryString, $context);
        $Statement->execute();
        return $Statement;
    }
    public function quote($string) {
        /* No quote in string, return string enclosed with quotes */
        if (strpos($string, "'") === false)
            return sprintf("'%s'", $string);
        /* Quote found: use concat function and return string enclosed with quotes */
        return sprintf("concat('%s')", str_replace("'", "',\"'\",'", $string));
    }
    public function getErrors() {
    }
    public function setStatementClass($class=null, array $args=null) {
        if (is_null($class)) {
            $class = '\dependency\datasource\Adapter\Database\Statement';
            $args = null;
        }
        $this->statementClass = array($class, (array)$args);
    }
    protected function newStatement($queryString, $context=null) {
        $statementClass = new \ReflectionClass($this->statementClass[0]);
        $statementArgs = $this->statementClass[1];
        array_unshift($statementArgs, $context);
        array_unshift($statementArgs, $queryString);
        array_unshift($statementArgs, $this);
        return $statementClass->newInstanceArgs($statementArgs);
    }
}