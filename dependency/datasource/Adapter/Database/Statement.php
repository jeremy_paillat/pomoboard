<?php
namespace dependency\datasource\Adapter\Database;

class Statement
    implements \dependency\datasource\StatementInterface,
               \dependency\datasource\ResultSetInterface,
               \IteratorAggregate
{
    /* Constants */
    
    /* Properties */    
    protected $PdoStatement;
    
    /* Methods */
    /**
     * Constructor
     * @param \PDOStatement $pdoStatement The PDO Statement object
     */
    public function __construct(\PDOStatement $pdoStatement)
    {
        $this->PdoStatement = $pdoStatement;
    }
    
    /* Statement methods */
    /**
     * Bind a parameter
     * @param string  $name
     * @param mixed   &$variable
     * @param string  $type
     * @param integer $length
     * @param array   $driver_options
     * 
     * @return bool
     */
    public function bindParam($name, &$variable, $type=\PDO::PARAM_STR, $length=null, $driver_options=array(), $ref=false)
    {
        $Param = new Param($name, $variable, $type, $length, $ref);
        $this->params[$name] = $Param;
        
        return $this->PdoStatement->bindParam(':' . $name, $variable, $type, $length);
    }
    
    public function bindValue($name, $value, $type=\PDO::PARAM_STR)
    {
        $this->params[$name] = $value;
        
        return $this->PdoStatement->bindValue(':' . $name, $value, $type);
    }
    
    public function getError()
    {
        $errInfo = $this->PdoStatement->errorInfo();
        
        return new \core\Error($errInfo[2], null, $errInfo[0], null, null, $errInfo);
    }
    
    public function execute($inputParameters=null)
    {
        if ($inputParameters && \laabs\is_assoc($inputParameters)) {
            foreach ($inputParameters as $name => $value) {
                $inputParameters[":" . $name] = $value;
                unset($inputParameters[$name]);
            }
        }
        
        return $this->PdoStatement->execute($inputParameters);
    }
    
    public function getParams()
    {
        return $this->params;
    }
    
    public function debugDumpParams()
    {
        return $this->PdoStatement->debugDumpParams();
    }

        
    /* ResultSet methods */
    public function fetch($class="\stdClass", array $ctor_args=array())
    {
        $object = $this->PdoStatement->fetchObject($class, $ctor_args);

        if ($object) {
            foreach ($object as $name => $value) {
                if (gettype($value) == 'resource') {
                    $contents = stream_get_contents($value);
                    $object->$name = \laabs::createMemoryStream($contents);
                }
            }
        }
        
        return $object;
    }
    
    public function fetchAll($class="\stdClass", array $ctor_args=array())
    {
        $resultSet = array();
        // Fix abnormal behavious of PDOStatement::fetchAll
        // Only ONE pointer used for LOBs so resources will all point to the last LOB retrieved
        while ($object = $this->PdoStatement->fetchObject($class, $ctor_args)) {
            foreach ($object as $name => $value) {
                if (gettype($value) == 'resource') {
                    $contents = stream_get_contents($value);
                    $object->$name = \laabs::createMemoryStream($contents);
                }
            }
            $resultSet[] = $object;
        }

        return $resultSet;
    }
    
    public function fetchItem($cursor_offset=0, $class="\stdClass")
    {
        $this->setFetchMode(\PDO::FETCH_CLASS, $class);
        
        return $this->PdoStatement->fetch(\PDO::FETCH_CLASS, \PDO::FETCH_ORI_ABS, $cursor_offset);
    }
    
    public function fetchColumn($offset=0)
    {
        return $this->PdoStatement->fetchColumn($offset);
    }
    
    public function rowCount()
    {
        return $this->PdoStatement->rowCount();
    }
    
    public function getQueryString()
    {
        return $this->PdoStatement->queryString;
    }
    
    public function getIterator()
    {
        $DataSet = $this->fetchAll();
        
        return new ArrayIterator($DataSet);
    }
}