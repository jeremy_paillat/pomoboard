<?php

namespace dependency\datasource\Adapter\Database;

class Param
    implements \dependency\datasource\ParamInterface
{

    /* Properties */
    protected $name;
    protected $value;
    protected $type;
    protected $length;
    protected $ref;

    /* Methods */
    public function __construct($name, &$value, $type=\PDO::PARAM_STR, $length=null, $ref=false) {
        $this->name = $name;
        $this->value = &$value;
        $this->type = $type;
        $this->length = $length;
        $this->ref = $ref;
    }

    public function getName() {
        return $this->name;
    }

    public function getValue() {
        return $this->value;
    }

    public function getType() {
        return $this->type;
    }

    public function getLength() {
        return $this->length;
    }

}