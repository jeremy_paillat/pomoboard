<?php
namespace dependency\datasource\Adapter\Database;

class Datasource
    implements \dependency\datasource\DatasourceInterface, \dependency\datasource\TransactionalInterface
{
    /* Constants */
    
    /* Properties */
    protected static $instances;

    protected $name;
    
    protected $driver;
    
    public $Pdo;
    
    /**
     * @var array $Options
     * Associative array of PDO class constructor options
     *  PDO::ATTR_AUTOCOMMIT
     *  PDO::ATTR_CASE
     *  PDO::ATTR_ERRMODE
     *  PDO::ATTR_ORACLE_NULLS
     *  PDO::ATTR_PERSISTENT
     *  PDO::ATTR_PREFETCH
     *  PDO::ATTR_TIMEOUT
     */
    protected $Options = array(
        //\PDO::ATTR_AUTOCOMMIT=> false,
        \PDO::ATTR_PERSISTENT => true
    );
    
    protected $PdoStatementOptions = array(
        \PDO::ATTR_CURSOR => \PDO::CURSOR_FWDONLY
    );
    
    /**
     * array to declare the class and constructor arguments for statement
     * [0] => name of the class
     * [1] => array of arguments
     *      [0] => PDOStatement retrieved will be inserted at runtime
     *      [1] => mixed args...
     */
    protected $statementClass;
    
    /* Methods */
    /** 
     * Constructor
     */
    public function __construct($Dsn, $Username=null, $Password=null, array $Options=null)
    {
        $this->driver = strtok($Dsn, ':');
        $this->name = strtok(':');

        $this->setStatementClass();

        if (!isset(self::$instances[$Dsn])) {
            self::$instances[$Dsn] = $this->newPdo($Dsn, $Username, $Password, (array)$this->Options);
        }

        $this->Pdo = self::$instances[$Dsn];
    }     

    protected function newPdo($Dsn, $Username=null, $Password=null, array $Options=null)
    {
        $pdo = new \PDO($Dsn, $Username, $Password, $Options);

        // Set error mode to exception
        $pdo->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);

        // When bool datatype is not available with the driver and a number (0/1) is used instead
        // As PDO does'nt have a FLOAT/DOUBLE param type, it is considered as string
        // But false to string gives empty string ''
        // And empty string conversion to NULL breaks NOT NULL constraints
        $pdo->setAttribute(\PDO::ATTR_ORACLE_NULLS, \PDO::NULL_NATURAL);

        return $pdo;
    }
   
    /* DatasourceInterface methods */
    /** 
     * Get the driver name
     * 
     * @return string
     */
    public function getDriver()
    {
        return $this->driver;
    }
    
    /** 
     * Get the datasource name
     * 
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }
   
    /** 
     * Execute a query string
     * @param string $queryString
     * 
     * @return mixed
     */
    public function exec($queryString) 
    {
        return $this->Pdo->exec($queryString);
    }
    
    /** 
     * Query the ds
     * @param string $queryString
     * 
     * @return object
     */
    public function query($queryString) 
    {
        $pdoStatement = $this->Pdo->query($queryString);

        $statement = $this->newStatement($pdoStatement);

        return $statement;
    }
    
    public function prepare($queryString, array $Options=null)
    {
        $pdoStatement = $this->Pdo->prepare($queryString);

        $statement = $this->newStatement($pdoStatement);
        
        return $statement;
    }
    
    public function quote($string) 
    {
        return $this->Pdo->quote($string);
    }
    
    public function getErrors() 
    {
        return $this->Pdo->errorInfo();
    }
    
    /* TransactionalInterface methods */
    public function beginTransaction() 
    {
        return $this->Pdo->beginTransaction();
    }
    
    public function commit() 
    {
        return $this->Pdo->commit();
    }
    
    public function rollback() 
    {
        return $this->Pdo->rollback();
    }
    
    public function inTransaction() 
    {
        return $this->Pdo->inTransaction();
    }
    
    /* Custom Database methods */
    public function setStatementClass($class=null, array $args=null) 
    {
        /* Back to default statement class and args */
        if (is_null($class)) {
            $class = '\dependency\datasource\Adapter\Database\Statement';
            $args = null;
        }
        $this->statementClass = array($class, (array)$args);
    }

    protected function newStatement($pdoStatement=null) 
    {
        if (!$pdoStatement) {
            $Error = $this->Pdo->errorInfo();
            throw new \Exception($Error[2]);
        }
        $statementClass = new \ReflectionClass($this->statementClass[0]);
        $statementArgs = $this->statementClass[1];
        array_unshift($statementArgs, $pdoStatement);
        
        return $statementClass->newInstanceArgs($statementArgs);
    }
    
}