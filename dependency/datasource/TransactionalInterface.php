<?php
namespace dependency\datasource;
interface TransactionalInterface
{
    /* Properties presented
        protected $inTransaction;
    */
    public function beginTransaction();
    public function commit();
    public function rollback();
    public function inTransaction();
}