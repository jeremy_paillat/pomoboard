<?php
namespace dependency\datasource;
interface DatasourceInterface
{
    /* Constants */
    /* Properties presented
        protected $name;
        protected $driver;
        protected $Statements (SplObjectStorage)
    */
    public function __construct($Dsn, $Username=null, $Password=null, array $Options=null);

    public function getName();

    public function exec($queryString);

    public function query($queryString);

    public function prepare($queryString);

    public function quote($string);
    
    public function getErrors();
}