<?php
namespace dependency\datasource;

interface ResultSetInterface
{
    public function fetch();
    
    public function fetchAll();
    
    public function fetchItem($offset);
    
    public function fetchColumn($offset);
    
    public function rowCount();
}