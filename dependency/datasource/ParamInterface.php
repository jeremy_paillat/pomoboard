<?php
namespace dependency\datasource;
interface ParamInterface
{
    public function getName();
    public function getValue();
    public function getType();
    public function getLength();
}